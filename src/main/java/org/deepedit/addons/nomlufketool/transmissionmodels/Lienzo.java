package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Lienzo extends Canvas {

	static int ancho;
	static int alto;
	static boolean ejes;
	static int pasopixel=15;
	static double pasoreal;
	static double cx=10;		//centro ejes, origen
	static double cy;			//centro ejes, origen
	static int py;
	static int px;
	static double xmax,xmaxpixel;
	static double xmin,xminpixel;
	static double ymax,ymaxpixel;
	static double ymin,yminpixel;
	boolean limpiar=false;
	boolean graficar=false;
	static boolean dibujarReceptor=false;
	static boolean dibujarTransmisor=false;
	static double cxpixeles;	//centro circulo
	static double cypixeles;	//centro circulo
	static double radiorpixeles;
	static double radiotpixeles;
	Zoom Pzoom;
	Acciones acc;
	static Circulo circulos;
	static Complex2[] parametros=new Complex2[6];
	static double ox;		//para heredar en metodos
	static double oy;
	static double paso;
	static double cxp;
	static double cyp;
	static double rp;

//CONSTRUCTOR***************
	Lienzo(int w, int h){
		ancho = w;	alto = h;
		setSize(ancho,alto);
		addMouseListener(new java.awt.event.MouseAdapter() {
		public void mouseClicked(java.awt.event.MouseEvent evt) {
		LienzoMouseClicked(evt);}});
	}//constr lienzo

public void Metodos(Zoom z, Acciones a){
	Pzoom=z;
	acc=a;
}

//MOUSE CLICKED
private void LienzoMouseClicked(java.awt.event.MouseEvent evt) {

		if(Pzoom.RadioButtonZoom1.isSelected()) acc.zoomIn(evt.getX(),evt.getY(),this);
		if(Pzoom.RadioButtonZoom2.isSelected()) acc.zoomOut(evt.getX(),evt.getY(),this);
}


//PAINT***********************
public void paint(Graphics g) {
		g.drawRect(0,0,ancho-1,alto-1);
		g.setColor(Color.white);
		g.fillRect(1,1,ancho-2,alto-2);
		g.setColor(Color.black);

//*******************funciones*************
		//if(ejes) ejes(g);
		if(limpiar) {ejes=false;dibujarReceptor=false; acc.graficado=false;limpiar(g);}
		//if(dibujarReceptor) dibujarReceptor(g);					//GRAFICAR
		if(dibujarReceptor&&ejes){ ejes(g); dibujarReceptor(g);}
		if(dibujarTransmisor&&ejes){ ejes(g); dibujarTransmisor(g);}


}//paint

//DIBUJAR RECEPTOR************
public void dibujarReceptor(int ox2,int oy2,double paso2,int cxp2,int cyp2,int rp2){//cambiado
		ox=ox2;
		oy=oy2;
		paso=paso2;
		cxp=cxp2;
		cyp=cyp2;
		rp=rp2;
		ejes=true;
		dibujarReceptor=true;
		repaint();}

//DIBUJAR Transmisor************
public void dibujarTransmisor(int ox2,int oy2,double paso2,int cxp2,int cyp2,int rp2){//cambiado
		ox=ox2;
		oy=oy2;
		paso=paso2;
		cxp=cxp2;
		cyp=cyp2;
		rp=rp2;
		ejes=true;
		dibujarTransmisor=true;
		repaint();}


//EJES******INTERNO
public void ejes(Graphics g){
	g.drawLine(15,(int)oy,ancho-15,(int)oy);		//eje x
	g.drawLine((int)ox,15,(int)ox,alto-15);		//eje y
	g.drawLine(ancho-15,(int)oy,ancho-20,(int)oy+5);	//flecha
	g.drawLine(ancho-15,(int)oy,ancho-20,(int)oy-5);	//flecha
	g.drawLine((int)ox-5,20,(int)ox,15);				//flecha
	g.drawLine((int)ox+5,20,(int)ox,15);				//flecha
	for(int i=0;(int)oy-i*15>15;i++) if(oy-i*15<alto-15) {g.drawLine((int)ox-5,(int)oy-i*15,(int)ox+5,(int)oy-i*15);if(i%2==1)g.drawString(""+aprox(i*paso,2),(int)ox-30,(int)oy-i*15+4);}
	for(int i=0;(int)oy+i*15<alto-15;i++)if(oy+i*15>15)  {g.drawLine((int)ox-5,(int)oy+i*15,(int)ox+5,(int)oy+i*15);if(i%2==1)g.drawString(""+aprox((-i*paso),2),(int)ox+10,(int)oy+i*15+4);}
	for(int i=0;(int)ox-i*15>15;i++) if(ox-i*15<ancho-15){g.drawLine((int)ox-i*15,(int)oy+5,(int)ox-i*15,(int)oy-5);if(i%2==1)g.drawString(""+aprox((-i*paso),2),(int)ox-i*15-10,(int)oy+16);}
	for(int i=0;(int)ox+i*15<ancho-15;i++)if(ox+i*15>15) {g.drawLine((int)ox+i*15,(int)oy+5,(int)ox+i*15,(int)oy-5);if(i%2==1)g.drawString(""+aprox(i*paso,2),(int)ox+i*15-10,(int)oy-11);
	g.drawString("P",ancho-9,(int)oy+4);
	g.drawString("Q",(int)ox-4,11);}
}

//DIBUJAR RECEPTOR INTERNO*******************
public void dibujarReceptor(Graphics g){
	g.drawArc((int)(cxp-rp),(int)(cyp-rp),(int)(2*rp),(int)(2*rp),-10,110);
	g.drawLine((int)ox,(int)oy,(int)cxp,(int)cyp);
	g.drawString("O",(int)cxp-5,(int)cyp+12);
	g.drawLine((int)(cxp+rp),(int)oy,(int)(cxp+rp),(int)cyp);
	g.drawString("Pmax",(int)cxp+(int)rp+5,(int)cyp);
}

//DIBUJAR TRANSMISOR INTERNO*******************
public void dibujarTransmisor(Graphics g){
	g.drawArc((int)(cxp-rp),(int)(cyp-rp),(int)(2*rp),(int)(2*rp),-120,120);
	g.drawLine((int)ox,(int)oy,(int)cxp,(int)cyp);	//OK
	g.drawString("O",(int)cxp-5,(int)cyp-12);		//O;K
	//g.drawLine((int)(cxp+rp),(int)oy,(int)(cxp+rp),(int)cyp+30);
	//g.drawString("Pmax",(int)cxp+(int)rp+5,(int)cyp+20);
}



//LIMPIAR************EXTERNO
public void limpiar(){
limpiar=true;
repaint();
}//limpiar

//LIMPIAR*************INTERNO
public void limpiar(Graphics g){
	g.drawRect(0,0,ancho-1,alto-1);
	g.setColor(Color.white);
	g.fillRect(1,1,ancho-2,alto-2);
	g.setColor(Color.black);
	limpiar=false;
}


//EJES*********EXTERNO
public void ejes(double xmin2, double xmax2, double ymin2, double ymax2){
	ejes=true;
	xmin=xmin2;
	xmax=xmax2;
	ymin=ymin2;
	ymax=ymax2;
	cx=autoescala(0,"x");
	cy=autoescala(0,"y");
	repaint();}


//APROX*****************
public double aprox(double x, int i){		//aproxima con i decimales
		return Math.rint(x*Math.pow(10,i))/Math.pow(10,i);
	}

//INICIALIZAR RECEPTOR*************agregado
public static void inicializarReceptor(Izquierdo I,Zoom P){
	parametros=I.getParams();
	circulos=new Circulo(parametros[0],parametros[1],parametros[2],parametros[3],parametros[4],parametros[5]);
	xmax=circulos.maxxr;		//maximos reales
	xmin=circulos.minxr;		//maximos reales
	ymax=circulos.maxyr;		//maximos reales
	ymin=circulos.minyr;		//maximos reales
	xminpixel=autoescala(xmin,"x");		//rangos reales pasados a pixeles
	xmaxpixel=autoescala(xmax,"x");		//rangos reales pasados a pixeles
	yminpixel=autoescala(ymin,"y");		//rangos reales pasados a pixeles
	ymaxpixel=autoescala(ymax,"y");		//rangos reales pasados a pixeles
	pasoreal=(15.0*(xmax-xmin))/(xmaxpixel-xminpixel);	//cuanto son 15 pixeles?
	cxpixeles=autoescala(circulos.centror.re,"x");		//posicion de centro circulo
	cypixeles=autoescala(circulos.centror.im,"y");		//posicion de centro circulo
	radiorpixeles=(circulos.radior*(xmaxpixel-xminpixel)/(xmax-xmin));	//radio en pixeles
	cx=autoescala(0,"x");	//origen de ejes en pixeles
	cy=autoescala(0,"y");	//origen de ejes en pixeles
}

//INICIALIZAR Transmisor*************agregado
public static void inicializarTransmisor(Izquierdo I,Zoom P){
	parametros=I.getParams();
	circulos=new Circulo(parametros[0],parametros[1],parametros[2],parametros[3],parametros[4],parametros[5]);
	xmax=circulos.maxxt;		//maximos reales
	xmin=circulos.minxt;		//maximos reales
	ymax=circulos.maxyt;		//maximos reales
	ymin=circulos.minyt;		//maximos reales
	xminpixel=autoescala(xmin,"x");		//rangos reales pasados a pixeles
	xmaxpixel=autoescala(xmax,"x");		//rangos reales pasados a pixeles
	yminpixel=autoescala(ymin,"y");		//rangos reales pasados a pixeles
	ymaxpixel=autoescala(ymax,"y");		//rangos reales pasados a pixeles
	pasoreal=(15.0*(xmax-xmin))/(xmaxpixel-xminpixel);	//cuanto son 15 pixeles?
	cxpixeles=autoescala(circulos.centrot.re,"x");		//posicion de centro circulo
	cypixeles=autoescala(circulos.centrot.im,"y");		//posicion de centro circulo
	radiotpixeles=(circulos.radiot*(xmaxpixel-xminpixel)/(xmax-xmin));	//radio en pixeles
	cx=autoescala(0,"x");	//origen de ejes en pixeles
	cy=autoescala(0,"y");	//origen de ejes en pixeles
}



//AUTOESCALA***********************agregado
public static double autoescala(double punto, String coor){
		double g, bx, by;
		int margen=10;
		if((ymax-ymin)/alto> (xmax-xmin)/ancho){
			g=(2*margen-alto)/(ymax-ymin);
			by=alto/2-g*(ymax+ymin)/2;
			bx=ancho/2+g*(xmax+xmin)/2;}
		else{
			g=(2*margen-ancho)/(xmax-xmin);
			by=alto/2-g*(ymax+ymin)/2;
			bx=ancho/2+g*(xmax+xmin)/2;	}
		if(coor=="x")
		return (-g*punto+bx);
		else return (g*punto+by);
}//autoescala


}//class Canvas