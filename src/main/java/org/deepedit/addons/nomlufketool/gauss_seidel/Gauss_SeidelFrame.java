package org.deepedit.addons.nomlufketool.gauss_seidel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Creates a new Gauss-Seidel app Window
 * @author frank
 */
public class Gauss_SeidelFrame extends JDialog implements ActionListener {

    public String V1texto;
    public String V2texto;
    public String V3texto;
    public String V4texto;

    public String P1texto;
    public String P2texto;
    public String P3texto;
    public String P4texto;

    public String Q1texto;
    public String Q2texto;
    public String Q3texto;
    public String Q4texto;

    public String D1texto;
    public String D2texto;
    public String D3texto;
    public String D4texto;

    public static final int CENTER = 0;
    public static final int RIGHT = 4;
    //definimos variables

    public int seleccion;
    public double FactorDeAceleracion;
    public boolean EstadoActualizacion;
    public String StringTemp;

    double ValorAux;

    double[][] ValoresTodo;
    double[][] Admitancias;
    String[][] AdmitanciasString;

    double[][] Salidas;

    double[][] Voltajes;
    double[][] Potencias;
    double[][] Reactivos;
    double[][] Angulos;
    calculo Resultados = new calculo();
    calculo auxiliar = new calculo();

    complejo V1in = new complejo();
    complejo V2in = new complejo();
    complejo V3in = new complejo();
    complejo V4in = new complejo();

    complejo ym11in = new complejo();
    complejo ym12in = new complejo();
    complejo ym13in = new complejo();
    complejo ym14in = new complejo();
    complejo ym22in = new complejo();
    complejo ym23in = new complejo();
    complejo ym24in = new complejo();
    complejo ym33in = new complejo();
    complejo ym34in = new complejo();
    complejo ym44in = new complejo();

    JButton mas11, mas12, mas13, mas14;
    JButton menos11, menos12, menos13, menos14;
    JButton mas21, mas22, mas23, mas24;
    JButton menos21, menos22, menos23, menos24;
    JButton mas31, mas32, mas33, mas34;
    JButton menos31, menos32, menos33, menos34;
    JButton mas41, mas42, mas43, mas44;
    JButton menos41, menos42, menos43, menos44;

    JLabel P1, V1, Q1, D1;
    JTextField Pot1, Volt1, React1, Delta1;
    JLabel P2, V2, Q2, D2;
    JTextField Pot2, Volt2, React2, Delta2;
    JLabel P3, V3, Q3, D3;
    JTextField Pot3, Volt3, React3, Delta3;
    JLabel P4, V4, Q4, D4;
    JTextField Pot4, Volt4, React4, Delta4;

    JLabel Barra1, Barra2, Barra3, Barra4;

    private Image Imagen1;
    int OrdenBarras;
    JLabel FactorAcel, SI, NO, Vacio;
    static JTextField Factor;
    JButton Ejecutar;
    JButton Reset;
    JButton Ayuda;

    Checkbox FactorAcelera;
    JLabel Iteraciones;
    JTextField NumeroIter;
    int NNiter;

    static Checkbox Actualizacion;
    JLabel Actualizacion2, Orden, OrdenBarra1, OrdenBarra2, OrdenBarra3, OrdenBarra4;
    JLabel VacioActualiza;
    JTextField OrdBarra1, OrdBarra2, OrdBarra3, OrdBarra4;

    JLabel LabelAdmit11, LabelAdmit12, LabelAdmit41, LabelAdmit42;
    JLabel AdmitFila1, AdmitFila2, AdmitFila3, AdmitFila4;
    JLabel AdmitCol1, AdmitCol2, AdmitCol3, AdmitCol4;
    JLabel Admit1, Admit2, Admit3, Admit4;
    JTextField ym11, ym12, ym13, ym14, ym21, ym22, ym23, ym24;
    JTextField ym31, ym32, ym33, ym34, ym41, ym42, ym43, ym44;

    JLabel prueba1, prueba2, prueba3, prueba4;
    JLabel LabelFoto;
    
    /**
     * Creates a new Gauss-Seidel app Window
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public Gauss_SeidelFrame(JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    private void initComponents() {
        setTitle("Gauss-Seidel Calculator");
        FactorAcelera = new Checkbox("Factor de Aceleraci�n", false);
        Actualizacion = new Checkbox(" Actualizaci�n ", false);

        JLabel Iteraciones = new JLabel("N� de Iteraciones:");
        NumeroIter = new JTextField("5", 4);

        P1 = new JLabel("P :", CENTER);
        V1 = new JLabel("V :", CENTER);
        Q1 = new JLabel("Q :", CENTER);
        D1 = new JLabel("Ang :", CENTER);

        P2 = new JLabel("P :", CENTER);
        V2 = new JLabel("V :", CENTER);
        Q2 = new JLabel("Q :", CENTER);
        D2 = new JLabel("Ang :", CENTER);

        P3 = new JLabel("P :", CENTER);
        V3 = new JLabel("V :", CENTER);
        Q3 = new JLabel("Q :", CENTER);
        D3 = new JLabel("Ang :", CENTER);

        P4 = new JLabel("P :", CENTER);
        V4 = new JLabel("V :", CENTER);
        Q4 = new JLabel("Q :", CENTER);
        D4 = new JLabel("Ang :", CENTER);

        Pot1 = new JTextField("0", 4);
        Volt1 = new JTextField("1.04", 4);
        React1 = new JTextField("0", 4);
        Delta1 = new JTextField("0", 4);

        Pot2 = new JTextField("1.0", 4);
        Volt2 = new JTextField("1.0", 4);
        React2 = new JTextField("-0.5", 4);
        Delta2 = new JTextField("0", 4);

        Pot3 = new JTextField("0.8", 4);
        Volt3 = new JTextField("1.02", 4);
        React3 = new JTextField("0", 4);
        Delta3 = new JTextField("0", 4);

        Pot4 = new JTextField("0.3", 4);
        Volt4 = new JTextField("1", 4);
        React4 = new JTextField("-0.1", 4);
        Delta4 = new JTextField("0", 4);

        Barra1 = new JLabel("Barra 1: Tipo Slack", CENTER);
        Barra2 = new JLabel("Barra 2: Tipo PQ", CENTER);
        Barra3 = new JLabel("Barra 3: Tipo PV", CENTER);
        Barra4 = new JLabel("Barra 4: Tipo PQ", CENTER);

        mas11 = new JButton("+");
        mas12 = new JButton("+");
        mas13 = new JButton("+");
        mas14 = new JButton("+");
        menos11 = new JButton("-");
        menos12 = new JButton("-");
        menos13 = new JButton("-");
        menos14 = new JButton("-");

        mas21 = new JButton("+");
        mas22 = new JButton("+");
        mas23 = new JButton("+");
        mas24 = new JButton("+");
        JButton menos21 = new JButton("-");
        JButton menos22 = new JButton("-");
        JButton menos23 = new JButton("-");
        JButton menos24 = new JButton("-");

        JButton mas31 = new JButton("+");
        JButton mas32 = new JButton("+");
        JButton mas33 = new JButton("+");
        JButton mas34 = new JButton("+");
        JButton menos31 = new JButton("-");
        JButton menos32 = new JButton("-");
        JButton menos33 = new JButton("-");
        JButton menos34 = new JButton("-");

        JButton mas41 = new JButton("+");
        JButton mas42 = new JButton("+");
        JButton mas43 = new JButton("+");
        JButton mas44 = new JButton("+");
        JButton menos41 = new JButton("-");
        JButton menos42 = new JButton("-");
        JButton menos43 = new JButton("-");
        JButton menos44 = new JButton("-");

        mas11.setEnabled(false);
        menos11.setEnabled(false);
        mas13.setEnabled(false);
        menos13.setEnabled(false);
        mas22.setEnabled(false);
        menos22.setEnabled(false);
        mas24.setEnabled(false);
        menos24.setEnabled(false);
        mas33.setEnabled(false);
        menos33.setEnabled(false);
        mas34.setEnabled(false);
        menos34.setEnabled(false);
        mas42.setEnabled(false);
        menos42.setEnabled(false);
        mas44.setEnabled(false);
        menos44.setEnabled(false);

        Pot1.setEnabled(false);
        React1.setEnabled(false);
        Volt2.setEnabled(false);
        Delta2.setEnabled(false);
        React3.setEnabled(false);
        Delta3.setEnabled(false);
        Volt4.setEnabled(false);
        Delta4.setEnabled(false);

        mas12.addActionListener(this);
        mas12.setActionCommand("mas12");
        menos12.addActionListener(this);
        menos12.setActionCommand("menos12");
        mas14.addActionListener(this);
        mas14.setActionCommand("mas14");
        menos14.addActionListener(this);
        menos14.setActionCommand("menos14");
        mas21.addActionListener(this);
        mas21.setActionCommand("mas21");
        menos21.addActionListener(this);
        menos21.setActionCommand("menos21");
        mas23.addActionListener(this);
        mas23.setActionCommand("mas23");
        menos23.addActionListener(this);
        menos23.setActionCommand("menos23");
        mas31.addActionListener(this);
        mas31.setActionCommand("mas31");
        menos31.addActionListener(this);
        menos31.setActionCommand("menos31");
        mas32.addActionListener(this);
        mas32.setActionCommand("mas32");
        menos32.addActionListener(this);
        menos32.setActionCommand("menos32");
        mas41.addActionListener(this);
        mas41.setActionCommand("mas41");
        menos41.addActionListener(this);
        menos41.setActionCommand("menos41");
        mas43.addActionListener(this);
        mas43.setActionCommand("mas43");
        menos43.addActionListener(this);
        menos43.setActionCommand("menos43");

        FactorAcel = new JLabel("Factor de Aceleraci�n");
        Factor = new JTextField("1", 4);
        Vacio = new JLabel("");
        Ejecutar = new JButton("Ejecutar!");

        Ejecutar.addActionListener(this);
        Ejecutar.setActionCommand("Ejecutar");

        Reset = new JButton("Reset");
        Reset.addActionListener(this);
        Reset.setActionCommand("Reset");
        Ayuda = new JButton("Ayuda");
        Ayuda.addActionListener(this);
        Ayuda.setActionCommand("Ayuda");

        Orden = new JLabel("Orden para actualizaci�n:", CENTER);
        VacioActualiza = new JLabel("");
        JLabel OrdenBarra1 = new JLabel("Primera Barra:  ", RIGHT);
        JLabel OrdenBarra2 = new JLabel("Segunda Barra:  ", RIGHT);
        JLabel OrdenBarra3 = new JLabel("Tercera Barra:  ", RIGHT);
        JLabel OrdenBarra4 = new JLabel("Cuarta Barra:  ", RIGHT);
        OrdBarra1 = new JTextField("2", 4);
        OrdBarra2 = new JTextField("3", 4);
        OrdBarra3 = new JTextField("4", 4);
        OrdBarra4 = new JTextField("Barra Slack", 4);
        OrdBarra4.setEnabled(false);

        JLabel Admit1 = new JLabel("");
        JLabel Admit2 = new JLabel("Matriz de ", RIGHT);
        JLabel Admit3 = new JLabel("Admitancias");
        JLabel Admit4 = new JLabel("");

        JLabel LabelAdmit11 = new JLabel("");
        JLabel LabelAdmit12 = new JLabel("");
        JLabel LabelAdmit41 = new JLabel("");
        JLabel LabelAdmit42 = new JLabel("");
        JLabel AdmitFila1 = new JLabel("Fila1  ", RIGHT);
        JLabel AdmitFila2 = new JLabel("Fila2  ", RIGHT);
        JLabel AdmitFila3 = new JLabel("Fila3  ", RIGHT);
        JLabel AdmitFila4 = new JLabel("Fila4  ", RIGHT);
        JLabel AdmitCol1 = new JLabel("Columna1", CENTER);
        JLabel AdmitCol2 = new JLabel("Columna2", CENTER);
        JLabel AdmitCol3 = new JLabel("Columna3", CENTER);
        JLabel AdmitCol4 = new JLabel("Columna4", CENTER);

        ym11 = new JTextField("3-j9", 4);
        ym12 = new JTextField("-2+j6", 4);
        ym13 = new JTextField("-1+j3", 4);
        ym14 = new JTextField("0+j0", 4);

        ym21 = new JTextField("-2+j6", 4);
        ym22 = new JTextField("3.666-j11", 4);
        ym23 = new JTextField("-0.666+j2", 4);
        ym24 = new JTextField("-1+j3", 4);

        ym31 = new JTextField("-1+j3", 4);
        ym32 = new JTextField("-0.666+j2", 4);
        ym33 = new JTextField("3.666-j11", 4);
        ym34 = new JTextField("-2+j6", 4);

        ym41 = new JTextField("0+j0", 4);
        ym42 = new JTextField("-1+j3", 4);
        ym43 = new JTextField("-2+j6", 4);
        ym44 = new JTextField("3-j9", 4);

        ym21.setEnabled(false);
        ym31.setEnabled(false);
        ym32.setEnabled(false);
        ym41.setEnabled(false);
        ym42.setEnabled(false);
        ym43.setEnabled(false);

        ym11.addActionListener(this);
        ym11.setActionCommand("ym11");
        ym12.addActionListener(this);
        ym12.setActionCommand("ym12");
        ym13.addActionListener(this);
        ym13.setActionCommand("ym13");
        ym14.addActionListener(this);
        ym14.setActionCommand("ym14");
        ym22.addActionListener(this);
        ym22.setActionCommand("ym22");
        ym23.addActionListener(this);
        ym23.setActionCommand("ym23");
        ym24.addActionListener(this);
        ym24.setActionCommand("ym24");
        ym33.addActionListener(this);
        ym33.setActionCommand("ym34");
        ym34.addActionListener(this);
        ym34.setActionCommand("ym34");
        ym44.addActionListener(this);
        ym44.setActionCommand("ym44");

        complejo y11 = new complejo(3, -9);
        complejo y12 = new complejo(-2, 6);
        complejo y13 = new complejo(-1, 3);
        complejo y14 = new complejo(0, 0);
        complejo y22 = new complejo(3.666, -11);
        complejo y23 = new complejo(-0.666, 2);
        complejo y24 = new complejo(-1, 3);
        complejo y33 = new complejo(3.666, -11);
        complejo y34 = new complejo(-2, 6);
        complejo y44 = new complejo(3, -9);

        //paneles aux
        JPanel Aux1 = new JPanel();
        JPanel Aux2 = new JPanel();
        JPanel Aux3 = new JPanel();
        JPanel Aux4 = new JPanel();
        JPanel Aux5 = new JPanel();
        JPanel Aux6 = new JPanel();
        JPanel Aux7 = new JPanel();
        JPanel Aux8 = new JPanel();
        JPanel Aux9 = new JPanel();
        JPanel Aux10 = new JPanel();
        JPanel Aux11 = new JPanel();
        JPanel Aux12 = new JPanel();
        JPanel Aux13 = new JPanel();
        JPanel Aux14 = new JPanel();
        JPanel Aux15 = new JPanel();
        JPanel Aux16 = new JPanel();
        JLabel LabelAux1 = new JLabel("");
        JLabel LabelAux2 = new JLabel("");
        JLabel LabelAux3 = new JLabel("");
        JLabel LabelAux4 = new JLabel("");
        JLabel LabelAux5 = new JLabel("");
        JLabel LabelAux6 = new JLabel("");
        JLabel LabelAux7 = new JLabel("");
        JLabel LabelAux8 = new JLabel("");
        JLabel LabelAux9 = new JLabel("");
        JLabel LabelAux10 = new JLabel("");
        JLabel LabelAux11 = new JLabel("");
        JLabel LabelAux12 = new JLabel("");
        JLabel LabelAux13 = new JLabel("");
        JLabel LabelAux14 = new JLabel("");
        JLabel LabelAux15 = new JLabel("");
        JLabel LabelAux16 = new JLabel("");
        Aux1.add(LabelAux1);
        Aux2.add(LabelAux2);
        Aux3.add(LabelAux3);
        Aux4.add(LabelAux4);
        Aux5.add(LabelAux5);
        Aux6.add(LabelAux6);
        Aux7.add(LabelAux7);
        Aux8.add(LabelAux8);
        Aux9.add(LabelAux9);
        Aux10.add(LabelAux10);
        Aux11.add(LabelAux11);
        Aux12.add(LabelAux12);
        Aux13.add(LabelAux13);
        Aux14.add(LabelAux14);
        Aux15.add(LabelAux15);
        Aux16.add(LabelAux16);

        //Principio Panel 11//
        JPanel Panel11 = new JPanel();
        Panel11.setLayout(new GridLayout(5, 1));

        JPanel Fila21 = new JPanel();
        Fila21.setLayout(new GridLayout(1, 2));
        JPanel Fila31 = new JPanel();
        Fila31.setLayout(new GridLayout(1, 2));
        JPanel Fila41 = new JPanel();
        Fila41.setLayout(new GridLayout(1, 2));
        JPanel Fila51 = new JPanel();
        Fila51.setLayout(new GridLayout(1, 2));

        JPanel Fila211 = new JPanel();
        Fila211.setLayout(new GridLayout(1, 3));
        JPanel Fila311 = new JPanel();
        Fila311.setLayout(new GridLayout(1, 3));
        JPanel Fila411 = new JPanel();
        Fila411.setLayout(new GridLayout(1, 3));
        JPanel Fila511 = new JPanel();
        Fila511.setLayout(new GridLayout(1, 3));

        Fila211.add(mas11);
        Fila211.add(menos11);
        Fila211.add(P1);
        Fila311.add(mas12);
        Fila311.add(menos12);
        Fila311.add(V1);
        Fila411.add(mas13);
        Fila411.add(menos13);
        Fila411.add(Q1);
        Fila511.add(mas14);
        Fila511.add(menos14);
        Fila511.add(D1);

        Fila21.add(Fila211);
        Fila21.add(Pot1);
        Fila31.add(Fila311);
        Fila31.add(Volt1);
        Fila41.add(Fila411);
        Fila41.add(React1);
        Fila51.add(Fila511);
        Fila51.add(Delta1);

        Panel11.add(Barra1);
        Panel11.add(Fila21);
        Panel11.add(Fila31);
        Panel11.add(Fila41);
        Panel11.add(Fila51);
        //Fin panel 1-1//
        //panel 2-1-------//
        JPanel Panel21 = new JPanel();
        Panel21.setLayout(new GridLayout(5, 1));

        JPanel Fila71 = new JPanel();
        Fila71.setLayout(new GridLayout(1, 2));
        JPanel Fila81 = new JPanel();
        Fila81.setLayout(new GridLayout(1, 2));
        JPanel Fila91 = new JPanel();
        Fila91.setLayout(new GridLayout(1, 2));
        JPanel FilaA1 = new JPanel();
        FilaA1.setLayout(new GridLayout(1, 2));

        JPanel Fila711 = new JPanel();
        Fila711.setLayout(new GridLayout(1, 3));
        JPanel Fila811 = new JPanel();
        Fila811.setLayout(new GridLayout(1, 3));
        JPanel Fila911 = new JPanel();
        Fila911.setLayout(new GridLayout(1, 3));
        JPanel FilaA11 = new JPanel();
        FilaA11.setLayout(new GridLayout(1, 3));

        Fila711.add(mas21);
        Fila711.add(menos21);
        Fila711.add(P2);
        Fila811.add(mas22);
        Fila811.add(menos22);
        Fila811.add(V2);
        Fila911.add(mas23);
        Fila911.add(menos23);
        Fila911.add(Q2);
        FilaA11.add(mas24);
        FilaA11.add(menos24);
        FilaA11.add(D2);

        Fila71.add(Fila711);
        Fila71.add(Pot2);
        Fila81.add(Fila811);
        Fila81.add(Volt2);
        Fila91.add(Fila911);
        Fila91.add(React2);
        FilaA1.add(FilaA11);
        FilaA1.add(Delta2);

        Panel21.add(Barra2);
        Panel21.add(Fila71);
        Panel21.add(Fila81);
        Panel21.add(Fila91);
        Panel21.add(FilaA1);
        //Fin Panel 2-1//
        //Principio Panel 14//
        JPanel Panel14 = new JPanel();
        Panel14.setLayout(new GridLayout(5, 1));

        JPanel Fila24 = new JPanel();
        Fila24.setLayout(new GridLayout(1, 2));
        JPanel Fila34 = new JPanel();
        Fila34.setLayout(new GridLayout(1, 2));
        JPanel Fila44 = new JPanel();
        Fila44.setLayout(new GridLayout(1, 2));
        JPanel Fila54 = new JPanel();
        Fila54.setLayout(new GridLayout(1, 2));

        JPanel Fila241 = new JPanel();
        Fila241.setLayout(new GridLayout(1, 3));
        JPanel Fila341 = new JPanel();
        Fila341.setLayout(new GridLayout(1, 3));
        JPanel Fila441 = new JPanel();
        Fila441.setLayout(new GridLayout(1, 3));
        JPanel Fila541 = new JPanel();
        Fila541.setLayout(new GridLayout(1, 3));

        Fila241.add(mas31);
        Fila241.add(menos31);
        Fila241.add(P3);
        Fila341.add(mas32);
        Fila341.add(menos32);
        Fila341.add(V3);
        Fila441.add(mas33);
        Fila441.add(menos33);
        Fila441.add(Q3);
        Fila541.add(mas34);
        Fila541.add(menos34);
        Fila541.add(D3);

        Fila24.add(Fila241);
        Fila24.add(Pot3);
        Fila34.add(Fila341);
        Fila34.add(Volt3);
        Fila44.add(Fila441);
        Fila44.add(React3);
        Fila54.add(Fila541);
        Fila54.add(Delta3);

        Panel14.add(Barra3);
        Panel14.add(Fila24);
        Panel14.add(Fila34);
        Panel14.add(Fila44);
        Panel14.add(Fila54);
        //Fin panel 1-1//
        //panel 2-4-------//
        JPanel Panel24 = new JPanel();
        Panel24.setLayout(new GridLayout(5, 1));

        JPanel Fila74 = new JPanel();
        Fila74.setLayout(new GridLayout(1, 2));
        JPanel Fila84 = new JPanel();
        Fila84.setLayout(new GridLayout(1, 2));
        JPanel Fila94 = new JPanel();
        Fila94.setLayout(new GridLayout(1, 2));
        JPanel FilaA4 = new JPanel();
        FilaA4.setLayout(new GridLayout(1, 2));

        JPanel Fila741 = new JPanel();
        Fila741.setLayout(new GridLayout(1, 3));
        JPanel Fila841 = new JPanel();
        Fila841.setLayout(new GridLayout(1, 3));
        JPanel Fila941 = new JPanel();
        Fila941.setLayout(new GridLayout(1, 3));
        JPanel FilaA41 = new JPanel();
        FilaA41.setLayout(new GridLayout(1, 3));

        Fila741.add(mas41);
        Fila741.add(menos41);
        Fila741.add(P4);
        Fila841.add(mas42);
        Fila841.add(menos42);
        Fila841.add(V4);
        Fila941.add(mas43);
        Fila941.add(menos43);
        Fila941.add(Q4);
        FilaA41.add(mas44);
        FilaA41.add(menos44);
        FilaA41.add(D4);

        Fila74.add(Fila741);
        Fila74.add(Pot4);
        Fila84.add(Fila841);
        Fila84.add(Volt4);
        Fila94.add(Fila941);
        Fila94.add(React4);
        FilaA4.add(FilaA41);
        FilaA4.add(Delta4);

        Panel24.add(Barra4);
        Panel24.add(Fila74);
        Panel24.add(Fila84);
        Panel24.add(Fila94);
        Panel24.add(FilaA4);
        //Fin Panel 2-4//
        //Panel 3-1 Aceleracion y ejecucion//
        JPanel Panel31 = new JPanel();
        Panel31.setLayout(new GridLayout(5, 1));

        JPanel FilaIter = new JPanel();
        FilaIter.setLayout(new GridLayout(1, 2));
        JPanel FilaEjec = new JPanel();
        FilaEjec.setLayout(new GridLayout(1, 2));

        FilaIter.add(Iteraciones);
        FilaIter.add(NumeroIter);

        FilaEjec.add(Reset);
        FilaEjec.add(Ejecutar);

        Panel31.add(FactorAcelera);
        Panel31.add(Factor);
        Panel31.add(FilaIter);
        Panel31.add(FilaEjec);
        //Fin Panel 3-1

        //Panel 3-4
        JPanel Panel34 = new JPanel();
        Panel34.setLayout(new GridLayout(6, 1));

        JPanel Fila34B = new JPanel();
        Fila34B.setLayout(new GridLayout(1, 2));
        JPanel Fila34D = new JPanel();
        Fila34D.setLayout(new GridLayout(1, 2));
        JPanel Fila34E = new JPanel();
        Fila34E.setLayout(new GridLayout(1, 2));
        JPanel Fila34F = new JPanel();
        Fila34F.setLayout(new GridLayout(1, 2));
        JPanel Fila34G = new JPanel();
        Fila34G.setLayout(new GridLayout(1, 2));

        Fila34B.add(VacioActualiza);
        Fila34B.add(Actualizacion);
        Fila34D.add(OrdenBarra1);
        Fila34D.add(OrdBarra1);
        Fila34E.add(OrdenBarra2);
        Fila34E.add(OrdBarra2);
        Fila34F.add(OrdenBarra3);
        Fila34F.add(OrdBarra3);
        Fila34G.add(OrdenBarra4);
        Fila34G.add(OrdBarra4);

        Panel34.add(Fila34B);
        Panel34.add(Orden);
        Panel34.add(Fila34D);
        Panel34.add(Fila34E);
        Panel34.add(Fila34F);
        Panel34.add(Fila34G);

        //Fin Panel 3-4
        //Panel 3-2
        JPanel Panel32 = new JPanel();
        Panel32.setLayout(new GridLayout(6, 3));

        Panel32.add(LabelAdmit11);
        Panel32.add(Admit1);
        Panel32.add(Admit2);
        Panel32.add(LabelAdmit12);
        Panel32.add(AdmitCol1);
        Panel32.add(AdmitCol2);
        Panel32.add(AdmitFila1);
        Panel32.add(ym11);
        Panel32.add(ym12);
        Panel32.add(AdmitFila2);
        Panel32.add(ym21);
        Panel32.add(ym22);
        Panel32.add(AdmitFila3);
        Panel32.add(ym31);
        Panel32.add(ym32);
        Panel32.add(AdmitFila4);
        Panel32.add(ym41);
        Panel32.add(ym42);
        //Fin Panel 3-2
        //Panel 3-3
        JPanel Panel33 = new JPanel();
        Panel33.setLayout(new GridLayout(6, 2));

        Panel33.add(Admit3);
        Panel33.add(Admit4);
        Panel33.add(AdmitCol3);
        Panel33.add(AdmitCol4);
        Panel33.add(ym13);
        Panel33.add(ym14);
        Panel33.add(ym23);
        Panel33.add(ym24);
        Panel33.add(ym33);
        Panel33.add(ym34);
        Panel33.add(ym43);
        Panel33.add(ym44);
        //Fin Panel 3-3
        //Fotos a agregar
        
        LabelFoto = new JLabel();
        LabelFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gauss_seidel/images/FotoSEP.gif")));
        
        //Fin fotos
        //tama�os paneles auxiliares
        Aux1.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux1.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux2.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux2.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux3.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux3.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux4.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux4.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux5.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux5.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux6.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux6.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux7.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux7.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux8.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux8.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux9.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux9.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux10.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux10.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux11.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux11.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux12.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux12.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux13.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux13.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux14.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux14.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux15.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux15.setPreferredSize(new java.awt.Dimension(55, 10));
        Aux16.setMinimumSize(new java.awt.Dimension(55, 10));
        Aux16.setPreferredSize(new java.awt.Dimension(55, 10));
        //tama�os 

        this.setMinimumSize(new java.awt.Dimension(880, 510));
        this.setPreferredSize(new java.awt.Dimension(880, 510));

        LabelFoto.setMinimumSize(new java.awt.Dimension(330, 340));
        LabelFoto.setPreferredSize(new java.awt.Dimension(330, 340));

        Panel11.setMinimumSize(new java.awt.Dimension(274, 170));
        Panel11.setPreferredSize(new java.awt.Dimension(274, 170));

        Panel21.setMinimumSize(new java.awt.Dimension(274, 170));
        Panel21.setPreferredSize(new java.awt.Dimension(274, 170));

        Panel14.setMinimumSize(new java.awt.Dimension(274, 170));
        Panel14.setPreferredSize(new java.awt.Dimension(274, 170));

        Panel24.setMinimumSize(new java.awt.Dimension(274, 170));
        Panel24.setPreferredSize(new java.awt.Dimension(274, 170));

        Panel31.setMinimumSize(new java.awt.Dimension(219, 170));
        Panel31.setPreferredSize(new java.awt.Dimension(219, 170));
        Panel32.setMinimumSize(new java.awt.Dimension(219, 170));
        Panel32.setPreferredSize(new java.awt.Dimension(219, 170));
        Panel33.setMinimumSize(new java.awt.Dimension(219, 170));
        Panel33.setPreferredSize(new java.awt.Dimension(219, 170));
        Panel34.setMinimumSize(new java.awt.Dimension(219, 170));
        Panel34.setPreferredSize(new java.awt.Dimension(219, 170));

        //fin tama�os
        //Panel TOTAL//------------------------------
        GridBagLayout TotalBag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel PanelCentral = new JPanel();

        gbc.gridx = 0; // El primer panel empieza en la columna cero.
        gbc.gridy = 0; // El primer panel empieza en la fila cero
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel11, gbc);
        PanelCentral.add(Panel11);

        gbc.gridx = 5;
        gbc.gridy = 0;
        gbc.gridwidth = 6;
        gbc.gridheight = 2;
        TotalBag.setConstraints(LabelFoto, gbc);
        PanelCentral.add(LabelFoto);

        gbc.gridx = 11;
        gbc.gridy = 0;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel14, gbc);
        PanelCentral.add(Panel14);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel21, gbc);
        PanelCentral.add(Panel21);

        gbc.gridx = 11;
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel24, gbc);
        PanelCentral.add(Panel24);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel31, gbc);
        PanelCentral.add(Panel31);

        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel32, gbc);
        PanelCentral.add(Panel32);

        gbc.gridx = 8;
        gbc.gridy = 2;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel33, gbc);
        PanelCentral.add(Panel33);

        gbc.gridx = 12;
        gbc.gridy = 2;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Panel34, gbc);
        PanelCentral.add(Panel34);

        //auxiliares
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        TotalBag.setConstraints(Aux1, gbc);
        PanelCentral.add(Aux1);

        gbc.gridx = 1;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux2, gbc);
        PanelCentral.add(Aux2);

        gbc.gridx = 2;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux3, gbc);
        PanelCentral.add(Aux3);

        gbc.gridx = 3;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux4, gbc);
        PanelCentral.add(Aux4);

        gbc.gridx = 4;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux5, gbc);
        PanelCentral.add(Aux5);

        gbc.gridx = 5;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux6, gbc);
        PanelCentral.add(Aux6);

        gbc.gridx = 6;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux7, gbc);
        PanelCentral.add(Aux7);

        gbc.gridx = 7;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux8, gbc);
        PanelCentral.add(Aux8);

        gbc.gridx = 8;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux9, gbc);
        PanelCentral.add(Aux9);

        gbc.gridx = 9;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux10, gbc);
        PanelCentral.add(Aux10);

        gbc.gridx = 10;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux11, gbc);
        PanelCentral.add(Aux11);

        gbc.gridx = 11;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux12, gbc);
        PanelCentral.add(Aux12);

        gbc.gridx = 12;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux13, gbc);
        PanelCentral.add(Aux13);

        gbc.gridx = 13;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux14, gbc);
        PanelCentral.add(Aux14);

        gbc.gridx = 14;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux15, gbc);
        PanelCentral.add(Aux15);

        gbc.gridx = 15;
        gbc.gridy = 3;
        TotalBag.setConstraints(Aux16, gbc);
        PanelCentral.add(Aux16);
        //fin auxiliares

        PanelCentral.setLayout(TotalBag);

        this.add("Center", PanelCentral);

        JPanel PanelTop = new JPanel();
        PanelTop.setLayout(new GridLayout(2, 1));

        JPanel PanelTitulo = new JPanel();
        PanelTitulo.setLayout(new FlowLayout(FlowLayout.CENTER));
        PanelTitulo.setBackground(new Color(204, 204, 204));

        JLabel Titulo = new JLabel();
        Titulo.setFont(new Font("Verdana", 1, 14));
        Titulo.setText("C�lculo de Flujo de Potencia utilizando Gauss - Seidel");
        PanelTitulo.add(Titulo);

        JPanel PanelBotones = new JPanel();

        PanelBotones.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton Aplicacion = new JButton("Aplicaci�n");
        Aplicacion.setEnabled(false);

        PanelBotones.add(Aplicacion);
        PanelBotones.add(Ayuda);

        PanelTop.add(PanelTitulo);
        PanelTop.add(PanelBotones);

        add("North", PanelTop);
        pack();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {

        //----Inicio Ejecutar------------------------------------------------------
        if (ev.getActionCommand().equals("Reset")) {
            Volt1.setText("1.04");
            Volt2.setText("1");
            Volt3.setText("1.02");
            Volt4.setText("1");

            Pot1.setText("0");
            Pot2.setText("1");
            Pot3.setText("0.8");
            Pot4.setText("0.3");

            React1.setText("0");
            React2.setText("-0.5");
            React3.setText("0");
            React4.setText("-0.1");

            Delta4.setText("0");
            Delta3.setText("0");
            Delta2.setText("0");
            Delta1.setText("0");

            NumeroIter.setText("5");
            FactorAcelera.setState(false);
            Actualizacion.setState(false);

            ym11.setText("3-j9");
            ym12.setText("-2+j6");
            ym13.setText("-1+j3");
            ym14.setText("0+j0");
            ym21.setText("-2+j6");
            ym22.setText("3.666-j11");
            ym23.setText("-0.666+j2");
            ym24.setText("-1+j3");
            ym31.setText("-1+j3");
            ym32.setText("-0.666+j2");
            ym33.setText("3.666-j11");
            ym34.setText("-2+j6");
            ym41.setText("0+j0");
            ym42.setText("-1+j3");
            ym43.setText("-2+j6");
            ym44.setText("3-j9");

        }

        if (ev.getActionCommand().equals("Ayuda")) {
            showHTML("Ayuda", "/gauss_seidel/help.html");
        }

        if (ev.getActionCommand().equals("Ejecutar")) {

            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();
            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }

            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //----Fin  Ejecutar------------------------------------------------------
        //-Inicio Voltaje1----------------------------------------------------
        else if (ev.getActionCommand().equals("mas12")) {

            ValorAux = new Double(Volt1.getText());
            Volt1.setText("" + (ValorAux + 0.05));
            ValorAux = new Double(Volt1.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Volt1.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //fijo los estados de aceleracion y atualizaion
            SetearEstados();
            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos12")) {

            ValorAux = new Double(Volt1.getText());
            Volt1.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Volt1.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Volt1.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Voltaje1----------------------------------------------------------------
        //-Inicio Angulo1----------------------------------------------------
        else if (ev.getActionCommand().equals("mas14")) {

            ValorAux = new Double(Delta1.getText());
            Delta1.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(Delta1.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Delta1.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos14")) {

            ValorAux = new Double(Delta1.getText());
            Delta1.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Delta1.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Delta1.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Angulo 1----------------------------------------------------------------
        //-Inicio Potencia 2----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas21")) {

            ValorAux = new Double(Pot2.getText());
            Pot2.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(Pot2.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot2.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos21")) {

            ValorAux = new Double(Pot2.getText());
            Pot2.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Pot2.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot2.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Potencia 2----------------------------------------------------------------
        //-Inicio Reactivos 2----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas23")) {

            ValorAux = new Double(React2.getText());
            React2.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(React2.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                React2.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos23")) {

            ValorAux = new Double(React2.getText());
            React2.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(React2.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                React2.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Reactivos 2----------------------------------------------------------------
        //-Inicio Potencia 3----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas31")) {

            ValorAux = new Double(Pot3.getText());
            Pot3.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(Pot3.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot3.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos31")) {

            ValorAux = new Double(Pot3.getText());
            Pot3.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Pot3.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot3.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Potencia 3----------------------------------------------------------------
        //-Inicio Voltaje 3----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas32")) {

            ValorAux = new Double(Volt3.getText());
            Volt3.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(Volt3.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Volt3.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos32")) {

            ValorAux = new Double(Volt3.getText());
            Volt3.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Volt3.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Volt3.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } //Fin Voltaje 3----------------------------------------------------------------
        //-Inicio Potencia 4----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas41")) {

            ValorAux = new Double(Pot4.getText());
            Pot4.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(Pot4.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot4.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos41")) {

            ValorAux = new Double(Pot4.getText());
            Pot4.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(Pot4.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                Pot4.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        } //Fin Potencia4----------------------------------------------------------------
        //-Inicio React4----------------------------------------------------------
        else if (ev.getActionCommand().equals("mas43")) {

            ValorAux = new Double(React4.getText());
            React4.setText("" + (ValorAux + 0.05));

            ValorAux = new Double(React4.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                React4.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);

        } else if (ev.getActionCommand().equals("menos43")) {

            ValorAux = new Double(React4.getText());
            React4.setText("" + (ValorAux - 0.05));

            ValorAux = new Double(React4.getText());
            if ("".concat("" + ValorAux).length() > 7) {
                React4.setText("".concat("" + ValorAux).substring(0, 5));
            }
            //<fijo los estados de aceleracion y atualizaion
            SetearEstados();

            //Saco los valores de la pantalla
            ValoresTodo = CapturarValores();
            AdmitanciasString = CapturarAdmitancia();
            //seteo los complejos para las matriz de admitancias
            SetearImpedancias(AdmitanciasString);
            //seteo los voltajes complejos para el metodo
            SetearVoltajes(ValoresTodo);

            NNiter = Integer.parseInt(NumeroIter.getText());

            if (Actualizacion.getState()) {
                EstadoActualizacion = true;
                String Ordenes = OrdBarra1.getText() + OrdBarra2.getText() + OrdBarra3.getText();
                OrdenBarras = Integer.parseInt(Ordenes);

                Resultados = auxiliar.Calcular_Gauss_Seidel_Actualizacion_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion, OrdenBarras);
            } else {
                EstadoActualizacion = false;
                Resultados = auxiliar.Calcular_Gauss_Seidel_Aceleracion(V1in, V2in, V3in, V4in,/*P2 y P3*/ ValoresTodo[0][1], ValoresTodo[0][2],
                        /*P4,Q2 y Q4*/ ValoresTodo[0][3], ValoresTodo[2][1], ValoresTodo[2][3],
                        ym11in, ym12in, ym13in, ym14in, ym22in, ym23in, ym24in, ym33in, ym34in, ym44in,
                        NNiter, FactorDeAceleracion);

            }
            Voltajes = Resultados.V;
            Potencias = Resultados.P;
            Reactivos = Resultados.Q;
            Angulos = Resultados.delta;
            ImprimirPantalla2(Voltajes, Potencias, Reactivos, Angulos);
        }

        //Fin React4----------------------------------------------------------------
        //matrices de admitancia nodal
        if (ev.getActionCommand().equals("ym12")) {
            ym21.setText(ym12.getText());

        }
        if (ev.getActionCommand().equals("ym13")) {
            ym31.setText(ym13.getText());

        }
        if (ev.getActionCommand().equals("ym14")) {
            ym41.setText(ym14.getText());

        }
        if (ev.getActionCommand().equals("ym23")) {
            ym32.setText(ym23.getText());

        }
        if (ev.getActionCommand().equals("ym24")) {
            ym42.setText(ym24.getText());

        }

    }

//metodos del applet	
    public double[][] CapturarValores() {
        Double VV1, VV2, VV3, VV4;
        Double PP1, PP2, PP3, PP4;
        Double QQ1, QQ2, QQ3, QQ4;
        Double DD1, DD2, DD3, DD4;

        double[][] valores = new double[4][4];
        valores[0][0] = new Double(Pot1.getText());
        valores[0][1] = new Double(Pot2.getText());
        valores[0][2] = new Double(Pot3.getText());
        valores[0][3] = new Double(Pot4.getText());
        valores[1][0] = new Double(Volt1.getText());
        valores[1][1] = new Double(Volt2.getText());
        valores[1][2] = new Double(Volt3.getText());
        valores[1][3] = new Double(Volt4.getText());
        valores[2][0] = new Double(React1.getText());
        valores[2][1] = new Double(React2.getText());
        valores[2][2] = new Double(React3.getText());
        valores[2][3] = new Double(React4.getText());
        valores[3][0] = new Double(Delta1.getText());
        valores[3][1] = new Double(Delta2.getText());
        valores[3][2] = new Double(Delta3.getText());
        valores[3][3] = new Double(Delta4.getText());
        return valores;
    }

    public String[][] CapturarAdmitancia() {
        String ymm11, ymm12, ymm13, ymm14;
        String ymm21, ymm22, ymm23, ymm24;
        String ymm31, ymm32, ymm33, ymm34;
        String ymm41, ymm42, ymm43, ymm44;

        String[][] admitancias = new String[4][4];
        admitancias[0][0] = new String(ym11.getText());
        admitancias[0][1] = new String(ym12.getText());
        admitancias[0][2] = new String(ym13.getText());
        admitancias[0][3] = new String(ym14.getText());
        admitancias[1][0] = new String(ym21.getText());
        admitancias[1][1] = new String(ym22.getText());
        admitancias[1][2] = new String(ym23.getText());
        admitancias[1][3] = new String(ym24.getText());
        admitancias[2][0] = new String(ym31.getText());
        admitancias[2][1] = new String(ym32.getText());
        admitancias[2][2] = new String(ym33.getText());
        admitancias[2][3] = new String(ym34.getText());
        admitancias[3][0] = new String(ym41.getText());
        admitancias[3][1] = new String(ym42.getText());
        admitancias[3][2] = new String(ym43.getText());
        admitancias[3][3] = new String(ym44.getText());

        return admitancias;
    }

    public void ImprimirPantalla2(double[][] Voltajes, double[][] Potencias,
            double[][] Reactivos, double[][] Angulos) {

        //Volt1.setText(""+Voltajes[0][(Voltajes[0].length)-1]);
        Volt2.setText("".concat("" + Voltajes[1][(Voltajes[0].length) - 1]).substring(0, 8));
        //Volt3.setText(""+Voltajes[2][(Voltajes[0].length)-1]);
        Volt4.setText("".concat("" + Voltajes[3][(Voltajes[0].length) - 1]).substring(0, 8));

        Pot1.setText("".concat("" + Potencias[0][(Potencias[0].length) - 1]).substring(0, 8));
        //Pot2.setText(""+Potencias[1][(Potencias[0].length)-1]);
        //Pot3.setText(""+Potencias[2][(Potencias[0].length)-1]);
        //Pot4.setText(""+Potencias[3][(Potencias[0].length)-1]);

        React1.setText("".concat("" + Reactivos[0][(Reactivos[0].length) - 1]).substring(0, 8));
        //React2.setText(""+Reactivos[1][(Reactivos[0].length)-1]);
        React3.setText("".concat("" + Reactivos[2][(Reactivos[0].length) - 1]).substring(0, 8));
        //React4.setText(""+Reactivos[3][(Reactivos[0].length)-1]);

        //D4texto=(""+Salidas[1][3]).substring(0,10);			
        //Delta1.setText(""+Angulos[0][(Angulos[0].length)-1]);
        Delta2.setText("".concat("" + Angulos[1][(Angulos[0].length) - 1]).substring(0, 8));
        Delta3.setText("".concat("" + Angulos[2][(Angulos[0].length) - 1]).substring(0, 8));
        Delta4.setText("".concat("" + Angulos[3][(Angulos[0].length) - 1]).substring(0, 8));
    }

    public void SetearImpedancias(String[][] AdmitanciasString) {

        ym11in = complejo.parseComplex(AdmitanciasString[0][0]);
        ym12in = complejo.parseComplex(AdmitanciasString[0][1]);
        ym13in = complejo.parseComplex(AdmitanciasString[0][2]);
        ym14in = complejo.parseComplex(AdmitanciasString[0][3]);
        ym22in = complejo.parseComplex(AdmitanciasString[1][1]);
        ym23in = complejo.parseComplex(AdmitanciasString[1][2]);
        ym24in = complejo.parseComplex(AdmitanciasString[1][3]);
        ym33in = complejo.parseComplex(AdmitanciasString[2][2]);
        ym34in = complejo.parseComplex(AdmitanciasString[2][3]);
        ym44in = complejo.parseComplex(AdmitanciasString[3][3]);
    }

    public void SetearVoltajes(double[][] ValoresTodo) {

        V1in = complejo.polar(ValoresTodo[1][0], ValoresTodo[3][0]);
        V2in = complejo.polar(ValoresTodo[1][1], ValoresTodo[3][1]);
        V3in = complejo.polar(ValoresTodo[1][2], ValoresTodo[3][2]);
        V4in = complejo.polar(ValoresTodo[1][3], ValoresTodo[3][3]);
    }

    public void SetearEstados() {

        if (FactorAcelera.getState()) {
            FactorDeAceleracion = Double.valueOf(Factor.getText()).doubleValue();

        } else {
            FactorDeAceleracion = 1;

        }
        if (Actualizacion.getState()) {
            EstadoActualizacion = true;
        } else {
            EstadoActualizacion = false;

        }

    }

    private void showHTML(String title, String archivo) {
        try {
            URL paginita = getClass().getResource(archivo);
            InputStream s_in = paginita.openStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(s_in));
            String lineas = "";
            try {
                while (true) {
                    String linea = in.readLine();
                    if (linea == null) {
                        break;
                    }
                    lineas += linea;
                }
            } catch (IOException q) {
                JOptionPane.showMessageDialog(this, "Couldn't open help source " + archivo, "Error getting help file", JOptionPane.ERROR_MESSAGE);
                q.printStackTrace(System.err);
            }

            final JDialog dialog = new JDialog((Frame) null, title);
//		    JLabel label = new JLabel(lineas);
            dialog.setModal(true);
            JEditorPane label = new JEditorPane(paginita);
            label.setEditable(false);

            JScrollPane scrollPane = new JScrollPane(label);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

            JButton closeButton = new JButton("Cerrar");
            closeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
            JPanel closePanel = new JPanel();
            closePanel.setLayout(new BoxLayout(closePanel,
                    BoxLayout.LINE_AXIS));
            closePanel.add(Box.createHorizontalGlue());
            closePanel.add(closeButton);
            closePanel.setBorder(BorderFactory.
                    createEmptyBorder(0, 0, 5, 5));

            JPanel contentPane = new JPanel(new BorderLayout());
            contentPane.add(scrollPane, BorderLayout.CENTER);
            contentPane.add(closePanel, BorderLayout.PAGE_END);
            contentPane.setOpaque(true);
            dialog.setContentPane(contentPane);

            //Show it.
            dialog.setSize(new Dimension(750, 500));
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
        } catch (HeadlessException x) {
            System.out.println(x.getMessage());
        } catch (IOException x) {
            System.out.println(x.getMessage());
        }

    }

}
//fin applet
