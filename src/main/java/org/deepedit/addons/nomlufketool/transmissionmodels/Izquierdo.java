package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import javax.swing.*;

public class Izquierdo extends JPanel{

		//***PanelCoorParam2
		JPanel PanelCoorParam;

		//***Coordenadas****
		JPanel PanelCoordenadas;
		JPanel PanelParametros;
		JPanel PanelOpcion;
		ButtonGroup ButtonGroupRectPolar;
		JRadioButton RadioButtonRectangular;
		JRadioButton RadioButtonPolar;
		JPanel PanelAceptarParametros;
		JButton ButtonAceptarParametros;

		//***Datos****
		JPanel PanelDatos;
		JPanel PanelDatos1;
		JPanel PanelDatos2;
		JPanel PanelRe;
		JLabel LabelRe;
		JLabel[] LabelParam;
		public JTextField[][] TextParam;

		//****PanelTipo***
		JPanel PanelTipo;
		JPanel PanelTituloTipo;
		JPanel PanelOpcionTipo;
		JLabel TituloTipo;
		ButtonGroup ButtonGroupTipo;
		JRadioButton RadioButtonTipo1;
		JRadioButton RadioButtonTipo2;
		//JRadioButton RadioButtonTipo3;
		double A1,A2,B1,B2,C1,C2,D1,D2,V11,V12,V21,V22;
		double cA1,cA2,cB1,cB2,cC1,cC2,cD1,cD2,cV11,cV12,cV21,cV22;	//Copias de seguridad
		Complex2 A,B,C,D,V1,V2, aux;
		boolean Polar=false;
		Acciones acc; //pal metodo
		Lienzo lienzo; //pal metodo
		Zoom Pzoom;

public Izquierdo() {
        initComponents();
    }

public void Metodos(Acciones a, Lienzo L, Zoom P){
	acc=a;
	lienzo=L;
	Pzoom=P;
}
private void initComponents() {

		setLayout(new GridBagLayout());
		setMaximumSize(new java.awt.Dimension(170, 350));
		setMinimumSize(new java.awt.Dimension(170, 350));
		setPreferredSize(new java.awt.Dimension(170, 350));

		//****PanelCoorParam2
		PanelCoorParam=new JPanel();
		PanelCoorParam.setLayout(new BorderLayout());

		//****PanelCoordenadas(200,50)
        java.awt.GridBagConstraints gridBagConstraints;
		PanelCoordenadas=new JPanel();
		PanelOpcion = new JPanel();
		ButtonGroupRectPolar = new ButtonGroup();
		RadioButtonRectangular = new JRadioButton();
		RadioButtonPolar = new JRadioButton();
		PanelAceptarParametros=new JPanel();
		ButtonAceptarParametros=new JButton("Graficar");

		PanelOpcion.setLayout(new java.awt.GridLayout(1,0));
		PanelOpcion.setMaximumSize(new java.awt.Dimension(150, 22));
		PanelOpcion.setMinimumSize(new java.awt.Dimension(150, 22));
	    PanelOpcion.setPreferredSize(new java.awt.Dimension(150, 22));

		RadioButtonRectangular.setText("Rectangular");
		ButtonGroupRectPolar.add(RadioButtonRectangular);

		PanelOpcion.add(RadioButtonRectangular);
       	RadioButtonRectangular.setSelected(true);
		RadioButtonPolar.setText("Polar");
		ButtonGroupRectPolar.add(RadioButtonPolar);

		PanelOpcion.add(RadioButtonPolar);

		PanelAceptarParametros.setMaximumSize(new java.awt.Dimension(60, 34));
		PanelAceptarParametros.setMinimumSize(new java.awt.Dimension(60, 34));
	    PanelAceptarParametros.setPreferredSize(new java.awt.Dimension(60, 34));
	    PanelAceptarParametros.add(ButtonAceptarParametros);

		PanelCoordenadas.setMinimumSize(new java.awt.Dimension(150, 70));
		PanelCoordenadas.setPreferredSize(new java.awt.Dimension(150, 70));
		PanelCoordenadas.setLayout(new java.awt.GridBagLayout());
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		PanelCoordenadas.add(PanelOpcion,gridBagConstraints);
		PanelCoorParam.add("North",PanelOpcion);	//********SALIDA PANELCOORDENADAS********



		//****PanelDatos***
		PanelDatos=new JPanel();
		PanelDatos.setLayout(new java.awt.GridBagLayout());
		PanelDatos1=new JPanel();
		PanelDatos1.setLayout(new GridLayout(0,1,0,2));
		PanelDatos2=new JPanel();
		PanelDatos2.setLayout(new GridLayout(0,2,3,2));

		PanelRe=new JPanel();
		PanelRe.setMinimumSize(new java.awt.Dimension(100, 22));
        PanelRe.setPreferredSize(new java.awt.Dimension(100, 22));

		PanelDatos.setMinimumSize(new java.awt.Dimension(160, 182));
        PanelDatos.setPreferredSize(new java.awt.Dimension(160, 182));
		PanelDatos1.setMinimumSize(new java.awt.Dimension(33, 160));
		PanelDatos1.setPreferredSize(new java.awt.Dimension(33, 160));
		PanelDatos2.setMinimumSize(new java.awt.Dimension(127, 160));
		PanelDatos2.setPreferredSize(new java.awt.Dimension(127, 160));

       	LabelRe=new JLabel("Re                 Im ");
       	PanelRe.add(LabelRe);
       	LabelParam=new JLabel[6];
		TextParam=new JTextField[6][2];

       	gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 0;
       	gridBagConstraints.gridx = 1;
		PanelDatos.add(PanelRe,gridBagConstraints);

		TextParam[0][0]=new JTextField("0.879698");
		TextParam[0][1]=new JTextField("0.023035");
		TextParam[1][0]=new JTextField("0.036447");
		TextParam[1][1]=new JTextField("0.196650");
		TextParam[2][0]=new JTextField("0.0");
		TextParam[2][1]=new JTextField("1.12");
		TextParam[3][0]=new JTextField("0.949994");
		TextParam[3][1]=new JTextField("0.0033161");
		TextParam[4][0]=new JTextField("1.0");
		TextParam[4][1]=new JTextField("0.0");
		TextParam[4][1].setText("0.0");
		TextParam[4][1].setEditable(false);
		TextParam[5][0]=new JTextField("1.0");
		TextParam[5][1]=new JTextField("0.0");
		TextParam[5][1].setEditable(false);

		A1=Double.parseDouble(TextParam[0][0].getText());
		A2=Double.parseDouble(TextParam[0][1].getText());
		B1=Double.parseDouble(TextParam[1][0].getText());
		B2=Double.parseDouble(TextParam[1][1].getText());
		C1=Double.parseDouble(TextParam[2][0].getText());
		C2=Double.parseDouble(TextParam[2][1].getText());
		D1=Double.parseDouble(TextParam[3][0].getText());
		D2=Double.parseDouble(TextParam[3][1].getText());
		V11=Double.parseDouble(TextParam[4][0].getText());
		V12=Double.parseDouble(TextParam[4][1].getText());
		V21=Double.parseDouble(TextParam[5][0].getText());
		V22=Double.parseDouble(TextParam[5][1].getText());

		for (int i=0; i<6; i++){
		LabelParam[i]=new JLabel();
		}

		LabelParam[0].setText(" A = ");
		LabelParam[1].setText(" B = ");
		LabelParam[2].setText(" C = ");
		LabelParam[3].setText(" D = ");
		LabelParam[4].setText(" V1 = ");
		LabelParam[5].setText(" V2 = ");

		for(int i=0;i<6;i++){
			PanelDatos1.add(LabelParam[i]);
			PanelDatos2.add(TextParam[i][0]);
			PanelDatos2.add(TextParam[i][1]);
		}

		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridx = 0;
		PanelDatos.add(PanelDatos1, gridBagConstraints);
		gridBagConstraints.gridx = 1;
		PanelDatos.add(PanelDatos2, gridBagConstraints);
		PanelCoorParam.add("Center",PanelDatos);
		PanelCoorParam.add("South",PanelAceptarParametros);
		PanelCoorParam.setBorder(new javax.swing.border.TitledBorder("Parametros de Linea"));


		//*****PanelTipo*******************

		PanelTipo=new JPanel();
		PanelOpcionTipo = new JPanel();
		ButtonGroupTipo = new ButtonGroup();
		RadioButtonTipo1 = new JRadioButton();
		RadioButtonTipo2 = new JRadioButton();
		//RadioButtonTipo3 = new JRadioButton();

		PanelOpcionTipo.setLayout(new java.awt.GridLayout(0,1));
		PanelOpcionTipo.setMaximumSize(new java.awt.Dimension(170, 75));
		PanelOpcionTipo.setMinimumSize(new java.awt.Dimension(170, 75));
		PanelOpcionTipo.setPreferredSize(new java.awt.Dimension(170, 75));
		RadioButtonTipo1.setText("Receptor");
		RadioButtonTipo1.setSelected(true);
		ButtonGroupTipo.add(RadioButtonTipo1);
		PanelOpcionTipo.add(RadioButtonTipo1);
		RadioButtonTipo2.setText("Transmisor");
		ButtonGroupTipo.add(RadioButtonTipo2);
		PanelOpcionTipo.add(RadioButtonTipo2);
		//RadioButtonTipo3.setText("Generalizado");
		//ButtonGroupTipo.add(RadioButtonTipo3);
		//PanelOpcionTipo.add(RadioButtonTipo3);


		PanelTipo.setMinimumSize(new java.awt.Dimension(160, 85));
		PanelTipo.setPreferredSize(new java.awt.Dimension(160, 85));
		PanelTipo.setLayout(new java.awt.GridBagLayout());
		gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridy = 0;
		PanelOpcionTipo.setBorder(new javax.swing.border.TitledBorder("Tipo de Diagrama"));

		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridx = 0;
		add(PanelCoorParam,gridBagConstraints);

		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridx = 0;

		add(PanelOpcionTipo,gridBagConstraints);	//********SALIDA PANELTIPO********

		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridx = 0;


//***************************ESCUCHADORES********************************
//***********************************************************************

ButtonAceptarParametros.addActionListener(new java.awt.event.ActionListener() {
    public void actionPerformed(java.awt.event.ActionEvent evt) {
	ButtonAceptarParametrosActionPerformed(evt);}});

RadioButtonRectangular.addActionListener(new java.awt.event.ActionListener() {
	public void actionPerformed(java.awt.event.ActionEvent evt) {
		RadioButtonRectangularActionPerformed(evt);}});

RadioButtonPolar.addActionListener(new java.awt.event.ActionListener() {
	public void actionPerformed(java.awt.event.ActionEvent evt) {
	RadioButtonPolarActionPerformed(evt);}});

RadioButtonPolar.addActionListener(new java.awt.event.ActionListener() {
	public void actionPerformed(java.awt.event.ActionEvent evt) {
	RadioButtonTipo1ActionPerformed(evt);}});

RadioButtonPolar.addActionListener(new java.awt.event.ActionListener() {
	public void actionPerformed(java.awt.event.ActionEvent evt) {
	RadioButtonTipo2ActionPerformed(evt);}});

//RadioButtonPolar.addActionListener(new java.awt.event.ActionListener() {
//	public void actionPerformed(java.awt.event.ActionEvent evt) {
//	RadioButtonTipo3ActionPerformed(evt);}});


}//init components



	public double aprox(double x, int i){		//aproxima con i decimales
		return Math.rint(x*Math.pow(10,i))/Math.pow(10,i);
	}

//OPCION POLAR******************
    private void RadioButtonPolarActionPerformed(java.awt.event.ActionEvent evt){
       LabelRe.setText("Mag                 Ang ");
	if(Polar==true)	return;

	for(int i=0; i<6;i++){
	aux=new Complex2(Double.parseDouble(TextParam[i][0].getText()),Double.parseDouble(TextParam[i][1].getText()));
	double a=Double.parseDouble(TextParam[i][1].getText());
	double b=Double.parseDouble(TextParam[i][0].getText());
	if(a==0&&b==0){
	TextParam[i][0].setText(""+0.0);
	TextParam[i][1].setText(""+0.0);
	}//if
	else{
	TextParam[i][0].setText(""+aprox(aux.mag,5));
	TextParam[i][1].setText(""+aprox(aux.ang,5));
	}//else
	Polar=true;
	}//for
    }//metodo

//OPCION RECTANGULAR*****************
	private void RadioButtonRectangularActionPerformed(java.awt.event.ActionEvent evt){
        LabelRe.setText("Re                 Im ");

	if(!Polar==true)return;
	for(int i=0; i<6;i++){
	aux=new Complex2(Double.parseDouble(TextParam[i][0].getText()),Double.parseDouble(TextParam[i][1].getText()),1);
	TextParam[i][0].setText(""+aprox(aux.re,5));
	TextParam[i][1].setText(""+aprox(aux.im,5));
	}//for
	Polar=false;
	}

//TRANSMISOR*************************
private void RadioButtonTipo1ActionPerformed(java.awt.event.ActionEvent evt){}


//RECEPTOR*************************
private void RadioButtonTipo2ActionPerformed(java.awt.event.ActionEvent evt){}


//GRAFICAR***********************
private void ButtonAceptarParametrosActionPerformed(java.awt.event.ActionEvent evt){
	error();
 }//ACEPTAR


//ERROR*******************
public void error(){
	try{
		//Se guardan Copias
		cA1=A1;
		cA2=A2;
		cB1=B1;
		cB2=B2;
		cC1=C1;
		cC2=C2;
		cD1=D1;
		cD2=D2;
		cV11=V11;
		cV12=V12;
		cV21=V21;
		cV22=V22;

		//se intenta
		A1=Double.parseDouble(TextParam[0][0].getText());
		A2=Double.parseDouble(TextParam[0][1].getText());
		B1=Double.parseDouble(TextParam[1][0].getText());
		B2=Double.parseDouble(TextParam[1][1].getText());
		C1=Double.parseDouble(TextParam[2][0].getText());
		C2=Double.parseDouble(TextParam[2][1].getText());
		D1=Double.parseDouble(TextParam[3][0].getText());
		D2=Double.parseDouble(TextParam[3][1].getText());
		V11=Double.parseDouble(TextParam[4][0].getText());
		V12=Double.parseDouble(TextParam[4][1].getText());
		V21=Double.parseDouble(TextParam[5][0].getText());
		V22=Double.parseDouble(TextParam[5][1].getText());
		acc.graficar(this, Pzoom,lienzo);
		} //try
		catch (Exception e){
		//si se cayo de dejan las variable como estaban antes del try
		A1=cA1;
		A2=cA2;
		B1=cB1;
		B2=cB2;
		C1=cC1;
		C2=cC2;
		D1=cD1;
		D2=cD2;
		V11=cV11;
		V12=cV12;
		V21=cV21;
		V22=cV22;
		JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
		}//catch
	}


//GETPARAMS***********************
public Complex2[] getParams(){


	Complex2[] co=new Complex2[6];

	if(RadioButtonPolar.isSelected()){
		co[0]=new Complex2(A1,A2,1);
		co[1]=new Complex2(B1,B2,1);
		co[2]=new Complex2(C1,C2,1);
		co[3]=new Complex2(D1,D2,1);
		co[4]=new Complex2(V11,V12,1);
		co[5]=new Complex2(V21,V22,1);
	}
	if(RadioButtonRectangular.isSelected()){
		co[0]=new Complex2(A1,A2);
		co[1]=new Complex2(B1,B2);
		co[2]=new Complex2(C1,C2);
		co[3]=new Complex2(D1,D2);
		co[4]=new Complex2(V11,V12);
		co[5]=new Complex2(V21,V22);
	}

	return co;

}//getParams



}