package org.deepedit.addons.nomlufketool.phasorcalculator;

public class Complejo{
	public double re;
	public double im;

	Complejo()
	{
		this.re=0;
		this.im=0;
	}
	
	Complejo (double x, double y){
		this.re=x;
		this.im=y;
	}

	Complejo (String com, int a){  //El int es s�lo para diferenciar este constructor
		//'com' es o un numero real, o la unidad imaginaria.

		int l=com.length();

		if (l==0)
		{
			this.re=0;
			this.im=0;
		}
		else if (l==1 && (com.charAt(0)=='i' | com.charAt(0)=='I' | com.charAt(0)=='j' | com.charAt(0)=='J'))
		{
			this.re=0;
			this.im=1;
		}
		else
		{
			this.re=Double.parseDouble(com);
			this.im=0;
		}
	}

	Complejo(String expresion_numerica_compleja){ //Acepta expresiones como: cos(2+3*i)
            Nodo n=new Nodo(expresion_numerica_compleja);
            Complejo a=n.evalua();
            this.re=a.re; 
            this.im=a.im;
	}

	Complejo mas(Complejo otro){
            Complejo nc=new Complejo();
            nc.re=this.re+otro.re;
            nc.im=this.im+otro.im;
            return nc;
	}

	Complejo menos(Complejo otro){
            Complejo nc=new Complejo();
            nc.re=this.re-otro.re;
            nc.im=this.im-otro.im;
            return nc;
	}

	Complejo por(Complejo otro){
            Complejo nc=new Complejo();
            nc.re=this.re*otro.re-this.im*otro.im;
            nc.im=this.im*otro.re+this.re*otro.im;
            return nc;
	}
        
	Complejo div(Complejo otro){
            Complejo nc=new Complejo();
            nc.re=(this.re*otro.re+this.im*otro.im)/(otro.re*otro.re+otro.im*otro.im);
            nc.im=(this.im*otro.re-this.re*otro.im)/(otro.re*otro.re+otro.im*otro.im);
            return nc;
	}
	
	static double cos(double a){
            return Math.cos(a);
	}

	static double sin(double a){
            return Math.sin(a);
	}
	
	static double exp(double a){
            return Math.exp(a);
	}
	
	static double ln(double a){
            return Math.log(a);
	}
	
	double mod(){
            return Math.sqrt(this.re*this.re+this.im*this.im);
	}
	
	double ang(){
            return Math.atan2(this.im,this.re);
	}

	void conjuga(){
            this.im=-this.im;
	}

	Complejo Clone(){
            return new Complejo(this.re,this.im);
	}

	/*static Complejo exp(Complejo c){
		double r=c.mod();
		double o=c.ang();
		
		double modulo=exp(r*cos(o));
		double angulo=r*sin(o);
		
		return new Complejo(modulo*cos(angulo),modulo*sin(angulo));		
	}*/

        static Complejo exp(Complejo c){
		return new Complejo(Math.exp(c.re)*cos(c.im),Math.exp(c.re)*sin(c.im));		
	}
        
	static Complejo ln(Complejo c){
		double r=c.mod();
                double o=c.ang();
		return new Complejo(Math.log(r),o);		
	}	

/*
	static Complejo pow(Complejo c1, Complejo c2)
	{
		double r1=c1.mod();
		double r2=c2.mod();
		double o1=c1.ang();
		double o2=c2.ang();
				
		double modulo=exp(r2*cos(o2)*ln(r1)-o1*r2*sin(o2));
		double angulo=o1*r2*cos(o2)+r2*sin(o2)*ln(r1);
		
		return new Complejo(modulo*cos(angulo),modulo*sin(angulo));
	}
*/

	static Complejo pow(Complejo c1, Complejo cc2){
            Complejo c2=cc2.Clone();
            return exp(c2.por(ln(c1)));
	}
	
	static double cosh(double a){
            return 0.5*(Math.exp(a)+Math.exp(-a));
	}

	static double sinh(double a){
            return 0.5*(Math.exp(a)-Math.exp(-a));
	}

	static Complejo cos(Complejo c1){
            return new Complejo(Math.cos(c1.re)*cosh(c1.im),-Math.sin(c1.re)*sinh(c1.im));
	}
	
	static Complejo sin(Complejo c1){
		return new Complejo(Math.sin(c1.re)*cosh(c1.im),Math.cos(c1.re)*sinh(c1.im));
	}
	
	static Complejo cosh(Complejo c1){
		return new Complejo(cosh(c1.re)*Math.cos(c1.im),sinh(c1.re)*Math.sin(c1.im));
	}

	static Complejo sinh(Complejo c1){
		return new Complejo(sinh(c1.re)*Math.cos(c1.im),cosh(c1.re)*Math.sin(c1.im));
	}
        
        /*double Re(){
            return this.Re();
        }
        
        double Im(){
            return this.Im();
        }*/
}
