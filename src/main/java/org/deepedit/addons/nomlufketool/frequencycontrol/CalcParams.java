package org.deepedit.addons.nomlufketool.frequencycontrol;

import org.deepedit.addons.nomlufketool.utils.Complex;

/*
 * CalcParams.java
 *
 * Created on June 15, 2003, 12:28 AM
 */

/**
 *
 * @author  midgard
 */
public class CalcParams {

	final static double omega=100*Math.PI;         //solamente 50 Hz

    static public ParamsData LLCP(double R, double L, double G, double C, double x)
    {
        ParamsData d = new ParamsData();
		d.Gamma		 = Complex.sqrt( Complex.multiply( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
		d.Zw		 = Complex.sqrt( Complex.divide( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
        d.Pi[0]		 = Complex.multiply( d.Zw , Complex.sinh( Complex.multiply( d.Gamma , x ) ) );		//Zs
		d.Pi[1]		 = Complex.divide( Complex.tanh( Complex.multiply( d.Gamma , x/2 ) ) , d.Zw );	//Yp
		d.ABCD[0][0] = Complex.cosh( Complex.multiply( d.Gamma , x ) );						//A
		d.ABCD[0][1] = d.Pi[0];															//B
		d.ABCD[1][0] = Complex.divide( Complex.sinh( Complex.multiply(d.Gamma , x) ) , d.Zw );	//C
		d.ABCD[1][1] = d.ABCD[0][0];														//D

		return d;
    }

	static public ParamsData LLSP(double L, double C, double x)
	{
		return LLCP( 0, L, 0, C, x);
	}

	static public ParamsData LCCP(double R, double L, double G, double C, double x)
	{
		ParamsData d	= new ParamsData();
		d.Zw			= Complex.sqrt( Complex.divide( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
		d.Pi[0]			= Complex.multiply( new Complex( R, omega*L ) , x );							//Zs
		d.Pi[1]			= Complex.multiply( new Complex( G, omega*C ) , x/2);							//Yp
		d.ABCD[0][0]	= Complex.add( Complex.multiply( d.Pi[0], d.Pi[1] ) , 1);				//A
		d.ABCD[0][1]	= d.Pi[0];															//B
		d.ABCD[1][0]	= Complex.multiply(d.Pi[1] ,
								   Complex.add(Complex.multiply(d.Pi[0], d.Pi[1]), 2 ) );	//C
		d.ABCD[1][1]	= d.ABCD[0][0];														//D

		return d;
	}

	static public ParamsData LCSP(double L, double C, double x)
	{
		return LCCP( 0, L, 0, C, x);
	}
}