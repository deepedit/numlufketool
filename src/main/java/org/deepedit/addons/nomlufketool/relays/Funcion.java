package org.deepedit.addons.nomlufketool.relays;

/*
 * funcion.java
 *
 * Created on 25 de junio de 2005, 10:33 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

/**
 *
 * @author Leonardo
 */
public class Funcion {
    
    public int n;
    public double[] m;
    public double[] h;
    public double[] x;
    public double[] f;
    public double[][] A;
    public double[] b;
    public double[] lambda;
    //private double[][] A2;
    //private double[] b2;
    /** Creates a new instance of funcion */
    public Funcion(double[] f2,double[] x2) {
        n=x2.length;
        m=new double[n-1];
        h=new double[n-1];
        x=x2;
        f=f2;
        A=new double[n][n];
        b=new double[n];
        lambda=new double[n];
        //A2=new double[n-1][n-1];
        //b2=new double[n-1];
        splineC();
        gaussC();
        lambdaC();
    }
    
    public void splineC() {
        for(int i=0; i<n-1; i++) {
            h[i]=x[i+1]-x[i];
            m[i]=(f[i+1]-f[i])/h[i];
        }
        A[0][0]=2/h[0]; A[0][1]=1/h[0];
        A[n-1][n-2]=1/h[n-2]; A[n-1][n-1]=2/h[n-2];
        b[0]=3*m[0]/h[0];
        b[n-1]=3*m[n-2]/h[n-2];
        for(int i=1; i<n-1; i++) {
            b[i]=3*(m[i-1]/h[i-1]+m[i]/h[i]);
            A[i][i-1]=1/h[i-1];
            A[i][i]=2*(1/h[i-1]+1/h[i]);
            A[i][i+1]=1/h[i];
        }
    }
    
    public void gaussC() {
        double aux;
        for(int i=1; i<n; i++) {
            aux=A[i][i-1]/A[i-1][i-1];
            A[i][i]-=aux*A[i-1][i];
            A[i][i-1]-=aux*A[i-1][i-1];
            b[i]-=aux*b[i-1];
        }
    }    
    
    public void lambdaC() {
        lambda[n-1]=b[n-1]/A[n-1][n-1];
        for(int i=n-2; i>=0; i--) {
            lambda[i]=(b[i]-A[i][i+1]*lambda[i+1])/A[i][i];
        }
    }   
    
    public double falla(double X) {
        for(int i=0; i<n-1; i++) {
            double aux=0;
            if(X<=x[i+1]) {
                aux=X-x[i];
                return f[i] + aux*lambda[i] + aux*aux*(m[i]-lambda[i])/h[i] + aux*aux*(X-x[i+1])*(lambda[i]-2*m[i]+lambda[i+1])/(h[i]*h[i]);
            }
            
        }
        return -1;
    }   
    
}
