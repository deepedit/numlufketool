package org.deepedit.addons.nomlufketool.quadripoles;

public class TetrapolosApplet2 extends javax.swing.JApplet {
    
    public TetrapolosApplet2() {
        TetrapolosCalc TC = new TetrapolosCalc();
//        Ayuda33 ayuda = new Ayuda33();
        Ayuda33 ayuda = new Ayuda33(TC);
        TC.Metodos(ayuda);
//        ayuda.PanelTop.Metodos(TC,ayuda);
        
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(TC);
        getContentPane().add(ayuda);
        
        TC.setVisible(false);
        ayuda.setVisible(true);
        repaint();
        
    }
}