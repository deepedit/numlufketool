/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         NomLufkeLabels.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Nom Lufke toolbox label factory
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */
package org.deepedit.addons.nomlufketool;

import java.util.EnumMap;
import org.deepedit.addons.nomlufketool.NomLufkeEnums.App;
import org.deepedit.addons.nomlufketool.NomLufkeEnums.Language;
import static org.deepedit.addons.nomlufketool.NomLufkeEnums.Language.English;
import static org.deepedit.addons.nomlufketool.NomLufkeEnums.Language.Spanish;

/**
 * Nom Lufke toolbox label factory
 *
 * @author Frank Leanez
 */
public class NomLufkeLabels {
    
    
    private static java.util.Map<App, String> mToolNameEnglish = null;
    private static java.util.Map<App, String> mToolNameSpanish = null;

    /**
     * Function to retrieve label for the given key and selected language
     * <br>Keys are currently hardwired to this class code
     * <br>To retrieve all keys use the function getAllNomLufkeLabels(English)
     * <br>Values in English are the same as keys
     *
     * @param key String representation of the key (in English)
     * @param language
     * @return null if key is not found in given language or there's a problem
     * initializing keys
     */
    public static String getNomLufkeToolName(App  key, Language language) {
        if (mToolNameSpanish == null) {
            initLabels();
        }
        if (mToolNameSpanish == null) {
            return null;
        }
        switch (language) {
            case English:
                if (mToolNameSpanish.containsKey(key)) {
                    return key.toString();
                } else {
                    return null;
                }
            case Spanish:
                return mToolNameSpanish.get(key);
        }
        return null;
    }

    /**
     * Retrieve all labels for the selected language
     *
     * @param language Numlufke supported languages
     * @return array with all labels. An empty (length 0) array if there was a
     * problem initializing the labels
     */
    public static String[] getAllNomLufkeToolNames(Language language) {
        if (mToolNameSpanish == null) {
            initLabels();
        }
        java.util.Collection<String> lLabels;

        switch (language) {
            case English:
                lLabels = mToolNameEnglish.values();
                return lLabels.toArray(new String[lLabels.size()]);
            case Spanish:
                lLabels = mToolNameSpanish.values();
                return lLabels.toArray(new String[lLabels.size()]);
        }
        return new String[0];
    }

    /**
     * <b>Temporary</b> Replace this with parsing a text file
     */
    private static void initLabels() {

        mToolNameEnglish = new EnumMap<App, String>(App.class);
        mToolNameEnglish.put(App.PhasorCalculator, App.PhasorCalculator.toString());
        mToolNameEnglish.put(App.RadialpuConversor, App.RadialpuConversor.toString());
        mToolNameEnglish.put(App.Quadripoles, App.Quadripoles.toString());
        mToolNameEnglish.put(App.SyncOperationCircle, App.SyncOperationCircle.toString());
        mToolNameEnglish.put(App.TransmissionParameters, App.TransmissionParameters.toString());
        mToolNameEnglish.put(App.TransformerModel, App.TransformerModel.toString());
        mToolNameEnglish.put(App.TransmissionLineModels, App.TransmissionLineModels.toString());
        mToolNameEnglish.put(App.Gradient, App.Gradient.toString());
        mToolNameEnglish.put(App.InfluenceFactors, App.InfluenceFactors.toString());
        mToolNameEnglish.put(App.Gauss_Seidel, App.Gauss_Seidel.toString());
        mToolNameEnglish.put(App.FrequencyControl, App.FrequencyControl.toString());
        mToolNameEnglish.put(App.UnbalancedComponents, App.UnbalancedComponents.toString());
        mToolNameEnglish.put(App.Relays_Example1, App.Relays_Example1.toString());
        mToolNameEnglish.put(App.Relays_Example2, App.Relays_Example2.toString());
        mToolNameEnglish.put(App.TransientStability, App.TransientStability.toString());
        mToolNameEnglish.put(App.EconomicDispatch, App.EconomicDispatch.toString());

        mToolNameSpanish = new EnumMap<App, String>(App.class);
        mToolNameSpanish.put(App.PhasorCalculator, "Calculadora Fasorial");
        mToolNameSpanish.put(App.RadialpuConversor, "Representaci�n en p.u. Sistemas Radiales");
        mToolNameSpanish.put(App.Quadripoles, "Tetrapolos");
        mToolNameSpanish.put(App.SyncOperationCircle, "Diagrama de C�rculo");
        mToolNameSpanish.put(App.TransmissionParameters, "Par�metros LT");
        mToolNameSpanish.put(App.TransformerModel, "Modelo del Transformador");
        mToolNameSpanish.put(App.TransmissionLineModels, "Modelos de LT");
        mToolNameSpanish.put(App.Gradient, "Gradiente");
        mToolNameSpanish.put(App.InfluenceFactors, "Factores de Influencia");
        mToolNameSpanish.put(App.Gauss_Seidel, "Gauss-Seidel");
        mToolNameSpanish.put(App.FrequencyControl, "Control de Frecuencia");
        mToolNameSpanish.put(App.UnbalancedComponents, "C�lculo de Componentes");
        mToolNameSpanish.put(App.Relays_Example1, "Protecciones (Ejemplo 1)");
        mToolNameSpanish.put(App.Relays_Example2, "Protecciones (Ejemplo 2)");
        mToolNameSpanish.put(App.TransientStability, "Estabilidad Transitoria");
        mToolNameSpanish.put(App.EconomicDispatch, "Despacho Econ�mico");

    }
    
}
