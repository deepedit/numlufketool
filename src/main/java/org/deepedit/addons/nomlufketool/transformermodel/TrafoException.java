package org.deepedit.addons.nomlufketool.transformermodel;

public class TrafoException extends Exception {

    public TrafoException(String s) {
        super(s);
    }
}
