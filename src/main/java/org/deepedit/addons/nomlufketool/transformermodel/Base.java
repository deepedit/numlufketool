package org.deepedit.addons.nomlufketool.transformermodel;

/** Base monofasica.
 * Voltajes fase-neutro, Corrientes de linea, Potencia monofasica.
 */
public class Base{
	private double S, V;

	public Base(double S, double V){
		this.S = S;
		this.V = V;
	}

	public double getS(){ return this.S; }

	public double getV(){ return this.V; }

	public double getZ(){ return this.V*this.V/this.S; }

	public double getI(){ return this.S/this.V; }

	public void setS(double S){ this.S = S; }

	public void setV(double V){ this.V = V; }

	public Object clone(){
		return new Base(S,V);
	}
}
