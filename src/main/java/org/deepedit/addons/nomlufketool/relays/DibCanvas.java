package org.deepedit.addons.nomlufketool.relays;

import java.awt.*; 
import java.awt.event.*;

public class DibCanvas extends Canvas implements MouseListener, MouseMotionListener{
    
    int M = 15;
    int inf=60;
    int lat=20;
    int uX, uY;
    private int ancho;
    private int alto;
    
    public int barra;
    
    public double zona1=0,zona2=0,falla=0;
    public String z1="",z2="",f="";
    int zona=0;
    

    public DibCanvas( int anc,int alt , String col, int bar) {
        barra=bar;
        ancho=anc;
        alto=alt;
        this.setForeground(Color.black);
        this.setSize(anc,alt);
        if(col.equals("blue")) setBackground(Color.blue);
        else if(col.equals("green")) setBackground(Color.green);
        else setBackground(Color.white);

        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }
     
    
 //interface MouseListener
    public void mousePressed(MouseEvent ev) {
        uX=ev.getX();
        uY=ev.getY();
    }

    public void mouseExited(MouseEvent event) {
        
    }
    public void mouseReleased(MouseEvent event) {}
    public void mouseClicked(MouseEvent event) {}
    public void mouseEntered(MouseEvent event) {}
 //interface MouseMotionListener
    public void mouseDragged(MouseEvent ev) {
        int x = ev.getX();
        int y = ev.getY();
        Graphics g=getGraphics();
        g.drawLine(uX, uY, x, y);
        uX=x;   uY=y;
        g.dispose();
    }
    public void mouseMoved(MouseEvent event) {}
    
    public void dibujarOvalo() {
        repaint();
    }
    
    public void Dimensiones() {
        ancho=this.getWidth();
        alto=this.getHeight();
        repaint();
    }
    
    public void paint(Graphics g) {
        int tam;
        dibujar(g);
        ejes(g);
        agregar(g);
        agregarD(g);
        agregarA(g);
    }
    
    public static void ejes(int a,int b,Graphics g) {
        g.drawLine(20,b-40, a-20, b-40);
        g.drawLine(a/2,20,a/2,b-40);
    }
    public void ejes(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawLine(lat,alto-inf, ancho-lat, alto-inf);
        g.drawLine(ancho/2,lat,ancho/2,alto-inf);
    }
    
    public void agregar(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawString("Protección de la Barra "+barra,80,alto-30/*,*/);
        g.drawString("X (ohm)", ancho/2+5,20);
        g.drawString("R (ohm)", ancho-60,alto-40);
    }
    
    public void agregarA(Graphics g) {
        int s = 40;
        g.drawString("Zona 1", ancho/2+60,40);
        g.drawString("Zona 2", ancho/2+60,60);
        
        g.setColor(Color.GREEN);
        g.fillRect(ancho/2+70+s,30, 10,10);
        g.setColor(Color.RED);
        g.fillRect(ancho/2+70+s,50, 10,10);
    }
    public void agregarD(Graphics g) {
        int porte=-10;
        g.setColor(Color.BLUE);
        g.drawString(f, (ancho-porte)/2, (alto)-((int) (falla*M))-porte/2-inf);
        g.setColor(Color.BLACK);
        g.drawString(z1, ancho/2-40, alto-((int) (zona1*M))-inf-0);
        g.drawString(z2, ancho/2-40, alto-((int) (zona2*M))-inf-0);
    }
    
    public void dibujar(Graphics g) {
        int porte;
        if(zona==1) {
            porte =(int) (zona1*M);
            
            g.setColor(Color.LIGHT_GRAY);
            g.fillOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            
            g.setColor(Color.GREEN);
            g.drawOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            
            porte =(int) (zona2*M);
            g.setColor(Color.RED);
            g.drawOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            
        } else
        if(zona==2) {
            porte =(int) (zona2*M);
            g.setColor(Color.LIGHT_GRAY);
            g.fillOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            
            g.setColor(Color.RED);
            g.drawOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            
            porte =(int) (zona1*M);
            g.setColor(Color.WHITE);
            g.fillOval((ancho-porte)/2,alto-porte-inf,porte,porte);
            g.setColor(Color.GREEN);
            g.drawOval((ancho-porte)/2,alto-porte-inf,porte,porte);

        }
        porte=9;
        g.setColor(Color.BLUE);
        g.fillOval((ancho-porte)/2, (alto)-((int) (falla*M))-porte/2-inf,porte,porte);
    }
    
    public void CargarDatos(double zona1,double zona2, double falla, String z1, String z2, String f,int zona) {
        this.zona1=zona1;
        this.zona2=zona2;
        this.falla=falla;
        this.z1=z1;
        this.z2=z2;
        this.f=f;
        this.zona=zona;
        repaint();
    }
}
























/*
 * DibCanvas.java
 *
 * Created on 3 de julio de 2005, 03:09 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

/**
 *
 * @author Leonardo
 */
/*
class DibCanvas extends Canvas {
    /*private int ancho;
    private int alto;

    public DibCanvas( int anc,int alt ) {
        ancho = anc;
        alto = alt;
        reshape( 0,0,anc,alt );
    }

    public void paint( Graphics g ) {
        g.setColor( Color.white );
        g.fillRect( 0,0,ancho,alto );
        g.drawLine(60,60,150,150);
        

    }

    public boolean mouseDown( Event evt,int x, int y ) {
        if( x < ancho && y < alto ) {
            System.out.println( "Raton en Canvas: ("+x+","+y+")" );
            return true;
        }
        return false;
    }
     **/
 /*   
    final int MAXPUNTOS=20;
    Point puntos[]=new Point[MAXPUNTOS];
    int nPuntos=0;

  public DibCanvas() {
    setBackground(Color.white);
 }
  
 void dibujaPunto(Point p){
     Graphics g=getGraphics();
     g.setColor(Color.red);
     if(nPuntos<MAXPUNTOS){
        puntos[nPuntos]=p;
        nPuntos++;
     }
     g.fillOval(p.x-4, p.y-4, 8, 8);
     g.dispose();
 }

 public void paint(Graphics g){
     g.setColor(Color.blue);
     for(int i=0; i<nPuntos; i++){
        g.drawOval(puntos[i].x-4, puntos[i].y-4, 8, 8);
    }
 }
}

*/