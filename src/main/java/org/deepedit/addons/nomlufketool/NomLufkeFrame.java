/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         NomLufkeFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke toolbox selector with html preview
 *   
 *   Copyright 1997-2020
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */
package org.deepedit.addons.nomlufketool;


import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.EnumMap;
import java.util.Properties;
import javax.swing.JDialog;
import org.deepedit.addons.nomlufketool.NomLufkeEnums.Language;
import static org.deepedit.addons.nomlufketool.NomLufkeEnums.Language.English;
import static org.deepedit.addons.nomlufketool.NomLufkeEnums.Language.Spanish;
import org.deepedit.addons.nomlufketool.economicdispatch.DespachoEcono;
import org.deepedit.addons.nomlufketool.frequencycontrol.LFCFrame;
import org.deepedit.addons.nomlufketool.gauss_seidel.Gauss_SeidelFrame;
import org.deepedit.addons.nomlufketool.gradient.GradienteFrame;
import org.deepedit.addons.nomlufketool.influencefactors.FactoresFrame;
import org.deepedit.addons.nomlufketool.phasorcalculator.Calc2;
import org.deepedit.addons.nomlufketool.quadripoles.TetrapolosFrame;
import org.deepedit.addons.nomlufketool.radialpuconversor.PU;
import org.deepedit.addons.nomlufketool.relays.RelaysFrame;
import org.deepedit.addons.nomlufketool.transformermodel.Transformadores2;
import org.deepedit.addons.nomlufketool.transientstability.EstabilidadtFrame;
import org.deepedit.addons.nomlufketool.transmissionmodels.LineasTxFrame;
import org.deepedit.addons.nomlufketool.transmissionparameters.PLTFrame;
import org.deepedit.addons.nomlufketool.unbalancedcomponents.FasorFrame;



/**
 * Display the Nom Lufke toolbox selector with html preview.
 * <br>The following apps are currently implemented:
 * <ul>
 * <li>Phasor Calculator</li>
 * <li>Radial p.u. conversor</li>
 * <li>Quadripoles</li>
 * <li>Sync Operation Circle</li>
 * <li>Transmission Parameters</li>
 * <li>Transformer Model</li>
 * <li>Transmission Line Models</li>
 * <li>Gradient</li>
 * <li>Influence Factors</li>
 * <li>Gauss-Seidel</li>
 * <li>Frequency Control</li>
 * <li>Unbalanced Components</li>
 * <li>Relays</li>
 * <li>Transient Stability</li>
 * <li>Economic Dispatch</li>
 * </ul>
 *
 * @author Frank Leanez
 */
public class NomLufkeFrame extends javax.swing.JDialog {

    private final java.util.Map<NomLufkeEnums.App, String> mURLEnglish = new EnumMap<>(NomLufkeEnums.App.class);
    private final java.util.Map<NomLufkeEnums.App, String> mURLSpanish = new EnumMap<>(NomLufkeEnums.App.class);
    private NomLufkeEnums.App[] listKey;
    private NomLufkeEnums.Language language = English;
//    private JList<String> lstToolbox; //<-Seems like diamond operator is not available in 1.6
    private JList lstToolbox;
    private static boolean hasOwnFrame = false;
    
    /**
     * Display the Nom Lufke toolbox selector with html preview
     * <br>This toolbox can be executed in 2 different modes is:
     * <ul>
     * <li>As DeepEdit add-on if executed in nodal mode</li>
     * <li>As independent java app if set in non-modal mode</li>
     * </ul>
     * Thus, the flag modal is important to determine the if some values should
     * be taken from DeepEdit or from defaults
     * This constructor will display the main frame in <b>english</b>
     * @param parent parent class
     * @param modal false as independent window, true as DeepEdit add-on
     */
    public NomLufkeFrame(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        initToolList();
        loadPropertyFiles();
        this.language = English;
        lblMessageText.setText("");
    }
    
    /**
     * Display the Nom Lufke toolbox selector with html preview
     * <br>This toolbox can be executed in 2 different modes is:
     * <ul>
     * <li>As DeepEdit add-on if executed in nodal mode</li>
     * <li>As independent java app if set in non-modal mode</li>
     * </ul>
     * Thus, the flag modal is important to determine the if some values should
     * be taken from DeepEdit or from defaults
     * <br>Curretly supported languages:
     * <ul>
     * <li>English (case sensitive)</li>
     * <li>Spanish (case sensitive)</li>
     * </ul>
     *
     * @param parent Use main DeepEdit frame
     * @param modal if true will be displayed as DeepEdit's dialog
     * @throws IllegalArgumentException - if language string does not map to
     * language enum (check for supported language strings and remember they are
     * case sensitive)
     */
    public NomLufkeFrame(java.awt.Frame parent, boolean modal, String language) {
        super(parent, modal);
        initComponents();
        initToolList();
        loadPropertyFiles();
        this.language = NomLufkeEnums.Language.valueOf(language);
        lblMessageText.setText("");
    }

    private void initToolList() {
        listKey = NomLufkeEnums.App.values();
        String[] listData = NomLufkeLabels.getAllNomLufkeToolNames(language);
        if (listData.length > 0) {
            lstToolbox = new JList(listData);
            pnlToolbox.setViewportView(lstToolbox);
            lstToolbox.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
                @Override
                public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                    selectedOption(evt);
                }
            });
        }
    }

    private void selectPanel(int index) {
        CardLayout card = (CardLayout) pnlPortada.getLayout();
        if (index < listKey.length) {
            NomLufkeEnums.App key = listKey[index];
            if (key != null) {
                card.show(pnlPortada, key.name());
            }
        }
    }

    private void selectedOption(javax.swing.event.ListSelectionEvent evt) {
        lblMessageText.setText("");
        displayPortada();
    }

    private void displayPortada() {
        int index = lstToolbox.getSelectedIndex();
        if (index < listKey.length) {
            NomLufkeEnums.App key = listKey[index];
            String url = getNomLufkeToolUrl(key, language);
            if (url != null) {
                JEditorPane htmlPane = createEditorPane(url);
                pnlScrollPortada.setViewportView(htmlPane);
            }
        }
    }

    /**
     * Retrieves the curretly used (and displayed) language
     *
     * @return the curretly used (and displayed) language
     */
    public String getLanguage() {
        return language.name();
    }

    /**
     * Sets a new curretly used (and displayed) language
     * <br>Currently supported languages:
     * <ul>
     * <li>English (case sensitive)</li>
     * <li>Spanish (case sensitive)</li>
     * </ul>
     *
     * @param language String repre
     * @throws IllegalArgumentException - if this enum type has no constant with
     * the specified name
     */
    public void setLanguage(String language) {
        this.language = NomLufkeEnums.Language.valueOf(language);
    }
    
    /**
     * This flaf will tell if this window has its own frame
     * <bt>Otherwise it was likely launched from DeepEdit
     *
     * @return true if this window has its own frame
     */
    public static boolean hasOwnFrame() {
        return hasOwnFrame;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NomLufkeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NomLufkeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NomLufkeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NomLufkeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                hasOwnFrame = true;
                NomLufkeFrame dialog = new NomLufkeFrame(new javax.swing.JFrame(), false);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }

                    @Override
                    public void windowClosed(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setLocationByPlatform(true);
                dialog.setVisible(true);
            }
        });
    }

    private void showSelectedApp() throws MalformedURLException {
        int index = lstToolbox.getSelectedIndex();
        if (index < listKey.length && index >= 0) {
            NomLufkeEnums.App key = listKey[index];
            if (key != null) {
                switch (key) {
                    case PhasorCalculator:
                        Calc2 c;
                        java.net.URL url = new URL(getNomLufkeToolUrl(key, language));
                        switch (language) {
                            case English:
                                c = new Calc2(1, "Phasor Calculator", url);
                                c.setLocationRelativeTo(this);
                                c.setVisible(true);
                                break;
                            case Spanish:
                                c = new Calc2(0, "Calculadora Fasorial", url);
                                c.setLocationRelativeTo(this);
                                c.setVisible(true);
                                break;
                        }
                        break;
                    case RadialpuConversor:
                        PU p = new PU(this, true);
                        p.setLocationRelativeTo(this);
                        p.setVisible(true);
                        break;
//                        throw new UnsupportedOperationException(key.toString() + " non implemented yet!");
                    case Quadripoles:
                        TetrapolosFrame t = new TetrapolosFrame(this, true);
                        t.setLocationRelativeTo(this);
                        t.setVisible(true);
                        break;
                    case SyncOperationCircle:
                        LineasTxFrame sync = new LineasTxFrame(LineasTxFrame.DISPLAY_CIRCLEDIAGRAM, this, true);
                        sync.setLocationRelativeTo(this);
                        sync.setVisible(true);
                        break;
                    case TransmissionParameters:
                        PLTFrame tp = new PLTFrame(this, true);
                        tp.setLocationRelativeTo(this);
                        tp.setVisible(true);
                        break;
                    case TransformerModel:
                        JDialog trf = Transformadores2.showInFrame(this, true);
                        trf.setLocationRelativeTo(this);
                        trf.setVisible(true);
                        break;
                    case TransmissionLineModels:
                        LineasTxFrame tl = new LineasTxFrame(LineasTxFrame.DISPLAY_PARAMETERS, this, true);
                        tl.setLocationRelativeTo(this);
                        tl.setVisible(true);
                        break;
                    case Gradient:
                        GradienteFrame gf = new GradienteFrame(this, true);
                        gf.setLocationRelativeTo(this);
                        gf.setVisible(true);
                        break;
                    case InfluenceFactors:
                        FactoresFrame ff = new FactoresFrame(this, true);
                        ff.setLocationRelativeTo(this);
                        ff.setVisible(true);
                        break;
                    case Gauss_Seidel:
                        Gauss_SeidelFrame gsf = new Gauss_SeidelFrame(this, true);
                        gsf.setLocationRelativeTo(this);
                        gsf.setVisible(true);
                        break;
                    case FrequencyControl:
                        LFCFrame fc = new LFCFrame(this, true);
                        fc.setLocationRelativeTo(this);
                        fc.setVisible(true);
                        break;
                    case UnbalancedComponents:
                        FasorFrame faf = new FasorFrame(this, true);
                        faf.setLocationRelativeTo(this);
                        faf.setVisible(true);
                        break;
                    case Relays_Example1:
                        RelaysFrame pf1 = new RelaysFrame(RelaysFrame.DISPLAY_EXAMPLE1, this, true);
                        pf1.setLocationRelativeTo(this);
                        pf1.setVisible(true);
                        break;
                    case Relays_Example2:
                        RelaysFrame pf2 = new RelaysFrame(RelaysFrame.DISPLAY_EXAMPLE2, this, true);
                        pf2.setLocationRelativeTo(this);
                        pf2.setVisible(true);
                        break;
                    case TransientStability:
                        EstabilidadtFrame etf = new EstabilidadtFrame(this, true);
                        etf.setLocationRelativeTo(this);
                        etf.setVisible(true);
                        break;
                    case EconomicDispatch:
                        JDialog deco = DespachoEcono.showInFrame(this, true);
                        deco.setLocationRelativeTo(this);
                        deco.setVisible(true);
                        break;
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblToolbox = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnlExecute = new javax.swing.JPanel();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        lblMessageText = new javax.swing.JLabel();
        pnlDescription = new javax.swing.JScrollPane();
        pnlPortada = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        pnlScrollPortada = new javax.swing.JScrollPane();
        pnlToolbox = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nom Lufke Toolbox");

        lblToolbox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblToolbox.setText("Toolboxes");
        lblToolbox.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Description and Credits");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        btnCancel.setText("Exit");
        btnCancel.setToolTipText("Exit Toolbox");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnOk.setText("Show");
        btnOk.setToolTipText("Display Selected Toolbox");
        btnOk.setMaximumSize(null);
        btnOk.setMinimumSize(null);
        btnOk.setPreferredSize(null);
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        lblMessageText.setForeground(java.awt.Color.blue);
        lblMessageText.setText("THIS IS THE MESSAGE TEXT");
        lblMessageText.setName(""); // NOI18N

        javax.swing.GroupLayout pnlExecuteLayout = new javax.swing.GroupLayout(pnlExecute);
        pnlExecute.setLayout(pnlExecuteLayout);
        pnlExecuteLayout.setHorizontalGroup(
            pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlExecuteLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lblMessageText, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );
        pnlExecuteLayout.setVerticalGroup(
            pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExecuteLayout.createSequentialGroup()
                .addGroup(pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
            .addComponent(lblMessageText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pnlPortada.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlScrollPortada, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlScrollPortada, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
        );

        pnlPortada.add(jPanel2, "card2");

        pnlDescription.setViewportView(pnlPortada);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblToolbox, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                    .addComponent(pnlToolbox))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDescription)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5))
            .addComponent(pnlExecute, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblToolbox, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDescription)
                    .addComponent(pnlToolbox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlExecute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        try {
            showSelectedApp();
        } catch (MalformedURLException e) {
            String key = lstToolbox.getSelectedValue().toString();
            showErrorMessage("Error finding related webpage toolbox " + key, e);
        } catch (UnsupportedOperationException e) {
            showErrorMessage(e.getMessage(), e);
        }
    }//GEN-LAST:event_btnOkActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblMessageText;
    private javax.swing.JLabel lblToolbox;
    private javax.swing.JScrollPane pnlDescription;
    private javax.swing.JPanel pnlExecute;
    private javax.swing.JPanel pnlPortada;
    private javax.swing.JScrollPane pnlScrollPortada;
    private javax.swing.JScrollPane pnlToolbox;
    // End of variables declaration//GEN-END:variables

    private JEditorPane createEditorPane(String name) {
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
//	editorPane.addHyperlinkListener(createHyperLinkListener());
        try {
            URL helpURL = new URL(name);
            editorPane.setPage(helpURL);
        } catch (IOException e) {
            showErrorMessage("Can't find the intro for the given toolbox: " + name, e);
        }
        return editorPane;
    }

    private void showErrorMessage(String err, Exception e) {
        lblMessageText.setText(err);
        lblMessageText.setToolTipText(err);
        JOptionPane.showMessageDialog(this, err, "Error executing application", JOptionPane.ERROR_MESSAGE);
        e.printStackTrace(System.err);
    }
    
    /**
     * Function to retrieve label for the given key and selected language
     * <br>Keys are currently hardwired to this class code
     * <br>To retrieve all keys use the function getAllNomLufkeLabels(English)
     * <br>Values in English are the same as keys
     *
     * @param key String representation of the key (in English)
     * @param language Language constant to represent language
     * @return null if key is not found in given language or there was a problem
     * initializing keys
     */
    public String getNomLufkeToolUrl(NomLufkeEnums.App key, Language language) {
        switch (language) {
            case English:
                return mURLEnglish.get(key);
            case Spanish:
                return mURLSpanish.get(key);
        }
        return null;
    }
    
    private void loadPropertyFiles ()  {
        try {
            Properties pURLEnglish = loadURLPropertyFile(English);
            NomLufkeEnums.App[] apps = NomLufkeEnums.App.values();
            for (NomLufkeEnums.App a : apps) {
                mURLEnglish.put(a, pURLEnglish.getProperty(a.name(), a.toString()));
            }
            Properties pURLSpanish = loadURLPropertyFile(Spanish);
            for (NomLufkeEnums.App a : apps) {
                mURLSpanish.put(a, pURLSpanish.getProperty(a.name(), a.toString()));
            }
        } catch (IOException e) {
            showErrorMessage("Couldn't load language URL defaults. No previews available for toolbox", e);
        }
    }
    
    private Properties loadURLPropertyFile (Language language) throws IOException {
        String sDeepEditDir = System.getProperty("user.home") + File.separator + ".deepedit";
        File f = new File(sDeepEditDir + File.separator + getURLPropertiesFileName(language));
        if (f.exists()) {
            FileInputStream in = new FileInputStream(f);
            Properties usr = new Properties();
            usr.load(in);
            in.close();
            return usr;
        }
        //Load the default properties file from resources:
        InputStream in = getClass().getResourceAsStream("/" + getURLPropertiesFileName(language));
        if (in == null) {
            throw new IOException("Couldn't load language URL defaults. No previews available for toolbox");
        }
        Properties def = new Properties();
        def.load(in);
        in.close();
        try {
            savePropertyFile (def, f);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return def;
    }
    
    private void savePropertyFile (Properties p, File f) throws IOException {
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdir();
        }
        FileOutputStream fo = new FileOutputStream(f.getAbsoluteFile());
        p.store(fo, "---DEEP-EDITOR NOM-LUKE TOOLBOX CONFIG FILE---");
        fo.close();
    }
    
    private static String getURLPropertiesFileName (Language language) {
        switch (language) {
            case English:
                return "address_eng.properties";
            case Spanish:
                return "address_esp.properties";
            default:
                return "address_eng.properties";
        }
    }

}
