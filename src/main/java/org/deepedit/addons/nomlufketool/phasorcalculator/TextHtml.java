package org.deepedit.addons.nomlufketool.phasorcalculator;

/* * Swing Version */ 
import javax.swing.*;
import java.awt.*;              //for layout managers
import java.awt.event.*;        //for action and window events
import java.net.URL;
import java.io.IOException;
import javax.swing.event.*;
import javax.swing.text.html.*;

public class TextHtml extends JFrame implements ActionListener  {
	private String newline = "\n";
	protected static final String textFieldString = "JTextField";
	protected static final String passwordFieldString = "JPasswordField";
	protected JLabel actionLabel;
    private JPanel contentPane;
	JEditorPane editorPane;
	String extension = ".html";

	public TextHtml(String text_html, String s_titulo,URL dirURL)  {

		setTitle(s_titulo);
		setResizable(false);
		//Create an editor pane.
		editorPane = createEditorPane(text_html,dirURL);

		JScrollPane editorScrollPane = new JScrollPane(editorPane);
		editorScrollPane.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(600, 400));
		editorScrollPane.setMinimumSize(new Dimension(590,390));
		//Create a text pane.

		JScrollPane paneScrollPane = new JScrollPane();
		//Put the editor pane and the text pane in a split pane.

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,  editorScrollPane,paneScrollPane);
		splitPane.setOneTouchExpandable(false);
		JPanel rightPane = new JPanel();
		rightPane.add(splitPane);
		rightPane.setBorder(BorderFactory.createCompoundBorder( BorderFactory.createTitledBorder(""), BorderFactory.createEmptyBorder(5,5,5,5)));
		//Put everything in the applet.
		JPanel leftPane = new JPanel();
		BoxLayout leftBox = new BoxLayout(leftPane, BoxLayout.Y_AXIS);
		leftPane.setLayout(leftBox);
		contentPane = new JPanel();
		BoxLayout box = new BoxLayout(contentPane, BoxLayout.X_AXIS);
		contentPane.setLayout(box);
		contentPane.add(leftPane);
		contentPane.add(rightPane);
		setContentPane(contentPane);
		pack();
	}


	public void actionPerformed(ActionEvent e)  {
		String prefix = "You typed \"";

		if (e.getActionCommand().equals(textFieldString))  {
			JTextField source = (JTextField)e.getSource();
			actionLabel.setText(prefix + source.getText() + "\"");
		}


		else  {
			JPasswordField source = (JPasswordField)e.getSource();
			actionLabel.setText(prefix + new String(source.getPassword()) + "\"");
		}

	}


	private JEditorPane createEditorPane(String name,URL dirURL)  {
		JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(false);
		editorPane.addHyperlinkListener(createHyperLinkListener());
		String s = null;

		try  {
			//s = "file:" + System.getProperty("user.dir") + System.getProperty("file.separator") + name + extension;
			s=name;
			System.out.println("Conectando a directorio "+dirURL+" ; archivo: " +s);
			URL helpURL = new URL(dirURL,s);
			displayURL(helpURL, editorPane);
		}


		catch (Exception e)  {
			System.err.println("No se puede crear URL auxiliar: " + s);
		}

		
		return editorPane;
	}


	private void displayURL(URL url, JEditorPane editorPane)  {

		try  {
			editorPane.setPage(url);
		}


		catch (IOException e)  {
			System.err.println("Attempted to read a bad URL: " + url);
		}

	}


	public HyperlinkListener createHyperLinkListener()  {

		return new HyperlinkListener()  {

			public void hyperlinkUpdate(HyperlinkEvent e)  {

				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)  {

					if (e instanceof HTMLFrameHyperlinkEvent)  {
						((HTMLDocument)editorPane.getDocument()).processHTMLFrameHyperlinkEvent( (HTMLFrameHyperlinkEvent)e);
					}


					else  {

						URL utemp;
						try  {
							utemp=e.getURL();
							try{
								editorPane.setPage(utemp);
							}
							catch (IOException ioe)  {
								System.out.println("IOE: " + ioe);
							}
							catch (Exception ex)  {
								System.out.println("*Error al accesar "+utemp);
							}
						}
						catch (Exception ex2) {
							System.out.println("*Error al accesar "+e);
						}
					}

				}

			}

		};

	}

}

