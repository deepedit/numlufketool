package org.deepedit.addons.nomlufketool.transformermodel;

public class Trafo3f{
	/** tipo de conexion: Yy, Yd, Dy o Dd */
	private Connection c;
	/** desfase: 0 a 11 (que significa 0° a 330°) */
	private int desfase;
	/** el trafo monofasico que forma parte del banco*/
	private Trafo trf;
	/** bases trifasicas del primario y secundario */
	private Base3f bp, bs;

	/** Crea un nuevo transformador trifasico Y-Y ideal (1:1) */
	public Trafo3f(){
		c = Connection.Yy;
		desfase = 0;
		trf = new Trafo();
	}

	/** Crea un nuevo transformador trifasico, de un tipo de conexion
	 * dado.
	 * <p>
	 * @param c	Tipo de conexion (clase {@link Connection})
	 * @param desfase	Angulo de desfase en multiplos de 30°
	 */
	public Trafo3f(Connection c, int desfase){
		this.c = c;
		this.desfase = desfase % 12;
		this.trf = new Trafo();
	}


	/** Crea un transformador trifasico a partir de un banco de 3
	 * transformadores monofasicos, conectados como lo indica la
	 * conexion y el desfase.
	 * <p>
	 * @param c	Tipo de conexion (clase {@link Connection})
	 * @param desfase	Angulo de desfase en multiplos de 30°
	 * @param trf	Transformador que se ocupara para construir el banco
	 */
	public Trafo3f(Connection c, int desfase, Trafo trf){
		this.c = c;
		this.desfase = desfase;	
		setTrafo(trf);
	}

	/** Indica el transformador que se utilizara en el banco de
	 * transformadores trifasicos.
	 * <p>
	 * @param trf	Transformador monofasico de la clase {@link Trafo}
	 */
	public void setTrafo(Trafo trf){
		this.trf = (Trafo)trf.clone();
		/* dependiendo de la conexion, hay que cambiar las
		 * bases del primario y secundario */
		double Vb1=1, Vb2=1, S;
		/* potencia trifasica */
		S = this.trf.getBasePrim().getS()*3.0;
		if(this.c == Connection.Yy || this.c == Connection.Yd){
			/* primario en Y: voltajes de f-n -> f-f */
			Vb1 = this.trf.getBasePrim().getV() * Math.sqrt(3);
		}
		else if(this.c == Connection.Dy || this.c == Connection.Dd){
			/* si esta en delta, ya sabemos que estan en f-f */
			Vb1 = this.trf.getBasePrim().getV();
		}
		if(this.c == Connection.Yy || this.c == Connection.Dy){
			/* secundario en Y */
			Vb2 = this.trf.getBaseSec().getV() * Math.sqrt(3);
		}
		else if(this.c == Connection.Yd || this.c == Connection.Dd){
			/* ya esta en delta */	
			Vb2 = this.trf.getBaseSec().getV();
		}
		bp = new Base3f(S,Vb1);
		bs = new Base3f(S,Vb2);
	}

	/** Obtiene el transformador utilizado actualmente
	 * @return El transformador como elemento de la clase {@link Trafo}
	 */
	public Trafo getTrafo(){
		return (Trafo)trf.clone();
	}

	/** Indica el tipo de conexion.
	 * <p>
	 * @param c	Tipo de conexion (clase {@link Connection})
	 */
	public void setConnection(Connection c){
		this.c = c;
		setTrafo(this.trf);
	}

	/** Retorna el tipo de conexion actualmente utilizado
	 * @return Tipo de conexion (clase {@link Connection})
	 */
	public Connection getConnection(){
		return c;
	}

	/** Retorna la base del lado primario
	 * @return Un objeto del tipo {@link Base3f}
	 */
	public Base3f getBasePrim(){
		return (Base3f)bp.clone();
	}

	/** Retorna la base del lado secundario
	 * @return Un objeto del tipo {@link Base3f}
	 */
	public Base3f getBaseSec(){
		return (Base3f)bs.clone();
	}

	/** Indica las bases a utilizar en el trafo trifasico
	 * y a la vez modifica el trafo monofasico tal que el banco
	 * cumpla con las condiciones.
	 * <p>
	 * @param bp	Base del lado AT
	 * @param bs	Base del lado BT
	 */
	public void setBases(Base3f bp, Base3f bs) throws TrafoException{
		this.bp = (Base3f)bp.clone();
		this.bs = (Base3f)bs.clone();
	
		/* parametros a llenar del monofasico */
		double Vb1=1, Vb2=1, S;
		if(this.c == Connection.Yy || this.c == Connection.Yd){
			Vb1 = this.bp.getV() / Math.sqrt(3);
		}
		else if(this.c == Connection.Dy || this.c == Connection.Dd){
			Vb1 = this.bp.getV();
		}
		if(this.c == Connection.Yy || this.c == Connection.Dy){
			Vb2 = this.bs.getV() / Math.sqrt(3);
		}
		else if(this.c == Connection.Yd || this.c == Connection.Dd){
			Vb2 = this.bs.getV();
		}
		S = this.bs.getS() / 3.0;

		this.trf = new Trafo();
		this.trf.setBases(new Base(S,Vb1), new Base(S,Vb2));
	}

	/** Obtiene el modelo unilineal del transformador trifasico, en pu.
	 * <p>
	 * @return	Un objeto del tipo {@link Trafo} que contiene los
	 *		valores en pu del modelo unilineal
	 */
	public Trafo getPU() throws TrafoException{
		/* depende del tipo de conexion */
		/* FALSO!!! no depende del tipo de conexion, ya estamos en pu */
//		if(c == Connection.Yy || c == Connection.Yd){ 
			/* nada que hacer */
			return trf.getPU();
//		}
/*
		if(c == Connection.Dy || c == Connection.Dd){
			// hay que transformar impedancias de delta a estrella
			Trafo t1 = trf.getPU();
			double nx0, nr0, nx1, nr1, nx2, nr2;
			nx0 = t1.getX0() / 3.0;
			nr0 = t1.getR0() / 3.0;
			nx1 = t1.getX1() / 3.0;
			nr1 = t1.getR1() / 3.0;
			nx2 = t1.getX2() / 3.0;
			nr2 = t1.getR2() / 3.0;
			return new Trafo(1,nx0,nr0,nx1,nr1,nx2,nr2,new Base(1,1),new Base(1,1));
		}
		return null;
		*/
	}

	/**
	 * Calcula parametros x1, x2, r1 y r2 a partir de la prueba de
	 * cortocircuito en el lado BT (AT cortocircuitado)
	 * <p>
	 * @param V	Voltaje entre fases [V]
	 * @param I	Corriente de linea [A]
	 * @param P	Potencia trifasica [W]
	 */
	public void setFromSCSec(double V, double I, double P){
		double pn, in=0, vn=1;
		pn = P/3.0; /* potencia monofasica */
		if( c == Connection.Yy || c == Connection.Dy ){
			/* secundario en Y */
			in = I;
			vn = V / Math.sqrt(3);
		}
		else if( c == Connection.Yd || c == Connection.Dd ){
			/* secundario en D */
			in = I / Math.sqrt(3);
			vn = V;
		}
		trf.setFromSCSec(vn,in,pn);
	}
	
	/**
	 * Calcula parametros x1, x2, r1 y r2 a partir de la prueba de
	 * cortocircuito en el lado AT (BT cortocircuitado)
	 * <p>
	 * @param V	Voltaje entre fases [V]
	 * @param I	Corriente de linea [A]
	 * @param P	Potencia trifasica [W]
	 */
	public void setFromSCPrim(double V, double I, double P){
		double pn, in=0, vn=1;
		pn = P/3.0; /* potencia monofasica */
		if( c == Connection.Yy || c == Connection.Yd ){
			/* primario en Y */
			in = I;
			vn = V / Math.sqrt(3);
		}
		else if( c == Connection.Dy || c == Connection.Dd ){
			/* primario en D */
			in = I / Math.sqrt(3);
			vn = V;
		}
		trf.setFromSCPrim(vn,in,pn);
	}

	/**
	 * Calcula parametros x0 y r0 a partir de la prueba de
	 * circuito abierto en el lado AT (BT abierto)
	 * <p>
	 * @param V	Voltaje entre fases [V]
	 * @param I	Corriente de linea [A]
	 * @param P	Potencia trifasica [W]
	 */
	public void setFromOCPrim(double V, double I, double P){
		double pn, in=0, vn=1;
		pn = P/3.0; /* potencia monofasica */
		if( c == Connection.Yy || c == Connection.Yd ){
			/* primario en Y */
			in = I;
			vn = V / Math.sqrt(3);
		}
		else if( c == Connection.Dy || c == Connection.Dd ){
			/* primario en D */
			in = I / Math.sqrt(3);
			vn = V;
		}
		trf.setFromOCPrim(vn,in,pn);
	}
	
	/**
	 * Calcula parametros x0 y r0 a partir de la prueba de
	 * circuito abierto en el lado BT (AT abierto)
	 * <p>
	 * @param V	Voltaje entre fases [V]
	 * @param I	Corriente de linea [A]
	 * @param P	Potencia trifasica [W]
	 */
	public void setFromOCSec(double V, double I, double P){
		double pn, in=0, vn=1;
		pn = P/3.0; /* potencia monofasica */
		if( c == Connection.Yy || c == Connection.Dy ){
			/* secundario en Y */
			in = I;
			vn = V / Math.sqrt(3);
		}
		else if( c == Connection.Yd || c == Connection.Dd ){
			/* secundario en D */
			in = I / Math.sqrt(3);
			vn = V;
		}
		trf.setFromOCSec(vn,in,pn);
	}
		
}
