package org.deepedit.addons.nomlufketool.unbalancedcomponents;

/* 
 * Swing 1.1 version (compatible with both JDK 1.1 and Java 2).
 */
import javax.swing.border.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;   
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.net.URL;


public class appletInicio extends JApplet
                        implements ActionListener {
    protected JButton b1, b2,b6,b7,b8,b9;
    public JRadioButton b3,b4,b5;
    public JTextField MyTextField1 = null;   
    public JTextField MyTextField2 = null;   
    public JTextField MyTextField3 = null;   
    public JTextField MyTextField4 = null;  
    String label;
    public c_012 myc_012;
    public Abc myAbc;
    public cAbc mycAbc;
    public c0ab myc0ab;
    public p0dq myp0dq;
    public pAbc mypAbc;
    
//    public CompSim myCompSim;
    private boolean inAnApplet = true;
    public int tipo_idioma, sel;
    protected Font TextDialogFont;   
    JRadioButton esp, eng, ger;
    JPanel pa,pb,pc,pd;
    JComboBox myChoice;
    
    boolean alter1, alter2; 

    JRadioButton alter1Checkbox;
    JRadioButton alter2Checkbox;
    // Fasor myfasor;
    // FasorABC myfasorABC;
    TextHtml html;
    
    // Choice myChoice;

    URL codeBase; //used for applet version only

    //Hack to avoid ugly message about system event access check.
    
    public appletInicio() {
        this(true);
    }
    public appletInicio(boolean inAnApplet) {
        this.inAnApplet = inAnApplet;
        if (inAnApplet) {
            getRootPane().putClientProperty("defeatSystemEventQueueCheck",
                                            Boolean.TRUE);
        }
    }

    public void init() {
        setContentPane(makeContentPane());
    }

    public Container makeContentPane() {

	BorderLayout border1 = new BorderLayout();	
	JPanel p = new JPanel();
	p.setLayout(border1);
	pa = botones();
	pb = idiomas();
	pc = relleno1();
	pd = relleno2();
	p.add("North",pa);
	p.add("Center",pb);
	p.add("West",pc);
	p.add("East",pd);	
        return p;
    }
    
    
    public JPanel relleno1(){
	JPanel p = new JPanel();
	p.setPreferredSize(new Dimension(80,70));
	return p;
    }
    public JPanel relleno2(){
	JPanel p = new JPanel();
	p.setPreferredSize(new Dimension(80,70));
	return p;
    }

    public JPanel botones() {
	BorderLayout border1 = new BorderLayout();	
	JPanel p = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();  
	GridBagConstraints c = new GridBagConstraints();  
	Border buttonBorder = new TitledBorder(null, "Calculo de Componentes", 
					       TitledBorder.LEFT, TitledBorder.TOP,
					       TextDialogFont);    
	Border emptyBorder = new EmptyBorder(5,5,5,5);
	Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);
	p.setBorder(compoundBorder);
	p.setPreferredSize(new Dimension(600,120));
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);        
	c.fill = GridBagConstraints.VERTICAL;
	p.setLayout(new GridLayout(3,2));
	
	b1 = new JButton("Componentes ABC");
	b1.setMnemonic(KeyEvent.VK_D);
        b1.setActionCommand("abc");
	b1.addActionListener(this);   
	
	b2 = new JButton("Componentes 012");
	b2.setMnemonic(KeyEvent.VK_D);
        b2.setActionCommand("c_012");
	b2.addActionListener(this);   

        b6 = new JButton("Comp. de Clarke (ABC) ");
	b6.setMnemonic(KeyEvent.VK_D);
        b6.setActionCommand("c_cabc");
	b6.addActionListener(this);   
        
        b7 = new JButton("Comp. de Clarke (0 alfa beta)");
	b7.setMnemonic(KeyEvent.VK_D);
        b7.setActionCommand("c_c0ab");
	b7.addActionListener(this);   

        b8 = new JButton("Comp. de Park-Blondel (ABC)");
	b8.setMnemonic(KeyEvent.VK_D);
        b8.setActionCommand("c_pabc");
	b8.addActionListener(this);   
        
        b9 = new JButton("Comp. de Park-Blondel (0 d q)");
	b9.setMnemonic(KeyEvent.VK_D);
        b9.setActionCommand("c_p0dq");
	b9.addActionListener(this); 
        
	p.add(b1);
	p.add(b2);
        p.add(b6);
        p.add(b7);
        p.add(b8);
        p.add(b9);
	b1.setToolTipText("C�lculo de componentes sim�tricas a partir de las componentes reales ABC.");
        b2.setToolTipText("C�lculo de componentes ABC a trav�s de las componentes sim�tricas.");
        b6.setToolTipText("C�lculo de componentes cero, alfa y beta a partir de las componentes ABC.");
        b7.setToolTipText("C�lculo de componentes ABC a trav�s de las componentes cero, alfa y beta.");
        b8.setToolTipText("C�lculo de componentes cero, d y q a partir de las componentes ABC.");
        b9.setToolTipText("C�lculo de componentes de ABC a trav�s de las componentes cero, d y q.");
	return p;
    }

   public JPanel idiomas() {
       
       JPanel p = new JPanel();
       GridBagLayout gridbag = new GridBagLayout();  
       GridBagConstraints c = new GridBagConstraints();  
       Border buttonBorder = new TitledBorder(null, "Selecci�n de Idioma", 
					      TitledBorder.LEFT, TitledBorder.TOP,
					      TextDialogFont);    
       Border emptyBorder = new EmptyBorder(5,5,5,5);
       Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);
       p.setBorder(compoundBorder);
       p.setPreferredSize(new Dimension(40,70));
       BorderLayout borderl = new BorderLayout();
       getContentPane().setLayout(borderl);        
       c.fill = GridBagConstraints.VERTICAL;
       p.setLayout(new GridLayout(1,1));

     // Create combo box 

       String[] idioma = { "Espa�ol", "English"/*, "German"*/ };
       myChoice = new JComboBox(idioma);
       myChoice.setPreferredSize(new Dimension(10,10));
       p.add(myChoice);
       return p;

    }

    public void actionPerformed(ActionEvent ev) {
	Object obj = ev.getSource();  
	String label = ev.getActionCommand();
	sel = myChoice.getSelectedIndex();
	
	if (label.equals("c_012")){
	    if (myc_012 == null){
		myc_012= new c_012(sel, gettitulo());
		return;	
	    }
	    
	    else if (myc_012 != null){
		int opcion =  myc_012.clave;
	    
		if(opcion == sel){	
		    myc_012.setVisible(true);
		}
		else if (opcion != sel){
		    myc_012.dispose();
		    myc_012 = new c_012(sel, gettitulo());
		}
	    }
	}
        
	if(label.equals("abc")){
	
	    if (myAbc == null){
		myAbc= new Abc(sel, gettitulo());	
		return;
	    }
 
	   else if (myAbc != null){
		int opcion =  myAbc.clave;
	    
		if(opcion == sel){	
		    myAbc.setVisible(true);
		}
		else if (opcion != sel){
		    myAbc.dispose();
		    myAbc = new Abc(sel, gettitulo());
		}
	    }
	}
        
        if (label.equals("c_cabc")){
	    if (mycAbc == null){
		mycAbc= new cAbc(sel, gettitulo());
		return;	
	    }
	    
	    else if (mycAbc != null){
		int opcion =  mycAbc.clave;
	    
		if(opcion == sel){	
		    mycAbc.setVisible(true);
		}
		else if (opcion != sel){
		    mycAbc.dispose();
		    mycAbc = new cAbc(sel, gettitulo());
		}
	    }
	}
         if (label.equals("c_pabc")){
	    if (mypAbc == null){
		mypAbc= new pAbc(sel, gettitulo());
		return;	
	    }
	    
	    else if (mypAbc != null){
		int opcion =  mypAbc.clave;
	    
		if(opcion == sel){	
		    mypAbc.setVisible(true);
		}
		else if (opcion != sel){
		    mypAbc.dispose();
		    mypAbc = new pAbc(sel, gettitulo());
		}
	    }
	}
        
         if (label.equals("c_c0ab")){
	    if (myc0ab == null){
		myc0ab= new c0ab(sel, gettitulo());
		return;	
	    }
	    
	    else if (myc0ab != null){
		int opcion =  myc0ab.clave;
	    
		if(opcion == sel){	
		    myc0ab.setVisible(true);
		}
		else if (opcion != sel){
		    myc0ab.dispose();
		    myc0ab = new c0ab(sel, gettitulo());
		}
	    }
	}
        
            if (label.equals("c_p0dq")){
	    if (myp0dq == null){
		myp0dq= new p0dq(sel, gettitulo());
		return;	
	    }
	    
	    else if (myp0dq != null){
		int opcion =  myp0dq.clave;
	    
		if(opcion == sel){	
		    myp0dq.setVisible(true);
		}
		else if (opcion != sel){
		    myp0dq.dispose();
		    myp0dq = new p0dq(sel, gettitulo());
		}
	    }
	}
        
    
    }
	    
    /* One day, JApplet will make this method obsolete. */
    protected URL getURL(String filename) {
        URL url = null;
        if (codeBase == null) {
            codeBase = getCodeBase();
        }

        try {	
            url = new URL(codeBase, filename);
        } catch (java.net.MalformedURLException e) {
            System.out.println("Couldn't create image: badly specified URL");
            return null;
        }

        return url;
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Application version: AppletDemo");

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        appletInicio applet = new appletInicio(false);
        frame.setContentPane(applet.makeContentPane());
        frame.pack();
        frame.setVisible(true);
    }
    
    public String gettitulo() {
    
    if(sel == 0){
    	String titulo = "Componentes Sim�tricas";
    	return titulo;
    	
    	}	
    	else if (sel == 1){
  		String titulo = "Simetrics Components";
    	return titulo;	
  	
    	}
    	
     else {
     	String titulo = "Componetes Sim�tricas GERMAN";
    	return titulo;	
     }
    }
}
