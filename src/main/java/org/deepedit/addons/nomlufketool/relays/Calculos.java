package org.deepedit.addons.nomlufketool.relays;

/*
 * Calculos.java
 *
 * Created on 13 de junio de 2005, 08:28 AM
 */

/**
 *
 * @author  Leonardo
 */
public class Calculos {
    
    /**************************INICIALIZACI�N DE VARIABLES**************************/
    
    //tabla con los voltajes y corrientes para un cortocircuito en la l�nea 5-8
    private final double[] x = {0,10,80,95,100};
    private final double[] U4v = {0.4965,0.5267,0.6481,0.6586,0.6607};
    private final double[] U5v = {0,0.0642,0.3411,0.3746,0.3841};
    private final double[] U8v = {0.5463,0.5229,0.2040,0.0606,0};
    private final double[] U7v = {0.6630,0.6567,0.5134,0.4430,0.4090};
    private final double[] I45v = {6.8954,6.4230,4.2639,3.9436,3.8415};
    private final double[] I8xv = {5.4627,5.8096,10.1993,12.1220,12.9321};
    private final double[] I78v = {0.6865,0.7871,1.8203,2.2337,2.4058};
    private final double[] Iccxv = {12.3581,12.2325,14.4632,16.0655,16.7736};
       
    //Voltajes y corrientes para una falla en cualquier punto de la l�nea 5-8
    public Funcion U4 = new Funcion(U4v,x);
    public Funcion U5 = new Funcion(U5v,x);
    public Funcion U8 = new Funcion(U8v,x);
    public Funcion U7 = new Funcion(U7v,x);
    public Funcion I45 = new Funcion(I45v,x);
    public Funcion I8x = new Funcion(I8xv,x);
    public Funcion I78 = new Funcion(I78v,x);
    public Funcion Iccx = new Funcion(Iccxv,x);
    
    //Impedancias reales de las l�neas 5-8, 4-5 y 7-8
    private double z58;
    private double z45;
    private double z87;
    
    //Razones de transformaci�n de los transformadores de medici�n
    private double rv8, rv5, ri5, ri8;
    
    //Reglas para establecimiento de las protecciones
    private double porc1Z1;
    private double porc1Z2,porc2Z2;
    
    //Zonas de cada protecci�n para las barras 5 y 8
    private double z5z1,z5z2,z8z1,z8z2;
    
    //Valores base
    private double Sbase,Vbase,Ibase,Zbase;
    
    //Impedancias medidas en las barras 5 y 8 cuando se produce una falla
    private double z5vs1,z8vs1;
    
    //lugar de la falla
    private double l58;
    
    //zona en que act�a la protecci�n
    private int zona5,zona8;
    
    //MAXIMOS Y M�NIMOS DE Z5 Y Z8
    private double MIN_Z5,MIN_Z8,MAX_Z5,MAX_Z8;
    
    
    /***********************************CONSTRUCTOR******************************************/
    public Calculos() {
        Sbase = 100;
        Vbase = 220;
        Ibase = IBASE(Sbase, Vbase);
        Zbase = ZBASE(Sbase, Vbase);
        
        //impedancias reales de lineas 5-8, 4-5 y 8-7
        z58 = 48.4;
        z45 = 34.848;
        z87 = 82.28;
        
        predeterminado();
        
    }
    
    
    //Valores por defecto de todos los par�metros
    public void predeterminado() {
        
        
        //relaciones de transformaci�n de los TT y TI
        rv5 = 2000;ri5 = 400;
        rv8 = 2000;ri8 = 500;
        
        //reglas para accionar las protecciones en zona 1 y 2
        porc1Z1 = 0.85;
        porc1Z2 = 1;porc2Z2 = 0.3;
        
        //impedancias de ajuste de las barras 5 y 8
        improt();
        
        l58 = 10;
        
        ZVS(l58);
        
        zona();
    }
    
    /***************************CLASES P�BLICAS*********************************/
    
    
    public int getZona(int barra) {
        if(barra==5) return zona5;
        if(barra==8) return zona8;
        return 0;
    }
    
    public void lugar(double l) {
        if(l>=0&&l<=100) {
            l58=l;
            ZVS(l58);zona();
        }
        
    }
    
    public String get(String q, int d) {
        if(q.equals("x"))       return aString(l58,     0);
        if(q.equals("z5"))      return aString(z5vs1,   d);
        if(q.equals("z8"))      return aString(z8vs1,   d);
        if(q.equals("z5z1"))    return aString(z5z1,    d);
        if(q.equals("z8z1"))    return aString(z8z1,    d);
        if(q.equals("z5z2"))    return aString(z5z2,    d);
        if(q.equals("z8z2"))    return aString(z8z2,    d);
        if(q.equals("rv5"))     return aString(rv5  ,   d);
        if(q.equals("rv8"))     return aString(rv8  ,   d);
        if(q.equals("ri5"))     return aString(ri5  ,   d);
        if(q.equals("ri8"))     return aString(ri8  ,   d);
        return "";
    }
    
    public double getZ(String q) {
        if(q.equals("z5"))      return z5vs1;
        if(q.equals("z8"))      return z8vs1;
        if(q.equals("z5z1"))    return z5z1;
        if(q.equals("z8z1"))    return z8z1;
        if(q.equals("z5z2"))    return z5z2;
        if(q.equals("z8z2"))    return z8z2;
        return 0;
    }
    
    public void set(String q, double t) {
        if(q.equals("rv5")) rv5=t;
        if(q.equals("rv8")) rv8=t;
        if(q.equals("ri5")) ri5=t;
        if(q.equals("ri8")) ri8=t;
        improt();ZVS(l58);zona();
    }
    
    
    
    public double inversaZVS5(double Z,double error) {
        double a = 0;
        
        if(Math.abs(Z-ZVS5(a))<=error)  return a;
        double b = 100;
        if(Math.abs(Z-ZVS5(b))<=error)  return b;
        double aux;
        double auxA = Z-ZVS5(a);
        double auxB = Z-ZVS5(b);
        while(true) {
            aux = a;
            a = b-auxB*(b-a)/(auxB-auxA);
            b = aux;
            auxA = Z-ZVS5(a);
            if(auxA<=error)  return a;
            auxB = Z-ZVS5(b);
        }
    }
    
    public double inversaZVS8(double Z,double error) {
        double a = 0;
        
        if(Math.abs(Z-ZVS8(a))<=error)  return a;
        double b = 100;
        if(Math.abs(Z-ZVS8(b))<=error)  return b;
        double aux;
        double auxA = Z-ZVS8(a);
        double auxB = Z-ZVS8(b);
        while(true) {
            aux = a;
            a = b-auxB*(b-a)/(auxB-auxA);
            b = aux;
            auxA = Z-ZVS8(a);
            if(auxA<=error)  return a;
            auxB = Z-ZVS8(b);
        }
    }
    
    public void MAXIMOS(String q) {
        double aux_min,aux_max;
        if(q.equals("z5")) {
            aux_min=ZVS5(0);
            aux_max=ZVS5(100);
            MIN_Z5 = Math.min(aux_min,aux_max);
            MAX_Z5 = Math.max(aux_min,aux_max);
        }
        if(q.equals("z8")) {
            aux_min=ZVS8(0);
            aux_max=ZVS8(100);
            MIN_Z8 = Math.min(aux_min,aux_max);
            MAX_Z8 = Math.max(aux_min,aux_max);
        }
    }
    
    public double getLim(String q,String M) {
        if(q.equals("z5")) {
            if(M.equals("min")) {
                return MIN_Z5;
            }
            if(M.equals("max")) {
                return MAX_Z5;
            }
        }
        if(q.equals("z8")) {
            if(M.equals("min")) {
                return MIN_Z8;
            }
            if(M.equals("max")) {
                return MAX_Z8;
            }
        }
        return -1;
    } 
    
    //otros
    public static String aString(double n,int d) {
        /*String aux = (""+n);
        int i = aux.indexOf(".");
        if(i<=0) return aux;
        return aux.substring(0,Math.min(i+d+1,aux.length()));*/
        if((n==(int)n)) return (""+(int)n);
        return (""+redondear(n, elevado(10, d)));
    }
    
    public static double redondear(double num, int b) {
        double aux = num * b;
        int tmp = (int) Math.round(aux);
        return (double) tmp / b;
    }
    
    
    /***************************CLASES PRIVADAS*******************************/
    
    
    private double ZVS5(double l) {
        return Zv1s(  Zv1( Ua(U5.falla(l), Vbase) ,  Ia(I45.falla(l), Ibase)),  rv5 , ri5  ) ;
    }
    private double ZVS8(double l) {
        return Zv1s(  Zv1( Ua(U8.falla(l), Vbase) ,  Ia(I8x.falla(l), Ibase)),  rv8 , ri8  ) ;
    }
    
    private void ZVS(double l) {
        z5vs1 = Zv1s(  Zv1( Ua(U5.falla(l), Vbase) ,  Ia(I45.falla(l), Ibase)),  rv5 , ri5  ) ;
        z8vs1 = Zv1s(  Zv1( Ua(U8.falla(l), Vbase) ,  Ia(I8x.falla(l), Ibase)),  rv8 , ri8  ) ;
    }
    
    private void zona() {
        zona5=zona(z5z1, z5z2, z5vs1);
        zona8=zona(z8z1, z8z2, z8vs1);
    }
    
    private void improt() {
        z5z1 = impedanciaZona1(z58,porc1Z1,rv5,ri5);
        z5z2 = impedanciaZona2(z58,z87,porc1Z2,porc2Z2,rv5,ri5);
        z8z1 = impedanciaZona1(z58,porc1Z1,rv8,ri8);
        z8z2 = impedanciaZona2(z58,z45,porc1Z2,porc2Z2,rv8,ri8);
    }
    
    // antes de tabla 9.9 en p�gina 530
    private static double IBASE(double P, double V) {
        return P*1000/V/Math.sqrt(3);
    }
    
    private static double ZBASE(double P, double V) {
        return V*V/P;
    }
    
    //tabla 9.9
    private static double Zreal(double Zbase, double Zpu) {
        return Zbase*Zpu;
    }
    
    //Regla: Zona 1: Zs=0.85*Zlinea; Zona 2:Zs=Zlinea+0.3*Zadyacente  (p1=0.85, p2=0.3)
    
    private static double impedanciaZona1(double Zlinea, double p1, double rv, double ri) {
        return p1*Zlinea*ri/rv;
    }
    // p1=1
    private static double impedanciaZona2(double Zlinea,double Zadyacente, double p1,double p2, double rv, double ri) {
        return (p1*Zlinea+p2*Zadyacente)*ri/rv;
    }
    
    //tabla 9.12, 9.13 y 9.14
    private static double Ua(double U, double Vbase) {
        return Vbase*U/Math.sqrt(3);
    }
    private static double Ia(double I, double Ibase) {
        return I*Ibase;
    }
    private static double Zv1(double ua, double ia) {
        return ua*1000/ia;
    }    
    private static double Zv1s(double zv1, double rv, double ri) {
        return zv1/rv*ri;
    }
    // para calcular en que zona act�a la protecci�n
    private static int zona(double zz1, double zz2, double zcalculado) {
        if(zcalculado<=zz1) return 1;
        if(zcalculado<=zz2) return 2;
        return 3;
    }
    
    private static int elevado(int a, int b) {
        if(b==0) return 1;
        return a*elevado(a,b-1);
    }
}
