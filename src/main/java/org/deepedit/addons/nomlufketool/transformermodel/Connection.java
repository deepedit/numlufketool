package org.deepedit.addons.nomlufketool.transformermodel;

public final class Connection{
	private int id_Yy = 0;
	private int id_Yd = 1;
	private int id_Dy = 2;
	private int id_Dd = 3;

	private String[] names = { "Yy", "Yd", "Dy", "Dd" };

	private String[][] desfases =
	{	{ "0", "2", "4", "6", "8", "10" } ,
		{ "1", "3", "5", "7", "9", "11" } ,
		{ "1", "3", "5", "7", "9", "11" } ,
		{ "0", "2", "4", "6", "8", "10" }
	};

	private int c;

	private Connection(int c) {this.c = c;}

	/** Conexion Y-Y (Yy?) */
	static public final Connection Yy = new Connection(0);
	/** Conexion Y-D (Yd?) */
	static public final Connection Yd = new Connection(1);
	/** Conexion D-Y (Dy?) */
	static public final Connection Dy = new Connection(2);
	/** Conexion D-D (Dd?) */
	static public final Connection Dd = new Connection(3);

	public String toString(){
		return names[c];
	}

	public String[] getDesfases(){
		return desfases[c];
	}
}