/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         LineasTxFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke Tramsmission Lines toolbox
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
*/

package org.deepedit.addons.nomlufketool.transmissionmodels;

/**
 * Display the Nom Lufke Tramsmission Lines toolbox
 * @author Frank Leanez
 */
public class LineasTxFrame extends javax.swing.JDialog {

    /**
     * Displays the Parameters tab when creating this frame. This is the default
     */
    public static final int DISPLAY_PARAMETERS = 0;
    /**
     * Displays the Methods tab  when creating this frame
     */
    public static final int DISPLAY_METHODS = 1;
     /**
      * Displays the Circle diagram tab when creating this frame
      */
    public static final int DISPLAY_CIRCLEDIAGRAM = 2;
    /**
     * Displays the Help tab when creating this frame
     */
    public static final int DISPLAY_HELP = 3;
    
    private int initialPanel = 0;
    
    /**
     * Creates a new Parameter Line's frame app
     *
     * @param display Use constants to set initially activated tab when this.
     * Use this class constants to set supported values this parameter
     * application is first run
     * @param parent owner of this dialog. Use of null is accepted
     * @param modal modal control flag
     */
    public LineasTxFrame(int display, javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        if (display >= 0 && display <= 2) {
            initialPanel = display;
        }
        initComponent();
    }
    
    private void initComponent() {
        setTitle("Transmission Lines Models");
        Ayuda33 ayuda = new Ayuda33();
        Op1 op1 = new Op1();
        Op2 op2 = new Op2();
        Op3 op3 = new Op3();
        op1.Metodos(op2, op3, ayuda);
        op2.Metodos(op1, op3, ayuda);
        op3.top.Metodos(op1, op2, op3, ayuda);
        ayuda.PanelTop.Metodos(op1, op2, op3, ayuda);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(op1);
        getContentPane().add(op2);
        getContentPane().add(op3);
        getContentPane().add(ayuda);
        op1.setVisible(initialPanel == DISPLAY_PARAMETERS);
        op2.setVisible(initialPanel == DISPLAY_METHODS);
        op3.setVisible(initialPanel == DISPLAY_CIRCLEDIAGRAM);
        ayuda.setVisible(initialPanel == DISPLAY_HELP);
        pack();
    }
    
}
