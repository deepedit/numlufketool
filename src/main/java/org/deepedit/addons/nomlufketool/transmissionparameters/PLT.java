package org.deepedit.addons.nomlufketool.transmissionparameters;

/*
Applet para el c�lculo de la estabilidad transitoria 
para un generador conectado a barra infinita. 
Autores: Cristian Chong, Nicol�s Castro  
Fecha: 12/07/2005
Version: 2.0 (basado en el applet del semestre I del a�o 2002)

*/
import java.awt.event.*;
import java.awt.*;
import java.applet.*;
import javax.swing.*;


public class PLT extends Applet implements ActionListener{
	
	
	JPanel PanelTop = new JPanel();
		JPanel PanelTitulo = new JPanel();
			JLabel LabelTitulo = new JLabel();
		
		JPanel PanelUpDown = new JPanel();
			Label l22 = new Label("                r[m] = ");
			Label l23 = new Label("               d[m] = ");
			Label l24 = new Label("          Dab[m] = ");
			Label l25 = new Label("          Dbc[m] = ");
			Label l26 = new Label("          Dac[m] = ");
			Label l27 = new Label("                f[Hz] = ");
			TextField t = new TextField("0.0113235", 10);
			TextField t2 = new TextField("0.4", 10);
			TextField t3 = new TextField("17.5",10);
			TextField t4 = new TextField("17.5",10);
			TextField t5 = new TextField("35",10);
			TextField t6 = new TextField("50", 10);
	
	JPanel PanelBottom = new JPanel();
		JButton c1 = new JButton("Req");      
		JButton c2 = new JButton("Deq");
		JButton c3 = new JButton("Inductancia [L]");
		JButton c4 = new JButton("Reactancia [Xl]");
		JButton b  = new JButton("Capacitancia [C]");
		JButton b1 = new JButton("Admitancia [Y]");
		Choice choice = new Choice();
	
	JPanel PanelCentral = new JPanel();
		
		Label l1 = new Label("Reql [m]   = ");
		Label l2 = new Label("Deq  [m]   = ");
		Label l3 = new Label("L [mH/Km]  = ");
		Label l4 = new Label("C [uF/Km]  = ");
		Label l13 = new Label("Reqc [m]  = ");
		Label l14 = new Label("Xl[Ohm/Km]= ");
		Label l15 = new Label("Y [mS/Km] = ");
		
		Label l6 = new Label("");
	
	
	
	public void Estabilidadt(){
	}
	
	public void init() {
		
		setLayout(new BorderLayout());
		setBackground ( new Color(240,240,240) );
        setMinimumSize(new java.awt.Dimension(768, 250));
        setPreferredSize(new java.awt.Dimension(768, 250));
        
	    PanelTop.setLayout(new GridLayout(2,1));
	        
	        PanelTitulo.setBackground(new Color(204,204,204));
	        LabelTitulo.setFont(new Font("Verdana",1,14));
	        LabelTitulo.setText("Par�metros de L�neas de Transmisi�n");
	        PanelTitulo.add(LabelTitulo);
	      
		PanelTop.add(PanelTitulo);
        
        	PanelUpDown.setLayout(new GridLayout(2,6));
        	PanelUpDown.setMinimumSize(new java.awt.Dimension(768, 50));
        	PanelUpDown.setMaximumSize(new java.awt.Dimension(768, 50));
        	PanelUpDown.setPreferredSize(new java.awt.Dimension(768, 50));
        	
        	
        	PanelUpDown.add(l22);PanelUpDown.add(t);PanelUpDown.add(l23);
        	PanelUpDown.add(t2);PanelUpDown.add(l24);PanelUpDown.add(t3);
        	
        	PanelUpDown.add(l25);PanelUpDown.add(t4);PanelUpDown.add(l26);
        	PanelUpDown.add(t5);PanelUpDown.add(l27);PanelUpDown.add(t6);
        
        PanelTop.add(PanelUpDown);
        
        
        add("North",PanelTop);
        
        
        PanelBottom.setLayout(new GridLayout(3,3));
        
		for(int i = 1; i<=4; i++){
			if(i==1){
			      choice.add(i+ " Fasciculado");
			}
			else{
			      choice.add(i+ " Fasciculados");
			}
		}
		choice.select(3);
		                
        PanelBottom.add(choice);PanelBottom.add(new JLabel(""));PanelBottom.add(new JLabel(""));
        PanelBottom.add(c1);PanelBottom.add(c2);PanelBottom.add(c3);
        PanelBottom.add(c4);PanelBottom.add(b);PanelBottom.add(b1);
        
		c1.addActionListener(new oye1());
		c2.addActionListener(new oye2());
		c3.addActionListener(new oye3());
		c4.addActionListener(new oye4());
		b.addActionListener(new oye5());
		b1.addActionListener(new oye6());
        
        add("South",PanelBottom);
        
        PanelCentral.setBackground(new Color(245,245,245));
        PanelCentral.setLayout(new GridLayout(3,3));
        
        l1.setFont(new Font("Verdana",1,14));
		l1.setText("Reql [m]   = ");
		l2.setFont(new Font("Verdana",1,14));
		l2.setText("Deq  [m]   = ");
		l3.setFont(new Font("Verdana",1,14));
		l3.setText("L [mH/Km]  = ");
		l4.setFont(new Font("Verdana",1,14));
		l4.setText("C [uF/Km]  = ");
		l13.setFont(new Font("Verdana",1,14));
		l13.setText("Reqc [m]  = ");
		l14.setFont(new Font("Verdana",1,14));
		l14.setText("Xl[Ohm/Km]= ");
		l15.setFont(new Font("Verdana",1,14));
		l15.setText("Y [mS/Km] = ");
		l6.setFont(new Font("Verdana",1,14));		
		l6.setText("");
        
        
		PanelCentral.add(l1);PanelCentral.add(l13);PanelCentral.add(l2);
		PanelCentral.add(l3);PanelCentral.add(l4);PanelCentral.add(l14);
		PanelCentral.add(l15);PanelCentral.add(new Label(""));PanelCentral.add(new Label(""));
			
        add(PanelCentral);
		
	}

	
//accion de los botones

	public void actionPerformed(ActionEvent e){	; }
	
      public class oye1 implements ActionListener{ //c�lculo de rmg
   
          public void actionPerformed(ActionEvent e){
          
                 double re;
                 Double dre = new Double(choice.getSelectedIndex());
                 re = dre.doubleValue();
                 if(re==0){
                 l6.setText("");
                 
                 double a;                        //conversi�n variable String a Double
                 Double da = new Double(t.getText());
                 a = da.doubleValue();
                 String Rmgl = new String("Reql [m] ="+ 0.778800783071*a);
                 
                 l1.setText(Rmgl.substring(0, 15));             //Se trunca el resultado para
                                                               //Una visualizaci�n mejor
                 
                 String Rmgc = new String("Reqc [m] = "+ a);
                 
                 l13.setText(Rmgc);             

 }
                 if(re==1){
                 l6.setText("");

                 
                 double a;                       
                 Double da = new Double(t.getText());
                 a = da.doubleValue();
                 
                 double b;                        
                 Double db = new Double(t2.getText());
                 b = db.doubleValue();

                 String Rmgl = new String("Reql [m] = "+ Math.sqrt(0.778800783071*b*a));
                 l1.setText(Rmgl.substring(0, 15));
                 
                 String Rmgc = new String("Reqc [m] = "+ Math.sqrt(b*a));
                 l13.setText(Rmgc.substring(0, 15));
                  
                 
                 }
                 if(re==2){
                 l6.setText("");
                 
                 double a;                       
                 Double da = new Double(t.getText());
                 a = da.doubleValue();
                 
                 double b;                        
                 Double db = new Double(t2.getText());
                 b = db.doubleValue();


                 String Rmgl = new String("Reql [m] = "+ Math.pow((0.778800783071*Math.pow(b,2)*a),0.3333333));
                 
                 l1.setText(Rmgl.substring(0, 15)); 
                 
                 
                 String Rmgc = new String("Reqc [m] = "+ Math.pow((Math.pow(b,2)*a),0.3333333));
                 
                 l13.setText(Rmgc.substring(0, 15)); 
                 
                 
                 }
                 if(re==3){
                 l6.setText("");
                 
                 double a;                       
                 Double da = new Double(t.getText());
                 a = da.doubleValue();
                 
                 double b;                        
                 Double db = new Double(t2.getText());
                 b = db.doubleValue();

                 String Rmgl = new String("Reql [m] = "+ 1.09*Math.pow((0.778800783071*Math.pow(b,3)*a),0.25));
                 
                 l1.setText(Rmgl.substring(0, 15)); 
                 
                 String Rmgc = new String("Reqc [m] = "+ 1.09*Math.pow((Math.pow(b,3)*a),0.25));
                 
                 l13.setText(Rmgc.substring(0, 15)); 

                 }
                }
          }  


	public class oye2 implements ActionListener{      //c�lculo de Dmg
   
          public void actionPerformed(ActionEvent e){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 String Dmg = new String("Deq [m] = "+  Math.pow(a*b*c, 0.3333));
                 l2.setText(Dmg.substring(0, 15));
                 

          }  
   }
	public class oye3 implements ActionListener{      //c�lculo de L 
   
          public void actionPerformed(ActionEvent e){
                 double re;
                 Double dre = new Double(choice.getSelectedIndex());
                 re = dre.doubleValue();
                 
                 if(re==0){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              // Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
      
                 String L = new String("L[mH/Km] = "+  0.2*Math.log(Math.pow(a*b*c, 0.33333333333)/(0.778800783071*d)));
                 l3.setText(L.substring(0, 15));
                 
                 }
                 
                 if(re==1){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 
                 
                 String L = new String("L[mH/Km] = "+  0.2*Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.sqrt(0.778800783071*k*d))));
                 l3.setText(L.substring(0, 15));
                 
                 }
                 if(re==2){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                                 
                 String L = new String("L[mH/Km] = "+  0.2*Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.pow((0.778800783071*Math.pow(k,2)*d),0.3333333))));
                 l3.setText(L.substring(0, 15));
                 
                 
                 }
                 if(re==3){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                                
                 String L = new String("L[mH/Km] = "+  0.2*Math.log(Math.pow(a*b*c, 0.33333333333)/(1.09*Math.pow((0.778800783071*Math.pow(k,3)*d),0.25))));
                 l3.setText(L.substring(0, 15));
                 
                 }
          }  
   }
	public class oye4 implements ActionListener{      //c�lculo de Xl
	   
          public void actionPerformed(ActionEvent e){
                 double re;

                 Double dre = new Double(choice.getSelectedIndex());
                 re = dre.doubleValue();
                 
                 if(re==0){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();

                 double f;                              //Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();
  

                 String X = new String("Xl[Ohm/Km]= "+ 0.001*2*3.141592653*f*0.2*Math.log(Math.pow(a*b*c, 0.3333)/(0.778800783071*d)));
                 l14.setText(X.substring(0, 16));
                 
                 }
                 if(re==1){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();

                 double f;                              //Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();                

                 
                 String X = new String("Xl[Ohm/Km]= "+ 0.001*2*3.141592653*f*0.2*Math.log(Math.pow(a*b*c, 0.3333)/(Math.sqrt(k*0.778800783071*d))));
                 l14.setText(X.substring(0, 16));
                 
           
                 }
                 if(re==2){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 

                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 
                 double f;                              //Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();                 

                 String X = new String("Xl[Ohm/Km]= "+ 0.001*2*3.141592653*f*0.2*Math.log(Math.pow(a*b*c, 0.3333)/(Math.pow((Math.pow(k,2)*0.778800783071*d),0.33333))));
                 l14.setText(X.substring(0, 16));
                            
                 }
                 if(re==3){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();

                 double f;                              //Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();  
                                 
                 String X = new String("Xl[Ohm/Km]= "+ 0.001*2*3.141592653*f*0.2*Math.log(Math.pow(a*b*c, 0.3333)/(1.09*Math.pow((Math.pow(k,3)*0.778800783071*d),0.25))));
                 l14.setText(X.substring(0, 16));
                 
           
                 }
       }    
   }
   
	public class oye5 implements ActionListener{      //c�lculo de C 
   
          public void actionPerformed(ActionEvent e){
                 double re;
                 Double dre = new Double(choice.getSelectedIndex());
                 re = dre.doubleValue();
                 
                 if(re==0){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              // Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 
                 String L = new String("C [uF/Km] = "+  0.0556061899/(Math.log(Math.pow(a*b*c, 0.33333333333)/(d))));
                 l4.setText(L.substring(0, 16));
                 
                 }
                 
                 if(re==1){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 
                 
                 String L = new String("C [uF/Km] = "+  0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.sqrt(k*d))));
                 l4.setText(L.substring(0, 16));
                      
                 }
                 if(re==2){
                 l6.setText("");

                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 

                 
                 String L = new String("C [uF/Km] = "+  0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.pow((Math.pow(k,2)*d),0.3333333))));
                 l4.setText(L.substring(0, 16));
                 

                 }
                 if(re==3){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 

                 
                 String L = new String("C [uF/Km] = "+  0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(1.09*Math.pow((Math.pow(k,3)*d),0.25))));
                 l4.setText(L.substring(0, 16));
                 

                 
                 }
                 
                 
          }  
   }

	public class oye6 implements ActionListener{      //c�lculo de Y 
   
          public void actionPerformed(ActionEvent e){
                 double re;
                 Double dre = new Double(choice.getSelectedIndex());
                 re = dre.doubleValue();
                 
                                  
                 if(re==0){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              // Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double f;                              // Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();

                 
                 String L = new String("Y [mS/Km] = "+  0.001*2*3.141592653*f*0.0556061899/(Math.log(Math.pow(a*b*c, 0.33333333333)/(d))));
                 l15.setText(L.substring(0, 15));
                 
                 }
                 
                 if(re==1){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 
                 double f;                              // Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();                 

                 String L = new String("Y [mS/Km] = "+  0.001*2*3.141592653*f*0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.sqrt(k*d))));
                 l15.setText(L.substring(0, 15));
                 
                 }
                 if(re==2){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();
                 
                 double f;                              // Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();                  

                 
                 String L = new String("Y [mS/Km] = "+  0.001*2*3.141592653*f*0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(Math.pow((Math.pow(k,2)*d),0.3333333))));
                 l15.setText(L.substring(0, 15));
 
                 }
                 if(re==3){
                 l6.setText("");
                 double a;                              //Dab
                 Double da = new Double(t3.getText());
                 a = da.doubleValue();
                 
                 double b;                              //Dbc
                 Double db = new Double(t4.getText());
                 b = db.doubleValue();
                 
                 double c;                              //Dac
                 Double dc = new Double(t5.getText());
                 c = dc.doubleValue();
                 
                 double d;                              //Radio Conductor
                 Double dd = new Double(t.getText());
                 d = dd.doubleValue();
                 
                 double k;                              //Distancia Fasciculados
                 Double dk = new Double(t2.getText());
                 k = dk.doubleValue();

                 double f;                              // Frecuencia
                 Double df = new Double(t6.getText());
                 f = df.doubleValue();                  
 

                 
                 String L = new String("Y [mS/Km] = "+  0.001*2*3.141592653*f*0.0556061899/Math.log(Math.pow(a*b*c, 0.33333333333)/(1.09*Math.pow((Math.pow(k,3)*d),0.25))));
                 l15.setText(L.substring(0, 15));
                 
                 }
          }  
   }	
}