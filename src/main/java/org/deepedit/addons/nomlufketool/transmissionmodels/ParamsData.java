package org.deepedit.addons.nomlufketool.transmissionmodels;

import org.deepedit.addons.nomlufketool.utils.Complex;

/*
 * ParamsData.java
 *
 * Created on June 15, 2003, 12:30 AM
 */

/**
 *
 * @author  midgard
 */
public class ParamsData {

    Complex Pi[];
    Complex ABCD[][];
	//Complex Admit[][];
	Complex Zw;
    Complex Gamma;
    
    public ParamsData()
    {
        Pi=new Complex[2];            // (Z,Y)
        ABCD=new Complex[2][2];       //[fila][columna]
        //Admit=new Complex[2][2];
    }
}
