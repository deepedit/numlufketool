package org.deepedit.addons.nomlufketool.radialpuconversor;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Rodrigo de la Castilleja
 *
 */
class SPU extends JDialog implements ActionListener {

    Elemento[] elementos;
    JLabel[][] visores;
    PU pu;
    JButton cerrar;
    ImageIcon R;

    public SPU(JDialog parent, Elemento[] elementosx, ImageIcon Rx, ImageIcon TL) {
        super(parent, true);
//        setLocation(100, 150);
        setTitle("Sistema en PU");
        R = Rx;
        elementos = elementosx;
        visores = new JLabel[7][5];
        JPanel pvisores1 = new JPanel();
        JPanel unilineal = new JPanel();
        JPanel pvisores2 = new JPanel();
        Label[] visorescolor = new Label[7];
        visorescolor[0] = new Label("");
        visorescolor[0].setBackground(Color.BLUE);
        pvisores1.add(visorescolor[0]);
        int zona = 0;
        for (int i = 1; i < 7; i++) {
            visorescolor[i] = new Label();
            if (elementos[i - 1].VBase != elementos[i].VBase) {
                zona++;
            }
            if (zona == 0) {
                visorescolor[i].setBackground(Color.blue);
            }
            if (zona == 1) {
                visorescolor[i].setBackground(Color.orange);
            }
            if (zona == 2) {
                visorescolor[i].setBackground(Color.cyan);
            }
            if (zona == 3) {
                visorescolor[i].setBackground(Color.YELLOW);
            }
            pvisores1.add(visorescolor[i]);
        }
        for (int i = 0; i < 7; i++) {
            visores[i][1] = new JLabel(elementos[i].getImageIcon());
            if (i > 0) {
                if (elementos[i].tipo == 3) {
                    if (elementos[i - 1].VBase == elementos[i].VoltajeA) {
                        visores[i][1].setIcon(R);
                    } else {
                        visorescolor[i].setText("1:" + trunca(elementos[i - 1].VBase / elementos[i].VoltajeA));
                        visores[i][1].setIcon(TL);
                    }
                }
            }
            if (elementos[i].Impedancia == 0 && elementos[i].ImpedanciaPUA == 0 && elementos[i].ImpedanciaI == 0 && elementos[i].ImpedanciaPUAI == 0) {
                visores[i][2] = new JLabel("S '/1");
            } else {
                visores[i][2] = new JLabel("Z '/1");
            }

            unilineal.add(visores[i][1]);
            pvisores2.add(visores[i][2]);
        }
        for (int i = 0; i < 7; i++) {
            if (elementos[i].tipo != 0) {
                visores[i][3] = new JLabel("" + trunca(elementos[i].calcula()));
            } else {
                visores[i][3] = new JLabel("");
            }
            pvisores2.add(visores[i][3]);
        }
        for (int i = 0; i < 7; i++) {
            if (elementos[i].tipo != 0) {
                if (trunca(elementos[i].calculaI()) >= 0) {
                    visores[i][4] = new JLabel("+j" + trunca(elementos[i].calculaI()));
                } else {
                    visores[i][4] = new JLabel("-j" + (-1) * trunca(elementos[i].calculaI()));
                }
            } else {
                visores[i][4] = new JLabel("");
            }
            pvisores2.add(visores[i][4]);
            //pvisores1.setSize(20,10);
        }
        pvisores1.setLayout(new GridLayout(1, 7));
        unilineal.setLayout(new GridLayout(1, 7));
        pvisores2.setLayout(new GridLayout(3, 7));
        JPanel todo = new JPanel();
        todo.setLayout(new GridLayout(3, 1));
        todo.add(pvisores1);
        todo.add(unilineal);
        todo.add(pvisores2);
        cerrar = new JButton("cerrar");
        cerrar.addActionListener(this);
        //getContentPane().add("North", new JLabel("Sistema en PU"));
        getContentPane().add("Center", todo);
        getContentPane().add("South", cerrar);
        this.setSize(592, 300);
        pack();
    }

    public double trunca(double x) {
        double tmp = ((int) (x * 10000));
        return tmp / 10000;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cerrar) {
            hide();
        }
    }
}
