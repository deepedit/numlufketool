package org.deepedit.addons.nomlufketool.frequencycontrol;

public class AppletLFC extends javax.swing.JApplet {

    public AppletLFC() {
        initComponents();
        Op1 op1=new Op1();
        Op2 op2=new Op2();
		op1.Metodos(op2);
		op2.Metodos(op1);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(op1);
        getContentPane().add(op2);
        op1.setVisible(true);
        op2.setVisible(false);
        repaint();
    }
    private void initComponents() {//GEN-BEGIN:initComponents

    }//GEN-END:initComponents
}