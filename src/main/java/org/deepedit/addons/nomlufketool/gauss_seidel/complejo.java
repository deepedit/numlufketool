package org.deepedit.addons.nomlufketool.gauss_seidel;

public class complejo{                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
 
	 	public double re;
	 	public double im;
	 
	 public complejo(double re, double im){
	 		this.re=re;
	 		this.im=im;
	 }
	 	
	 public complejo(){
	 		this.re=0;
	 		this.im=0;
	 }
	 
	 public static double modulo(complejo a){
	  	return Math.sqrt(a.re*a.re+a.im*a.im);
	 }
	 
	 public static double angulo_rad(complejo a){
	 
	 	double ang=0;
    	
	   		if (a.re>0 && a.im>0){
		 		ang=Math.atan(a.im/a.re);
	   		}
	   		
	   		if (a.re>0 && a.im<0){
		 		ang=Math.atan(a.im/a.re);
	 		}
	 			   		
	   		if (a.re<0 && a.im>0){ 
		 		ang=Math.PI+Math.atan(a.im/a.re);
	 		}
	 	
	 		if (a.re<0 && a.im<0){
		 		ang=-Math.PI+Math.atan(a.im/a.re);
	 		}
	 		if (a.im == 0){
		 		ang=0;
	 		}
	  	    return ang;
	 }
	 
 	 public static double angulo_grados(complejo a){
	  	return angulo_rad(a)*180/Math.PI;
	 }
	  
  
     public static complejo conjugado(complejo a){
	 	
	 	 complejo conj=new complejo(a.re,-1*a.im);
	 	 return conj;
	 }
	 
	 
	 public static complejo suma2(complejo a,complejo b){
    	
   		complejo sum=new complejo(a.re + b.re , a.im + b.im);
    	return sum;
   	 }
    
     public static complejo resta2(complejo a, complejo b){
    	
    	complejo res=new complejo(a.re - b.re , a.im - b.im);
    	return res;
    }
    
    public static complejo mult2(complejo c1, complejo c2){
    	
       	complejo mul=new complejo(c1.re*c2.re-c1.im*c2.im,c1.re*c2.im + c1.im*c2.re);
    	return mul;
    }
    
    public static complejo mult_escalar_complejo(double a, complejo c1){
    	
    	complejo mul=new complejo(c1.re*a,c1.im*a);
    	return mul;
    }
    
    public static complejo suma3(complejo a,complejo b,complejo c){
    	
       	complejo suma=new complejo(a.re+b.re+c.re,a.im+b.im+c.im);
    	return suma;
    }
    
    public static complejo suma4(complejo a,complejo b,complejo c,complejo d){
    	
    	complejo suma=new complejo(a.re+b.re+c.re+d.re,a.im+b.im+c.im+d.im);
    	return suma;
       	
    }
     
     // el angulo esta en radianes
     public static complejo toma_angulo(double modulo,double angulo){
     	
    	complejo a=new complejo(0,0);
    	a.re=modulo*Math.cos(angulo);
    	a.im=modulo*Math.sin(angulo);
    	return a;
    }
        
    public static complejo div2(complejo a,complejo b){
    	
    	complejo div= new complejo((a.re*b.re+a.im*b.im)/(b.re*b.re+b.im*b.im),(a.im*b.re-a.re*b.im)/(b.re*b.re+b.im*b.im));
    	return div;
    }    
    
    public static String notacion_polar(complejo a){
    	
    	double mod=Math.sqrt(a.re*a.re+a.im*a.im);
    	double ang=0;
    	
	   		if (a.re>0 && a.im>0){
		 		ang=Math.atan(a.im/a.re);
	   		}
	   		
	   		if (a.re>0 && a.im<0){
		 		ang=Math.atan(a.im/a.re);
	 		}
	 			   		
	   		if (a.re<0 && a.im>0){ 
		 		ang=Math.PI+Math.atan(a.im/a.re);
	 		}
	 	
	 		if (a.re<0 && a.im<0){
		 		ang=-Math.PI+Math.atan(a.im/a.re);
	 		}
	 		
	 	ang=180/Math.PI*ang;	
    	String modulo=String.valueOf(mod);
    	String angulo=String.valueOf(ang);
    	String polar ="("+ modulo + " < "+ angulo + ")";
    	return polar;
    	}
    	public static double[] par_ordenado(complejo a){
    	double mod=Math.sqrt(a.re*a.re+a.im*a.im);
    	double ang=0;
    	
	   		if (a.re>0 && a.im>0){
		 		ang=Math.atan(a.im/a.re);
	   		}
	   		
	   		if (a.re>0 && a.im<0){
		 		ang=Math.atan(a.im/a.re);
	 		}
	 			   		
	   		if (a.re<0 && a.im>0){ 
		 		ang=Math.PI+Math.atan(a.im/a.re);
	 		}
	 	
	 		if (a.re<0 && a.im<0){
		 		ang=-Math.PI+Math.atan(a.im/a.re);
	 		}
	 		
	 	ang=180/Math.PI*ang;	
    	String modulo=String.valueOf(mod);
    	String angulo=String.valueOf(ang);
    	String polar ="("+ modulo + " < "+ angulo + ")";
    	//return polar;
		
    	double []metodo= new double[2];
    	metodo[0]=mod;
    	metodo[1]=ang;
    	return metodo;
    	
    }
    
    public static complejo parseComplex(String ss){
                complejo aa = new complejo();

                int i = ss.indexOf('j');
                if(i==-1){
                        i = ss.indexOf('i');
                }
                if(i==-1)throw new NumberFormatException("no i or j found");

                int imagSign=1;
                int j = ss.indexOf('+');
                if(j==-1){
                j = ss.indexOf('-');
                if(j>-1) imagSign=-1;
                }
                if(j==-1)throw new NumberFormatException("no + or - found");

                int r0=0;
                int r1=j;
                int i0=i+1;
                int i1=ss.length();
                String sreal=ss.substring(r0,r1);
                String simag=ss.substring(i0,i1);
                aa.re=Double.parseDouble(sreal);
                aa.im=imagSign*Double.parseDouble(simag);
                return aa;
        }
        
		// Set the values of real and imag
        public void reset(double real, double imag){
                this.re = real;
                this.im = imag;
        }
        
        // Set real and imag given the modulus and argument (en grados)
        public static complejo polar(double mod, double arg){
        		complejo ayuda=new complejo();
        		arg=arg*(Math.PI/180);
                ayuda.re = mod*Math.cos(arg);
                ayuda.im = mod*Math.sin(arg);
                return ayuda;
        }
    }    	
  

