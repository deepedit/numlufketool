package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Acciones extends JPanel{

		JPanel PanelTituloAcciones;
		JPanel PanelOpcionAcciones;
		JLabel TituloAcciones;
		JButton ButtonAcciones1;//Graficar
		JButton ButtonAcciones2;//autoescalar
		JButton ButtonAcciones3;//borrar


		static Circulo circulos;	//se podran graficar hasta 10 circulos
		int dibujados=0;		//numero de cicrculos graficados
		static Complex2[] parametros=new Complex2[6];
		static double xmax,xmin,ymax,ymin;

		Zoom Pzoom;
		Lienzo lienzo;
		Graphics g;
		Izquierdo Izq;

		static int ancho, alto;
		static double ox,oy,cxp,cyp;
		static double paso;
		static double radiop;
		boolean graficado=false;

public Acciones(int x,int y) {
	//ancho=x;
	//alto=y;
        initComponents(x,y);
    }

public void Metodos(Zoom z, Lienzo l, Izquierdo i){
	Pzoom=z;
	lienzo=l;
	Izq=i;
}

//INIT COMPONETS*********************
private void initComponents(int x,int y) {

        java.awt.GridBagConstraints gc=new GridBagConstraints();


		setMinimumSize(new java.awt.Dimension(x,y));
		setPreferredSize(new java.awt.Dimension(x,y));
		setLayout(new GridBagLayout());
		PanelTituloAcciones = new JPanel();
		PanelOpcionAcciones = new JPanel();
		TituloAcciones=new JLabel("Acciones de Diagrama");
		ButtonAcciones1=new JButton();
		ButtonAcciones2=new JButton();
		ButtonAcciones3=new JButton();

		PanelTituloAcciones.add(TituloAcciones);
		PanelOpcionAcciones.setLayout(new java.awt.GridLayout(0,1,0,4));
		PanelOpcionAcciones.setMaximumSize(new java.awt.Dimension(100, 60));
		PanelOpcionAcciones.setMinimumSize(new java.awt.Dimension(100, 60));
	    PanelOpcionAcciones.setPreferredSize(new java.awt.Dimension(100, 60));

		ButtonAcciones1.setText("Redibujar");
		PanelOpcionAcciones.add(ButtonAcciones1);
		ButtonAcciones3.setText("Borrar");
	    PanelOpcionAcciones.add(ButtonAcciones3);

	gc.gridx=0;
	gc.gridy=0;
  	add(PanelTituloAcciones,gc);

  	gc.gridx = 0;
	gc.gridy = 1;
    add(PanelOpcionAcciones,gc);



//ESCUCHADORES***********************
		ButtonAcciones1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonAcciones1ActionPerformed(evt);}});


		ButtonAcciones3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				ButtonAcciones3ActionPerformed(evt);}});
}//FIN INIT***********



//*******************METODOS********************
//**********************************************


//BOTON REDIBUJAR*************************
private void ButtonAcciones1ActionPerformed(ActionEvent evt){
	redibujar(lienzo);

}


public void redibujar(Lienzo L){
	if(!graficado){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "No hay nada graficado. Imposible Redibujar.", "Error Grafico", JOptionPane.ERROR_MESSAGE);
		return;}
	if(L.dibujarReceptor)
		L.dibujarReceptor((int)L.cx,(int)L.cy,L.pasoreal,(int)L.cxpixeles,(int)L.cypixeles,(int)L.radiorpixeles);
	if(L.dibujarTransmisor)
		L.dibujarTransmisor((int)L.cx,(int)L.cy,L.pasoreal,(int)L.cxpixeles,(int)L.cypixeles,(int)L.radiorpixeles);
	}

//GRAFICAR***********************
public void graficar(Izquierdo I, Zoom Z, Lienzo L){
		graficado=true;
		if(Izq.RadioButtonTipo1.isSelected()){
			L.dibujarTransmisor=false;
			L.inicializarReceptor(I,Z);
			L.dibujarReceptor((int)L.cx,(int)L.cy,L.pasoreal,(int)L.cxpixeles,(int)L.cypixeles,(int)L.radiorpixeles);
			ox=L.cx; oy=L.cy; cxp=L.cxpixeles; cyp=L.cypixeles;
			paso=L.pasoreal; radiop=L.radiorpixeles;
			}
		if(Izq.RadioButtonTipo2.isSelected()){
			L.dibujarReceptor=false;
			L.inicializarTransmisor(I,Z);
			L.dibujarTransmisor((int)L.cx,(int)L.cy,L.pasoreal,(int)L.cxpixeles,(int)L.cypixeles,(int)L.radiorpixeles);
			ox=L.cx; oy=L.cy; cxp=L.cxpixeles; cyp=L.cypixeles;
			paso=L.pasoreal; radiop=L.radiorpixeles;
			}
}


//ZOOM IN****************************
public void zoomIn(int x, int y, Lienzo L){
	double f;
	if(!graficado){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "No hay nada graficado. Imposible hacer Zoom.", "Error de Zoom", JOptionPane.ERROR_MESSAGE);
		return;}
	try{f=Double.parseDouble(Pzoom.TFactor.getText());}
	catch (Exception e){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "El factor de Zoom debe ser un real mayor o igual que 1.", "Error de Zoom", JOptionPane.ERROR_MESSAGE);
		return;}//catch
	if(f<1){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "El factor de Zoom debe ser un real mayor o igual que 1.", "Error de Zoom", JOptionPane.ERROR_MESSAGE);
		return;}
	ox=(L.ancho/2.0+(ox-x)*f);
	oy=(L.alto/2.0 -(y-oy)*f);
	cxp=(L.ancho/2.0-(x-cxp)*f);
	cyp=(L.alto/2.0 +(cyp-y)*f);
	paso/=f;
	radiop=(radiop*f);
	if(Izq.RadioButtonTipo1.isSelected())L.dibujarReceptor((int)ox,(int)oy,paso,(int)cxp,(int)cyp,(int)radiop);
	if(Izq.RadioButtonTipo2.isSelected())L.dibujarTransmisor((int)ox,(int)oy,paso,(int)cxp,(int)cyp,(int)radiop);
	}

//ZOOM OUT***************************
public void zoomOut(int x, int y, Lienzo L){
	double f;
	if(!graficado){JOptionPane error=new JOptionPane();

			error.showMessageDialog(null, "No hay nada graficado. Imposible hacer Zoom.", "Error de Zoom", JOptionPane.ERROR_MESSAGE);
		return;}
	try{f=Double.parseDouble(Pzoom.TFactor.getText());}
	catch (Exception e){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "El factor de Zoom debe ser un real mayor o igual que 1.", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;}//catch
	if(f<1){JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "El factor de Zoom debe ser un real mayor o igual que 1.", "Error de Zoom", JOptionPane.ERROR_MESSAGE);
		return;}
	ox=(L.ancho/2.0+(ox-x)/f);
	oy=(L.alto/2.0 -(y-oy)/f);
	cxp=(L.ancho/2.0-(x-cxp)/f);
	cyp=(L.alto/2.0 +(cyp-y)/f);
	paso*=f;
	radiop=(radiop/f);
	if(Izq.RadioButtonTipo1.isSelected())L.dibujarReceptor((int)ox,(int)oy,paso,(int)cxp,(int)cyp,(int)radiop);
	if(Izq.RadioButtonTipo2.isSelected())L.dibujarTransmisor((int)ox,(int)oy,paso,(int)cxp,(int)cyp,(int)radiop);
	}

//AUTOESCALA*******************************************************
//private void ButtonAcciones2ActionPerformed(ActionEvent evt){
//			Pzoom.TFactor.setText("Cambie");
//			lienzo.getGraphics().drawLine(10,10,100,100);
//		}

//BORRAR*************************
private void ButtonAcciones3ActionPerformed(ActionEvent evt){
			lienzo.limpiar();
	}

}