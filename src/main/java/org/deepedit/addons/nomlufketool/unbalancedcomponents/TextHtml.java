package org.deepedit.addons.nomlufketool.unbalancedcomponents;

import javax.swing.*;
import java.awt.*;              //for layout managers
import java.awt.event.*;        //for action and window events
import java.net.URL;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.swing.event.*;
import javax.swing.text.html.*;

public class TextHtml extends JDialog implements ActionListener {

    private String newline = "\n";
    protected static final String textFieldString = "JTextField";
    protected static final String passwordFieldString = "JPasswordField";

    protected JLabel actionLabel;
    JEditorPane editorPane;

    public TextHtml(JFrame parent, String text_html) {
        super(parent, "Ayuda Calculadora Fasores");
        //Create an editor pane.
        editorPane = createEditorPane(text_html);
        JScrollPane editorScrollPane = new JScrollPane(editorPane);
        editorScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(300, 200));
        editorScrollPane.setMinimumSize(new Dimension(290, 200));

        //Create a text pane.
        JScrollPane paneScrollPane = new JScrollPane();

        //Put the editor pane and the text pane in a split pane.
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, editorScrollPane, paneScrollPane);
        splitPane.setOneTouchExpandable(false);
        JPanel rightPane = new JPanel();
        rightPane.add(splitPane);
        rightPane.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(""),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        //Put everything in the applet.
        JPanel leftPane = new JPanel();
        BoxLayout leftBox = new BoxLayout(leftPane, BoxLayout.Y_AXIS);
        leftPane.setLayout(leftBox);

        JPanel contentPane = new JPanel();
        BoxLayout box = new BoxLayout(contentPane, BoxLayout.X_AXIS);
        contentPane.setLayout(box);
        contentPane.add(leftPane);
        contentPane.add(rightPane);
        setContentPane(contentPane);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String prefix = "You typed \"";
        if (e.getActionCommand().equals(textFieldString)) {
            JTextField source = (JTextField) e.getSource();
            actionLabel.setText(prefix + source.getText() + "\"");
        } else {
            JPasswordField source = (JPasswordField) e.getSource();
            actionLabel.setText(prefix + new String(source.getPassword())
                    + "\"");
        }
    }

    private JEditorPane createEditorPane(String name) {
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
        editorPane.addHyperlinkListener(createHyperLinkListener());
        String s = null;

        try {
            URL helpURL = new URL(name);
            displayURL(helpURL, editorPane);
        } catch (MalformedURLException e) {
            System.err.println("Couldn't create help URL: " + name);
        }

        return editorPane;
    }

    private void displayURL(URL url, JEditorPane editorPane) {
        try {
            editorPane.setPage(url);
        } catch (IOException e) {
            System.err.println("Attempted to read a bad URL: " + url);
        }
    }

    public HyperlinkListener createHyperLinkListener() {
        return new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if (e instanceof HTMLFrameHyperlinkEvent) {
                        ((HTMLDocument) editorPane.getDocument()).processHTMLFrameHyperlinkEvent(
                                (HTMLFrameHyperlinkEvent) e);
                    } else {
                        try {
                            editorPane.setPage(e.getURL());
                        } catch (IOException ioe) {
                            System.out.println("IOE: " + ioe);
                        }
                    }
                }
            }
        };
    }
}
