package org.deepedit.addons.nomlufketool.gradient;

import javax.swing.*;

 public class AppletParam extends javax.swing.JApplet {

    public AppletParam() {
    	
        Grad Gradiente = new Grad();
        Tension Tens = new Tension();
        Parametros Param = new Parametros();
        Ayuda Ayu = new Ayuda();
        
		Gradiente.Metodos(Tens,Param,Ayu);
		Tens.Metodos(Gradiente,Param,Ayu);
		Param.Metodos(Gradiente,Tens,Ayu);
		Ayu.Metodos(Gradiente,Tens,Param);
		
		
		getContentPane().setLayout(new java.awt.FlowLayout());
		
        getContentPane().add(Gradiente);
        getContentPane().add(Tens);
        getContentPane().add(Param);
        getContentPane().add(Ayu);
        
        Gradiente.setVisible(false);       
        Gradiente.primeraAccion();
        Tens.setVisible(false);
        Param.setVisible(true);
        Ayu.setVisible(false);
                
        repaint();
    }
}

