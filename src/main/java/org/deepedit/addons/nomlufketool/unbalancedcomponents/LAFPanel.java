package org.deepedit.addons.nomlufketool.unbalancedcomponents;

/* PANEL INFERIOR CON LOS TEMAS DE JAVA
 * @(#)LAFPanel.java	 
 *
 * @author 	Fernando Eduardo Flatow Garrido
 * @version 	1.0, Septiembre de 2000
 */
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
//import javax.swing.plaf.synth.SynthLookAndFeel;
//import javax.swing.plaf.multi.MultiLookAndFeel;
import com.sun.java.swing.plaf.motif.MotifLookAndFeel;
//import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import org.deepedit.addons.nomlufketool.NomLufkeFrame;

/*
 * Clase LAFPanel
 */
public class LAFPanel extends JPanel implements ActionListener {

    public final MetalLookAndFeel metalLAF = new MetalLookAndFeel();
    public final MotifLookAndFeel motifLAF = new MotifLookAndFeel();
//    public final static WindowsLookAndFeel winLAF = new WindowsLookAndFeel();
    public final NimbusLookAndFeel nimbusLAF = new NimbusLookAndFeel();
//    public final static SynthLookAndFeel synthLAF = new SynthLookAndFeel();
//    public final static MultiLookAndFeel multiLAF = new MultiLookAndFeel();
    
    private JRadioButton metal, motif, nimbus;
    private JFrame applet;
    private boolean withPack;

    public LAFPanel(JFrame applet) {
        this(applet, true);
    }

    /*
     * Crea los tipos de botones
     */
    public LAFPanel(JFrame applet, boolean pack) {
        this.applet = applet;
        withPack = pack;
        setLayout(new GridLayout(1, 3));
        ButtonGroup lafGrp = new ButtonGroup();
        nimbus = new JRadioButton("Nimbus");
        nimbus.setEnabled(NomLufkeFrame.hasOwnFrame());
        nimbus.setSelected(true);
        metal = new JRadioButton("Metal");
        metal.setEnabled(NomLufkeFrame.hasOwnFrame());
        motif = new JRadioButton("Motif");
        motif.setEnabled(NomLufkeFrame.hasOwnFrame());
        lafGrp.add(nimbus);
        add(nimbus);
        lafGrp.add(metal);
        add(metal);
        lafGrp.add(motif);
        add(motif);
        nimbus.addActionListener(this);
        metal.addActionListener(this);
        motif.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == metal) {
            setLAF(metalLAF);
        } else if (obj == motif) {
            setLAF(motifLAF);
        } else if (obj == nimbus) {
            setLAF(nimbusLAF);
        }
    }

    private void setLAF(LookAndFeel laf) {
        if (NomLufkeFrame.hasOwnFrame()) {
            try {
                UIManager.setLookAndFeel(laf);
                SwingUtilities.updateComponentTreeUI(applet);
                if (withPack) {
                    applet.repaint();
                }
            } catch (UnsupportedLookAndFeelException e) {
                JOptionPane.showMessageDialog(applet, "Look and feel error", "Unspported LAF: " + e.getMessage(), JOptionPane.ERROR_MESSAGE);
                System.err.println("Could not switch to special look and feel: " + laf + "\n" + e);
                e.printStackTrace(System.err);
            }
        }
    }
}
