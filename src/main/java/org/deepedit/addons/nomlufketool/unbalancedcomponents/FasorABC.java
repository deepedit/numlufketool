package org.deepedit.addons.nomlufketool.unbalancedcomponents;

/*
 * @Version 1.0
 * @Autor Nolberto Emilio Oyarce Seguin
 */

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import org.deepedit.addons.nomlufketool.utils.Complex;

public class FasorABC extends JDialog {
    final static int maxCharHeight = 15;
    final static int minFontSize = 6;
    LAFPanel  pLAF;
    JPanel p0;
    final static Color bg = Color.white;
    final static Color fg = Color.black;
    final static Color red = Color.red;
    final static Color white = Color.white;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f, 
                                                      BasicStroke.CAP_BUTT, 
                                                      BasicStroke.JOIN_MITER, 
                                                      10.0f, dash1, 0.0f);
    Dimension totalSize;
    FontMetrics fontMetrics;

    /*
     *Seteo de los complejos en (0.0, 0.0)
     */
    Complex A =  new Complex((double)0,(double)0);
    Complex B =  new Complex((double)0,(double)0);
    Complex C =  new Complex((double)0,(double)0);
    double presicion = 0.0000001;
    
    public  double  r,pi,alpha,beta, x11_a, y11_a, x11_b, y11_b, x11_c, y11_c, max_a, max_b, max_c,max_ab, max_global;
    protected Font TextDialogFont;   
    protected FontMetrics TextDialogFontMetrics;
   public double  tolerancia = (double)0; 
    public FasorABC(JFrame parent, int numero, double re_a, double im_a,double re_b, double im_b,double re_c, double im_c){
	super(parent, true);
        setTitle("Representaci�n Gr�fica de los Fasores");
	
	x11_a = re_a;
	y11_a = im_a;
	x11_b = re_b;
	y11_b = im_b;
	x11_c = re_c;
	y11_c = im_c;
	
	setBackground(Color.lightGray);
	Font TextDialogFont = new Font("TimesRoman",Font.PLAIN, 12);
	TextDialogFontMetrics = getFontMetrics(TextDialogFont);
	setFont(TextDialogFont);
	setLocation(150,100);

	/*
	 *Creaci�n de los Paneles
	 */
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);
	p0 = panel0();
	getContentPane().add("Center",p0); 
	
	pack();
	setVisible(true);
	return;
    }

    public JPanel panel0(){
	JPanel p = new JPanel();
        p.setSize(new Dimension(550,500));
        setBackground(bg);
        setForeground(fg);
	return p;
    }
    
 FontMetrics pickFont(Graphics2D g2,
                         String longString,
                         int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();
	setSize(500,500);

        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            }
            else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                }
                else {
                    g2.setFont(font = new Font(name,
                                               style,
                                               --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

 
    /*
     *Funcion que pinta todo.
     */

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //Dimension d = getSize();
        Dimension d = new Dimension(550,500);
        int gridWidth = d.width ;
        int gridHeight = d.height;

        fontMetrics = pickFont(g2, "Filled and Stroked GeneralPath",
                               gridWidth);

        Color fg3D = Color.blue;

	/*
	 *Asignacion de los Complejos con las variables de entrada
	 */

	Complex A = new Complex(x11_a, y11_a);
	Complex B = new Complex(x11_b, y11_b);
	Complex C = new Complex(x11_c, y11_c);

	/*
	 *Seteo de los bordes de la zona de gr�fico
	 */

        g2.setPaint(fg3D);
        g2.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g2.draw3DRect(3, 3, d.width - 7, d.height - 7, true);
        g2.setPaint(fg);
	int x=5;
	int y=7;
	int rectWidth = gridWidth - 2*x;
        int stringY = gridHeight - 3 - fontMetrics.getDescent();
        int rectHeight = stringY - fontMetrics.getMaxAscent() - y - 2;
	
	/*
	 * Centro de los Fasores
	 */
	
	double x00_ab = gridWidth/2;
        double y00_ab = gridHeight/2;
	double x00_c = gridWidth/2;
	double y00_c = gridHeight/2;
	
	//funcion que calcula los maximos de los fasores
	max_a = Math.max(Math.abs(x11_a),Math.abs(y11_a));
	max_b = Math.max(Math.abs(x11_b),Math.abs(y11_b));
	max_c = Math.max(Math.abs(x11_c),Math.abs(y11_c));
	max_ab = Math.max(max_a,max_b);
	max_global = Math.max(max_c, max_ab);
	
	/*
	 * Escalamiento de los fasores a el pixelado de la pantalla
	 */
	double escala = 200/max_global;
	double X11_a = escala*x11_a;
	double Y11_a = escala*y11_a;
	double X11_b = escala*x11_b;
	double Y11_b = escala*y11_b;
	double X11_c = escala*x11_c;
	double Y11_c = escala*y11_c;

	String imageFile = "logo.gif";
	/*
	 *Dibujo de los Fasores
	 */

	




	/*
	* Lineas de los FAsores A, B, C.
	*/
	
	if (Complex.abs(A)<= presicion && Complex.abs(B)<= presicion && Complex.abs(C)<=presicion){

	g2.draw(new Line2D.Double(-200+x00_ab, y00_ab,200+x00_ab, y00_ab));
	g2.draw(new Line2D.Double(x00_ab, -200+y00_ab,x00_ab,200+ y00_ab));
/*
	 *Salida de los complejos en forma num�rica en pantalla
	 */
	/*
	 *Salida A
	 */
	g2.setColor(Color.cyan);
	double Real_A = Math.round(10000*A.re);
	double Real_a = Real_A/10000;
	String label_A_re = String.valueOf(Real_a);

	double Imag_A = Math.round(10000*A.im);
	double Imag_a = Imag_A/10000;
	String label_A_im = String.valueOf(Imag_a);
	g2.drawString("A = "+label_A_re+ "+j"+label_A_im,10,420);
	
	/*
	 *Salida B
	 */
	double Real_B = Math.round(10000*B.re);
	double Real_b = Real_B/10000;
	String label_B_re = String.valueOf(Real_b);

	double Imag_B = Math.round(10000*B.im);
	double Imag_b = Imag_B/10000;
	String label_B_im = String.valueOf(Imag_b);
	g2.drawString("B = "+label_B_re+ "+j"+label_B_im,10,440);
	

	/*
	 *Salida C
	 */
	double Real_C = Math.round(10000*C.re);
	double Real_c = Real_C/10000;
	String label_C_re = String.valueOf(Real_c);

	double Imag_C = Math.round(10000*C.im);
	double Imag_c = Imag_C/10000;
	String label_C_im = String.valueOf(Imag_c);
	g2.drawString("C = "+label_C_re+ "+j"+label_C_im,10,460);
	


	}
	else {

	g2.setColor(Color.red);
	g2.draw(new Line2D.Double(x00_ab,y00_ab,X11_a+x00_ab,-Y11_a+y00_ab));
	g2.setColor(Color.cyan);
	g2.draw(new Line2D.Double(x00_ab,y00_ab,X11_b+x00_ab,-Y11_b+y00_ab));
	g2.setColor(Color.black);
	g2.draw(new Line2D.Double(x00_c,y00_c,X11_c+x00_c,-Y11_c+y00_c));
	
	
	
	/*
	 *Punta de flecha de los fasores.
	 */
	double L = 10;
	double grados = 30;
	

	double alpha = grados*Math.PI/180;
	double beta_a = Complex.argument(A);
	double flecha_x1a = L*(Math.sin(Math.PI/2-beta_a-alpha));
	double flecha_y1a = L*(Math.cos(Math.PI/2-beta_a-alpha));

	double flecha_x2a = L*(Math.cos(beta_a-alpha));
	double flecha_y2a = L*(Math.sin(beta_a-alpha));
	g2.setColor(Color.red);
	g2.draw(new Line2D.Double(X11_a+x00_ab,-Y11_a+y00_ab,-flecha_x1a+x00_ab+X11_a,flecha_y1a+y00_ab-Y11_a));
	g2.draw(new Line2D.Double(X11_a+x00_ab,-Y11_a+y00_ab,-flecha_x2a+x00_ab+X11_a,flecha_y2a+y00_ab-Y11_a));


	

	double beta_b = Complex.argument(B);
	double flecha_x1b = L*(Math.sin(Math.PI/2-beta_b-alpha));
	double flecha_y1b = L*(Math.cos(Math.PI/2-beta_b-alpha));

	double flecha_x2b = L*(Math.cos(beta_b-alpha));
	double flecha_y2b = L*(Math.sin(beta_b-alpha));
	g2.setColor(Color.cyan);
	g2.draw(new Line2D.Double(X11_b+x00_ab,-Y11_b+y00_ab,-flecha_x1b+x00_ab+X11_b,flecha_y1b+y00_ab-Y11_b));
	g2.draw(new Line2D.Double(X11_b+x00_ab,-Y11_b+y00_ab,-flecha_x2b+x00_ab+X11_b,flecha_y2b+y00_ab-Y11_b));
	
	double beta_c = Complex.argument(C);
	double flecha_x1c = L*(Math.sin(Math.PI/2-beta_c-alpha));
	double flecha_y1c = L*(Math.cos(Math.PI/2-beta_c-alpha));

	double flecha_x2c = L*(Math.cos(beta_c-alpha));
	double flecha_y2c = L*(Math.sin(beta_c-alpha));
	g2.setColor(Color.black);
	g2.draw(new Line2D.Double(X11_c+x00_c,-Y11_c+y00_c,-flecha_x1c+x00_c+X11_c,flecha_y1c+y00_c-Y11_c));
	g2.draw(new Line2D.Double(X11_c+x00_c,-Y11_c+y00_c,-flecha_x2c+x00_c+X11_c,flecha_y2c+y00_c-Y11_c));
	

	/*
	 *Asignaci�n de Nombres
	 */

	int X_a = (int)(X11_a+x00_ab);
	int Y_a = (int)(-Y11_a+y00_ab);
	g2.setColor(Color.red);
	g2.drawString("  A",X_a,Y_a);


	int X_b = (int)(X11_b+x00_ab);
	int Y_b = (int)(-Y11_b+y00_ab);
	g2.setColor(Color.cyan);
	g2.drawString("  B",X_b,Y_b);

	int X_c = (int)(X11_c+x00_ab);
	int Y_c = (int)(-Y11_c+y00_ab);
	g2.setColor(Color.black);
	g2.drawString("  C",X_c,Y_c);
	g2.setColor(Color.black);

	/*
	 *Creaci�n de los ejes
	 */

	g2.draw(new Line2D.Double(-200+x00_ab, y00_ab,200+x00_ab, y00_ab));
	g2.draw(new Line2D.Double(x00_ab, -200+y00_ab,x00_ab,200+ y00_ab));

	/*
	 *FOR para la creaci�n de la rejilla
	 */

	if (escala<= tolerancia){

	    return;
	}
	else{
	for (int i=-10;i<=10; i++){
	    
	    double paso =  max_global*i/10;
	    double inc = escala*paso;
	    g2.draw(new Line2D.Double(-5+x00_ab, y00_ab+ inc,5+x00_ab, y00_ab+ inc ));
	    g2.draw(new Line2D.Double(inc + x00_ab,-5+y00_ab,inc + x00_ab,5+ y00_ab));
	}
	}
	/*
	 *Label de los ejes
	 */
	g2.setColor(red);
	for (int j = -5; j<=5; j++){

	    double paso_par =  max_global*2*j/10;
	    double inc = escala*paso_par;
	    double y_label =Math.round(1000*paso_par);
	    double Y_label = y_label/1000;
	    String label = String.valueOf(Y_label);
	    int label_x_y = (int)(-5+x00_ab);
	    int label_y_y = (int)(y00_ab-inc);
	    g2.setColor(Color.blue);
	    g2.drawString(label,label_x_y+7,label_y_y);
	    int label_x_x = (int)(x00_ab-inc);
	    int label_y_x= (int)(y00_ab-5);
	    g2.drawString(label,label_x_x,label_y_x+17);
	    
	}
	
	/*
	 *Salida de los complejos en forma num�rica en pantalla
	 */
	/*
	 *Salida A
	 */
	double Real_A = Math.round(10000*A.re);
	double Real_a = Real_A/10000;
	String label_A_re = String.valueOf(Real_a);

	double Imag_A = Math.round(10000*A.im);
	double Imag_a = Imag_A/10000;
	String label_A_im = String.valueOf(Imag_a);
	g2.drawString("A = "+label_A_re+ "+j"+label_A_im,10,420);
	
	/*
	 *Salida B
	 */
	double Real_B = Math.round(10000*B.re);
	double Real_b = Real_B/10000;
	String label_B_re = String.valueOf(Real_b);

	double Imag_B = Math.round(10000*B.im);
	double Imag_b = Imag_B/10000;
	String label_B_im = String.valueOf(Imag_b);
	g2.drawString("B = "+label_B_re+ "+j"+label_B_im,10,440);
	

	/*
	 *Salida C
	 */
	double Real_C = Math.round(10000*C.re);
	double Real_c = Real_C/10000;
	String label_C_re = String.valueOf(Real_c);

	double Imag_C = Math.round(10000*C.im);
	double Imag_c = Imag_C/10000;
	String label_C_im = String.valueOf(Imag_c);
	g2.drawString("C = "+label_C_re+ "+j"+label_C_im,10,460);
	


	}
    }


    	
    
   
}
   
