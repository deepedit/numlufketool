package org.deepedit.addons.nomlufketool.radialpuconversor;

import java.awt.FlowLayout;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;

/**
 * @author Rodrigo de la Castilleja
 *
 */
class Param extends JDialog implements ActionListener {

    JTextField TVoltajeA, TPotencia, TVoltajeB, T1, T2, T3, T4;
    JLabel L1, L2, L3, L4, U1, U2, U3, uni1, uni2, uni3;
    JButton setButton, cancelButton;
    Elemento elemento;
    PU pu;
    JComboBox Tipodatos;
    boolean todobien;
    JRadioButton C1, C2, C3, C4, C5, C6, C7;

    Param(Elemento x, PU pux) {
        super();
        elemento = x;
        pu = pux;
        setTitle("Par�metros de " + elemento);
        //parent = (DialogWindow)dw;
        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(6, 3));
        if (elemento.tipo == 3) {
            p1.setLayout(new GridLayout(9, 3));
        }
        if (elemento.tipo == 4) {
            p1.setLayout(new GridLayout(8, 3));
        }
        JPanel p = new JPanel();
        p1.add(new JLabel("Potencia nominal:"));
        TPotencia = new JTextField(8);
        TPotencia.setText(String.valueOf(elemento.Potencia));
        p1.add(TPotencia);
        p1.add(new JLabel("[MVA]"));
        if (elemento.getCampos(0)) {
            p1.add(new JLabel("tensi�n nominal:"));
            TVoltajeA = new JTextField(8);
            TVoltajeA.setText(String.valueOf(elemento.VoltajeA));
            p1.add(TVoltajeA);
            p1.add(new JLabel("[KV]  "));
        }
        if (elemento.getCampos(1)) {
            if (elemento.tipo == 4) {
                p1.add(new JLabel("tensi�n del Generador   "));
            } else {
                p1.add(new JLabel("tensi�n del Secundario  "));
            }

            TVoltajeB = new JTextField(8);
            TVoltajeB.setText(String.valueOf(elemento.VoltajeB));
            p1.add(TVoltajeB);
            p1.add(new JLabel("[KV]  "));
        }
        String[] lista = {"Potencia", "Impedancia", "Impedancia Base propia", "FP"};
        Tipodatos = new JComboBox(lista);
        Tipodatos.addActionListener(this);
        p1.add(new JLabel("Tipo de datos:"));
        p1.add(Tipodatos);
        p1.add(new JLabel(""));
        L1 = new JLabel();
        p1.add(L1);
        T1 = new JTextField(8);
        p1.add(T1);
        U1 = new JLabel("      ");
        p1.add(U1);
        L2 = new JLabel();
        p1.add(L2);
        T2 = new JTextField(8);
        p1.add(T2);
        U2 = new JLabel("      ");
        p1.add(U2);
        L3 = new JLabel("");
        L2.disable();
        T2.disable();

        p1.add(L3);
        L4 = new JLabel("");
        C1 = new JRadioButton("");
        C1.setActionCommand("si");
        C1.setSelected(true);

        C2 = new JRadioButton("");
        C2.setActionCommand("no");

        C1.addActionListener(this);
        C2.addActionListener(this);
        ButtonGroup group = new ButtonGroup();
        group.add(C1);
        group.add(C2);

        JPanel radioPanel = new JPanel(new GridLayout(1, 0));
        radioPanel.add(C1);
        radioPanel.add(C2);
        p1.add(radioPanel);
        p1.add(new JLabel(""));

        setLocation(100, 150);

        if (elemento.getCampos(2)) {
            L4.setText("Referida a ...");
            p1.add(L4);
            C3 = new JRadioButton("Alta");
            C4 = new JRadioButton("Baja");
            C5 = new JRadioButton("Dy");
            C6 = new JRadioButton("Yd");
            C7 = new JRadioButton("Yy");

            if (elemento.alta) {
                C3.setSelected(true);
            } else {
                C4.setSelected(true);
            }

            if (elemento.tconex == 1) {
                C5.setSelected(true);
            }
            if (elemento.tconex == 2) {
                C6.setSelected(true);
            }
            if (elemento.tconex == 3) {
                C7.setSelected(true);
            }

            C3.addActionListener(this);
            C4.addActionListener(this);
            C5.addActionListener(this);
            C6.addActionListener(this);
            C7.addActionListener(this);
            group = new ButtonGroup();
            group.add(C3);
            group.add(C4);
            group = new ButtonGroup();
            group.add(C5);
            group.add(C6);
            group.add(C7);
            radioPanel = new JPanel(new GridLayout(1, 0));
            radioPanel.add(C3);
            radioPanel.add(C4);

            p1.add(radioPanel);
            p1.add(new JLabel(""));
            p1.add(new JLabel("tipo de conexi�n"));
            radioPanel = new JPanel(new GridLayout(1, 0));
            radioPanel.add(C5);
            radioPanel.add(C6);
            radioPanel.add(C7);
            p1.add(radioPanel);
            p1.add(new JLabel(""));

        }
        if (elemento.getCampos(3)) {
            L4.setText("n�mero de circuitos");
            T4 = new JTextField("" + elemento.numero);
            p1.add(L4);
            p1.add(T4);
        }

        getContentPane().add("Center", p1);
        //Create bottom row.
        JPanel p2 = new JPanel();
        p2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        cancelButton = new JButton("Cancelar");
        cancelButton.addActionListener(this);
        setButton = new JButton("Actualizar");
        setButton.addActionListener(this);
        p2.add(cancelButton);
        p2.add(setButton);
        getContentPane().add("South", p2);
        Tipodatos.setSelectedItem(elemento.tipoda);
        setea(elemento.tipoda);
        //Initialize this dialog to its preferred size.
        pack();
    }

    public void actionPerformed(ActionEvent e) {
        if (Tipodatos == e.getSource()) {
            setea((String) Tipodatos.getSelectedItem());

        }
        if (e.getSource() == setButton) {
            todobien = true;
            elemento.reset();
            elemento.Potencia = convierte(TPotencia.getText(), true, "Potencia");

            if (elemento.getCampos(0)) {
                elemento.VoltajeA = convierte(TVoltajeA.getText(), true, "Tensi�n en el primario");
                if (C5.isSelected()) {
                    elemento.tconex = 1;
                }
                if (C6.isSelected()) {
                    elemento.tconex = 2;
                }
                if (C7.isSelected()) {
                    elemento.tconex = 3;
                }
            }
            if (elemento.getCampos(1)) {
                if (elemento.tipo == 4) {
                    elemento.VoltajeB = convierte(TVoltajeB.getText(), true, "Tensi�n en el generador");
                } else {
                    elemento.VoltajeB = convierte(TVoltajeB.getText(), true, "Tensi�n en el secundario");
                }
            }

            if (((String) Tipodatos.getSelectedItem()).equals("Potencia")) {
                elemento.PotenciaI = convierte(T1.getText(), false, "");
                elemento.tipoda = "Potencia";
                if (elemento.Potencia == 0) {
                    JOptionPane.showMessageDialog(this,
                            "La potencia no puede ser 0",
                            "advertencia",
                            JOptionPane.WARNING_MESSAGE);
                    todobien = false;
                }
            }
            if (((String) Tipodatos.getSelectedItem()).equals("Impedancia")) {
                elemento.Impedancia = convierte(T1.getText(), true, "impedancia");
                elemento.ImpedanciaI = convierte(T2.getText(), false, "");
                elemento.tipoda = "Impedancia";
                if (elemento.getCampos(2)) {
                    if (C3.isSelected()) {
                        elemento.alta = true;
                    } else {
                        elemento.alta = false;
                    }
                }

            }
            if (((String) Tipodatos.getSelectedItem()).equals("Impedancia Base propia")) {
                elemento.ImpedanciaPUA = convierte(T1.getText(), true, "impedancia por unidad");
                elemento.ImpedanciaPUAI = convierte(T2.getText(), false, "");
                elemento.tipoda = "Impedancia Base propia";
                if (elemento.Potencia == 0) {
                    JOptionPane.showMessageDialog(this,
                            "La potencia no puede ser 0",
                            "advertencia",
                            JOptionPane.WARNING_MESSAGE);
                    todobien = false;
                }
            }
            if (((String) Tipodatos.getSelectedItem()).equals("FP")) {
                elemento.FP = convierte(T1.getText(), true, "factor de potencia");
                elemento.tipoda = "FP";
                if (C1.isSelected()) {
                    elemento.ind = false;
                } else {
                    elemento.ind = true;
                }
                if (elemento.FP < 0) {

                    todobien = false;
                }
            }

            if (elemento.getCampos(3)) {
                elemento.numero = convierte(T4.getText(), true, "n�mero");
            }

            if (todobien) {
                pu.actualiza();
                hide();
            }
        }

        if (e.getSource() == cancelButton) {
            hide();
        }

    }

    public double convierte(String x, boolean pos, String dato) {
        try {
            Double tmp = new Double(x);
            if (pos && tmp.doubleValue() < 0) {
                JOptionPane.showMessageDialog(this,
                        "La " + dato + " no puede ser un n�mero negativo",
                        "advertencia",
                        JOptionPane.WARNING_MESSAGE);
                todobien = false;
            }
            return tmp.doubleValue();
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "\"" + x + "\"" + " no es un numero!",
                    "advertencia",
                    JOptionPane.WARNING_MESSAGE);
            todobien = false;
            return 0;
        }
    }

    public void setea(String tmp) {
        if (tmp.equals("Potencia")) {
            L1.setText("Potencia Reactiva: ");
            T1.setText(String.valueOf(elemento.PotenciaI));
            U1.setText("[MVAR]");
            L2.setText("");
            L2.disable();
            T2.setText("");
            T2.disable();
            U2.setText("      ");
        }
        if (tmp.equals("Impedancia")) {
            C1.disable();
            C2.disable();
            L1.setText("resistencia: ");
            T1.setText(String.valueOf(elemento.Impedancia));
            U1.setText("[ohm] ");
            L2.enable();
            T2.enable();
            L2.setText("reactancia:  ");
            T2.setText(String.valueOf(elemento.ImpedanciaI));
            U2.setText("[ohm] ");
        }
        if (tmp.equals("Impedancia Base propia")) {
            C1.disable();
            C2.disable();
            L1.setText("resistencia en Pu: ");
            T1.setText(String.valueOf(elemento.ImpedanciaPUA));
            U1.setText("[/1]bp");
            L2.enable();
            T2.enable();
            L2.setText("reactancia  en Pu: ");
            T2.setText(String.valueOf(elemento.ImpedanciaPUAI));
            U2.setText("[/1]bp");
            L3.setText("");
            C1.setText("");
            C2.setText("");

        }
        if (tmp.equals("FP")) {
            C1.enable();
            C2.enable();
            L1.setText("Factor de potencia: ");
            T1.setText(String.valueOf(elemento.FP));
            U1.setText("cos(phi)");
            L2.setText("");
            L2.disable();
            T2.setText("");
            T2.disable();
            U2.setText("      ");
            L3.setText("tipo");
            C1.setText("Capacitivo");

            if (!elemento.ind) {
                C1.setSelected(true);
            } else {
                C2.setSelected(true);
            }
            C2.setText("Inductivo");
        }
    }
}
