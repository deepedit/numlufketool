package org.deepedit.addons.nomlufketool.phasorcalculator;


public class Calc2 extends javax.swing.JDialog {
    
    double[] re;
    double[] im;
    boolean[] ModoCart;
    String ultlin;
    DibFasor mono;
    int clave;
    
    java.net.URL dirURL;
    TextHtml html;

    //Seteo de la cantidad de decimales
    //
    
    static double precision = 0.0000001;

    String text[];
    String espanol[] = {
	"Calculadora de Fasores",	//0
	"Datos de los Fasores",		//1
	"Re",						//2
	"Im",						//3
	"M�dulo",						//4
	"Ang[�]",						//5
	"Graficar ", 				//6		//Para seleccionar si grafica o no
	"Sistema de Coordenadas",					//7
	"Expresi�n a calcular:",			//8
	"Acciones Generales",		//9
	"Graficar",					//10
	"Limpiar",					//11
	"Salir",					//12
	"Ayuda",					//13
	"index_esp.html",			//14
	"Representaci�n Gr�fica",	//15
	"Calcular",					//16
	"Ayuda en L�nea",			//17
	"Calcular la operaci�n escrita",		//18
	"Graficar los n�meros seleccionados",	//19
	"Limpiar todos los n�meros almacenados",		//20
	"Ayuda en l�nea (desde Internet)",		//21
	"Cambiar el Sistema de Coordenadas de la variable A: Cartesiano <-> Polar",	//22
	"Cambiar el Sistema de Coordenadas de la variable B: Cartesiano <-> Polar",	//23
	"Cambiar el Sistema de Coordenadas de la variable C: Cartesiano <-> Polar",	//24
	"Cambiar el Sistema de Coordenadas de la variable D: Cartesiano <-> Polar",	//25
	"Cambiar el Sistema de Coordenadas de la variable E: Cartesiano <-> Polar",	//26
        "Ingresar Ecuaci�n"                     //27
        };

    String english[] = {
	"Fasor Calculator",			//0
	"Fasor Data",				//1
	"Re",						//2
	"Im",						//3
	"   Mod",						//4
	"Ang[�]",						//5
	"Graph", 					//6		//Para seleccionar si grafica o no
	"Coordinates System",					//7
	"Expression:",				//8
	"General Actions",			//9
	"Graph",					//10
	"Reset",					//11
	"Exit",						//12
	"Help",						//13
	"index_eng.html",			//14
	"Graphic Representation",	//15
	"Calculate",				//16
	"Help online",				//17
	"Calculate the written operation",	//18
	"Graph the selectioned numbers",	//19
	"Reset the stored numbers",			//20
	"Help Online (Internet)",			//21
	"Switch Coordinates System of variable A: Cartesian <-> Polar", 	//22
	"Switch Coordinates System of variable B: Cartesian <-> Polar", 	//23
	"Switch Coordinates System of variable C: Cartesian <-> Polar", 	//24
	"Switch Coordinates System of variable D: Cartesian <-> Polar", 	//25
	"Switch Coordinates System of variable E: Cartesian <-> Polar",  	//26
        "Add Equation"                                     //27
                };

    String german[] = {
	"Fasor Rechner",			//0
	"Fasor Data",				//1
	"Re",						//2
	"Im",						//3
	"Modul",						//4
	"Ang[�]",						//5
        "Graph", 				//6		//Para seleccionar si grafica o no
	"Austauschen",				//7
	"Ausdruck:",				//8
	"Allgemein Aktion",			//9
	"Graph",					//10
	"Kl�ren",					//11
	"Ausgang",					//12
	"Hilfe",					//13
	"index_ger.html",			//14
	"Graphisch Anzeichen",		//15
	"Berechnen",				//16
	"Hilfe",					//17
	"Errechnen Sie den schriftlichen Betrieb",		//18
	"Stellen Sie selectioned Zahlen graphisch dar",	//19
	"Stellen Sie die gespeicherten Zahlen zur�ck",	//20
	"Hilfe (Internet)",			//21
	"Schalten Sie Modus von Variable A: kartesisch <-> polar", 	//22
	"Schalten Sie Modus von Variable B: kartesisch <-> polar", 	//23
	"Schalten Sie Modus von Variable C: kartesisch <-> polar", 	//24
	"Schalten Sie Modus von Variable D: kartesisch <-> polar", 	//25
	"Schalten Sie Modus von Variable E: kartesisch <-> polar",	//26
        "Funktion"                                      //27
	};


    private javax.swing.JLabel LAccGen;
    private javax.swing.JLabel LOper;
    private javax.swing.JTextField TReA;
    private javax.swing.JTextField TReB;
    private javax.swing.JTextField TReC;
    private javax.swing.JTextField TReD;
    private javax.swing.JTextField TReE;
    private javax.swing.JButton BCalc;
    private javax.swing.JLabel LA;
    private javax.swing.JLabel LB;
    private javax.swing.JLabel LC;
    private javax.swing.JLabel LD;
    private javax.swing.JLabel LE;
    private javax.swing.JPanel PanIngreso;
    private javax.swing.JButton BGraf;
    private javax.swing.JPanel panel6;
    private javax.swing.JPanel panel5;
    private javax.swing.JLabel LImA;
    private javax.swing.JLabel LImB;
    private javax.swing.JLabel LImC;
    private javax.swing.JLabel LImD;
    private javax.swing.JLabel LImE;
    private javax.swing.JPanel PanReA;
    private javax.swing.JPanel PanReB;
    private javax.swing.JPanel PanReC;
    private javax.swing.JPanel PanReD;
    private javax.swing.JPanel PanReE;
    private javax.swing.JButton BReset;
    private javax.swing.JPanel PanAccGen;
    private javax.swing.JCheckBox ChGrafA;
    private javax.swing.JCheckBox ChGrafB;
    private javax.swing.JCheckBox ChGrafC;
    private javax.swing.JCheckBox ChGrafD;
    private javax.swing.JCheckBox ChGrafE;
    private javax.swing.JTextField TImA;
    private javax.swing.JTextField TImB;
    private javax.swing.JTextField TImC;
    private javax.swing.JTextField TImD;
    private javax.swing.JTextField TImE;
    private javax.swing.JPanel PanInf;
    private javax.swing.JPanel PanA;
    private javax.swing.JPanel PanB;
    private javax.swing.JPanel PanC;
    private javax.swing.JPanel PanD;
    private javax.swing.JPanel PanE;
    private javax.swing.JPanel PanImA;
    private javax.swing.JPanel PanImB;
    private javax.swing.JPanel PanImC;
    private javax.swing.JPanel PanImD;
    private javax.swing.JPanel PanImE;
    private javax.swing.JButton BAyuda;
    private javax.swing.JButton BConmA;
    private javax.swing.JButton BConmB;
    private javax.swing.JButton BConmC;
    private javax.swing.JButton BConmD;
    private javax.swing.JButton BConmE;
    private javax.swing.JButton BFuncIngreso;
    private javax.swing.JPanel PanSup;
    private javax.swing.JTextField TOper;
    private javax.swing.JLabel LReA;
    private javax.swing.JLabel LReB;
    private javax.swing.JLabel LReC;
    private javax.swing.JLabel LReD;
    private javax.swing.JLabel LReE;

    //Lo mismo, pero indexado
    private javax.swing.JTextField[] TRe;
    private javax.swing.JTextField[] TIm;
    private javax.swing.JLabel[] LRe;
    private javax.swing.JLabel[] LIm;
    private javax.swing.JCheckBox[] ChGraf;
    public final String autores="autores: Javier Gallegos, Patricio Loncomilla - 2003";
	

    public Calc2(int idioma, String Titulo, java.net.URL url) {
        int i;
        clave=idioma;
        if (idioma == 0){
            text = espanol;
        }
        else if (idioma == 1){
            text = english;
        }
        else {
            text = german;
        }

        dirURL=url;

        initComponents();
        setTitle(Titulo);
        setResizable(false);
        
//Seteo de texto ayuda a los botones./////////       
        BCalc.setMnemonic((int)'C');        //
        BCalc.setToolTipText(text[18]);     //
                                            //
        BGraf.setToolTipText(text[19]);     //
                                            //
        BReset.setToolTipText(text[20]);    //
                                            //
        BAyuda.setToolTipText(text[21]);    //
                                            //
        BConmA.setToolTipText(text[22]);    //
        BConmB.setToolTipText(text[23]);    //
        BConmC.setToolTipText(text[24]);    //
        BConmD.setToolTipText(text[25]);    //
        BConmE.setToolTipText(text[26]);    //
//////////////////////////////////////////////

        re=new double[5];
        im=new double[5];
        ModoCart=new boolean[5];
        for(i=0; i<5; i++){
            re[i]=0;
            im[i]=0;
            ModoCart[i]=true;
            TRe[i].setHorizontalAlignment(javax.swing.JLabel.LEFT);
            TIm[i].setHorizontalAlignment(javax.swing.JLabel.LEFT);
        }
        ultlin="";
    }
    

    private void initComponents() {
        PanSup = new javax.swing.JPanel();
        
        PanA = new javax.swing.JPanel();
        LA = new javax.swing.JLabel();
        PanReA = new javax.swing.JPanel();
        LReA = new javax.swing.JLabel();
        TReA = new javax.swing.JTextField();
        PanImA = new javax.swing.JPanel();
        LImA = new javax.swing.JLabel();
        TImA = new javax.swing.JTextField();
        BConmA = new javax.swing.JButton();
        ChGrafA = new javax.swing.JCheckBox();

        PanB = new javax.swing.JPanel();
        LB = new javax.swing.JLabel();
        PanReB = new javax.swing.JPanel();
        LReB = new javax.swing.JLabel();
        TReB = new javax.swing.JTextField();
        PanImB = new javax.swing.JPanel();
        LImB = new javax.swing.JLabel();
        TImB = new javax.swing.JTextField();
        BConmB = new javax.swing.JButton();
        ChGrafB = new javax.swing.JCheckBox();

        PanC = new javax.swing.JPanel();
        LC = new javax.swing.JLabel();
        PanReC = new javax.swing.JPanel();
        LReC = new javax.swing.JLabel();
        TReC = new javax.swing.JTextField();
        PanImC = new javax.swing.JPanel();
        LImC = new javax.swing.JLabel();
        TImC = new javax.swing.JTextField();
        BConmC = new javax.swing.JButton();
        ChGrafC = new javax.swing.JCheckBox();

        PanD = new javax.swing.JPanel();
        LD = new javax.swing.JLabel();
        PanReD = new javax.swing.JPanel();
        LReD = new javax.swing.JLabel();
        TReD = new javax.swing.JTextField();
        PanImD = new javax.swing.JPanel();
        LImD = new javax.swing.JLabel();
        TImD = new javax.swing.JTextField();
        BConmD = new javax.swing.JButton();
        ChGrafD = new javax.swing.JCheckBox();

        PanE = new javax.swing.JPanel();
        LE = new javax.swing.JLabel();
        PanReE = new javax.swing.JPanel();
        LReE = new javax.swing.JLabel();
        TReE = new javax.swing.JTextField();
        PanImE = new javax.swing.JPanel();
        LImE = new javax.swing.JLabel();
        TImE = new javax.swing.JTextField();
        BConmE = new javax.swing.JButton();
        ChGrafE = new javax.swing.JCheckBox();
		
        PanInf = new javax.swing.JPanel();

        PanIngreso = new javax.swing.JPanel();
        LOper = new javax.swing.JLabel();
        TOper = new javax.swing.JTextField();
        BFuncIngreso = new javax.swing.JButton();
        
        PanAccGen = new javax.swing.JPanel();
        panel5 = new javax.swing.JPanel();
        LAccGen = new javax.swing.JLabel();
        panel6 = new javax.swing.JPanel();
        BCalc = new javax.swing.JButton();
        BGraf = new javax.swing.JButton();
        BReset = new javax.swing.JButton();
        BAyuda = new javax.swing.JButton();

        getContentPane().setLayout(new java.awt.GridLayout(2, 1));

        //Better handled by default window operation:
//        addWindowListener(new java.awt.event.WindowAdapter() {
//            public void windowClosing(java.awt.event.WindowEvent evt) {
//                exitForm(evt);
//            }
//        });
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        PanSup.setLayout(new java.awt.GridLayout(1, 5));

        PanA.setLayout(new java.awt.GridLayout(5, 1, 8, 0));

        LA.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        LA.setText("A");
        PanA.add(LA);

        PanReA.setLayout(new javax.swing.BoxLayout(PanReA, javax.swing.BoxLayout.X_AXIS));

        LReA.setName(text[2]);
        LReA.setText("  "+text[2]);
        PanReA.add(LReA);
        TReA.setText("5");
        TReA.setColumns(10);
        TReA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TReAActionPerformed(evt);
            }
        });
        PanReA.add(TReA);
        PanA.add(PanReA);

        PanImA.setLayout(new javax.swing.BoxLayout(PanImA, javax.swing.BoxLayout.X_AXIS));
        LImA.setText("  "+text[3]);
        PanImA.add(LImA);
        TImA.setText("1");
        TImA.setColumns(10);
        TImA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TImAActionPerformed(evt);
            }
        });
        PanImA.add(TImA);
        PanA.add(PanImA);

        BConmA.setText(text[7]);
        BConmA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BConmAActionPerformed(evt);
            }
        });
        PanA.add(BConmA);

        ChGrafA.setText(text[6]);
        PanA.add(ChGrafA);

        PanSup.add(PanA);

//////////////////  PANEL B   ///////////////////////////////////////     
        PanB.setLayout(new java.awt.GridLayout(5, 1, 8, 0));

        LB.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        LB.setText("B");
        PanB.add(LB);

        PanReB.setLayout(new javax.swing.BoxLayout(PanReB, javax.swing.BoxLayout.X_AXIS));

        LReB.setName(text[2]);
        LReB.setText("  "+text[2]);
        PanReB.add(LReB);

        TReB.setText("2");
        TReB.setColumns(10);
        TReB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TReBActionPerformed(evt);
            }
        });

        PanReB.add(TReB);
        PanB.add(PanReB);

        PanImB.setLayout(new javax.swing.BoxLayout(PanImB, javax.swing.BoxLayout.X_AXIS));
        LImB.setText("  "+text[3]);
        PanImB.add(LImB);

        TImB.setText("5");
        TImB.setColumns(10);
        TImB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TImBActionPerformed(evt);
            }
        });

        PanImB.add(TImB);
        PanB.add(PanImB);

        BConmB.setText(text[7]);
        BConmB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BConmBActionPerformed(evt);
            }
        });
        PanB.add(BConmB);

        ChGrafB.setText(text[6]);
        PanB.add(ChGrafB);

        PanSup.add(PanB);

//////////////////  PANEL C   ///////////////////////////////////////       
        PanC.setLayout(new java.awt.GridLayout(5, 1, 8, 0));

        LC.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        LC.setText("C");
        PanC.add(LC);

        PanReC.setLayout(new javax.swing.BoxLayout(PanReC, javax.swing.BoxLayout.X_AXIS));

        LReC.setName(text[2]);
        LReC.setText("  "+text[2]);
        PanReC.add(LReC);

        TReC.setText("0");
        TReC.setColumns(10);
        TReC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TReCActionPerformed(evt);
            }
        });

        PanReC.add(TReC);

        PanC.add(PanReC);

        PanImC.setLayout(new javax.swing.BoxLayout(PanImC, javax.swing.BoxLayout.X_AXIS));

        LImC.setText("  "+text[3]);
        PanImC.add(LImC);

        TImC.setText("0");
        TImC.setColumns(10);
        TImC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TImCActionPerformed(evt);
            }
        });
        PanImC.add(TImC);

        PanC.add(PanImC);

        BConmC.setText(text[7]);
        BConmC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BConmCActionPerformed(evt);
            }
        });

        PanC.add(BConmC);

        ChGrafC.setText(text[6]);
        PanC.add(ChGrafC);

        PanSup.add(PanC);

//////////////////  PANEL D   ///////////////////////////////////////
        PanD.setLayout(new java.awt.GridLayout(5, 1, 8, 0));

        LD.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        LD.setText("D");
        PanD.add(LD);

        PanReD.setLayout(new javax.swing.BoxLayout(PanReD, javax.swing.BoxLayout.X_AXIS));

        LReD.setName(text[2]);
        LReD.setText("  "+text[2]);
        PanReD.add(LReD);

        TReD.setText("0");
        TReD.setColumns(10);
        TReD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TReDActionPerformed(evt);
            }
        });

        PanReD.add(TReD);

        PanD.add(PanReD);

        PanImD.setLayout(new javax.swing.BoxLayout(PanImD, javax.swing.BoxLayout.X_AXIS));

        LImD.setText("  "+text[3]);
        PanImD.add(LImD);

        TImD.setText("0");
        TImD.setColumns(10);
        TImD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TImDActionPerformed(evt);
            }
        });

        PanImD.add(TImD);

        PanD.add(PanImD);

        BConmD.setText(text[7]);
        BConmD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BConmDActionPerformed(evt);
            }
        });

        PanD.add(BConmD);

        ChGrafD.setText(text[6]);
        PanD.add(ChGrafD);

        PanSup.add(PanD);

//////////////////  PANEL E   ///////////////////////////////////////
        PanE.setLayout(new java.awt.GridLayout(5, 1, 8, 0));

        LE.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        LE.setText("E");
        PanE.add(LE);

        PanReE.setLayout(new javax.swing.BoxLayout(PanReE, javax.swing.BoxLayout.X_AXIS));

        LReE.setName(text[2]);
        LReE.setText("  "+text[2]);
        PanReE.add(LReE);

        TReE.setText("0");
        TReE.setColumns(10);
        TReE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TReEActionPerformed(evt);
            }
        });

        PanReE.add(TReE);

        PanE.add(PanReE);

        PanImE.setLayout(new javax.swing.BoxLayout(PanImE, javax.swing.BoxLayout.X_AXIS));

        LImE.setText("  "+text[3]);
        PanImE.add(LImE);

        TImE.setText("0");
        TImE.setColumns(10);
        TImE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TImEActionPerformed(evt);
            }
        });

        PanImE.add(TImE);

        PanE.add(PanImE);

        BConmE.setText(text[7]);
        BConmE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BConmEActionPerformed(evt);
            }
        });

        PanE.add(BConmE);

        ChGrafE.setText(text[6]);
        PanE.add(ChGrafE);

        PanSup.add(PanE);
///////////
        getContentPane().add(PanSup);
        
        PanInf.setLayout(new java.awt.GridLayout(2, 1));
        
///////////////PANEL DE INGRESO DE DATOS///////////////////////////   
        PanIngreso.setLayout(new java.awt.GridLayout(3, 1));

        LOper.setText(text[8]);
        PanIngreso.add(LOper);

        TOper.setText("C=A+B");
        TOper.setEditable(false);
        TOper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TOperActionPerformed(evt);
            }
        });
        PanIngreso.add(TOper);

        BFuncIngreso.setText(text[27]);
        BFuncIngreso.setFont(new java.awt.Font("Arial", 1, 14));
        BFuncIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BFuncIngresoActionPerformed(evt);
            }
        });
        PanIngreso.add(BFuncIngreso);
        
        
 ///////////////PANEL DE INFERIOR///////////////////////////        
        PanInf.add(PanIngreso);

        PanAccGen.setLayout(new java.awt.GridLayout(2, 1, 2, 0));

        LAccGen.setText(text[9]);
        panel5.add(LAccGen);

        PanAccGen.add(panel5);

        panel6.setLayout(new java.awt.GridLayout(1, 4, 3, 0));

        BCalc.setText(text[16]);
        BCalc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BCalcActionPerformed(evt);
            }
        });
        panel6.add(BCalc);

        BGraf.setText(text[6]);
        BGraf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BGrafActionPerformed(evt);
            }
        });
        panel6.add(BGraf);

        BReset.setText(text[11]);
        BReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BResetActionPerformed(evt);
            }
        });
        panel6.add(BReset);

        BAyuda.setText(text[13]);
        BAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BAyudaActionPerformed(evt);
            }
        });
        panel6.add(BAyuda);

        PanAccGen.add(panel6);

        PanInf.add(PanAccGen);

        getContentPane().add(PanInf);
        
        setLocation(50,80);

        pack();

////// Se juntan los elementos en arreglos

		TRe=new javax.swing.JTextField[5];
		TIm=new javax.swing.JTextField[5];
		LRe=new javax.swing.JLabel[5];
		LIm=new javax.swing.JLabel[5];
		ChGraf=new javax.swing.JCheckBox[5];

		TRe[0]=TReA;
		TIm[0]=TImA;
		LRe[0]=LReA;
		LIm[0]=LImA;
		ChGraf[0]=ChGrafA;

		TRe[1]=TReB;
		TIm[1]=TImB;
		LRe[1]=LReB;
		LIm[1]=LImB;
		ChGraf[1]=ChGrafB;

		TRe[2]=TReC;
		TIm[2]=TImC;
		LRe[2]=LReC;
		LIm[2]=LImC;
		ChGraf[2]=ChGrafC;
	
		TRe[3]=TReD;
		TIm[3]=TImD;
		LRe[3]=LReD;
		LIm[3]=LImD;
		ChGraf[3]=ChGrafD;

		TRe[4]=TReE;
		TIm[4]=TImE;
		LRe[4]=LReE;
		LIm[4]=LImE;
		ChGraf[4]=ChGrafE;
	
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		Aqu� comienza lo importante.
//		Lo anterior son s�lo declaraciones e inicializaci�n.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    static double cerca(double in0)  // Transforma 5.999999999 en 6
    {
        String st;
        int signo;
        double in;
        double base;
        double dif;
        int i,l;

        signo=(in0>=0)?1:-1;
        in=in0*signo; //Aqu� in0 es positivo

        //360 para transformar 1.25000000001 en 1.25

        base=(in*360000+0.4999999);
        dif=Math.abs( (in*360000)-Math.floor(base) );

        if (dif<precision && dif!=0)
        {
                in=Math.floor(in/precision*9+0.4999999)*precision/9;
        }

        in=in*signo;

        st=in+"";  //15
        l=st.length();

        if (l<15) return in;

        for (i=0; i<l; i++)
        {
                if (st.charAt(i)=='e'||st.charAt(i)=='E')
                {
                        st=st.substring(0,15-(l-i))+st.substring(i,l);
                        break;
                }
        }

        in=Double.parseDouble(st.substring(0,15));

        return in;
    }

    
/*******************************************************
 ******** Bot�n para mostrar Editor de Ecuaciones*******
 *******************************************************/ 
    
    private void BFuncIngresoActionPerformed(java.awt.event.ActionEvent evt) {
        Frame_calculadora_2 panelCalculadora=new Frame_calculadora_2(this, clave);
        panelCalculadora.setVisible(true);       
    }
    
    
/**************************************
 ********Acciones Generales************
 **************************************/ 

    private void TOperActionPerformed(java.awt.event.ActionEvent evt) {
    	BCalcActionPerformed(null);
    }

    private void BAyudaActionPerformed(java.awt.event.ActionEvent evt) {
        html = new TextHtml(text[14],text[17],dirURL); 
        html.pack();
        html.setVisible(true);
    }

    private void BResetActionPerformed(java.awt.event.ActionEvent evt) {
        int i;
        for (i=0; i<5;i++ )
        {
                TRe[i].setText("0");
                TIm[i].setText("0");
                ChGraf[i].setSelected(false);
                LRe[i].setText("  "+text[2]);
                LIm[i].setText("  "+text[3]);

                ModoCart[i]=true;
                re[i]=0;
                im[i]=0;
        }
        TOper.setText("");
    }

    private void BGrafActionPerformed(java.awt.event.ActionEvent evt) {
        double[] grre=new double[5];
        double[] grim=new double[5];
        int grn;
        int i;
        char[] grletras=new char[5];
        double retemp, imtemp, motemp,antemp;

        grn=0;

        for(i=0; i<5; i++){
            if(ModoCart[i]){
                re[i]=Double.parseDouble(TRe[i].getText());
                im[i]=Double.parseDouble(TIm[i].getText());

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
            else{
                motemp=Double.valueOf(TRe[i].getText()).doubleValue();
                antemp=Double.valueOf(TIm[i].getText()).doubleValue();

                re[i]=motemp*java.lang.Math.cos(antemp*java.lang.Math.PI/180);
                im[i]=motemp*java.lang.Math.sin(antemp*java.lang.Math.PI/180);

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
        }

        for (i=0;i<5 ;i++ ){
            if(ChGraf[i].isSelected()){
                grletras[grn]=(char)((int)('A')+i);
                grre[grn]=re[i];
                grim[grn]=im[i];
                grn++;
            }
        }

        if (grn>0){
            if(ultlin.length()==0){
                mono=new DibFasor(grn,grre,grim,grletras,text[15]);
            }
            else{
                mono=new DibFasor(grn,grre,grim,grletras,text[15]+":  \""+ultlin+"\"");
            }
        }
    }

    private void BCalcActionPerformed(java.awt.event.ActionEvent evt) {
        int lobj; //n� de la letra en la que se guarda el resultado
        String oper;  //igual a ingreso, pero ya sin letras
        String ingreso;  //Lo ingresado
        int i,l;
        char ch;
        double motemp, antemp;
        Complejo res;

        //Calcular todos los n�meros en forma cartesiana
        for(i=0; i<5; i++){
            ChGraf[i].setSelected(false);
            if(ModoCart[i]){
                re[i]=Double.parseDouble(TRe[i].getText());
                im[i]=Double.parseDouble(TIm[i].getText());

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
            else{
                motemp=Double.valueOf(TRe[i].getText()).doubleValue();
                antemp=Double.valueOf(TIm[i].getText()).doubleValue();

                re[i]=motemp*java.lang.Math.cos(antemp*java.lang.Math.PI/180);
                im[i]=motemp*java.lang.Math.sin(antemp*java.lang.Math.PI/180);

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
        }

        ingreso=TOper.getText().toUpperCase();
        ultlin=ingreso;
        l=ingreso.length();

        switch (ingreso.charAt(0))
        {
                case 'a':
                case 'A':
                        lobj=0;
                        break;
                case 'b':
                case 'B':
                        lobj=1;
                        break;
                case 'c':
                case 'C':
                        lobj=2;
                        break;
                case 'd':
                case 'D':
                        lobj=3;
                        break;
                case 'e':
                case 'E':
                        lobj=4;
                        break;
                default:
                        lobj=0;
        }

        ChGraf[lobj].setSelected(true);

        oper="";

        for(i=2; i<l; i++)  //Aqui se reemplazan las variables por sus valores
        {
            ch=ingreso.charAt(i);

            switch(ch){
                        case 'a':
                        case 'A':
                                if(i<l-1)  //detecta "arccos", "arc..."
                                {
                                        if (ingreso.charAt(i+1)=='r' || ingreso.charAt(i+1)=='R')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			
                                oper=oper+"(";
                                oper=oper+re[0];
                                oper=oper+"+";
                                oper=oper+im[0];
                                oper=oper+"*i)";
                                ChGraf[0].setSelected(true);
                                break;

                        case 'b':
                        case 'B':
                                oper=oper+"(";
                                oper=oper+re[1];
                                oper=oper+"+";
                                oper=oper+im[1];
                                oper=oper+"*i)";
                                ChGraf[1].setSelected(true);
                                break;

                        case 'c':
                        case 'C':
                                if(i>2)  //detecta "arccos", "arc..."
                                {
                                        if (ingreso.charAt(i-1)=='r' || ingreso.charAt(i-1)=='R')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			
                                if(i<l-1)  //detecta "cos", "cosh"
                                {
                                        if (ingreso.charAt(i+1)=='o' || ingreso.charAt(i+1)=='O')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			
                                oper=oper+"(";
                                oper=oper+re[2];
                                oper=oper+"+";
                                oper=oper+im[2];
                                oper=oper+"*i)";
                                ChGraf[2].setSelected(true);
                                break;

                        case 'd':
                        case 'D':
                                oper=oper+"(";
                                oper=oper+re[3];
                                oper=oper+"+";
                                oper=oper+im[3];
                                oper=oper+"*i)";
                                ChGraf[3].setSelected(true);
                                break;

                        case 'e':
                        case 'E':
                                if(i>2)  //detecta forma tipo 2.36E+4 (notaci�n cient�fica)
                                {
                                        if (ingreso.charAt(i-1)>='0' && ingreso.charAt(i-1)<='9')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			

                                if(i>2)  //detecta "sen", "senh"
                                {
                                        if (ingreso.charAt(i-1)=='s' || ingreso.charAt(i-1)=='S')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			
                                if(i<l-1)  //detecta "exp"
                                {
                                        if (ingreso.charAt(i+1)=='x' || ingreso.charAt(i+1)=='X')
                                        {
                                                oper=oper+(ch+"");
                                                continue;
                                        }
                                }			
                                oper=oper+"(";
                                oper=oper+re[4];
                                oper=oper+"+";
                                oper=oper+im[4];
                                oper=oper+"*i)";
                                ChGraf[4].setSelected(true);
                                break;

                        default:
                                oper=oper+(ch+"");
                }
        }

        //arbol=new Nodo(oper);
        //res=arbol.evalua();

        res=new Complejo(oper);

        if (!ModoCart[lobj]){
                BConmActionPerformed(null, lobj);
        }

        TRe[lobj].setText(cerca(res.re)+"");
        TIm[lobj].setText(cerca(res.im)+"");

    }


////////////Para letra A	
	
	
    private void BConmAActionPerformed(java.awt.event.ActionEvent evt) {
        BConmActionPerformed(evt,0);
    }
    private void TImAActionPerformed(java.awt.event.ActionEvent evt) {
  
    }

    private void TReAActionPerformed(java.awt.event.ActionEvent evt) {
     
    }
    



////////////Para letra B

	
	
    private void BConmBActionPerformed(java.awt.event.ActionEvent evt) {
        BConmActionPerformed(evt,1);
    }

    private void TImBActionPerformed(java.awt.event.ActionEvent evt) {
     
    }

    private void TReBActionPerformed(java.awt.event.ActionEvent evt) {
     
    }
    



////////////Para letra C


    private void BConmCActionPerformed(java.awt.event.ActionEvent evt) {
		BConmActionPerformed(evt,2);
    }

    private void TImCActionPerformed(java.awt.event.ActionEvent evt) {
     
    }

    private void TReCActionPerformed(java.awt.event.ActionEvent evt) {
     
    }
    


////////////Para letra D


    private void BConmDActionPerformed(java.awt.event.ActionEvent evt) {
	BConmActionPerformed(evt,3);
    }

    private void TImDActionPerformed(java.awt.event.ActionEvent evt) {
     
    }

    private void TReDActionPerformed(java.awt.event.ActionEvent evt) {
     
    }
    


////////////Para letra E


    private void BConmEActionPerformed(java.awt.event.ActionEvent evt) {
        BConmActionPerformed(evt,4);
    }

    private void TImEActionPerformed(java.awt.event.ActionEvent evt) {
        
    }

    private void TReEActionPerformed(java.awt.event.ActionEvent evt) {
        
    }
    

/////////Generico

	private void BConmActionPerformed(java.awt.event.ActionEvent evt, int lt) {
		double retemp, imtemp, motemp, antemp;

		if (ModoCart[lt]){  //Cambiar de cartesiano a polar
		    retemp=Double.parseDouble(TRe[lt].getText());
		    imtemp=Double.parseDouble(TIm[lt].getText());
			
                    motemp=java.lang.Math.sqrt(retemp*retemp+imtemp*imtemp);
                    antemp=java.lang.Math.atan2(imtemp,retemp)/java.lang.Math.PI*180;

                    if(motemp<precision && -precision<motemp) motemp=0;
                    if(antemp<precision && -precision<antemp) antemp=0;

                    LRe[lt].setText(text[4]);
                    LIm[lt].setText(text[5]);

                    TRe[lt].setText(cerca(motemp)+"");
                    TIm[lt].setText(cerca(antemp)+"");

                    ModoCart[lt]=false;
		}
		else{
		    motemp=Double.parseDouble(TRe[lt].getText());
		    antemp=Double.parseDouble(TIm[lt].getText());

                    retemp=motemp*java.lang.Math.cos(antemp*java.lang.Math.PI/180);
                    imtemp=motemp*java.lang.Math.sin(antemp*java.lang.Math.PI/180);

                    if (retemp<precision && -precision<retemp) retemp=0;

                    if(imtemp<precision && -precision<imtemp) imtemp=0;

                    LRe[lt].setText("   "+text[2]);
                    LIm[lt].setText("   "+text[3]);

                    TRe[lt].setText(cerca(retemp)+"");
                    TIm[lt].setText(cerca(imtemp)+"");

                    ModoCart[lt]=true;
            }
	}
	

	
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {
        try
        {
            System.exit(0);
        }
        catch(Exception e)
        {
            BResetActionPerformed(null);
            setVisible(false);
        }
    }
    
    public void setText(String t){
        TOper.setText(t);
    }

    public static void main(String args[]) {

		java.net.URL dirURL=null;

		java.io.File classFile = new java.io.File("");	// get current directory

		try{
			dirURL = new java.net.URL("file:" + classFile.getAbsolutePath());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

        new Calc2(0,"Calculadora Fasorial",dirURL).show();
    }
    
}
