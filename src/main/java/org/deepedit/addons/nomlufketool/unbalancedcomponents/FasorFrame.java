/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         FasorFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke Unbalanced Components toolbox selector with html preview
 *   
 *   Copyright 1997-2018
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */
package org.deepedit.addons.nomlufketool.unbalancedcomponents;

import java.awt.Component;
import javax.swing.JDialog;

/**
 * Nom Lufke Unbalanced Components toolbox selector with html preview
 * @author Frank Leanez
 */
public class FasorFrame extends JDialog {
    
    public FasorFrame(JDialog parent, boolean modal) {
        super(parent, modal);
        appletInicio app = new appletInicio();
        app.setVisible(false);
        Component c = app.makeContentPane();
        getContentPane().add(c);
        pack();
    }
    
}
