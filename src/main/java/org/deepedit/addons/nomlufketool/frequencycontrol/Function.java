package org.deepedit.addons.nomlufketool.frequencycontrol;

import org.deepedit.addons.nomlufketool.utils.Complex;

/**
 * Function utility
 */
public class Function {

    public double[] num;
    public double[] den;
    int Ln;
    int Ld;

    /**
     * Creates a new function object
     */
    public Function() {
        Ln = 0;
        Ld = 0;
    }
    
    /**
     * Creates a new function with the given argument
     * @param n
     * @param d
     * @param Sn
     * @param Sd
     */
    public Function(double[] n, double[] d, int Sn, int Sd) {
        num = n;
        den = d;
        Ln = Sn;
        Ld = Sd;
    }

    private static void Step(Function f) {
        f.num = new double[1];
        f.den = new double[2];
        f.Ln = 1;
        f.Ld = 2;
        f.num[0] = 1;
        f.den[0] = 0;
        f.den[1] = 1;
    }

    /**
     * Evaluates a function with the complex in the argument
     * @param f
     * @param s
     * @return
     */
    static public Complex eval_f(Function f, Complex s) {

        int i = 0;
        Complex aux = new Complex(1, 0);
        Complex Cxnum = new Complex();
        Complex Cxden = new Complex();

        for (i = 0; i < f.Ld; i++) {

            if (i < f.Ln) {
                Cxnum = Complex.add(Cxnum, Complex.multiply(aux, f.num[i]));
            }

            Cxden = Complex.add(Cxden, Complex.multiply(aux, f.den[i]));

            aux = Complex.multiply(aux, s);
        }

        return Complex.divide(Cxnum, Cxden);
    }

    /**
     * Evalua la respuesta al Pulso
     *
     * @param f
     * @param s
     * @return Evalua la respuesta al Pulso
     */
    public static Complex eval_fxh(Function f, Complex s) {
        Function h = new Function();
        Function.Step(h);
        return Complex.multiply(eval_f(f, s), eval_f(h, s));
    }
}
