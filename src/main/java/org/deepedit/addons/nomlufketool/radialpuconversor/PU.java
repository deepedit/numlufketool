package org.deepedit.addons.nomlufketool.radialpuconversor;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @version 7.4
 * @author Rodrigo de la Castilleja, Frank Leanez
 */
public class PU extends JDialog implements MouseListener {

    private boolean inAnApplet = true;
    private JTextArea textArea, SBASE;
    Label results[][];
    JLabel visores[][];
    Elemento elementos[];
    JButton button, Muestra;
    double Sbase;
    ImageIcon L, R, TL;
    boolean todobien;

    /**
     * DEPRECATED:
     * <br>Prefer using the constructor of the window with parent and modal
     * parameters to display better on screen
     * @see PU(JDialog, boolean)
     */
    @Deprecated
    public PU() {
        this(null, false);
    }
    
    /**
     * Creates a new window for the Per Unit converter NomLufke Toolbox
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public PU (JDialog parent, boolean modal) {
        super(parent, modal);
        todobien = true;
        ImageIcon nada = createAppletImageIcon("/radialpuconversor/images/linea.gif");
        ImageIcon CL = createAppletImageIcon("/radialpuconversor/images/CL.gif");
        R = createAppletImageIcon("/radialpuconversor/images/R.gif");
        TL = createAppletImageIcon("/radialpuconversor/images/TL.gif");
        ImageIcon L = createAppletImageIcon("/radialpuconversor/images/L.gif");
        ImageIcon C = createAppletImageIcon("/radialpuconversor/images/C.gif");
        ImageIcon T = createAppletImageIcon("/radialpuconversor/images/T.gif");
        ImageIcon G = createAppletImageIcon("/radialpuconversor/images/G.gif");
        elementos = new Elemento[7];
        visores = new JLabel[7][4];
        JPanel pvisores = new JPanel();
        pvisores.setLayout(new GridLayout(4, 7));
        JPanel unilineal = new JPanel();
        setMinimumSize(new java.awt.Dimension(600, 450));
        setPreferredSize(new java.awt.Dimension(600, 450));

        for (int i = 0; i < 7; i++) {
            elementos[i] = new Elemento(nada, CL, L, C, T, G, this);
//		elementos[i].setSize(50,50);
            elementos[i].addMouseListener(this);
            unilineal.add(elementos[i]);
        }
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 7; i++) {
                visores[i][j] = new JLabel();
                pvisores.add(visores[i][j]);
            }
        }
        //configuracion inicial
        elementos[0].setType(4);
        elementos[0].Potencia = 40;
        elementos[0].ImpedanciaPUA = 0.0;
        elementos[0].ImpedanciaPUAI = 0.9;
        elementos[0].VoltajeB = 6.6;
        elementos[0].tipoda = "Impedancia Base propia";
        elementos[1].setType(3);
        elementos[1].VoltajeA = 6.9;
        elementos[1].VoltajeB = 69.0;
        elementos[1].Potencia = 45;
        elementos[1].alta = true;
        elementos[1].ImpedanciaPUA = 0.0;
        elementos[1].ImpedanciaPUAI = 0.122;
        elementos[1].tipoda = "Impedancia Base propia";
        elementos[2].setType(1);
        elementos[2].Potencia = 0.0;
        elementos[2].Impedancia = 10;
        elementos[2].ImpedanciaI = 16.8;
        elementos[2].numero = 2;
        elementos[2].tipoda = "Impedancia";
        elementos[3].setType(3);
        elementos[3].VoltajeA = 38.1;
        elementos[3].VoltajeB = 12;
        elementos[3].Potencia = 45;
        elementos[3].alta = true;
        elementos[3].Impedancia = 0.0;
        elementos[3].ImpedanciaI = 9.5;
        elementos[3].tipoda = "Impedancia";
        elementos[3].tconex = 2;
        elementos[4].setType(2);
        elementos[4].Potencia = 10.307;
        elementos[4].FP = 0.97;
        elementos[4].ind = true;
        elementos[4].tipoda = "FP";
        elementos[5].setType(5);
        elementos[5].Potencia = 10.307;
        elementos[5].FP = 0.97;
        elementos[5].ind = true;
        elementos[5].tipoda = "FP";

        unilineal.setLayout(new GridLayout(1, 7));
        SBASE = new JTextArea("100");
        unilineal.setSize(50, 200);

        JPanel todo = new JPanel();
        todo.setLayout(new GridLayout(2, 1));
        JPanel resultados = new JPanel();
        resultados.setLayout(new GridLayout(7, 5));
        results = new Label[7][5];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 5; j++) {
                results[i][j] = new Label("");
                resultados.add(results[i][j]);
            }
        }
        this.actualiza();

        JPanel tmp = new JPanel();
        tmp.setLayout(new GridLayout(2, 1));
        tmp.add(unilineal);
        tmp.add(pvisores);
        todo.add(tmp);
        todo.add(resultados);

        JPanel creditos = new JPanel();
        creditos.setLayout(new GridLayout(4, 1));
        creditos.add(new Label());
        creditos.add(new Label());
        creditos.add(new Label());
        Panel tmp2 = new Panel();
        tmp2.add(new JLabel("S base"));
        tmp2.add(SBASE);
        tmp2.add(new JLabel("[MVA]"));
        for (int j = 0; j < 4; j++) {
            tmp2.add(new JLabel());
        }

        tmp2.setLayout(new GridLayout(1, 7));
        creditos.add(tmp2);
        this.getContentPane().add("North", creditos);

        getContentPane().add("Center", todo);
        button = new JButton("Calcular Zonas Pu");
        button.addMouseListener(this);
        Muestra = new JButton("Mostrar el sistema en Pu");
        Muestra.addMouseListener(this);
        JPanel panel = new JPanel();
        panel.add(button);
        panel.add(Muestra);
        getContentPane().add("South", panel);
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        for (int i = 0; i < 7; i++) {
            if (e.getSource() == elementos[i] && e.getButton() == 1) {
                if ((elementos[i].tipo == 5 || elementos[i].tipo == 0) && i == 6) {
                    if (elementos[i].tipo == 5) {
                        elementos[i].setType(0);
                    } else {
                        elementos[i].setType(5);
                    }
                } else {
                    elementos[i].setType((elementos[i].tipo + 1) % 5);
                }
                actualiza();
            }
            if (e.getSource() == elementos[i] && e.getButton() == 3) {
                elementos[i].getParametros();
            }
        }
        if (e.getSource() == button) {
            CalculosPU(elementos);
        }
        if (e.getSource() == Muestra) {
            CalculosPU(elementos);
            JDialog spu = new SPU(this, elementos, R, TL);
            spu.setLocationRelativeTo(this);
            spu.setVisible(true);
        }
    }

    public boolean CalculosPU(Elemento[] elementos) {
        todobien = true;
        Sbase = this.convierte(SBASE.getText(), true, "potencia base");
        boolean tranfo = false;
        for (int i = 0; i < 7; i++) {
            if (elementos[i].tipo == 3 || elementos[i].tipo == 4) {
                if (elementos[i].VoltajeB == 0 && elementos[i].VoltajeB == 0) {
                    tranfo = true;
                }
            }
        }
        if (tranfo) {
            JOptionPane.showMessageDialog(this,
                    "Existe un transformador o un generador con Tensi�n 0",
                    "advertencia",
                    JOptionPane.WARNING_MESSAGE);
            todobien = false;
        }
        if (Sbase == 0) {
            JOptionPane.showMessageDialog(this,
                    "La potencia base  no puede ser 0!",
                    "advertencia",
                    JOptionPane.WARNING_MESSAGE);
            todobien = false;
        }
        if (todobien) {
            results[0][2].setText("C�lculos en PU");
            results[2][0].setText("Zonas de Tensi�n:");
            results[3][0].setText("             ");
            results[4][0].setText("Impedancia base:");
            results[5][0].setText("Intensidad base:");
            results[6][0].setText("Potencia base:");
            int zonas = 0;
            double ZB = 0, IB, VB = 0;
            Color zona = Color.WHITE;
            for (int i = 0; i < 7; i++) {
                if (elementos[i].tipo == 3 || elementos[i].tipo == 4) {
                    zonas++;
                    if (zonas == 1) {
                        zona = Color.blue;
                    }
                    if (zonas == 2) {
                        zona = Color.orange;
                    }
                    if (zonas == 3) {
                        zona = Color.cyan;
                    }
                    if (zonas == 4) {
                        zona = Color.yellow;
                    }
                    for (int j = 0; j < 7; j++) {
                        results[2][zonas].setBackground(zona);
                    }

                    results[2][zonas].setText("Zona " + zonas);
                    VB = elementos[i].VoltajeB;
                    results[3][zonas].setText("" + elementos[i].VoltajeB + " KV");
                    ZB = elementos[i].VoltajeB * elementos[i].VoltajeB / Sbase;
                    results[4][zonas].setText("" + trunca(ZB) + " Ohm");
                    IB = Sbase * 1000 / (elementos[i].VoltajeB * 1.732050);
                    results[5][zonas].setText("" + trunca(IB) + " A");
                    results[6][zonas].setText("" + Sbase + " MVA");
                }

                elementos[i].VBase = VB;
                elementos[i].ZBase = ZB;
                elementos[i].PBase = Sbase;

                //System.out.println(""+elementos[i]+elementos[i].calcula()+"" );
            }
        }
        return true;
    }

    public double trunca(double x) {
        double tmp = ((int) (x * 1000));
        return tmp / 1000;
    }

    public void actualiza() {
        for (int i = 0; i < 7; i++) {

            if (elementos[i].tipo != 0) {
                visores[i][0].setText(elementos[i].caracteristica(0));
                visores[i][1].setText(elementos[i].caracteristica(1));
                visores[i][2].setText(elementos[i].caracteristica(2));
            } else {
                visores[i][0].setText("");
                visores[i][1].setText("");
                visores[i][2].setText("");
            }
        }
        for (int i = 1; i < 7; i++) {
            if (elementos[i].tipo == 0 && elementos[i - 1].tipo == 2) {
                elementos[i - 1].setType(5);
            }
            if (elementos[i].tipo != 0 && elementos[i - 1].tipo == 5) {
                elementos[i - 1].setType(2);
            }
            if (elementos[i].tipo == 2 && i == 6) {
                elementos[i].setType(5);
            }
        }
    }

    protected static ImageIcon createAppletImageIcon(String path) {
        int MAX_IMAGE_SIZE = 75000; //Change this to the size of
        //your biggest image, in bytes.
        int count = 0;
        BufferedInputStream imgStream = new BufferedInputStream(PU.class.getResourceAsStream(path));
        if (imgStream != null) {
            byte buf[] = new byte[MAX_IMAGE_SIZE];
            try {
                count = imgStream.read(buf);
            } catch (IOException ieo) {
                System.err.println("Couldn't read stream from file: " + path);
            }

            try {
                imgStream.close();
            } catch (IOException ieo) {
                System.err.println("Can't close file " + path);
            }

            if (count <= 0) {
                System.err.println("Empty file: " + path);
                return null;
            }
            return new ImageIcon(Toolkit.getDefaultToolkit().createImage(buf),
                    "image");
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    public double convierte(String x, boolean pos, String dato) {
        try {
            Double tmp = new Double(x);
            if (pos && tmp.doubleValue() < 0) {
                JOptionPane.showMessageDialog(this,
                        "La " + dato + " no puede ser un n�mero negativo",
                        "advertencia",
                        JOptionPane.WARNING_MESSAGE);
                todobien = false;
            }
            return tmp.doubleValue();
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "\"" + x + "\"" + " no es un numero!",
                    "advertencia",
                    JOptionPane.WARNING_MESSAGE);
            todobien = false;
            return 0;
        }
    }
}
