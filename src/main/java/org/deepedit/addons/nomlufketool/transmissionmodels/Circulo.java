package org.deepedit.addons.nomlufketool.transmissionmodels;

class Circulo{

double radiot, radior;
Complex2 centrot, centror;
double maxxr, maxyr, minxr, minyr;
double maxxt, maxyt, minxt, minyt;

Circulo(Complex2 A, Complex2 B, Complex2 C, Complex2 D, Complex2 V1, Complex2 V2){

	Complex2 aux=Complex2.div(Complex2.mult(V1,V2),B);		// V1*V2/B
	radior=aux.mag;
	radiot=radior;
	aux=Complex2.mult(A,Complex2.mult(V2,V2));		// A*V2^2
	aux=Complex2.div(aux,B);					// A*V2^2/B
	Complex2 auxt=Complex2.mult(D,Complex2.mult(V1,V1));
	auxt=Complex2.div(auxt,B);
	double magnitud=aux.mag;
	double angulo=180+B.ang-A.ang;
	double magnitudt=auxt.mag;
	double angulot=B.ang-D.ang;

	centror=new Complex2(magnitud,angulo,1);
	maxyr=centror.im+(7*radior/6);
	minyr=centror.im-(radior/4);
	maxxr=centror.re+(7*radior/6);
	minxr=centror.re-(radior/5);

	centrot=new Complex2(magnitudt,angulot,1);
	maxyt=centrot.im+(radiot/4);
	minyt=centrot.im-(7*radiot/6);
	maxxt=centrot.re+(7*radiot/6);
	minxt=centrot.re-(radiot/3);

	}

Circulo(Complex2[] c){
	new Circulo(c[0],c[1],c[2],c[3],c[4],c[5]);
}
}

class Complex2{
	double re, im, mag, ang;

	Complex2(double x, double y){
		re=x;
		im=y;
		mag=Math.sqrt(x*x+y*y);
		ang=Math.atan(y/x)*180/Math.PI;
	}

	Complex2(double x, double y, int polar ){
		mag=x;
		ang=y;
		re=x*Math.cos(y*Math.PI/180);
		im=x*Math.sin(y*Math.PI/180);
	}

	Complex2(String x, String y){
		double dx=Double.parseDouble(x);
		double dy=Double.parseDouble(y);
		re=dx;
		im=dy;
		mag=Math.sqrt(dx*dx+dy*dy);
		ang=Math.atan(dy/dx)*180/Math.PI;
	}

	Complex2(String x, String y, int polar ){
		double dx=Double.parseDouble(x);
		double dy=Double.parseDouble(y);
		mag=dx;
		ang=dy;
		re=dx*Math.cos(dy*Math.PI/180);
		im=dx*Math.sin(dy*Math.PI/180);
	}

	public static Complex2 mult(Complex2 x,Complex2 y){
		return new Complex2(y.re*x.re-y.im*x.im,y.im*x.re+y.re*x.im);
	}

	public static Complex2 mult(double x, Complex2 y){
		return new Complex2(y.re*x,y.im*x);
	}

	public static Complex2 mult(Complex2 x, double y){
		return mult(y,x);
	}

	public static Complex2 sum(Complex2 x, Complex2 y){
		return new Complex2(x.re+y.re,x.im+y.im);
	}

	public static Complex2 conj(Complex2 x){
		return new Complex2(x.re, -1*x.im);
	}

	public static Complex2 div(Complex2 x, Complex2 y){
		Complex2 aux=mult(x,conj(y));
		aux=mult(1/(y.mag*y.mag),aux);
		return aux;
	}

	public static String toString(Complex2 x){
		return ""+x.re+"+i*"+x.im;
	}

}