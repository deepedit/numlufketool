package org.deepedit.addons.nomlufketool.gradient;

import org.deepedit.addons.nomlufketool.utils.Complex;

public class Gradiente{

static public Complex Corriente (double P,double cos_phi,double Sb){
	double phi = Math.acos(cos_phi); // el ?gulo en radianes
	double Q = (P/cos_phi)*Math.sin(phi);
	Complex S = new Complex(P,Q);
	Complex Spu = Complex.divide(S,Sb); //S en la carga en pu
	Complex I = Complex.conjugate(Spu);
	//System.out.println("P=" + P);
	//System.out.println("Q=" + Q);
	//System.out.println("S=" + S.real + "+j" + S.imag);
	//System.out.println("Spu=" + Spu.real + "+j" + Spu.imag);

	//System.out.println("Corriente" + I.real + "+ j" + I.imag);
	return  I;//asumiendo V=1+j0 en el receptor.
}

// el siguiente m?odo devuelve los par?etros AB en pu
static public ParamsData LCCP(double R, double L, double G, double C, double x,double Zb){
	double omega=100*Math.PI; //50 hz
	ParamsData d=new ParamsData();
	d.Zw=Complex.sqrt( Complex.divide( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
	d.Pi[0]=Complex.multiply( new Complex( R, omega*L ) , x );//Zs
	d.Pi[1]=Complex.multiply( new Complex( G, omega*C ) , x/2);//Yp
	d.ABCD[0][0]=Complex.add( Complex.multiply( d.Pi[0], d.Pi[1] ) , 1);//A en pu
	d.ABCD[0][1]=Complex.divide(d.Pi[0],Zb);//en pu B
	d.ABCD[1][0]=Complex.multiply(Complex.multiply(d.Pi[1] ,Complex.add(Complex.multiply(d.Pi[0], d.Pi[1]), 2 ) ),Zb); //en pu C
	d.ABCD[1][1]=d.ABCD[0][0];														//D
	return d;
}


static public double[] CalculaGradiente (ParamsData d,Complex I, double x){
	Complex A = d.ABCD[0][0];
	Complex B = d.ABCD[0][1];
	Complex Vr = new Complex(1,0); //voltaje en el receptor.
	Complex Vt =Complex.add(Complex.multiply(A,Vr),Complex.multiply(B,I));
	double res[] = new double[2];
	res[0]=x;
	double modVt = Complex.abs(Vt);
	res[1]=(modVt-1)*100;
	//res[1]=Complex.abs(I);
	return res;
}


static public void main (String args[]){

double x = 150;//en km, se lee del txtfield
double Vb = 154; // en KVff, se lee del txtfield
double Sb = 100; // en MVA, se lee del txtfield
double Zb = Vb*Vb/Sb;
double paso = 1;

double P = 150;// se lee del txtfield
double cos_phi = 0.85; // se lee del textfield
double R = 0.0329;// se lee del textfield
double L = 1.089e-3;// se lee del textfield
double G = 0;// se lee del textfield
double C = 1.023e-8;// se lee del textfield

Complex I = new Complex();
I = Corriente(P,cos_phi,Sb);
ParamsData p = new ParamsData();
double par[] = new double[2];
String salida = "";
for(double i=paso;i<=250;i+=paso){
	p = LCCP(R,L,G,C,i,Zb);
	par = CalculaGradiente(p,I,i);
	salida = salida + par[0] +" "+ par[1];
	System.out.println(salida);
	salida = "";

}//for
}//main

}//class
