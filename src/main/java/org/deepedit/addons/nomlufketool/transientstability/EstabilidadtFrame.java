/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         EstabilidadtFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke Transient Stability toolbox
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */
package org.deepedit.addons.nomlufketool.transientstability;

/**
 * Display the Nom Lufke Transient Stability toolbox
 *
 * @author Frank Leanez
 */
public class EstabilidadtFrame extends javax.swing.JDialog {

    /**
     * Creates a new Nom Lufke Transient Stability toolbox
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public EstabilidadtFrame(javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    private void initComponents() {
        Estabilidadt app = new Estabilidadt();
        setTitle("Aplicación de Estabilidad Transitoria");
        getContentPane().add(app.getContentPane());
        pack();
    }

}
