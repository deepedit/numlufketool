package org.deepedit.addons.nomlufketool.transformermodel;

public class Trafo{
	/** x2 y r2 referidos a primario
	 * en estricto rigor, deberian llamarse x2' y r2'
	 */
	private double x0, r0, x1, r1, x2, r2; 
	/** n es la relación de vueltas. No consideramos caso de trafo
	 * desfasador (n complejo)
	 */
	private double n;
	/** voltajes nominales y potencia nominal en la base propia */
	private Base bp, bs;

	public Trafo(){
		// trafo sin parametros: ideal 1:1
		x0 = r0 = Double.POSITIVE_INFINITY;
		x1 = r1 = x2 = r2 = 0;
		n = 1;
		bp = bs = new Base(1,1);
	}

	/** Crea un nuevo transformador con todos los parametros dados,
	 * referidos al primario.
	 */
	public Trafo(double n, double x0, double r0, double x1, double r1, double x2, double r2, Base bp, Base bs) throws TrafoException{
		// trafo completo
		this.n = n;
		this.bp = bp;
		this.bs = bs;
		if(this.bs.getS() != this.bp.getS()) throw new TrafoException("Las potencias de las bases del transformador no son iguales");
		/* todo referido a primario */
		this.x0 = x0 / bp.getZ();
		this.x1 = x1 / bp.getZ();
		this.x2 = x2 / bp.getZ();
		this.r0 = r0 / bp.getZ();
		this.r1 = r1 / bp.getZ();
		this.r2 = r2 / bp.getZ();
	}

	public double getN(){
		return this.n;
	}

	/** x0 referido al primario */
	public double getX0(){
		return this.x0 * bp.getZ();
	}

	/** x1 referido al primario */
	public double getX1(){
		return this.x1 * bp.getZ();
	}

	/** x2 referido al primario */
	public double getX2(){
		return this.x2 * bp.getZ();
	}

	/** r0 referido al primario */
	public double getR0(){
		return this.r0 * bp.getZ();
	}

	/** r1 referido al primario */
	public double getR1(){
		return this.r1 * bp.getZ();
	}

	/** r2 referido al primario */
	public double getR2(){
		return this.r2 * bp.getZ();
	}

	/** numero de vueltas */
	public void setN(double n){
		this.n = n;
		// actualiza Vsecundario (Vprimario no lo cambia)
		this.bs.setV( this.bp.getV()/this.n );
	}

	/** x0 referido al primario */
	public void setX0(double x0){
		this.x0 = x0 / bp.getZ();
	}

	/** x1 referido al primario */
	public void setX1(double x1){
		this.x1 = x1 / bp.getZ();
	}

	/** x2 referido al primario */
	public void setX2(double x2){
		this.x2 = x2 / bp.getZ();
	}

	/** r0 referido al primario */
	public void setR0(double r0){
		this.r0 = r0 / bp.getZ();
	}

	/** r1 referido al primario */
	public void setR1(double r1){
		this.r1 = r1 / bp.getZ();
	}

	/** r2 referido al primario */
	public void setR2(double r2){
		this.r2 = r2 / bp.getZ();
	}

	/** Obtiene la base del primario.
	 * @return la base del primario como un objeto de la clase {@link Base}
	 */
	public Base getBasePrim(){
		return (Base)this.bp.clone();
	}

	/** Obtiene la base del secundario.
	 * @return la base del secundario como un objeto de la clase {@link Base}
	 */
	public Base getBaseSec(){
		return (Base)this.bs.clone();
	}

	/** Indica que bases se utilizan en el primario y secundario.
	 *
	 * @param bp	Base del primario (V primario, S nominal)
	 * @param bs	Base del secundario (V secundario, S nominal)
	 * @throws TrafoException	si las potencias no son iguales en las bases
	 */
	public void setBases(Base bp, Base bs) throws TrafoException{
		this.bp = (Base)bp.clone();
		this.bs = (Base)bs.clone();
		// actualiza n
		this.n = this.bp.getV()/this.bs.getV();
		if(this.bs.getS() != this.bp.getS()) throw new TrafoException("Las potencias de las bases del transformador no son iguales");
	}

	/**
	 * Calcula parametros x1, x2, r1 y r2 a partir de la prueba de
	 * cortocircuito en el lado de BT (AT cortocircuitado)
	 * <p>
	 * @param V	Voltaje reducido de la prueba [V]
	 * @param I	Corriente medida en la prueba [A]
	 * @param P	Potencia medida en la prueba [W]
	 */
	public void setFromSCSec(double V, double I, double P){
		double x, r; // x = x1 + x2, r = r1 + r2
		double Q;

		r = P/(I*I);
		Q = Math.sqrt(V*V*I*I - P*P);
		x = Q/(I*I);
		/* hasta aqui, unidades fisicas
		 * corresponde pasar a pu
		 */
		this.r1 = this.r2 = (r/2.0) / bs.getZ();
		this.x1 = this.x2 = (x/2.0) / bs.getZ();
	}

	/**
	 * Calcula parametros x1, x2, r1 y r2 a partir de la prueba de
	 * cortocircuito en el lado de AT (BT cortocircuitado)
	 * <p>
	 * @param V	Voltaje reducido de la prueba [V]
	 * @param I	Corriente medida en la prueba [A]
	 * @param P	Potencia medida en la prueba [W]
	 */
	public void setFromSCPrim(double V, double I, double P){
		double x, r; // x = x1 + x2, r = r1 + r2
		double Q;

		r = P/(I*I);
		Q = Math.sqrt(V*V*I*I - P*P);
		x = Q/(I*I);
		/* hasta aqui, unidades fisicas
		 * corresponde pasar a pu
		 */
		this.r1 = this.r2 = (r/2.0) / bp.getZ();
		this.x1 = this.x2 = (x/2.0) / bp.getZ();
	}

	/**
	 * Calcula parametros x0 y r0 a partir de la prueba de
	 * circuito abierto en el lado de AT (BT abierto)
	 * <p>
	 * @param V	Voltaje nominal de la prueba [V]
	 * @param I	Corriente medida en la prueba [A]
	 * @param P	Potencia medida en la prueba [W]
	 */
	public void setFromOCPrim(double V, double I, double P){
		double Q, r, x;
		r = V*V/P;
		Q = Math.sqrt(V*V*I*I - P*P);
		x = V*V/Q;
		/* hasta aqui, unidades fisicas
		 * corresponde pasar a pu
		 */
		this.r0 = r / bp.getZ();
		this.x0 = x / bp.getZ();
	}
	
	/**
	 * Calcula parametros x0 y r0 a partir de la prueba de
	 * circuito abierto en el lado de BT (AT abierto)
	 * <p>
	 * @param V	Voltaje nominal de la prueba [V]
	 * @param I	Corriente medida en la prueba [A]
	 * @param P	Potencia medida en la prueba [W]
	 */
	public void setFromOCSec(double V, double I, double P){
		double Q, r, x;
		r = V*V/P;
		Q = Math.sqrt(V*V*I*I - P*P);
		x = V*V/Q;
		/* hasta aqui, unidades fisicas
		 * corresponde pasar a pu
		 */
		this.r0 = r / bs.getZ();
		this.x0 = x / bs.getZ();
	}

	/** obtiene el modelo en pu.
	 * @return El modelo en pu en un objeto de la clase {@link Trafo}
	 */
	public Trafo getPU(){
		try{
			return new Trafo(1,x0,r0,x1,r1,x2,r2,new Base(1,1),new Base(1,1));
		} catch (TrafoException e){ return null; }
	}

	public Object clone(){
		try{
			return new Trafo(n,getX0(),getR0(),getX1(),getR1(),getX2(),getR2(),(Base)bp.clone(),(Base)bs.clone());
		} catch (TrafoException e){ return null; }
	}

}
