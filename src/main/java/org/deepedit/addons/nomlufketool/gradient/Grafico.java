package org.deepedit.addons.nomlufketool.gradient;

import java.awt. *;
import org.deepedit.addons.nomlufketool.utils.Complex;

public class Grafico extends Canvas{

	static public ParamsData LLCP(double R, double L, double G, double C, double x,double Zb) {
        double omega = 100*Math.PI;//50hz
		ParamsData d=new ParamsData();
		d.Gamma=Complex.sqrt( Complex.multiply( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
		d.Zw=Complex.sqrt( Complex.divide( new Complex(R, omega*L) , new Complex(G, omega*C) ) );
        	d.Pi[0]=Complex.multiply( d.Zw , Complex.sinh( Complex.multiply( d.Gamma , x ) ) );		//Zs
		d.Pi[1]=Complex.divide( Complex.tanh( Complex.multiply( d.Gamma , x/2 ) ) , d.Zw );	//Yp
		d.ABCD[0][0]=Complex.cosh( Complex.multiply( d.Gamma , x ) );//A
		d.ABCD[0][1]=Complex.divide(d.Pi[0],Zb);//B en pu
		d.ABCD[1][0]=Complex.divide( Complex.sinh( Complex.multiply(d.Gamma , x) ) , d.Zw );	//C
		d.ABCD[1][1]=d.ABCD[0][0];														//D

		return d;
    }
	
	static public double[] CalculaGradiente
	(ParamsData d,double P, double cos_phi,boolean inductivo,double Sb, double x){
		Complex A = d.ABCD[0][0];
		Complex B = d.ABCD[0][1];
		double res[] = new double[4];
		double resV[] = new double[2];
		resV = CalculaVoltaje(d,P,cos_phi,inductivo,Sb);
		res[0]=x;
		res[2]=resV[0];
		res[1]=(1-resV[0])*100/resV[0];
		return res;
	}


	static public double[] S_phi(double P,double cos_phi,boolean inductivo,double Sb){
		double res[] = new double[2];
		double phi = Math.acos(cos_phi); // el �gulo en radianes
		double Q = (P/cos_phi)*Math.sin(phi);
		Complex S = new Complex(P,Q);
		Complex Spu = Complex.divide(S,Sb); //S en la carga en pu

		if(inductivo == true){
			res[0] = Complex.abs(Spu);//S en pu
			res[1] = Complex.argument(Spu);//phi correspondiente
		}
		else{
			Spu = Complex.conjugate(Spu);//si es capacitivo => Q<0
			res[0] = Complex.abs(Spu);//S en pu
			res[1] = Complex.argument(Spu);//phi correspondiente
		}
		return res;
	}

	//el siguiente m�todo entrega las dos posibles soluciones matem�ticas para
	//el voltaje en el receptor.

	static public double[] CalculaVoltaje
		(ParamsData d,double P, double cos_phi,boolean inductivo,double Sb){
			Complex A = d.ABCD[0][0];
			Complex B = d.ABCD[0][1];
			double res[] = new double[2];
			double a = Complex.abs(A);

		if (P==0){
			res[0]=1/a;
			res[1]=1/a;
		}

		else{

			double alpha = Complex.argument(A);
			double b = Complex.abs(B);
			double beta = Complex.argument(B);
			double Svec[] = S_phi(P,cos_phi,inductivo,Sb);
			double S = Svec[0];
			double phi = Svec[1];
			double p = (2*b*S*Math.cos(alpha+phi-beta)/a)-1;//Vt=1
			double q = b*b*S*S/(a*a);

			// la ecuaci� es Vr^4+pVr^2 + q = 0

			res[0]=Math.sqrt((-p+Math.sqrt(p*p-4*q))/2);
			res[1]=Math.sqrt((-p-Math.sqrt(p*p-4*q))/2);
		}//else
		return res;

	}

    static public double closerTo(double[] d,double b){
		double d1 = Math.abs(d[0]-b);
		double d2 = Math.abs(d[1]-b);
		if(d1>d2) return d[1];
		else return d[0];

	}

    static public double Truncar(double x,int k){
			return Math.round( Math.pow(10,k) * x )/Math.pow(10,k);
	}

	static public int detectaTrunc(double max, double min){
		int res;
		if(Math.abs(max-min)>=100) res=0;
		else if(Math.abs(max-min)>=10 && Math.abs(max-min)<100) res = 0;
		else if(Math.abs(max-min)>=1 && Math.abs(max-min)<10) res = 1;
		else if(Math.abs(max-min)>=0.1 && Math.abs(max-min)<1) res = 2;
		else if(Math.abs(max-min)>=0.01 && Math.abs(max-min)<0.1) res = 3;
		else if(Math.abs(max-min)>=0.001 && Math.abs(max-min)<0.01) res = 4;
		else if(Math.abs(max-min)>=0.0001 && Math.abs(max-min)<0.001) res = 5;
		else if(Math.abs(max-min)>=0.00001 && Math.abs(max-min)<0.0001) res = 6;
		else if(Math.abs(max-min)>=0.000001 && Math.abs(max-min)<0.00001) res = 7;
		else if(Math.abs(max-min)>=0.0000001 && Math.abs(max-min)<0.000001) res = 8;
		else if(Math.abs(max-min)>=0.00000001 && Math.abs(max-min)<0.0000001) res = 9;
		else res = 10;

		return res;
	}


	static public double Maximo (double[] vector){
		int n = vector.length;
		double max = vector[0];
		for (int i=0;i<n;i++){
			if(vector[i]>max) max = vector[i];
		}
		return max;
	}

	static public double Minimo (double[] vector){
			int n = vector.length;
			double min = vector[0];
			for (int i=0;i<n;i++){
				if(vector[i]<min) min = vector[i];
			}
			return min;
	}

	private Color[] colores;
	private int indiceColor;
	private double Zb;

	public Grafico(Canvas cv){
		setSize(cv.getSize());
		// Obtiene el area gr�fica del canvas
		colores = new Color[9];
		colores[0]=Color.blue;
		colores[1]=Color.magenta;
		colores[2]=Color.red;
		colores[3]=Color.green;
		colores[4]=Color.orange;
		colores[5]=Color.gray;
		colores[6]=Color.cyan;
		colores[7]=Color.pink;
		colores[8]=Color.black;
		indiceColor = 8;
		indice = 0;
		max_cant_graf = 30;
		puntos = 500;

		par = new double[3];
		datosX= new double[max_cant_graf][puntos];

		datosY = new double[max_cant_graf][puntos];
		voltajes = new double[2];
		Vbase = new double[max_cant_graf];
		Pcarga = new double[max_cant_graf];
		FPcarga= new double[max_cant_graf];
		bolFPcarga = new boolean[max_cant_graf];
		comp = 2;
	}


	private double x,Vb,Sb,P,cos_phi,R,L,Gr,C;
	private double paso;
	private boolean inductivo,gradiente;

	public void getParam(double xx,double Vbb,double Sbb,double PP,double cos_phii,double RR,double LL,double Grr,double CC,boolean inductivoo,boolean gradientee){
		x=xx;//largo
		Vb=Vbb;//Voltaje base
		Sb=Sbb;//Potencia base
		P=PP;//potencia de carga
		cos_phi=cos_phii;//coseno pi de la carga
		R=RR;//resistencia de la linea
		L=LL;//inductancia de la linea
		C=CC;//capacitancia de la linea
		Gr=Grr;//Suceptancia capacitiva de la linea
		inductivo=inductivoo;//carga inductiva o capacitiva?
		gradiente=gradientee;//graficar gradiente o voltaje?



	}//end getParam(%%)

	private ParamsData p;
	private double par[];
	private double datosX[][];
	private double datosY[][];
	private double voltajes[];
	private double Vbase[];
	private double Pcarga[];
	private double FPcarga[];
	private double VoltajeBueno;
	private double distancia;
	private double comp;
	private boolean bolFPcarga[];

	private int indice;
	private int max_cant_graf;
	private int puntos;
	private double maxY,maxX,minY,minX;


	public void realizarCalculos(){ //primero actualizar par�metros antes de llamar a esta funci�n!
		Zb = Vb*Vb/Sb;
		paso = x/puntos; //siempre se grafica con 500 puntos (puntos = 500)
		//p = new ParamsData();

		for(int i=1;i<=puntos;i+=1){
			p = LLCP(R,L,Gr,C,i*paso,Zb);
			par = CalculaGradiente(p,P,cos_phi,inductivo,Sb,i*paso);
			datosX[indice][i-1]=par[0];//x
			if (gradiente) datosY[indice][i-1]=par[1];
			else datosY[indice][i-1]=par[2];//y
		}//for

		maxY = Maximo(datosY[indice]);
		minY = Minimo(datosY[indice]);
		maxX = Maximo(datosX[indice]);
		minX = Minimo(datosX[indice]);

		Vbase[indice] = Vb;
		Pcarga[indice] = P;
		FPcarga[indice] = cos_phi;
		bolFPcarga[indice] = inductivo;
		indice++;
		indice = indice%max_cant_graf;

	}






	public void paint (Graphics G) {


		Graphics g = getGraphics ();
//desde aca
		// obtiene dimensiones efectivas del canvas
		int largoX = (getPreferredSize()).width ;
        int largoY = (getPreferredSize()).height ;

		// selecciona el color blanco
		g.setColor (Color.white);

		// pinta de blanco todo el canvas
		g.fillRect (0 , 0 ,largoX ,largoY);

		// selecciona el color negro
		g.setColor (Color.black);

		// dibuja ejes
		g.drawLine (2*largoX/20 , 2*largoY/20 ,2*largoX/20 , 18*largoY/20);
		g.drawLine (2*largoX/20 ,18*largoY/20, 18*largoX/20 , 18*largoY/20);



		int pasoX = (int)16*largoX/(20*10);
		int pasoY = (int)16*largoY/(20*10);
		int x0 = (int)2*largoX/20 ;
		int y0 = (int)18*largoY/20 ;

		if(gradiente) g.drawString("G[%]",x0-10,largoY/20);
		else g.drawString("V[pu]",x0-10,largoY/20);
		g.drawString("d[Km]",18*largoX/20+10,y0);

		String sX = "";
		String sY = "";
		double aX = 0;
		double aY = minY;


		for (int y=1;y<=10;y++){


			g.drawLine(x0+y*pasoX,y0,x0+y*pasoX,y0-5);
			aX += maxX/10;
			sX = String.valueOf(Truncar(aX,1));
			g.drawString(sX,x0+(y*pasoX-15),y0+largoY/20);

			g.drawLine(x0,y0-y*pasoY,x0+5,y0-y*pasoY);
			aY += (maxY-minY)/10;
			sY = String.valueOf(Truncar(aY,detectaTrunc(maxY,minY)));
			g.drawString(sY,x0-(25+6*detectaTrunc(maxY,minY)),y0-y*pasoY+5);
//hasta aca
		}//for
		indiceColor=8;
		boolean GraficoCae = true;
		for (int i = 0;i<indice;i++){
			indiceColor++;
			indiceColor = indiceColor%9;
			g.setColor(colores[indiceColor]);

			GraficoCae = (datosY[i][0]-datosY[i][1]>0);
			for ( int  j = 0 ; j < puntos ; j++ ) {


					int posX = (int) (datosX[i][j]*(16*largoX/20)/maxX) ;
					int posY = (int) (datosY[i][j]*(16*largoY/20)/(maxY-minY)) ;
					int posExtraY = (int) (minY*(16*largoY/20)/(maxY-minY)) ;

					if(j==puntos/20){
						String bolfp;
						if(bolFPcarga[i]){bolfp="ind";}else{bolfp="cap";}
						if(GraficoCae) g.drawString("V = " + String.valueOf((int)Vbase[i]) + "[kV], " +"\n P = "+String.valueOf((int)Pcarga[i])+"[MW], "+"fp = "+String.valueOf(FPcarga[i])+" "+bolfp,x0 + posX ,y0 - posY +posExtraY+120+i*30);
						else g.drawString("V = " + String.valueOf((int)Vbase[i]) + "[kV], " +"\n P = "+String.valueOf((int)Pcarga[i])+"[MW], "+"fp = "+String.valueOf(FPcarga[i])+" "+bolfp,x0 + posX ,y0 - posY +posExtraY-230+i*30);
					}
					g.fillOval (x0 + posX ,y0 - posY +posExtraY,3 ,3);
			}//for
		}//for
	}//paint



	public void blanquear(){
		indice = 0;
		Graphics g = getGraphics();
		this.paint(g);

	}



}


