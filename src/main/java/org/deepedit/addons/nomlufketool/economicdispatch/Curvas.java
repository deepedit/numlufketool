package org.deepedit.addons.nomlufketool.economicdispatch;

/*
 * Curvas.java
 *
 * Created on July 4, 2003, 12:18 AM
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;


/**
 *
 * @author  Loli
 */
public class Curvas extends Grafico {
    
    private double[] beta = null;
    private double[] gamma = null;
    private double demanda = 0;
    
    private boolean limites = false;
    private double[] min = null;
    private double[] max = null;
    
    private double lambda = 0;
	private double lambda_print = 0;
    private double[] p = {0,0,0};
	private double[] p_print = {0,0,0};
    
    private boolean factible = true;
    
    DespachoEcono eco;
    
    /** Creates new form Curvas */
    public Curvas() {
        initComponents();
    }
    
    public Curvas(DespachoEcono eco) {
        initComponents();
        this.eco = eco;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents

    }//GEN-END:initComponents
    
    public void setBeta(double[] b) {
        beta=b;
    }
    
    public void setGamma(double[] g) {
        gamma=g;
    }
    
    public void setDemanda(double a) {
        demanda=a;
    }
    
    public void setLimites(boolean limites) {
        this.limites = limites;
    }
    
    public void setMinimos(double[] mi) {
        min=mi;
    }
    
    public void setMaximos(double[] ma) {
        max=ma;
    }
    
    public double[] getP() {
        return p_print;
    }
    
    public double getLambda() {
        return lambda_print;
    }
    
    public boolean isFactible() {
        return factible;
    }
    
    public void setFactible(boolean fact) {
        factible=fact;
    }
    
    @Override
    public void paint(java.awt.Graphics graphics) {
        super.paint(graphics);
        /*Ver si hay que dibujar*/
        if(demanda !=0 && gamma != null) {
            
            /*factible*/
            factible = true;
            
            /*Caso sin restricciones*/
            /*C�lculo de Lambda*/
            lambda = calculaLambda(beta, gamma, demanda);
            /*Potencias en cada generador*/
            p = calculaP(beta, gamma, lambda, p);
            /*Fin caso sin restricciones*/
            
            /*Posibles casos con l�mites*/
            if(limites) {
                double sum_gamma = 0;
                for(int j = 0; j<gamma.length; j++)
                    sum_gamma = sum_gamma + gamma[j];
                
                if (sum_gamma == 0){
                    /*Caso lista de merito*/
                    double sum_min = 0;
                    double dem_aux = demanda;
                    for(int j = 0; j < min.length; j++){
                        sum_min = sum_min + min[j];
                        p[j]=min[j];
                    }
                    dem_aux = dem_aux - sum_min;
                    
                    int[] baratos = masBaratos(beta);
                    for(int j = 0; j < p.length; j++){
                        if (dem_aux - (max[baratos[j]]-min[baratos[j]]) < 0){
                            p[baratos[j]] = dem_aux;
                            lambda = beta[baratos[j]];
                            break;
                        }
                        else {
                            p[baratos[j]] = max[baratos[j]];
                            dem_aux = dem_aux - (max[baratos[j]]-min[baratos[j]]);
                        }
                    }
                    /*Fin caso lista de merito*/
                }
                else {
                    /*veo cuantos gammas = 0*/
                    int n_g0 = 0;
                    int elem_0 = 0;
                    int no_0 = 0;
                    for(int j = 0; j < gamma.length; j++){
                        if(gamma[j]==0){
                            n_g0 = n_g0 + 1;
                            elem_0 = j;
                        }
                        else {
                            no_0 = j;
                        }
                    }
                    /*Si hay un gamma cero*/
                    /*Caso en que alg�n gamma es cero*/
                    if(n_g0 == 1) {
                        double[] p1 = null;
                        double l1 = 0;
                        double[] p2 = null;
                        double l2 = 0;
                        double d_aux;
                        double generaci�n_t;
                        /*Primero al m�nimo*/
                        d_aux = demanda;
                        p[elem_0] = min[elem_0];
                        d_aux = d_aux - min[elem_0];
                        
                        lambda = calculaLambda(beta, gamma, d_aux);
                        p = calculaP(beta, gamma, lambda, p);
                        max_min(d_aux);
                        generaci�n_t = 0;
                        for(int j = 0; j < p.length;j++)
                            generaci�n_t = generaci�n_t + p[j];
                        if(generaci�n_t != demanda){
                            lambda = calculaLambda(beta, gamma, d_aux);
                            p = calculaP(beta, gamma, lambda, p);
                            min_max(d_aux);
                            generaci�n_t = 0;
                            for(int j = 0; j < p.length;j++)
                                generaci�n_t = generaci�n_t + p[j];
                            if(generaci�n_t == demanda) {
                                p1 = copiaArray(p);
                                l1=lambda;
                            }
                        }
                        else{
                            p1 = copiaArray(p);
                            l1 =lambda;
                        }
                        
                        /*Luego al maximo*/
                        d_aux = demanda;
                        p[elem_0] = max[elem_0];
                        d_aux = d_aux - max[elem_0];
                        if(d_aux>0){
                            lambda = calculaLambda(beta, gamma, d_aux);
                            p = calculaP(beta, gamma, lambda, p);
                            max_min(d_aux);
                            generaci�n_t = 0;
                            for(int j = 0; j < p.length;j++)
                                generaci�n_t = generaci�n_t + p[j];
                            if(generaci�n_t != demanda){
                                lambda = calculaLambda(beta, gamma, d_aux);
                                p = calculaP(beta, gamma, lambda, p);
                                min_max(d_aux);
                                generaci�n_t = 0;
                                for(int j = 0; j < p.length;j++)
                                    generaci�n_t = generaci�n_t + p[j];
                                if(generaci�n_t == demanda){
                                    p2 = copiaArray(p);
                                    l2 = lambda;
                                }
                            }
                            else {
                                p2 = copiaArray(p);
                                l2 = lambda;
                            }
                        }
                        
                        /*Comparo Resultados*/
                        if (p1 == null && p2 == null){
                            for(int k = 0; k < p.length; k++)
                                p[k]=0;
                            lambda = 0;
                            factible = false;
                        }
                        else if (p1 == null) {
                            p=p2;
                            lambda=l2;
                        }
                        else if (p2 == null){
                            p=p1;
                            lambda=l1;
                        }
                        else {
                            double c_gen1 = 0;
                            double c_gen2 = 0;
                            for(int j = 0; j<p.length; j++) {
                                c_gen1 = c_gen1 + beta[j]*p1[j]+gamma[j]*p1[j]*p1[j];
                                c_gen1 = c_gen1 + beta[j]*p2[j]+gamma[j]*p2[j]*p2[j];
                            }
                            if (c_gen1<=c_gen2){
                                p=p1;
                                lambda=l1;
                            }
                            else {
                                p=p2;
                                lambda=l2;
                            }
                        }
                    }
                    /*Fin caso en que alg�n gamma es cero*/
                    
                    /*Si hay dos*/
                    else if (n_g0 == 2){
                        double[] costos = new double[4];
                        for(int j = 0; j < 4; j++)
                            costos[j] = costosDosCeros(j,no_0);
                        int[] barato = masBaratos(costos);
                        int ind_sol = 0;
                        for(ind_sol = 0; ind_sol < barato.length; ind_sol++) {
                            System.out.println(barato[ind_sol]+" "+costos[barato[ind_sol]]+" "+no_0);
                            if(costos[barato[ind_sol]] != -1){
                                double opt = costosDosCeros(barato[ind_sol], no_0);
                                lambda = calculaLambda(beta, gamma, p[no_0]);
                                break;
                            }
                        }
                        if (ind_sol==barato.length){
                            factible = false;
                            for(int k = 0; k < p.length; k++)
                                p[k]=0;
                            lambda = 0;
                        }
                    }
                    /*fin caso dos gammas = 0*/
                    else {
                        /*Caso con restricciones y gammas != 0*/
                        /*primero voy fijando los m�ximos*/
                        max_min(demanda);
                        
                        /*Ver si la condici�n de generaci�n es correcta*/
                        double generaci�n_t = 0;
                        for(int j = 0; j < p.length;j++)
                            generaci�n_t = generaci�n_t + p[j];
                        
                        /*Si no funcion� lo anterior fijo primero los m�nimos*/
                        if(generaci�n_t != demanda){
                            p = calculaP(beta, gamma, lambda, p);
                            min_max(demanda);
                        }
                        /*Caso con restricciones y gammas != 0*/
                    }
                }/*Cierra else*/
            }/*Cierra l�mites*/
            
            /*Trunco valores*/
            for(int j = 0; j < p.length;j++)
            p_print[j] = (int)(Math.round(p[j]*10000))/10000.0;
            lambda_print =(int)(Math.round(lambda*10000))/10000.0;
            
            /*Tama�o del panel donde dibujar*/
            int ancho = this.getWidth();
            int alto = this.getHeight();
            double escala = (alto/(3*lambda));
            
            /*Coordenadas 'y' de las ecuaciones, solo para graficar*/
            int[][] y = calculaY(beta, gamma, ancho, escala);
            
            /*Casos Extra�os en que las pendientes son muy peque�as*/
            if (lambda_print < 0) {
                for (int k = 0; k < p.length; k++) {
                    p_print[k] = 0;
                }
                lambda_print = 0;
                factible = false;
            }
			
            /*Dibujo de los Gr�ficos*/
            if (factible){
//                super.paint(graphics);
                
                Color c = new Color(0,51,255);
                int i = 0;
                while(i < p.length) {
                    int n_gen = i+1;
                    //System.out.println(i+" "+p[i]+"\t"+gamma[i]);
                    if (i == 0) c = new Color(0,51,255);
                    if (i == 1) c = new Color(0,153,0);
                    if (i == 2) c = new Color(153,0,153);
                    graphics.setColor(c);
                    if (alto-20-y[i][1] < 20)
                        graphics.drawLine(20, alto-20-y[i][0], (int)(((alto-40)*escala-beta[i])/(2*gamma[i])+20), 20);
                    else
                        graphics.drawLine(20, alto-20-y[i][0], ancho-20, alto-20-y[i][1]);
                    graphics.drawString("G"+n_gen+":"+p_print[i]+"",(int)p[i]+20,alto-25-15*i);
                    
                    c = new Color(153,153,153);
                    graphics.setColor(c);
                    double li = beta[i]+2*gamma[i]*p[i];
					double li_print = (int)(Math.round((beta[i]+2*gamma[i]*p[i])*10000))/10000.0;
                    graphics.drawLine((int)p[i]+20, alto-20, (int)p[i]+20, (int)(alto-20-li*escala));
                    
                    if(li > lambda) {
                        c = new Color(180,0,0);
                        graphics.setColor(c);
                        graphics.drawLine(15, (int)(alto-20-li*escala), ancho-20, (int)(alto-20-li*escala));
                        graphics.drawString("CMg"+n_gen+": "+li_print+"",ancho-130-130*i,(int)(alto-25-li*escala));
                    }
                    
                    if(li < lambda) {
                        c = new Color(153,153,0);
                        graphics.setColor(c);
                        graphics.drawLine(15, (int)(alto-20-li*escala), ancho-20, (int)(alto-20-li*escala));
                        graphics.drawString("CMg"+n_gen+": "+li_print+"",ancho-130-130*i,(int)(alto-25-li*escala));
                    }
                    
                    i++;
                }
                
                c = new Color(255,0,0);
                graphics.setColor(c);
                graphics.drawLine(15, (int)(alto-20-lambda*escala), ancho-20, (int)(alto-20-lambda*escala));
                graphics.drawString("L: "+lambda_print+"",ancho-70,(int)(alto-25-lambda*escala));
            }
            else {
                //System.out.println("No factible!");
//                super.paint(graphics);
                Color c = new Color(0,0,0);
                graphics.setColor(c);
                graphics.drawString("Problema no factible de solucionar",(int)(ancho/2-100),(int)(alto/2));
            }
        }
        eco.setValores();
    }
    
    private int[][] calculaY(double[] b, double[] g, int ancho, double escala) {
        int[][] y = new int[b.length][2];
        int i = 0;
        while(i < b.length) {
            y[i][0] = (int)(b[i]*escala);
            y[i][1] = (int)((b[i]+2*g[i]*(ancho-40))*escala);
            
            i++;
        }
        return y;
    }
    
    private double calculaLambda(double[] b, double[] g, double d) {
        double num = d;
        double den = 0;
        int i = 0;
        while(i < g.length){
            if(g[i] != 0) {
                num = num + 1.0*b[i]/(2*g[i]);
                den = den + 1.0/(2*g[i]);
            }
            i++;
        }
        double l =num/den;
        return l;
    }
    
    private double[] calculaP(double[] b, double[] g, double l, double[] pot) {
        int i = 0;
        while (i < b.length){
            if(g[i] != 0) {
                pot[i] = (l-b[i])/(2*g[i]);
            }
            i++;
        }
        return pot;
    }
    
    private double[] copiaArray(double[] a) {
        double[] array = new double[a.length];
        double tmp = 0;
        int i = 0;
        while (i < array.length) {
            tmp = a[i];
            array[i] = tmp;
            i++;
        }
        return array;
    }
    
    private void max_min(double dem) {
        double[] g_aux = copiaArray(gamma);
        double d_aux = dem;
        int i = 0;
        for(int j = 0; j < p.length; j++) {
            i = 0;
            while(i < p.length) {
                if(p[i] > max[i]) {
                    g_aux[i] = 0;
                    d_aux = d_aux - max[i];
                    lambda = calculaLambda(beta, g_aux, d_aux);
                    p = calculaP(beta, g_aux, lambda, p);
                    p[i] = max[i];
                }
                i++;
            }
            i = 0;
            while(i < p.length) {
                if(p[i] < min[i]) {
                    g_aux[i] = 0;
                    d_aux = d_aux - min[i];
                    lambda = calculaLambda(beta, g_aux, d_aux);
                    p = calculaP(beta, g_aux, lambda, p);
                    p[i] = min[i];
                }
                i++;
            }
        }
    }
    
    private void min_max(double dem) {
        double[] g_aux = copiaArray(gamma);
        double d_aux = dem;
        int i = 0;
        for(int j = 0; j < p.length; j++) {
            i = 0;
            while(i < p.length) {
                if(p[i] < min[i]) {
                    g_aux[i] = 0;
                    d_aux = d_aux - min[i];
                    lambda = calculaLambda(beta, g_aux, d_aux);
                    p = calculaP(beta, g_aux, lambda, p);
                    p[i] = min[i];
                }
                i++;
            }
            i = 0;
            while(i < p.length) {
                if(p[i] > max[i]) {
                    g_aux[i] = 0;
                    d_aux = d_aux - max[i];
                    lambda = calculaLambda(beta, g_aux, d_aux);
                    p = calculaP(beta, g_aux, lambda, p);
                    p[i] = max[i];
                }
                i++;
            }
            
        }
    }
    
    private int[] masBaratos(double[] beta2) {
        double[] b = copiaArray(beta2);
        int[] ind = new int[b.length];
        for(int k = 0; k < ind.length; k++)
            ind[k]=k;
        
        for(int j = b.length; j > 1; j--){
            for(int i = 0; i < j-1; i++){
                if(b[i] > b[i+1]){
                    double tmp = b[i];
                    b[i]=b[i+1];
                    b[i+1]=tmp;
                    int tmp_int = ind[i];
                    ind[i]=ind[i+1];
                    ind[i+1]=tmp_int;
                }
            }
        }
        //        System.out.println(ind[0]+" "+ind[1]+" "+ind[2]);
        //        System.out.println(b[0]+" "+b[1]+" "+b[2]);
        //        System.out.println(beta[0]+" "+beta[1]+" "+beta[2]);
        return ind;
    }
    
    private double costosDosCeros(int comb, int nocero) {
        /*Comb 0=>min,min, 1=>min,max, 2=>max,min, 3=>max,max*/
        double d_aux = demanda;
        if(comb==0) {
            for(int j = 0; j<p.length; j++)
                if (j != nocero){
                    p[j] = min[j];
                    d_aux = d_aux-p[j];
                }
        }
        if(comb==1){
            for(int j = 0; j<p.length; j++)
                if (j != nocero){
                    p[j] = min[j];
                }
            for(int j = p.length-1; j>=0; j--)
                if (j != nocero){
                    p[j] = max[j];
                    break;
                }
            for(int j = 0; j<p.length; j++)
                if (j != nocero)
                    d_aux = d_aux-p[j];
        }
        if(comb==2) {
            for(int j = 0; j<p.length; j++)
                if (j != nocero){
                    p[j] = max[j];
                }
            for(int j = p.length-1; j>=0; j--)
                if (j != nocero){
                    p[j] = min[j];
                    break;
                }
            for(int j = 0; j<p.length; j++)
                if (j != nocero)
                    d_aux = d_aux-p[j];
        }
        if(comb==3) {
            for(int j = 0; j<p.length; j++)
                if (j != nocero){
                    p[j] = max[j];
                    d_aux = d_aux-p[j];
                }
        }
        
        p[nocero] = d_aux;
        double c_gen = 0;
        for(int j = 0; j<p.length; j++)
            if (p[j]<min[j] || p[j]>max[j])
                return -1;
        for(int j = 0; j<p.length; j++)
            c_gen = c_gen + beta[j]*p[j]+gamma[j]*p[j]*p[j];
        return c_gen;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
}


class Grafico extends JPanel {

    public Grafico() {
        initComponents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());

        setBackground(new Color(255, 255, 255));
        setPreferredSize(new Dimension(420, 420));
    }

    public void paint(Graphics paramGraphics) {
        super.paint(paramGraphics);

        int i = getWidth();
        int j = getHeight();

        paramGraphics.drawLine(20, j - 20, i - 20, j - 20);
        paramGraphics.drawString("Potencia", i - 60, j - 25);

        paramGraphics.drawLine(20, 20, 20, j - 20);
        paramGraphics.drawString("Costos", 25, 20);
    }
}

//class Grafico extends Canvas {
//
//    /* Variables de instancia del canvas */
//    public double Pmax;
//    public double Ppost;
//    public double Pdura;
//    public double Pmec;
//    public double Pnegro;
//    public double Pverde;
//    public double Pceleste1;
//    public double Pceleste2;
//    public double Projo;
//    public double Pamarillo;
//    public double eac;
//
//    int aux2;
//    int ancho;
//    int alto;
//    int gAlto;
//    int gAncho;
//    int gBordeX;
//    int gBordeY;
//
//    private int altura(double Pmax, double h) {
//        double alturaPantallaMax = gAlto / 15.0 * 14.0;
//        return gBordeY + gAlto - (int) (h * alturaPantallaMax / Pmax);
//    }
//
//    private int ancho(double w) {
//        return (int) (w / Math.PI * gAncho);
//    }
//
//    @Override
//    public void paint(Graphics g) {
//
//        NumberFormat nf = NumberFormat.getInstance();
//        nf.setMaximumFractionDigits(3);
//
//        // Calcular los bordes
//        ancho = (int) (this.getSize().getWidth());
//        alto = (int) (this.getSize().getHeight());
//        gAlto = alto * 7 / 10;
//        gAncho = ancho * 8 / 10;
//        gBordeX = ancho / 10;
//        gBordeY = alto / 10;
//
//        //Ejes
//        g.drawLine(gBordeX, gBordeY, gBordeX, gBordeY + gAlto);
//        g.drawLine(gBordeX, gBordeY + gAlto, gBordeX + gAncho + 10, gBordeY + gAlto);
//        g.drawString("CT[$]", gBordeX - 35, gBordeY + gAlto / 8);
//        g.drawString("Pmax[MW]", gBordeX + gAncho, gBordeY + gAlto + 12);
////        g.drawString("Pi/2", (gBordeX + gAncho) / 2, gBordeY + gAlto + 12);
//        g.drawString("0", gBordeX, gBordeY + gAlto + 12);
//
//        //area azul y magenta
//        double area1 = 0.0;
//        double area2 = 0.0;
//        int aux = 0;
//        Pamarillo = 0.0;
////
////        // graficar las 4 regiones
////        for (int i = ancho(Pnegro); i < ancho(Pceleste2); ++i) {
////            double y;
////            y = Math.sin(Math.PI * i / gAncho);
////            if (i < ancho(Pverde)) {
////                y *= Pdura;
////            } else {
////                y *= Ppost;
////            }
////
////            if (y > Pmec) {
////                g.setColor(Color.magenta);
////                area2 += (y - Pmec) / gAncho;
////            } else {
////                g.setColor(Color.blue);
////                area1 += (Pmec - y) / gAncho;
////            }
////
////            if (aux == 0 && area1 < area2) {
////                Pamarillo = (double) i / gAncho * Math.PI;
////                aux = i;
////            }
////
////            g.drawLine(gBordeX + i, altura(Pmax, Pmec), gBordeX + i, altura(Pmax, y));
////            if (eac == 1 && aux == i) {
////                break;
////            }
////        }
//
//        //Incluyo las areas en el grafico
////        g.drawString("�rea Frenado:" + nf.format(area2), gBordeX + gAncho - 100, gBordeY + 12);
////        g.setColor(Color.blue);
////        g.drawString("�rea Aceleraci�n:" + nf.format(area1), gBordeX + gAncho - 100, gBordeY);
////        g.setColor(Color.red);
////        if (area2 > area1) {
////            g.drawString("Sistema ESTABLE", gBordeX + gAncho - 100, altura(Pmax, 0.0) + 36);
////        } else {
////            g.drawString("Sistema INESTABLE!!", gBordeX + gAncho - 100, altura(Pmax, 0.0) + 36);
////        }
////
////        // Potencia mec�nica
////        g.setColor(Color.blue);
////        g.drawLine(gBordeX, altura(Pmax, Pmec), gBordeX + gAncho, altura(Pmax, Pmec));
////        g.drawString("Pm =" + nf.format(Pmec), gBordeX + 10, altura(Pmax, Pmec) - 2);
////        g.drawString("Pmax =" + nf.format(Pmax), (gBordeX + ancho) / 2 - 50, altura(Pmax, Pmax) - 12);
////
////        // graficar las l�neas de Potencia
////        for (int i = 0; i < gAncho; i++) {
////            double y1, y2;
////            y1 = Math.sin((Math.PI * i / gAncho));
////            y2 = Math.sin((Math.PI * (i + 1) / gAncho));
////
////            g.setColor(Color.black);
////            g.drawLine(gBordeX + i, altura(Pmax, Pmax * y1), gBordeX + i + 1, altura(Pmax, Pmax * y2));
////            g.setColor(Color.cyan);
////            g.drawLine(gBordeX + i, altura(Pmax, Ppost * y1), gBordeX + i + 1, altura(Pmax, Ppost * y2));
////            g.setColor(Color.red);
////            g.drawLine(gBordeX + i, altura(Pmax, Pdura * y1), gBordeX + i + 1, altura(Pmax, Pdura * y2));
////        }
////
////        // graficar las 4 l�neas y los �gulos importantes
////        double y;
////        g.setColor(Color.black);
////        g.drawLine(gBordeX + ancho(Pnegro), altura(Pmax, Pmec), gBordeX + ancho(Pnegro), altura(Pmax, 0.0));
////        g.setColor(Color.cyan);
////        y = Ppost * Math.sin(Pceleste1);
////        g.drawLine(gBordeX + ancho(Pceleste1), altura(Pmax, y), gBordeX + ancho(Pceleste1), altura(Pmax, 0.0));
////        g.drawLine(gBordeX + ancho(Pceleste2), altura(Pmax, y), gBordeX + ancho(Pceleste2), altura(Pmax, 0.0));
////        g.setColor(Color.green);
////        y = Ppost * Math.sin(Pverde);
////        g.drawLine(gBordeX + ancho(Pverde), altura(Pmax, y), gBordeX + ancho(Pverde), altura(Pmax, 0.0));
////        g.setColor(Color.red);
////        y = Ppost * Math.sin(Projo);
////        g.drawLine(gBordeX + ancho(Projo), altura(Pmax, y), gBordeX + ancho(Projo), altura(Pmax, 0.0));
////        g.setColor(Color.yellow);
////        y = Ppost * Math.sin(Pamarillo);
////        g.drawLine(gBordeX + ancho(Pamarillo), altura(Pmax, y), gBordeX + ancho(Pamarillo), altura(Pmax, 0.0));
////
////        g.setColor(Color.cyan);
////        g.drawString("� =" + nf.format(Pceleste2), ancho(Pceleste2), altura(Pmax, 0.0) + 12);
////        g.drawString("� =" + nf.format(Pceleste1), ancho(Pceleste1), altura(Pmax, 0.0) + 12);
////        g.setColor(Color.green);
////        g.drawString("� =" + nf.format(Pverde), ancho(Pverde), altura(Pmax, 0.0) + 24);
////        g.setColor(Color.black);
////        g.drawString("� =" + nf.format(Pnegro), gBordeX + ancho(Pnegro), altura(Pmax, 0.0) - 5);
////        g.setColor(Color.red);
////        y = Ppost * Math.sin(Projo);
////        g.drawString("� =" + nf.format(Projo), ancho(Projo), altura(Pmax, 0.0) - 17);
//    }
//
//    public void repaint(Graphics g) {
//        paint(g);
//    }
//}
