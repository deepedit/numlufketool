package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import javax.swing.*;

public class PanelCirculo extends JPanel{


		JPanel Panel;

		JSeparator s1,s2,s3;
		JLabel foto;

		Top top;
		Zoom zoom;
		Acciones acciones;
		Lienzo lienzo;
		Izquierdo izquierdo;


public PanelCirculo() {
        initComponents();
    }

private void initComponents() {

	GridBagConstraints gc=new GridBagConstraints();

	Panel=new JPanel();

	Panel.setLayout(new FlowLayout());
	Panel.setMaximumSize(new java.awt.Dimension(180, 350));
	Panel.setMinimumSize(new java.awt.Dimension(180, 350));
	Panel.setPreferredSize(new java.awt.Dimension(180, 350));

	s1=new JSeparator();
	s2=new JSeparator();
	s3=new JSeparator();
	s1.setOrientation(javax.swing.SwingConstants.VERTICAL);
	s3.setOrientation(javax.swing.SwingConstants.VERTICAL);
	foto=new JLabel();
	top=new Top(3);
	zoom=new Zoom(200,100);
	acciones=new Acciones(200,100);
	lienzo=new Lienzo(410,310);
	izquierdo=new Izquierdo();
	setLayout(new GridBagLayout());

	gc.gridx=0;
	gc.gridy=0;
	gc.gridwidth=5;
	gc.gridheight=1;
	add(top,gc);

	gc.gridx=0;
	gc.gridy=1;
	gc.gridwidth=1;
	gc.gridheight=3;
	Panel.add(izquierdo);
	add(Panel,gc);

	gc.gridx=2;
	gc.gridy=1;
	gc.gridwidth=3;
	gc.gridheight=1;
	add(lienzo,gc);

	gc.gridx=2;
	gc.gridy=3;
	gc.gridwidth=1;
	gc.gridheight=1;
	add(acciones,gc);

	gc.gridx=4;
	gc.gridy=3;
	gc.gridwidth=1;
	gc.gridheight=1;
	add(zoom,gc);

	gc.gridx=1;
	gc.gridy=1;
	gc.gridwidth=1;
	gc.gridheight=3;
	gc.fill = GridBagConstraints.VERTICAL;
	add(s1,gc);

/*
	gc.gridx=2;
	gc.gridy=2;
	gc.gridwidth=3;
	gc.gridheight=1;
	gc.fill = GridBagConstraints.HORIZONTAL;
	add(s2,gc);
*/
	gc.gridx=3;
	gc.gridy=3;
	gc.gridwidth=1;
	gc.gridheight=1;
	gc.fill = GridBagConstraints.VERTICAL;
	add(s3,gc);

/*
	gc.gridx=0;
	gc.gridy=3;
	gc.gridwidth=1;
	gc.gridheight=1;
	add(foto,gc);
*/

	acciones.Metodos(zoom,lienzo,izquierdo);
	lienzo.Metodos(zoom,acciones);
	izquierdo.Metodos(acciones,lienzo,zoom);


	}
}