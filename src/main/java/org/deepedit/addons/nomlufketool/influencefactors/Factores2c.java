package org.deepedit.addons.nomlufketool.influencefactors;

public class Factores2c extends javax.swing.JApplet {

    public Factores2c() {
		Ayuda ayuda=new Ayuda();
        conCarga cC=new conCarga();
        sinCarga sC=new sinCarga();
		cC.Metodos(sC,ayuda);
		sC.Metodos(cC,ayuda);
		ayuda.Metodos(sC,cC);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(cC);
        getContentPane().add(sC);
		getContentPane().add(ayuda);
        cC.setVisible(false);
        sC.setVisible(false);
        ayuda.setVisible(true);
        repaint();
     }
}