package org.deepedit.addons.nomlufketool.utils;

/*
 * Complex.java
 *
 * Created on June 15, 2003, 12:48 AM
 */

/**
 *
 * @author  midgard
 */
public class Complex {
    
    public double re;
    public double im;
    
    public Complex() {
        re=0;
        im=0;
    }

    public Complex(double r,double i)
    {
        re=r;
        im=i;
    }
    
    public Complex(double abs, double ang, int x){
        re = abs*Math.cos(Math.toRadians(ang)); 
        im = abs*Math.sin(Math.toRadians(ang));       
    }
    @Override
    public String toString() {
        if (im >= 0) {
            return round(re, 4) + "+j" + round(im, 4);
        } else {
            return round(re, 4) + "-j" + round(Math.abs(im), 4);
        }
    }

    private double round(double x,int k)
    {
            return Math.round( Math.pow(10,k) * x )/Math.pow(10,k);
    }
	
    static public double abs(Complex x)
    {
        return Math.sqrt(x.re*x.re+x.im*x.im);
    }
    
    static public double argument(Complex x)
    {
        return Math.atan2(x.im,x.re);
    }
    
    static public double argument_degree(Complex x)
    {
        return Math.toDegrees(argument(x));
    }
    
    static public Complex conjugate(Complex x)
    {
        return new Complex( x.re, -x.im );
    }
    
    static public Complex exp(Complex x)
    {
        return new Complex( Math.exp(x.re) * Math.cos(x.im) ,
                            Math.exp(x.re) * Math.sin(x.im) );
    }
    
    static public Complex log(Complex x)
    {
            return new Complex( Math.log(abs(x)) , argument(x) );
    }
    
    static public Complex sqrt(Complex x)
    {
        return exp(multiply(log(x) , 0.5 ) );
	}
	
    static public Complex sinh(Complex x)
    {
        return new Complex( 0.5*( Math.cos(x.im) * (Math.exp(x.re)-Math.exp(-x.re)) ) ,
                            0.5*( Math.sin(x.im) * (Math.exp(x.re)+Math.exp(-x.re)) ) );
    }

    static public Complex cosh(Complex x)
    {
        return new Complex( 0.5*( Math.cos(x.im) * (Math.exp(x.re)+Math.exp(-x.re)) ) ,
                            0.5*( Math.sin(x.im) * (Math.exp(x.re)-Math.exp(-x.re)) ) );
    }
    
    static public Complex tanh(Complex x)
    {
            return Complex.divide(sinh(x) , cosh(x) );
    }
	
    static public Complex add(Complex x,Complex y)
    {
            return new Complex( x.re+y.re , x.im+y.im );
    }
	
    static public Complex add(Complex x,double y)
	{
		return new Complex( x.re+y , x.im );
	}
	
    static public Complex subtract(Complex x,Complex y)
	{
		return new Complex( x.re-y.re , x.im-y.im );
	}
	
    static public Complex multiply(Complex x,Complex y)
    {
        return new Complex( x.re*y.re - x.im*y.im ,
                            x.re*y.im + x.im*y.re );
    }
    
    static public Complex multiply(Complex y,double x)
    {
        return new Complex( x*y.re , x*y.im );
    }
    
    static public Complex divide(Complex x,Complex y)
    {
        return divide(Complex.multiply(x, conjugate(y) ) , abs(y)*abs(y) );
    }

    static public Complex divide(Complex x,double y)
    {
        return new Complex( x.re/y , x.im/y );
    }

    static public Complex Inv(Complex y)
    {
        return divide(conjugate(y) , abs(y)*abs(y) );
    }

    static public Complex Paralelo(Complex x, Complex y)
    {
            return Complex.divide(Complex.multiply(x,y) , Complex.add(x,y) );
    }
}