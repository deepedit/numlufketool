package org.deepedit.addons.nomlufketool.frequencycontrol;

public class calcpu {

    private double resultado, base, fisico, otro;

    /**
     * Calculates and parses H
     * @param b
     * @param f
     * @return h
     */
    public double h(String b, String f) {
        base = Double.parseDouble(b);
        fisico = Double.parseDouble(f);
        resultado = fisico / base;
        return resultado;
    }

    /**
     * Calculates and parses B
     * @param b
     * @param f
     * @param o
     * @return b
     */
    public double b(String b, String f, String o) {
        base = Double.parseDouble(b);
        fisico = Double.parseDouble(f);
        otro = Double.parseDouble(o);
        resultado = fisico * base / otro;
        return resultado;

    }
}
