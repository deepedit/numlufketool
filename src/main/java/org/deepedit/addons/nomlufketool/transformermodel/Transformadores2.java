package org.deepedit.addons.nomlufketool.transformermodel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
import java.io.*;
import java.net.*;

/**
 * Creates new Nom Lufke Transformer model toolbox viewer and help
 * @author Carlos Aravena, Patricio Mendoza, Frank Leanez
 */
public class Transformadores2 extends JApplet implements ActionListener, FocusListener {

    //componentes graficos
    static final int W = 780, H = 500;	// ancho y alto en pixeles
    Color fondo = new Color(0.81f, 0.87f, 0.97f);
    JLabel titulo = new JLabel("Transformador Trifasico");

    JLabel inferior;

    /* panel de datos trifasico */
    JLabel v_nominal_3 = new JLabel("V [kV]");	//panel1_1
    JLabel l_at1 = new JLabel("AT");
    JLabel l_bt1 = new JLabel("BT");
    JLabel p_nominal_3 = new JLabel("Potencia");
    JLabel conexion = new JLabel("Conexion");
    JLabel relleno1 = new JLabel();
    JTextField v_at_3 = new JTextField("3.811");
    JTextField v_bt_3 = new JTextField("0.3811");
    JTextField pot_3 = new JTextField("30.0");
    Connection[] conex = {Connection.Yy, Connection.Yd, Connection.Dy, Connection.Dd};
    String[] desfases = Connection.Yy.getDesfases();
    String[] tipo_potencia = {"kVA", "MVA"};

    JComboBox conexion_ch = new JComboBox(conex);
    JComboBox potencia1_ch = new JComboBox(tipo_potencia);
    JComboBox desfase_ch = new JComboBox(desfases);

    /* datos panel monofasico */
    JLabel v_nominal_1 = new JLabel("V [kV]");	//panel 1_2
    JLabel l_at2 = new JLabel("AT");
    JLabel l_bt2 = new JLabel("BT");
    JLabel relleno2 = new JLabel();
    JLabel p_nominal_1 = new JLabel("Potencia");
    JLabel relleno5 = new JLabel();
    JTextField v_at_1 = new JTextField("2.2");
    JTextField v_bt_1 = new JTextField("0.22");
    JTextField pot_1 = new JTextField("10.0");
    JComboBox potencia2_ch = new JComboBox(tipo_potencia);
//  JButton tri2mono = new JButton("3>1");
//  JButton mono2tri = new JButton("3<1");
    /* datos prueba CA */
    JLabel v_ca = new JLabel("Voltaje [kV]");	//panel 1_3
    JLabel i_ca = new JLabel("Corriente [A]");
    JLabel p_ca = new JLabel("Potencia[kW]");
//  JLabel fp_ca = new JLabel ("FP");
    JRadioButton ca_primario = new JRadioButton("en AT", false);
    JRadioButton ca_secundario = new JRadioButton("en BT", true);
    ButtonGroup ca_group = new ButtonGroup();
    JTextField v_ca_tx = new JTextField("0.22");
    JTextField i_ca_tx = new JTextField("2.5");
    JTextField pot_ca_tx = new JTextField("0.1");
//  JTextField fp_ca_tx = new JTextField ();

    /* datos prueba CC */
    JLabel v_cc = new JLabel("Voltaje [kV]");	//panel 1_4
    JLabel i_cc = new JLabel("Corriente [A]");
    JLabel p_cc = new JLabel("Potencia[kW]");
//  JLabel fp_cc = new JLabel ("FP");
    JRadioButton cc_primario = new JRadioButton("en AT", true);
    JRadioButton cc_secundario = new JRadioButton("en BT", false);
    ButtonGroup cc_group = new ButtonGroup();
    JTextField v_cc_tx = new JTextField("0.15");
    JTextField i_cc_tx = new JTextField("4.55");
    JTextField pot_cc_tx = new JTextField("0.215");
//  JTextField fp_cc_tx = new JTextField ();

    JTabbedPane dibujos = new JTabbedPane();	//panel 2
    JTabbedPane resultados = new JTabbedPane();
    Canvas conexiones = new Canvas() {
        @Override
        public void paint(Graphics g) {
            drawConex(g);
        }
    };

    JLabel modelo = new JLabel(new ImageIcon(getClass().getResource("/transformermodel/images/modelo.gif")));	//panel 2_1
    JLabel rm1 = new JLabel("Rm [Ohm]=");
    JLabel rm2 = new JLabel();
    JLabel xm1 = new JLabel("Xm [Ohm] =");
    JLabel xm2 = new JLabel();
    JLabel req1 = new JLabel("Req [Ohm]=");
    JLabel req2 = new JLabel();
    JLabel xeq1 = new JLabel("Xeq [Ohm]=");
    JLabel xeq2 = new JLabel();
    JLabel razon1 = new JLabel("N1:N2 =");
    JLabel razon2 = new JLabel();

    JTextArea result = new JTextArea("");	//panel 2_2
    JTextArea observaciones = new JTextArea("Aqui van las observaciones acerca del tipo de conexion.");

    JButton limpiar = new JButton("Borrar");	//panel superior
    JButton calcular = new JButton("Calcular");
    JButton help = new JButton("Ayuda");

    /**
     * Creates a new Transformer applet
     */
    public Transformadores2() {
        init();
    }
  
    /**
     * Creates new Nom Lufke Transformer model toolbox viewer and help
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     * @return new Nom Lufke Economic Dispatch toolbox viewer and help (not
     * visible!)
     */
    public static JDialog showInFrame(JDialog parent, boolean modal) {

        JDialog d = new JDialog(parent, modal);
        Transformadores2 trafo = new Transformadores2();
        d.getContentPane().add(trafo.getContentPane());
        d.pack();
        return d;

    }
  
    /**
     * Inicia el applet
     */
    public void init() {

        //creacion de paneles
        JPanel p0 = new JPanel();	//panel maestro
        JPanel p1 = new JPanel();	//panel superior
        JPanel p2 = new JPanel();	//panel central
        JPanel p3 = new JPanel();	//panel inferior

        JPanel p1_1 = new JPanel();	//panel izq
        JPanel p1_2 = new JPanel();	//panel centro. izq
        JPanel p1_3 = new JPanel();	//panel centro. der
        JPanel p1_4 = new JPanel();	//panel der

        JPanel p2_1 = new JPanel();	//panel modelo
        JPanel p2_1_1 = new JPanel();	//panel datos modelo
        JPanel p2_2 = new JPanel();	//panel resultados

        JPanel superior = new JPanel();	//titulo y control

        //distribucion de cada panel
        p0.setLayout(new BorderLayout());
        p1.setLayout(new GridLayout(1, 4));

        superior.setLayout(new GridLayout(2, 1));

        JPanel PanelBotones = new JPanel();
        PanelBotones.setLayout(new GridLayout(1, 3));

        JPanel PanelTitulo = new JPanel();
        PanelTitulo.setBackground(new Color(204, 204, 204));
        JLabel LabelTitulo = new JLabel("Transformador Trif�sico");
        LabelTitulo.setFont(new Font("Dialog", 1, 14));

        PanelTitulo.add(LabelTitulo);

        JPanel PanelInferior = new JPanel();
        PanelInferior.setBackground(new Color(204, 204, 204));

        inferior = new JLabel(" ");

        PanelInferior.add(inferior);

        p1_1.setLayout(new GridLayout(4, 3));
        p1_1.setBorder(BorderFactory.
                createTitledBorder("Datos Placa Trf. Trifasico"));
        p1_2.setLayout(new GridLayout(4, 3));
        p1_2.setBorder(BorderFactory.
                createTitledBorder("Datos Trf. Monofasico"));
        p1_3.setLayout(new GridLayout(4, 2));
        p1_3.setBorder(BorderFactory.createTitledBorder("Datos Prueba CA"));
        p1_4.setLayout(new GridLayout(4, 2));
        p1_4.setBorder(BorderFactory.createTitledBorder("Datos Prueba CC"));
        p2.setLayout(new GridLayout(1, 2));
        p2_1.setLayout(new BorderLayout());
        p2_1.setBackground(fondo);
        p2_1_1.setLayout(new GridLayout(3, 4));
        p2_1_1.setBackground(fondo);
        p2_1_1.setBorder(BorderFactory.
                createTitledBorder("Valores de parametros del modelo monofasico"));
        p2_2.setLayout(new GridLayout(1, 1));
        p2_2.setBackground(fondo);
        result.setBackground(fondo);
        observaciones.setBackground(fondo);
        dibujos.addTab("Conexiones", conexiones);
        dibujos.addTab("Modelo", p2_1);
        resultados.addTab("Resultados", p2_2);
        resultados.addTab("Observaciones", new JScrollPane(observaciones));

        // modificacion componentes
        conexion_ch.setLightWeightPopupEnabled(false);
        desfase_ch.setLightWeightPopupEnabled(false);
        observaciones.setLineWrap(true);
        observaciones.setWrapStyleWord(true);

        limpiar.setToolTipText("Borra todos los datos");
        calcular.setToolTipText("Calcula los parámetros");

        //agregrar componentes a panel
        p1_1.add(relleno1);
        p1_1.add(l_at1);
        p1_1.add(l_bt1);
        p1_1.add(v_nominal_3);
        p1_1.add(v_at_3);
        v_at_3.addFocusListener(this);
        p1_1.add(v_bt_3);
        v_bt_3.addFocusListener(this);
        p1_1.add(p_nominal_3);
        p1_1.add(pot_3);
        pot_3.addFocusListener(this);
        p1_1.add(potencia1_ch);
        potencia1_ch.addActionListener(this);
        p1_1.add(conexion);
        p1_1.add(conexion_ch);
        conexion_ch.addActionListener(this);
        p1_1.add(desfase_ch);
        desfase_ch.addActionListener(this);

        p1_2.add(relleno2);
        p1_2.add(l_at2);
        p1_2.add(l_bt2);
        p1_2.add(v_nominal_1);
        p1_2.add(v_at_1);
        v_at_1.addFocusListener(this);
        p1_2.add(v_bt_1);
        v_bt_1.addFocusListener(this);
        p1_2.add(p_nominal_1);
        p1_2.add(pot_1);
        pot_1.addFocusListener(this);
        p1_2.add(potencia2_ch);
        potencia2_ch.addActionListener(this);
        p1_2.add(relleno5); //    p1_2.add (mono2tri); mono2tri.addActionListener(this);
        p1_2.add(relleno5);
        p1_2.add(relleno5); //    p1_2.add (tri2mono); tri2mono.addActionListener(this);

        p1_3.add(v_ca);
        p1_3.add(v_ca_tx);
        v_ca_tx.addActionListener(this);
        p1_3.add(i_ca);
        p1_3.add(i_ca_tx);
        i_ca_tx.addActionListener(this);
        p1_3.add(p_ca);
        p1_3.add(pot_ca_tx);
        pot_ca_tx.addActionListener(this);
        pot_ca_tx.addFocusListener(this);
        /*
    p1_3.add (fp_ca);
    p1_3.add (fp_ca_tx); fp_ca_tx.addActionListener(this); fp_ca_tx.addFocusListener(this);
         */
        p1_3.add(ca_primario);
        p1_3.add(ca_secundario);
        ca_group.add(ca_primario);
        ca_group.add(ca_secundario);

        p1_4.add(v_cc);
        p1_4.add(v_cc_tx);
        v_cc_tx.addActionListener(this);
        p1_4.add(i_cc);
        p1_4.add(i_cc_tx);
        i_cc_tx.addActionListener(this);
        p1_4.add(p_cc);
        p1_4.add(pot_cc_tx);
        pot_cc_tx.addActionListener(this);
        pot_cc_tx.addFocusListener(this);
        /*
    p1_4.add (fp_cc);
    p1_4.add (fp_cc_tx); fp_cc_tx.addActionListener(this); fp_cc_tx.addFocusListener(this);
         */
        p1_4.add(cc_primario);
        p1_4.add(cc_secundario);
        cc_group.add(cc_primario);
        cc_group.add(cc_secundario);

        p2_1_1.add(rm1);
        p2_1_1.add(rm2);
        p2_1_1.add(xm1);
        p2_1_1.add(xm2);
        p2_1_1.add(req1);
        p2_1_1.add(req2);
        p2_1_1.add(xeq1);
        p2_1_1.add(xeq2);
        p2_1_1.add(razon1);
        p2_1_1.add(razon2);

        p2_1.add("Center", modelo);
        p2_1.add("South", p2_1_1);

        p2_2.add(new JScrollPane(result));

        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);
        p1.add(p1_4);

        p2.add(dibujos);
        p2.add(resultados);

        PanelBotones.add(limpiar);
        PanelBotones.add(calcular);
        PanelBotones.add(help);

        superior.add(PanelTitulo);
        superior.add(PanelBotones);

        limpiar.addActionListener(this);
        /*   superior.add (calcular);*/ calcular.addActionListener(this);
        /*    superior.add (help);*/ help.addActionListener(this);

        limpiar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                limpiarMouseEntered(evt);
            }
        });

        calcular.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                calcularMouseEntered(evt);
            }
        });

        help.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                helpMouseEntered(evt);
            }
        });

        p0.add("North", p1);
        p0.add("Center", p2);

        //distribucion de la ventana
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.setSize(W, H);
        contentPane.add("North", superior);
        contentPane.add("Center", p0);
        contentPane.add("South", PanelInferior);
    }

    private void limpiarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldZsMouseEntered
        inferior.setText("Este bot�n borra todos los datos.");
    }//GEN-LAST:event_jTextFieldZsMouseEntered

    private void calcularMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldZsMouseEntered
        inferior.setText("Este bot�n calcula los par�metros.");
    }//GEN-LAST:event_jTextFieldZsMouseEntered

    private void helpMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldZsMouseEntered
        inferior.setText("Elija esta opci�n si necesita Ayuda.");
    }//GEN-LAST:event_jTextFieldZsMouseEntered


    /**
     * implementacion de ActionListener
     * @param e
     */
  @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == calcular) {
            calcula();
        }

        /* cambios en el trifasico */
        if (e.getSource() == potencia1_ch || e.getSource() == conexion_ch || e.getSource() == desfase_ch) {
            potencia2_ch.removeActionListener(this);
            t2m();
            potencia2_ch.addActionListener(this);
        } /* cambios en el monofasico */ else if (e.getSource() == potencia2_ch) {
            potencia1_ch.removeActionListener(this);
            conexion_ch.removeActionListener(this);
            desfase_ch.removeActionListener(this);
            m2t();
            potencia1_ch.addActionListener(this);
            conexion_ch.addActionListener(this);
            desfase_ch.addActionListener(this);
        }

        /* boton limpiar */
        if (e.getSource() == limpiar) {
            cleanAll();
        }

        /* cambio de tipo de conexion */
        if (e.getSource() == conexion_ch) {
            actualizaDesfases();
            conexiones.repaint();
        }
        if (e.getSource() == desfase_ch) {
            conexiones.repaint();
        }
        /* ayuda */
        if (e.getSource() == help) {
            showHTML("Ayuda", "/transformermodel/help.html");
        }

    }

    /**
     * implementacion de FocusListener
     * @param e
     */
    public void focusGained(FocusEvent e) {
        /* generalmente nada */
    }

    /**
     * implementacion de FocusListener
     * @param e
     */
    public void focusLost(FocusEvent e) {
        /* deja un dato trifasico */
        if (e.getSource() == v_at_3 || e.getSource() == v_bt_3 || e.getSource() == pot_3) {
            potencia2_ch.removeActionListener(this);
            t2m();
            potencia2_ch.addActionListener(this);
        } /* deja un dato monofasico */ else if (e.getSource() == v_at_1 || e.getSource() == v_bt_1 || e.getSource() == pot_1) {
            potencia1_ch.removeActionListener(this);
            conexion_ch.removeActionListener(this);
            desfase_ch.removeActionListener(this);
            m2t();
            potencia1_ch.addActionListener(this);
            conexion_ch.addActionListener(this);
            desfase_ch.addActionListener(this);
        }
        /* actualizar fp y potencia de las pruebas */

    }

    /**
     * Calcula los parametros a partir de los datos hasta ahora introducidos por
     * el usuario.
     * <p>
     */
    public void calcula() {
        /* voltajes en kV y potencias */
        try {
            double v1 = Double.parseDouble(v_at_1.getText()) * 1000;
            double v2 = Double.parseDouble(v_bt_1.getText()) * 1000;
            double s = Double.parseDouble(pot_1.getText());
            if (potencia2_ch.getSelectedItem() == tipo_potencia[0]) // kVA
            {
                s = s * 1e3;
            } else //MVA
            {
                s = s * 1e6;
            }

            razon2.setText("" + trunca(v1 / v2));

            try {
                Base b1 = new Base(s, v1);
                Base b2 = new Base(s, v2);

                Trafo tr1 = new Trafo();
                tr1.setBases(b1, b2);


                /* prueba de CA */
                try {
                    double vca = Double.parseDouble(v_ca_tx.getText()) * 1000;
                    double ica = Double.parseDouble(i_ca_tx.getText());
                    double pca = Double.parseDouble(pot_ca_tx.getText()) * 1000;
                    if (ca_primario.isSelected()) {
                        tr1.setFromOCPrim(vca, ica, pca);
                    } else if (ca_secundario.isSelected()) {
                        tr1.setFromOCSec(vca, ica, pca);
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "Error numerico en datos de C.A.:\n" + e.getMessage());
                }

                /* prueba de CC */
                try {
                    double vcc = Double.parseDouble(v_cc_tx.getText()) * 1000;
                    double icc = Double.parseDouble(i_cc_tx.getText());
                    double pcc = Double.parseDouble(pot_cc_tx.getText()) * 1000;
                    if (cc_primario.isSelected()) {
                        tr1.setFromSCPrim(vcc, icc, pcc);
                    } else if (cc_secundario.isSelected()) {
                        tr1.setFromSCSec(vcc, icc, pcc);
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "Error numerico en datos de C.C.:\n" + e.getMessage());
                }

                /* obtenemos el trifasico a partir de monofasico */
                int d = Integer.parseInt((String) desfase_ch.getSelectedItem());
                Connection c = (Connection) conexion_ch.getSelectedItem();
                Trafo3f tr3 = new Trafo3f(c, d, tr1);

                Trafo tpu = tr3.getPU();
                Base3f b3 = tr3.getBasePrim();/* referido a AT */
                Base3f b3s = tr3.getBaseSec();
                /* referido a BT */

                rm2.setText("" + trunca(tpu.getR0() * b3.getZ()));
                xm2.setText("" + trunca(tpu.getX0() * b3.getZ()));
                req2.setText("" + trunca((tpu.getR1() + tpu.getR2()) * b3.getZ()));
                xeq2.setText("" + trunca((tpu.getX1() + tpu.getX2()) * b3.getZ()));

                //calculo de corrientes
                double il1 = tr3.getBasePrim().getS() / tr3.getBasePrim().getV() / Math.sqrt(3.0);
                double il2 = tr3.getBaseSec().getS() / tr3.getBaseSec().getV() / Math.sqrt(3.0);
                double ie1, ie2;
                if (c == Connection.Yy) {
                    ie1 = il1;
                    ie2 = il2;
                } else if (c == Connection.Yd) {
                    ie1 = il1;
                    ie2 = il2 / Math.sqrt(3.0);
                } else if (c == Connection.Dy) {
                    ie1 = il1 / Math.sqrt(3.0);
                    ie2 = il2;
                } else //(c==Connection.Dd)
                {
                    ie1 = il1 / Math.sqrt(3.0);
                    ie2 = il2 / Math.sqrt(3.0);
                }
                //calculo de perdidas
                double p_fe = Double.parseDouble(pot_ca_tx.getText());
                double p_cu = Double.parseDouble(pot_cc_tx.getText());

                //resultados
                DecimalFormat fmt_eta = new DecimalFormat("0.#######E0");
                /* rendimiento a potencia nominal */
                double eta = b3s.getS() / (b3s.getS() + p_fe * 3 + p_cu * 3);
                try {
                    eta = fmt_eta.parse(fmt_eta.format(eta)).doubleValue();
                } catch (ParseException x) {
                    eta = trunca(eta);
                }

                result.setText(
                        "Datos Transformador trif�sico:\n"
                        + "------------------------------\n"
                        + "Voltaje Fase-Fase Prim [kV]\t=\t" + trunca(b3.getV() / 1000) + "\n"
                        + "Voltaje Fase-Fase Sec  [kV]\t=\t" + trunca(b3s.getV() / 1000) + "\n"
                        + "Voltaje Fase-Neutro Prim [kV]\t=\t" + trunca(b3.getV() / 1000 / Math.sqrt(3.0)) + "\n"
                        + "Voltaje Fase-Neutro Sec  [kV]\t=\t" + trunca(b3s.getV() / 1000 / Math.sqrt(3.0)) + "\n" + "\n"
                        + "Corriente Linea Prim  [A]\t=\t" + trunca(il1) + "\n"
                        + "Corriente Linea Sec   [A]\t=\t" + trunca(il2) + "\n"
                        + "Corriente Enrollado Prim [A]\t=\t" + trunca(ie1) + "\n"
                        + "Corriente Enrollado Sec  [A]\t=\t" + trunca(ie2) + "\n" + "\n"
                        + "Desfase [grados]\t=\t" + 30 * d + "\n"
                        + "Perdidas Fe [kW]\t=\t" + trunca(3 * p_fe) + "\n"
                        + "Perdidas Cu [kW]\t=\t" + trunca(3 * p_cu) + "\n"
                        + "Rendimiento\t\t=\t" + eta + "\n"
                        + "\n"
                        + "Parametros Transformador en p.u.:\n"
                        + "----------------------------\n"
                        + "r0\t=\t" + trunca(tpu.getR0()) + "\n"
                        + "x0\t=\t" + trunca(tpu.getX0()) + "\n"
                        + "r1\t=\t" + trunca(tpu.getR1()) + "\n"
                        + "x1\t=\t" + trunca(tpu.getX1()) + "\n"
                        + "r2\t=\t" + trunca(tpu.getR2()) + "\n"
                        + "x2\t=\t" + trunca(tpu.getX2()) + "\n"
                );
                // observaciones
                String conex = "";
                String armonicos = "";
                String carga_desb = "";

                if (c == Connection.Yy) {
                    conex = "Los transformadores estrella-estrella se emplean para tensiones altas, ya que las bobinas requieren de menos aislaci�n producto de estar sometidas a una tensi�n fase-neutro (menor que la fase-fase) Facilitan la alimentaci�n de redes tetrafilares de distribuci�n, al tener un neutro en baja.\n Casi siempre se trabajan con ambos neutros conectados a tierra para minimizar los problemas de circulaci�n de arm�nicas y cargas desbalancedas.";
                    armonicos = "La componente de tercera arm�nica de la corriente de excitaci�n, s�lo puede circular si el neutro se encuentra puesto a tierra. De no ser este el caso, la corriente contiene s�lo componentes de quinta arm�nica y superiores. Por otro lado, las fem inducidas en este caso, contienen una fuerte componente de tercera arm�nica (de hasta 70%) que hace crecer el valor m�ximo de la tensi�n fase-neutro a cifras cercanas a la tensi�n entre fases.\n En caso de que el sistema de alimentaci�n tenga su neutro conectado a tierra, aparecer� la tensi�n de tercera arm�nica (150 Hz) entre el punto com�n de la estrella y tierra, y el neutro oscilar� a triple frecuencia.\n Para minimizar la circulaci�n de arm�nicas se utilizan transformadores tipo n�cleo.";
                    carga_desb = "Debido al corrimiento del neutro en tensiones fase-neutro desequilibradas, esta conexi�n no debe usarse ante el riesgo freceunte de cargas desbalancedas.";
                } else if (c == Connection.Yd) {
                    conex = "El transformador estrella-delta es el m�s empleado en las centrales, por ser m�s econ�mico al aprovechar las ventajas de la estrella en el lado de mayor tensi�n, y las de la delta en el lado de menor tensi�n. La delta estabiliza el neutro de la estrella, eliminando el principal problema de dicha conexi�n. Se utiliza principalmente como reductora de tensi�n en redes de transmisi�n  y distribuci�n de energ�a en que no se requiera neutro en el secundario.";
                    armonicos = "La tercera arm�nica de la corriente de magnetizaci�n, circula sin ocasionar problemas de sobreexcitaci�n por los enrrollados del secundario (sin salir a las líneas)";
                    carga_desb = "El desequilibrio en las cargas del secundario, se transmite al primario de manera compensada para cada fase, de manera muy similar a la conexión Dd. No se produce sobreexcitación en ninguna de las fases.";
                } else if (c == Connection.Dy) {
                    conex = "Esta conexión es la más ampliamente utilizada como elevadora de tensión y en redes de distribución en que se requiera neutro. En este último caso, la delta en el lado de alta tensión aísla a la red de transmisión de los efectos de cargas desbalancedas y armónicas en la corriente.";
                    armonicos = "La tercera armónica de la corriente de magnetización se establece en la delta del primario, sin afectar a la red secundaria si ésta no cierra su retorno por tierra.";
                    carga_desb = "Al igual que en el caso de conexión Dd y Yd, el desequilibrio en las cargas secundarias no afecta en gran medida, debido a la acción neutralizadora de la delta primaria en las tres fases. No se produce sobreexcitación por corriente en ningun de las fases.";
                } else //if(c==Connection.Dd)
                {
                    conex = "En el transformador en delta-delta, los enrollados van dispuestos entre fases, formando un anillo cerrado. El aislamiento requerido es mayor, puesto que corresponde a la tensión entre fases, mientras que las corrientes de delta son menores que las de línea, de modo que se requiere menos cobre.\n Se emplean, cuando se tienen corrientes de línea altas, para aprovechar el hecho de que las corrientes de delta son más bajas. Como carece de neutros, no posibilita la alimentación de redes tetrafilares. Una ventaja importante es que si falla una de las fases, sigue transformando trifásicamente. Se emplean por ello, en servicios auxiliares de subestaciones u otros consumos pequeños pero importantes.";
                    armonicos = "Las terceras armónicas de la corriente de excitación pueden circular dentro de la delta, por lo que las fem inducidas serán sinusoidales. Sin embargo, las terceras armónicas (y sus múltiplos) no saldrán hacia el sistema de alimentación, ya que se anulan al restar las corrientes de delta para formar la corriente de línea. Pese a ello, estas últimas corrientes no serán sinusoidales, porque estarán presentes las quintas armónicas, de secuencia negativa, y otras superiores.";
                    carga_desb = "La corriente producida por una carga monofásica entre fases del secundario provoca corrientes en el primario, distribuidas en forma equitativa por los enrollados. No se produce sobreexcitación alguna en las fases y sólo existe un desequilibrio de tensiones debido a la modificación de las impedancias.";
                }
                observaciones.setText(
                        "Conexion:\n"
                        + "----------\n"
                        + conex + "\n"
                        + "\n"
                        + "Armonicos:\n"
                        + "----------\n"
                        + armonicos + "\n"
                        + "\n"
                        + "Cargas Desbalanceadas:\n"
                        + "---------------------\n"
                        + carga_desb + "\n"
                        + "\n"
                );

                dibujos.setSelectedIndex(1);

            } catch (TrafoException e) {
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Error numerico en datos de trafo:\n" + e.getMessage());
        }

    }

    /**
     * convierte los datos del monofasico a trifasico, como si se tratara de un
     * banco de trafos monofasicos.
     */
    public void m2t() {
        /* voltajes en kV y potencias monofasicas */
        try {
            double v1 = Double.parseDouble(v_at_1.getText()) * 1000;
            double v2 = Double.parseDouble(v_bt_1.getText()) * 1000;
            double s = Double.parseDouble(pot_1.getText());
            if (potencia2_ch.getSelectedItem() == tipo_potencia[0]) // kVA
            {
                s = s * 1e3;
            } else //MVA
            {
                s = s * 1e6;
            }

            try {
                Base b1 = new Base(s, v1);
                Base b2 = new Base(s, v2);

                Trafo tr1 = new Trafo();
                tr1.setBases(b1, b2);


                /* obtenemos el trifasico a partir de monofasico */
                int d = Integer.parseInt((String) desfase_ch.getSelectedItem());
                Connection c = (Connection) conexion_ch.getSelectedItem();
                Trafo3f tr3 = new Trafo3f(c, d, tr1);

                /* AT */
                Base3f b3 = tr3.getBasePrim();
                v_at_3.setText("" + trunca(b3.getV() / 1000));
                /* BT */
                b3 = tr3.getBaseSec();
                v_bt_3.setText("" + trunca(b3.getV() / 1000));
                /* Potencia 3f */
                double s3 = s * 3;
                if (s3 >= 1e6) {
                    /* MVA */
                    pot_3.setText("" + trunca(s3 / 1e6));
                    potencia1_ch.setSelectedIndex(1);
                } else {
                    /* kVA */
                    pot_3.setText("" + trunca(s3 / 1e3));
                    potencia1_ch.setSelectedIndex(0);
                }

            } catch (TrafoException e) {
            }

        } catch (NumberFormatException e) {
//		JOptionPane.showMessageDialog(this,"Error numerico en datos de trafo:\n" + e.getMessage() );
        }

    }

    /**
     * convierte los datos de trifasico a monofasico, suponiendo que el
     * trifasico se construye como un banco de trafos monofasicos.
     */
    void t2m() {
        /* voltajes en kV y potencias trifasicas */
        try {
            double v1 = Double.parseDouble(v_at_3.getText()) * 1000;
            double v2 = Double.parseDouble(v_bt_3.getText()) * 1000;
            double s = Double.parseDouble(pot_3.getText());
            if (potencia1_ch.getSelectedItem() == tipo_potencia[0]) // kVA
            {
                s = s * 1e3;
            } else //MVA
            {
                s = s * 1e6;
            }

            try {
                Base3f b1 = new Base3f(s, v1);
                Base3f b2 = new Base3f(s, v2);

                int d = Integer.parseInt((String) desfase_ch.getSelectedItem());
                Connection c = (Connection) conexion_ch.getSelectedItem();
                Trafo3f tr3 = new Trafo3f(c, d);
                tr3.setBases(b1, b2);

                /* obtenemos monofasico a partir de trifasico */
                Trafo tr1 = tr3.getTrafo();

                /* AT */
                Base bn = tr1.getBasePrim();
                v_at_1.setText("" + trunca(bn.getV() / 1000));
                /* BT */
                bn = tr1.getBaseSec();
                v_bt_1.setText("" + trunca(bn.getV() / 1000));
                /* Potencia 1f */
                double s1 = s / 3;
                if (s1 >= 1e6) {
                    /* MVA */
                    pot_1.setText("" + trunca(s1 / 1e6));
                    potencia2_ch.setSelectedIndex(1);
                } else {
                    /* kVA */
                    pot_1.setText("" + trunca(s1 / 1e3));
                    potencia2_ch.setSelectedIndex(0);
                }

            } catch (TrafoException e) {
            }

        } catch (NumberFormatException e) {
//		JOptionPane.showMessageDialog(this,"Error numerico en datos de trafo:\n" + e.getMessage() );
        }
    }

    /**
     * formato por defecto de los numeros
     */
    private static DecimalFormat fmt = new DecimalFormat("0.###E0");

    /**
     * trunca un numero a un formato fijo.
     * <p>
     * @param n	numero a truncar
     * @return	el numero truncado por el formato
     */
    private double trunca(double n) {
//	if(n>0.01)
//		return (Math.round(n*1e2))/1.0e2;
//	else
        try {
            return (fmt.parse(fmt.format(n))).doubleValue();
        } catch (ParseException e) {
            System.out.println("Error de formato: " + e.getMessage());
            return n;
        }
    }	

    /**
     * limpia las celdas de texto
     */
    public void cleanAll() {
        v_at_3.setText("");
        v_bt_3.setText("");
        pot_3.setText("");
        potencia1_ch.setSelectedIndex(0);
        conexion_ch.setSelectedIndex(0);
        desfase_ch.setSelectedIndex(0);

        v_at_1.setText("");
        v_bt_1.setText("");
        pot_1.setText("");
        potencia2_ch.setSelectedIndex(0);

        v_ca_tx.setText("");
        i_ca_tx.setText("");
        pot_ca_tx.setText("");
//	fp_ca_tx.setText("");

        v_cc_tx.setText("");
        i_cc_tx.setText("");
        pot_cc_tx.setText("");
//	fp_cc_tx.setText("");

        rm2.setText("");
        xm2.setText("");
        req2.setText("");
        xeq2.setText("");
        razon2.setText("");

        observaciones.setText("");
        result.setText("");
    }

    /**
     * a partir del tipo de conexion, rellena los desfases posibles
     */
    public void actualizaDesfases() {
        Connection c = (Connection) conexion_ch.getSelectedItem();
        String[] ds = c.getDesfases();
        desfase_ch.removeAllItems();
        for (int i = 0; i < ds.length; i++) {
            desfase_ch.addItem(ds[i]);
        }
        desfase_ch.setSelectedIndex(0);
        desfase_ch.repaint();
    }

    /**
     * redibuja el modo de conexion en el canvas
     * @param g
     */
    public void drawConex(Graphics g) {
        g.drawString("Dibujando...", 160, 160);
        conexiones.setBackground(Color.WHITE);
        Image ref = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/transformermodel/images/conex.gif"));
        int radio = 1;
        /* dibujamos imagen */
        while (g.drawImage(ref, 0, 0, dibujos) == false);

        /* dibujamos lineas segun tipo de conexion */
        Color color_line = new Color(0, 0, 0.6f);
        Connection c = (Connection) conexion_ch.getSelectedItem();

        double n1 = 1, n2 = 1;
        try {
            n1 = Double.parseDouble(v_at_3.getText());
            n2 = Double.parseDouble(v_bt_3.getText());
        } catch (NumberFormatException x) {
            n1 = 2.0;
            n2 = 1.0;
        }

        g.setColor(Color.RED);
        if (c == Connection.Yy) {
            g.drawLine(34, 70, 128, 70);//conexion y
            g.drawLine(34, 257, 128, 257);//conexion Y
            radio = (int) (93 * (n2 / n1));
        } else if (c == Connection.Yd) {
            g.drawLine(34, 70, 55, 70);//conexion d
            g.drawLine(55, 70, 55, 130);
            g.drawLine(55, 130, 80, 130);
            g.drawLine(80, 70, 100, 70);
            g.drawLine(100, 70, 100, 130);
            g.drawLine(100, 130, 128, 130);
            g.drawLine(128, 70, 128, 60);
            g.drawLine(128, 60, 15, 60);
            g.drawLine(15, 60, 15, 130);
            g.drawLine(15, 130, 34, 130);
            g.drawLine(34, 257, 128, 257);//conexion Y
            radio = (int) ((1 / Math.sqrt(3.0) * 93) * (n2 / n1));
        } else if (c == Connection.Dy) {
            g.drawLine(34, 70, 128, 70);//conexion y
            g.drawLine(34, 257, 55, 257);//conexion D
            g.drawLine(55, 257, 55, 191);
            g.drawLine(55, 191, 80, 191);
            g.drawLine(80, 257, 100, 257);
            g.drawLine(100, 257, 100, 191);
            g.drawLine(100, 191, 128, 191);
            g.drawLine(128, 257, 128, 267);
            g.drawLine(128, 267, 15, 267);
            g.drawLine(15, 267, 15, 191);
            g.drawLine(15, 191, 34, 191);
            radio = (int) (Math.sqrt(3.0) * 93 * (n2 / n1));
        } else // Connection.Dd
        {
            g.drawLine(34, 257, 55, 257);//conexion D
            g.drawLine(55, 257, 55, 191);
            g.drawLine(55, 191, 80, 191);
            g.drawLine(80, 257, 100, 257);
            g.drawLine(100, 257, 100, 191);
            g.drawLine(100, 191, 128, 191);
            g.drawLine(128, 257, 128, 267);
            g.drawLine(128, 267, 15, 267);
            g.drawLine(15, 267, 15, 191);
            g.drawLine(15, 191, 34, 191);
            g.drawLine(34, 70, 55, 70);//conexion d
            g.drawLine(55, 70, 55, 130);
            g.drawLine(55, 130, 80, 130);
            g.drawLine(80, 70, 100, 70);
            g.drawLine(100, 70, 100, 130);
            g.drawLine(100, 130, 128, 130);
            g.drawLine(128, 70, 128, 60);
            g.drawLine(128, 60, 15, 60);
            g.drawLine(15, 60, 15, 130);
            g.drawLine(15, 130, 34, 130);
            radio = (int) (93 * (n2 / n1));
        }

        /* Dibujar Vectores  */
        int d = Integer.parseInt(desfase_ch.getSelectedItem().toString());
        double desfase_rad = d * 30 * Math.PI / 180;

        if (radio < 20) {
            radio = 20;
            /* guardamos config. actual */
            Font fnt_orig = g.getFont();
            Color color_orig = g.getColor();
            /* mensaje */
            Font fnt_tiny = fnt_orig.deriveFont((float) 10.0);
            g.setFont(fnt_tiny);
            FontMetrics fnt_m = g.getFontMetrics(fnt_tiny);
            g.setColor(Color.BLACK);
            String msg = "Vectores no estan a escala";
            int px = conexiones.getWidth() - fnt_m.stringWidth(msg);
            int py = conexiones.getHeight() - fnt_m.getHeight();
            g.drawString(msg, px, py);
            g.setFont(fnt_orig);
            g.setColor(color_orig);
        }

        //calculo de coordenadas vectores
        int xa = 296 + (int) (radio * Math.sin(desfase_rad));
        int xb = 296 + (int) (radio * Math.sin(desfase_rad + 2.0 / 3.0 * Math.PI));
        int xc = 296 + (int) (radio * Math.sin(desfase_rad - 2.0 / 3.0 * Math.PI));
        int ya = 163 - (int) (radio * Math.cos(desfase_rad));
        int yb = 163 - (int) (radio * Math.cos(desfase_rad + 2.0 / 3.0 * Math.PI));
        int yc = 163 - (int) (radio * Math.cos(desfase_rad - 2.0 / 3.0 * Math.PI));
        //dibujar lineas
        g.drawLine(296, 163, xa, ya);
        g.drawLine(296, 163, xb, yb);
        g.drawLine(296, 163, xc, yc);
        g.setColor(Color.BLUE);
        g.drawString("a", xa, ya);
        g.drawString("b", xb, yb);
        g.drawString("c", xc, yc);

        /* nombre de las fases del secundario */
        int[] pos_nombres = {40, 88, 136};
        String[] nombres = new String[3];
        if (d == 0 || d == 1 || d == 6 || d == 7) {
            /* no hay permutacion */
            nombres[0] = "a";
            nombres[1] = "b";
            nombres[2] = "c";
        } else if (d == 4 || d == 5 || d == 10 || d == 11) {
            /* 1 permutacion */
            nombres[0] = "c";
            nombres[1] = "a";
            nombres[2] = "b";
        } else if (d == 8 || d == 9 || d == 2 || d == 3) {
            /* 2 permutaciones */
            nombres[0] = "b";
            nombres[1] = "c";
            nombres[2] = "a";
        }
        for (int i = 0; i < 3; i++) {
            g.drawString(nombres[i], pos_nombres[i], 146);
        }

        /* puntos de polaridad */
        boolean invierte_polaridad = false;
        int[] px = {42, 90, 136};
        int[] py = {200, 126, 78};
        if (((c == Connection.Yy || c == Connection.Dd)
                && (d == 6 || d == 10 || d == 2)) || ((c == Connection.Yd)
                && (d == 7 || d == 11 || d == 3)) || ((c == Connection.Dy)
                && (d == 1 || d == 5 || d == 9))) {
            invierte_polaridad = true;
        }
        for (int i = 0; i < 3; i++) {
            g.fillOval(px[i] - 2, py[0] - 2, 4, 4);
            g.fillOval(px[i] - 2, (invierte_polaridad ? py[2] : py[1]) - 2, 4, 4);
        }

        /* nombre */
        g.setFont(new Font("Arial", Font.BOLD, 14));
        g.drawString(c.toString() + desfase_ch.getSelectedItem().toString(), 173, 40);

    }

    private void showHTML(String title, String archivo) {
        try {
            URL paginita = getClass().getResource(archivo);
//            InputStream s_in = paginita.openStream();
//            BufferedReader in = new BufferedReader(new InputStreamReader(s_in));
//            String lineas = "";
//            try {
//                while (true) {
//                    String linea = in.readLine();
//                    if (linea == null) {
//                        break;
//                    }
//                    lineas += linea;
//                }
//            } catch (IOException q) {
//            }
            
            final JDialog dialog = new JDialog((Frame) null, title);
            dialog.setModal(true);
            JEditorPane label = new JEditorPane(paginita);
            label.setEditable(false);

            JScrollPane scrollPane = new JScrollPane(label);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

            JButton closeButton = new JButton("Cerrar");
            closeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            });
            JPanel closePanel = new JPanel();
            closePanel.setLayout(new BoxLayout(closePanel,
                    BoxLayout.LINE_AXIS));
            closePanel.add(Box.createHorizontalGlue());
            closePanel.add(closeButton);
            closePanel.setBorder(BorderFactory.
                    createEmptyBorder(0, 0, 5, 5));

            JPanel contentPane = new JPanel(new BorderLayout());
            contentPane.add(scrollPane, BorderLayout.CENTER);
            contentPane.add(closePanel, BorderLayout.PAGE_END);
            contentPane.setOpaque(true);
            dialog.setContentPane(contentPane);

            //Show it.
            dialog.setSize(new Dimension(400, 300));
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
        } catch (IOException x) {
            System.out.println(x.getMessage());
        }

    }

}
