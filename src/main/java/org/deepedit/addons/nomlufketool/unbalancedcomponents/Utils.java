package org.deepedit.addons.nomlufketool.unbalancedcomponents;
/*
 * @(#)Utils.java	 
 *
 * @author 	Fernando Eduardo Flatow Garrido
 * @version 	1.0, Septiembre de 2000
 */

import java.awt.*; 
import javax.swing.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Utils {  
    /*
     * Make tool  
     */
  
    public static Button makeButton(Object d, String name,GridBagLayout gridbag,GridBagConstraints c){ 
	Button button = new Button(name);
	button.setActionCommand(name); 
	gridbag.setConstraints(button, c); 
	((Panel)d).add(button);
	return button; 
    } 
 
    public static void makeLabel(Object d, String name,GridBagLayout gridbag,GridBagConstraints c){ 
	Label label = new Label(name); 
	gridbag.setConstraints(label, c); 
        ((Panel)d).add(label); 
    } 

    public static void makeLabel(Object d, String name,GridBagLayout gridbag,GridBagConstraints c, Color cl){ 
	Label label = new Label(name); 
	gridbag.setConstraints(label, c); 
	label.setForeground(cl); 
        ((Panel)d).add(label); 
    }  
 
    public static TextField  makeTField(Object d, String content,GridBagLayout gridbag,GridBagConstraints c,int width){ 
	TextField tfield = new TextField(content, width); 
	gridbag.setConstraints(tfield, c); 
	((Panel)d).add(tfield); 
	return tfield; 
    } 
 
    public static Choice  makeChoiceMetodo(Object d, String item0, GridBagLayout gridbag, GridBagConstraints c){ 
	Choice cchoice = new Choice();
	cchoice.addItem("Hard K-Means");      
	cchoice.addItem("Fuzzy C-Means");      
	cchoice.addItem("Gustafson-Kessel");      
	cchoice.select(item0); 
	gridbag.setConstraints(cchoice, c); 
	((Panel)d).add(cchoice); 	 
	return cchoice; 
    } 
    public static JComboBox  makeJComboBoxMetodo(Object d, String item0, GridBagLayout gridbag, GridBagConstraints c){ 
	JComboBox cchoice = new JComboBox();
	cchoice.addItem("Hard C-Means");      
	cchoice.addItem("Fuzzy C-Means");    
	cchoice.addItem("Gustafson-Kessel");        
	cchoice.setSelectedItem(item0);        
	gridbag.setConstraints(cchoice, c); 
	((JPanel)d).add(cchoice); 	 
	return cchoice; 
    } 

    public static JButton makeJButton(Object d, String name,GridBagLayout gridbag,GridBagConstraints c){ 
      JButton button = new JButton(name);
      button.setActionCommand(name); 
      gridbag.setConstraints(button, c); 
      ((JPanel)d).add(button);
      return button; 
    } 


    public static JButton makeJButtonIcon(Object d, String name,String name2,GridBagLayout gridbag,GridBagConstraints c){ 
	ImageIcon image = new ImageIcon(name2);  	
	JButton button = new JButton(name, image);
	button.setActionCommand(name); 
	gridbag.setConstraints(button, c); 
	((JPanel)d).add(button);
	return button; 
    } 

  public static JRadioButton makeJRadioButton(Object d, String name,GridBagLayout gridbag,GridBagConstraints c){ 
      JRadioButton rbutton = new JRadioButton(name);
      rbutton.setActionCommand(name); 
      gridbag.setConstraints(rbutton, c); 
      ((JPanel)d).add(rbutton);
      return rbutton; 
  } 
 
  public static void makeJLabel(Object d, String name,GridBagLayout gridbag,GridBagConstraints c){ 
      JLabel label = new JLabel(name); 
      gridbag.setConstraints(label, c); 
      ((JPanel)d).add(label); 
  } 

  public static void makeJLabel(Object d, String name,GridBagLayout gridbag,GridBagConstraints c, Color cl){ 
      JLabel label = new JLabel(name); 
      gridbag.setConstraints(label, c); 
      label.setForeground(cl); 
      ((JPanel)d).add(label); 
  }  

  public static JLabel makeJLabel2(Object d, String name,GridBagLayout gridbag,GridBagConstraints c, Color cl){ 
      JLabel label = new JLabel(name); 
      gridbag.setConstraints(label, c); 
      label.setForeground(cl); 
      ((JPanel)d).add(label);
      return label;
  }  

 
    public static JTextField  makeJTField(Object d, String content,GridBagLayout gridbag,GridBagConstraints c,int width){ 
	JTextField tfield = new JTextField(content, width); 
	gridbag.setConstraints(tfield, c); 
	((JPanel)d).add(tfield); 
	return tfield; 
    } 
 
} 


