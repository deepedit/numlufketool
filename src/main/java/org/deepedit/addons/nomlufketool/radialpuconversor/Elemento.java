package org.deepedit.addons.nomlufketool.radialpuconversor;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
/**
 * @author Rodrigo de la Castilleja
 *
 */
public class Elemento extends JLabel {
//  Dimension minSize = new Dimension (50, 50);
//  Dimension prefSize = new Dimension (50, 50);
  int tipo,tconex;
  ImageIcon nada,CL,L,C,T,G;
  boolean[] campos;
  double Potencia,PotenciaI,VoltajeA,VoltajeB,Impedancia,ImpedanciaI,ImpedanciaPUAI,ImpedanciaPUA,PBase,IBC,ZBase,VBase;  
  double FP,numero;
  boolean ind,alta;
  String tipoda;
  PU pu;
  
  public Elemento (ImageIcon nadax,ImageIcon CLx, ImageIcon Lx,ImageIcon Cx,ImageIcon Tx,ImageIcon Gx,PU pux){
	  nada = nadax;
	  CL = CLx;
	  L = Lx;
	  C = Cx;
	  T = Tx;
	  G = Gx;
	  pu=pux;
	  setIcon(nada);
	  setType(0);
	  reset();
	  tipoda="Potencia";
  }
  
  public void setType(int n){
	  tipo=n;
	  switch (n){ 
		case 0: {setIcon(nada); boolean[] tmp= {false,false,false,false}; campos = tmp; break;} 
		case 1: {setIcon(L); boolean[] tmp= {false,false,false,true};campos = tmp; break;}
		case 2: {setIcon(C); boolean[] tmp= {false,false,false,false};campos = tmp; break;}
		case 3: {setIcon(T); boolean[] tmp= {true,true,true,false};campos = tmp;   break;}
		case 4: {setIcon(G); boolean[] tmp= {false,true,false,false};campos = tmp; break;}
		case 5: {setIcon(CL); boolean[] tmp= {false,false,false,false};campos = tmp; break;}
		// en orden de declaracion, Potencia, VoltajeA, VoltajeB,referido a alta?
		}
  }
  
  public String toString(){
	  switch (tipo){ 
		case 0: {return "nada";} 
		case 1: {return "l�nea";}
		case 2: {return "consumo";}
		case 3: {return "tranformador";}
		case 4: {return "generador";}
		case 5: {return "consumo";}
	  }
		return "";
  }
  
  public void getParametros(){
	if (this.tipo != 0){
	JDialog param = new Param(this,pu);
	param.show();}
  }
  
  public boolean getCampos(int n){
  	return campos[n];
  }
  
  public ImageIcon getImageIcon(){
  switch (tipo){ 
	case 0: {return nada;} 
	case 1: {return L;}
	case 2: {return C;}
	case 3: {return T;}
	case 4: {return G;}
	case 5: {return CL;}
	}
	return null;
  }
  
  public String caracteristica(int n){
	if (this.tipo == 1 && n ==2) return ""; 
	if ((Impedancia    != 0 || ImpedanciaI !=0) && n ==0){ 
		if(ImpedanciaI >= 0)  return (String)(Impedancia+"+j"+ImpedanciaI+" Ohm");
		else return (String)(Impedancia+"-j"+(-1)*ImpedanciaI+" Ohm");
		}
	if ((ImpedanciaPUA != 0 || ImpedanciaPUAI !=0)&& n ==0){
		if(ImpedanciaPUAI >= 0)return (String)(ImpedanciaPUA*100+"+j"+ImpedanciaPUAI*100+"%");
		else return (String)(ImpedanciaPUA*100+"-j"+(-1)*ImpedanciaPUAI*100+"%");
		}
	if (PotenciaI != 0 && n ==0)  return (String)(PotenciaI+" MVAR");
	if (FP !=0 && n ==0 ) return (String)("FP "+FP);
	if (Potencia != 0      && n == 1) return (String)(""+ Potencia+ " MVA");
	if (this.tipo == 3 && n ==2) return (String)(VoltajeA+"/"+VoltajeB);
	if (this.tipo == 4 && n ==2) return (String)(VoltajeB+" KV,");
	return "";
  }
  
    //double Potencia,VoltajeA,VoltajeB,Impedancia,ImpedanciaPUA,PBase,IBC;  
  public double calcula(){
	if (ImpedanciaPUA != 0){
		IBC=(ImpedanciaPUA/numero)*PBase/Potencia;
	}
	if (Impedancia != 0 ){	
			if( alta  && this.VoltajeB > this.VoltajeA )
				IBC=Impedancia/ZBase;
			if( alta  && this.VoltajeB < this.VoltajeA )
				IBC=Impedancia/ZBase*(VoltajeB/VoltajeA)*(VoltajeB/VoltajeA);
			if( !alta && this.VoltajeB > this.VoltajeA )
				IBC=Impedancia/ZBase*(VoltajeA/VoltajeB)*(VoltajeA/VoltajeB);
			if( !alta && this.VoltajeB < this.VoltajeA )
				IBC=Impedancia/ZBase;
			if( alta   && tconex==1)
				IBC=IBC/3;
			if( !alta  && tconex==2)
				IBC=IBC/3;
	}
	if ( Impedancia != 0  && this.VoltajeB == this.VoltajeA )
		IBC=IBC=(Impedancia/numero)/ZBase;
		
	if (PotenciaI !=0 ){
		IBC=Math.sqrt(Potencia*Potencia-PotenciaI*PotenciaI)/PBase;
	}
	if (FP != 0){
		IBC=Potencia*FP/PBase;
		}
  return IBC;
  }
  
  public double calculaI(){
	if (ImpedanciaPUAI != 0){
		IBC=(ImpedanciaPUAI/numero)*PBase/Potencia;
	}
	
	if (ImpedanciaI != 0 ){
		
			if( alta  && this.VoltajeB > this.VoltajeA )
				IBC=ImpedanciaI/ZBase;
			if( alta  && this.VoltajeB < this.VoltajeA )
				IBC=ImpedanciaI/ZBase*(VoltajeB/VoltajeA)*(VoltajeB/VoltajeA);
			if( !alta && this.VoltajeB > this.VoltajeA )
				IBC=ImpedanciaI/ZBase*(VoltajeA/VoltajeB)*(VoltajeA/VoltajeB);
			if( !alta && this.VoltajeB < this.VoltajeA )
				IBC=ImpedanciaI/ZBase;
			if( alta   && tconex==1)
				IBC=IBC/3;
			if( !alta  && tconex==2)
				IBC=IBC/3;
	}
	if ( ImpedanciaI != 0  && this.VoltajeB == this.VoltajeA )
		IBC=(ImpedanciaI/numero)/ZBase;
		
	if (PotenciaI != 0){
		IBC=PotenciaI/PBase;
	}
	if (FP != 0){
		if (ind) 
			IBC=Math.sqrt(Potencia*Potencia-Potencia*FP*Potencia*FP)/PBase;
		else IBC=-Math.sqrt(Potencia*Potencia-Potencia*FP*Potencia*FP)/PBase; 
	}
  return IBC;
  }
  //Potencia,VoltajeA,VoltajeB,Impedancia,ImpedanciaPUA;
  //public int tipo{return tipo;}
  public void reset(){
  	tconex = 3	;
	numero = 1;
  	Potencia=0;
	PotenciaI=0;
	VoltajeA=0;
	VoltajeB=0;
	Impedancia=0;
	ImpedanciaPUA=0;
	IBC=0;
	ZBase=0;
	ImpedanciaI=0;
	ImpedanciaPUAI=0;
	FP=0;
  }
  
//  public Dimension getMinimumSize (){return minSize;}
//  public Dimension getPreferredSize (){return prefSize;}
  
}