package org.deepedit.addons.nomlufketool.influencefactors;

public class Factores2a extends javax.swing.JApplet {

    public Factores2a() {
		Ayuda ayuda=new Ayuda();
        conCarga cC=new conCarga();
        sinCarga sC=new sinCarga();
		cC.Metodos(sC,ayuda);
		sC.Metodos(cC,ayuda);
		ayuda.Metodos(sC,cC);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(cC);
        getContentPane().add(sC);
		getContentPane().add(ayuda);
        cC.setVisible(false);
        sC.setVisible(true);
        ayuda.setVisible(false);
        repaint();
    }
}
