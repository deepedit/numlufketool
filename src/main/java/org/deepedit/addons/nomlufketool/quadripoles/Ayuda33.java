package org.deepedit.addons.nomlufketool.quadripoles;

import java.awt.*;
import javax.swing.*;

public class Ayuda33 extends JPanel{

	JPanel PanelAyuda;

	//***Botones****
	JButton ButtonAyudaTematica;
	JButton ButtonAyudaInterfaz;
	JButton ButtonAnterior;
	JButton ButtonSiguiente;
	JPanel PanelCentral;
	private Top2 PanelTop;
	JLabel Foto1 = new JLabel();
	JLabel Foto2 = new JLabel();

	String Estado="";
	String Foto_1="F10";
	String Foto_2="F20";
	
	TetrapolosCalc TC;

    public Ayuda33(TetrapolosCalc tc) {
        TC=tc;
        initComponents();
    }

    public void Metodos(TetrapolosCalc tc){
    	TC=tc;
    }

	private void initComponents(){

		setLayout(new BorderLayout());
//		PanelTop = new Top2(4);		
		PanelTop = new Top2(TC, this, 4);
		Foto1 = new JLabel("Ayuda Temática : Esta ayuda se encuentra en construcción");
    	
		Foto2 = new JLabel("Ayuda Intefaz : Esta ayuda se encuentra en construcción");
        	
		PanelAyuda=new JPanel();
		ButtonAyudaTematica = new JButton();
		ButtonAyudaInterfaz = new JButton();
		ButtonAnterior = new JButton();
		ButtonSiguiente = new JButton();

		PanelAyuda.setLayout(new GridBagLayout());
		PanelAyuda.setMaximumSize(new java.awt.Dimension(768, 30));
		PanelAyuda.setMinimumSize(new java.awt.Dimension(768, 30));
		PanelAyuda.setPreferredSize(new java.awt.Dimension(768, 30));
		
		ButtonAnterior.setText("Anterior");
		PanelAyuda.add(ButtonAnterior);
		ButtonAnterior.setEnabled(false);

		ButtonAyudaTematica.setText("Ayuda Tematica");
		PanelAyuda.add(ButtonAyudaTematica);

       	ButtonAyudaInterfaz.setText("Ayuda Interfaz");
		PanelAyuda.add(ButtonAyudaInterfaz);

		ButtonSiguiente.setText("Siguiente");
		PanelAyuda.add(ButtonSiguiente);
		ButtonSiguiente.setEnabled(false);

		GridBagConstraints gc = new GridBagConstraints();

		PanelCentral = new JPanel();
		PanelCentral.setLayout(new java.awt.FlowLayout());
		PanelCentral.setMaximumSize(new java.awt.Dimension(768, 550));
		PanelCentral.setMinimumSize(new java.awt.Dimension(768, 550));
		PanelCentral.setPreferredSize(new java.awt.Dimension(768, 550));

			gc.gridx=0;
			gc.gridy=0;
			gc.gridwidth=4;
			gc.gridheight=1;
			PanelCentral.add(PanelAyuda);
		
			gc.gridx=0;
			gc.gridy=1;
			
			PanelCentral.add(Foto1);
			Foto1.setVisible(false);
			PanelCentral.add(Foto2);
			Foto2.setVisible(false);
		
		add(PanelTop,"North");
		add(PanelCentral,"Center");

//***************************ESCUCHADORES********************************
//***********************************************************************

		ButtonAyudaTematica.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonAyudaTematicaActionPerformed(evt);}});
		
		ButtonAyudaInterfaz.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonAyudaInterfazActionPerformed(evt);}});
		
		ButtonAnterior.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonAnteriorPerformed(evt);}});
		
		ButtonSiguiente.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonSiguientePerformed(evt);}});
	
	}//init components

	private void ButtonAnteriorPerformed(java.awt.event.ActionEvent evt){
	}

	private void ButtonSiguientePerformed(java.awt.event.ActionEvent evt){
	}
		
	private void ButtonAyudaTematicaActionPerformed(java.awt.event.ActionEvent evt){
		Foto2.setVisible(false);
		Foto1.setVisible(true);	
	}

	private void ButtonAyudaInterfazActionPerformed(java.awt.event.ActionEvent evt){
		Foto1.setVisible(false);
		Foto2.setVisible(true);
	}
}