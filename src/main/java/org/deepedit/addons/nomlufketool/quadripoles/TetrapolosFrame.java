/*
 *  ****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         TetrapolosFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   July 2013
 *  
 *  
 *  ---- Description ---- 
 *   Quadripole visulizer. Adapted to display in a dialog instead of deprecated Applets
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ***************************************************************************
 */
package org.deepedit.addons.nomlufketool.quadripoles;

/**
 * Quadripole visulizer. 
 * <br>Adapted to display in a dialog instead of deprecated Applets
 * 
 * @author Frank Leanez
 */
public class TetrapolosFrame extends javax.swing.JDialog {
    
    private TetrapolosCalc TC;
    private Ayuda33 ayuda;
    
    public TetrapolosFrame (javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    private void initComponents () {
        TC = new TetrapolosCalc();
        ayuda = new Ayuda33(TC);
        TC.Metodos(ayuda);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(TC);
        getContentPane().add(ayuda);
        TC.setVisible(true);
        ayuda.setVisible(false);
        pack();
    }
    
}
