package org.deepedit.addons.nomlufketool.frequencycontrol;

import java.awt.*;
import org.deepedit.addons.nomlufketool.utils.Complex;

public class MCanvas extends Canvas {

	private boolean StepResponse;

	public void Calcular(double[] num, double[] den,int Nnum, int Nden,double pl,double Tf){

		//System.out.println(indice);

		StepResponse = true;
		Function Fs = new Function(num,den,Nnum,Nden);

		/*DATOS PARA LA INTEGRAL*/

		double	to	= 0;
		double	tf	= Tf;
		double	dt	= (tf-to)/puntos;

		int j;
		for(j=0;j<puntos;j++){
			datosY[indice][j]= -pl*inv_Lap(Fs,to+j*dt);
			datosX[indice][j]=to+j*dt;
		}

		maxY = Max(maxY,Maximo(datosY[indice]));
		minY = Min(minY,Minimo(datosY[indice]));
		maxX = Max(maxX,Maximo(datosX[indice]));
		minX = Min(minX,Minimo(datosX[indice]));

		indice++;
		indice = indice%max_cant_graf;
	}
/***********************************************************/

		private double sigma= 0.05;
		private double wi 	= 0;
		private double wf	= 20;
		private int	 n		= 500;

		public double inv_Lap (Function f, double t){

		Complex s = new Complex(sigma,0);

		double h=(wf-wi)/n;
		double suma=(arg_int(f,new Complex(sigma,wi),t)+arg_int(f,new Complex(sigma,wf),t));
		for(int i=1; i<n; i+=2){
			s.im	= wi+i*h;
			suma+=4*arg_int(f,s,t);
		}
		for(int i=2; i<n; i+=2){
			s.im	= wi+i*h;
			suma+=2*arg_int(f,s,t);
		}
		return Math.exp(sigma*t)*(suma*h/3)/Math.PI;
	}
                
    /**
     * 
     * @param f
     * @param s
     * @param t
     * @return
     */
    public double arg_int(Function f, Complex s, double t) {

        Complex aux = new Complex(0, s.im * t);

        if (StepResponse) {
            /*Evalua la respuesta al Pulso de la Funcion f*/
            return Complex.multiply(Function.eval_fxh(f, s), Complex.exp(aux)).re;
        } else {
            /*No Evalua la Funcion de Transferencia*/
            return Complex.multiply(Function.eval_f(f, s), Complex.exp(aux)).re;
        }

    }

	private Color[] colores;
	private int indiceColor;
	private int indice;
	private int max_cant_graf;
	private int puntos;
	private double datosX[][];
	private double datosY[][];
	private double maxY,maxX,minY,minX;

	public MCanvas() {
		colores = new Color[9];
		colores[0]=Color.blue;
		colores[1]=Color.magenta;
		colores[2]=Color.red;
		colores[3]=Color.green;
		colores[4]=Color.orange;
		colores[5]=Color.gray;
		colores[6]=Color.cyan;
		colores[7]=Color.pink;
		colores[8]=Color.black;
		indiceColor = 8;
		indice = 0;

		max_cant_graf 	= 30;
		puntos 			= 1000;

		datosX	= new double[max_cant_graf][puntos];
		datosY	= new double[max_cant_graf][puntos];
	}
        
    /**
     * Truncates double number with the given decimal position number
     *
     * @param x original double value
     * @param k number of decimal places
     * @return double number with the given decimal position number
     */
    public static double Truncar(double x, int k) {
        return Math.round(Math.pow(10, k) * x) / Math.pow(10, k);
    }
    
    /**
     * Truncates double number
     * @param max
     * @param min
     * @return Truncates double number
     */
    public static int detectaTrunc(double max, double min) {
        int res;
        if (Math.abs(max - min) >= 100) {
            res = 0;
        } else if (Math.abs(max - min) >= 10 && Math.abs(max - min) < 100) {
            res = 0;
        } else if (Math.abs(max - min) >= 1 && Math.abs(max - min) < 10) {
            res = 1;
        } else if (Math.abs(max - min) >= 0.1 && Math.abs(max - min) < 1) {
            res = 2;
        } else if (Math.abs(max - min) >= 0.01 && Math.abs(max - min) < 0.1) {
            res = 3;
        } else if (Math.abs(max - min) >= 0.001 && Math.abs(max - min) < 0.01) {
            res = 4;
        } else if (Math.abs(max - min) >= 0.0001 && Math.abs(max - min) < 0.001) {
            res = 5;
        } else if (Math.abs(max - min) >= 0.00001 && Math.abs(max - min) < 0.0001) {
            res = 6;
        } else if (Math.abs(max - min) >= 0.000001 && Math.abs(max - min) < 0.00001) {
            res = 7;
        } else if (Math.abs(max - min) >= 0.0000001 && Math.abs(max - min) < 0.000001) {
            res = 8;
        } else if (Math.abs(max - min) >= 0.00000001 && Math.abs(max - min) < 0.0000001) {
            res = 9;
        } else {
            res = 10;
        }

        return res;
    }
    
    /**
     * Max of 2 doubles
     * @param a
     * @param b
     * @return Max of 2 doubles
     */
    private static double Max(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * Max double in the given array
     * @param vector
     * @return Max double value in the given array
     */
    private static double Maximo(double[] vector) {
        int n = vector.length;
        double max = vector[0];
        for (int i = 0; i < n; i++) {
            if (vector[i] > max) {
                max = vector[i];
            }
        }
        return max;
    }
    
    private static double Min(double a, double b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    private static double Minimo(double[] vector) {
        int n = vector.length;
        double min = vector[0];
        for (int i = 0; i < n; i++) {
            if (vector[i] < min) {
                min = vector[i];
            }
        }
        return min;
    }

    @Override
    public void paint(Graphics g) {
        // obtiene dimensiones efectivas del canvas
        int largoX = (getPreferredSize()).width;
        int largoY = (getPreferredSize()).height;

        // selecciona el color blanco
        g.setColor(Color.white);

        // pinta de blanco todo el canvas
        g.fillRect(0, 0, largoX, largoY);

        // selecciona el color negro
        g.setColor(Color.black);

        int Bigdx = (int) largoX / 20;
        int Bigdy = (int) largoY / 20;
        int x0 = (int) 2 * Bigdx;
        int y0 = (int) 18 * Bigdy;
        int x1 = (int) 18 * Bigdx;
        int y1 = (int) 2 * Bigdy;

        // dibuja ejes Vertical y Horizontal
        g.drawLine(x0, y0, x0, y1);
        g.drawLine(x0, y0, x1, y0);

        int pasoX = (int) 16 * largoX / (20 * 10);
        int pasoY = (int) 16 * largoY / (20 * 10);

        g.drawString("F,pu ", x0 - 10, y1 / 2);
        g.drawString("t,sec", x1 + Bigdx / 2, y0);

        String sX = "";
        String sY = "";
        double aX = 0;
        double aY = minY;

        //Dibuja los valores en los Ejes
        for (int y = 1; y <= 10; y++) {

            g.drawLine(x0 + y * pasoX, y0, x0 + y * pasoX, y0 - 5);
            //Dibuja Linea Punteada
            for (int dot = 0; dot <= 50; dot++) {
                g.fillOval(x0 + y * pasoX, y0 + (y1 - y0) * dot / 50, 1, 1);
            }
            aX += maxX / 10;
            sX = String.valueOf(Truncar(aX, 1));
            g.drawString(sX, x0 + (y * pasoX - 15), y0 + largoY / 20);

            g.drawLine(x0, y0 - y * pasoY, x0 + 5, y0 - y * pasoY);
            //Dibuja Linea Punteada
            for (int dot = 0; dot <= 50; dot++) {
                g.fillOval(x0 + (x1 - x0) * dot / 50, y0 - y * pasoY, 1, 1);
            }
            aY += (maxY - minY) / 10;
            sY = String.valueOf(Truncar(aY, detectaTrunc(maxY, minY)));
            g.drawString(sY, x0 - (25 + 6 * detectaTrunc(maxY, minY)), y0 - y * pasoY + 5);
        }

        int posX = (int) (5 * (16 * largoX / 20) / maxX);
        int posY = (int) (5 * (16 * largoY / 20) / (maxY - minY));
        int posExtraY = (int) (minY * (16 * largoY / 20) / (maxY - minY));

        indiceColor = 8;
        for (int i = 0; i < indice; i++) {
            indiceColor++;
            indiceColor = indiceColor % 9;
            g.setColor(colores[indiceColor]);

            for (int j = 0; j < puntos; j++) {

                posX = (int) (datosX[i][j] * (16 * largoX / 20) / maxX);
                posY = (int) (datosY[i][j] * (16 * largoY / 20) / (maxY - minY));
                posExtraY = (int) (minY * (16 * largoY / 20) / (maxY - minY));

                g.fillOval(x0 + posX, y0 - posY + posExtraY, 3, 3);
            }//for
        }//for

    }
    
    /**
     * Resets values and repaint chart
     */
    public void blanquear() {
        indice = 0;
        Graphics g = getGraphics();
        this.paint(g);
    }
}