# README #

Nomlufke toolbox repository is the DeepEdit's addons that extend the text book "Atrapando el Sol en los Sistemas Electricos de Potencia" available online [here] (http://sepsolar.centroenergia.cl)

### What is this repository for? ###

* This is the Nomlufke toolbox for DeepEdit
* Version is 1.0.1 (as distributed in DeepEdit)
* Nomlufke toolbox is a DeepEdit's addon [https://deepedit.org](https://deepedit.org)

### How do I get set up? ###

* Clone the repository to your disk using the https link
* Configuration: No special consideraition
* JDK: This toolbox should work with Java 8 or superior
* No Dependencies: Nomlufke is fully java based. App's hmtl are available at sepsolar
* Database configuration: No database configuration is required
* Deployment instructions: Please contact fleanez@gmail.com

### How to include my own toolbox to DeepEdit? ###

* Create a new java package and develop your own new toolbox
* (optional) Create the hmtl introductory page and upload to [sepsolar](http://sepsolar.centroenergia.cl) website (contact administrator). Otherwise, just update links in address_[LANGUAGE].properties file to local resources
* Setup nomlufketool version in DeepEdit's pom file
* Compile nomlufketool and DeepEdit and ready! DeepEdit should include the latest version

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin [fleanez@gmail.com](mailto:fleanez@gmail.com)
* Other community or team contact [fleanez@gmail.com](mailto:fleanez@gmail.com)