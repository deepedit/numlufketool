package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import javax.swing.*;

public class Top2 extends JPanel{


	JPanel PanelTop;
	JPanel PanelTitulo;
	JLabel LabelTitulo;
	JSeparator jSeparator1,jSeparator2;
	JPanel PanelBotones;
	JButton BotonOp1,BotonOp2,BotonOp3,BotonAyuda;
	Op1 op1;
	Op2 op2;
	Op3 op3;
Ayuda33 ayuda;





public Top2(int i){
	initComponents(i);
	}


public void Metodos(Op1 o1, Op2 o2, Op3 o3,Ayuda33 a)
  {
op1=o1;
op2=o2;
op3=o3;
ayuda=a;
}

private void initComponents(int i){

	PanelTop=new JPanel();
	PanelTitulo=new JPanel();
	LabelTitulo=new JLabel();
	jSeparator1=new JSeparator();
	jSeparator2=new JSeparator();
	PanelBotones=new JPanel();
	BotonOp1=new JButton();
	BotonOp2=new JButton();
	BotonOp3=new JButton();
	BotonAyuda=new JButton();

	setLayout(new BorderLayout());

	PanelTop.setLayout(new javax.swing.BoxLayout(PanelTop, javax.swing.BoxLayout.Y_AXIS));
	PanelTop.setMinimumSize(new java.awt.Dimension(768, 70));
	PanelTop.setPreferredSize(new java.awt.Dimension(768, 70));
	PanelTitulo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));
	LabelTitulo.setFont(new java.awt.Font("Dialog", 1, 14));
    PanelTitulo.setBackground(new java.awt.Color(204,204,204));
	if(i==1) LabelTitulo.setText("C\u00e1lculo de Par\u00e1metros para Modelos Circuitales de L\u00edneas de Transmisi\u00f3n");
	if(i==2) LabelTitulo.setText("Comparaci\u00f3n de Modelos Circuitales de L\u00edneas de Transmisi\u00f3n");
	if(i==3) LabelTitulo.setText("Grafico del Diagrama de Circulo de la L\u00edneas de Transmisi\u00f3n");
	if(i==4) LabelTitulo.setText("Ayuda");

	PanelTitulo.add(LabelTitulo);
	PanelTop.add(PanelTitulo);
	PanelTop.add(jSeparator2);
        PanelBotones.setLayout(new java.awt.GridLayout(1,4,0,5));//FlowLayout(java.awt.FlowLayout.RIGHT));


	BotonOp1.setText("C\u00e1lculo de Par\u00e1metros");
	if(i==1)BotonOp1.setEnabled(false);
	PanelBotones.add(BotonOp1);

	BotonOp1.addActionListener(new java.awt.event.ActionListener() {
       public void actionPerformed(java.awt.event.ActionEvent evt) {
       BotonOp1ActionPerformed(evt);}});


	BotonOp2.setText("Comparaci\u00f3n de Modelos");
	if(i==2)BotonOp2.setEnabled(false);
	PanelBotones.add(BotonOp2);

	BotonOp2.addActionListener(new java.awt.event.ActionListener() {
       public void actionPerformed(java.awt.event.ActionEvent evt) {
       BotonOp2ActionPerformed(evt);}});


	BotonOp3.setText("Diagrama de Circulo");
	if(i==3)BotonOp3.setEnabled(false);
	PanelBotones.add(BotonOp3);

	BotonOp3.addActionListener(new java.awt.event.ActionListener() {
       public void actionPerformed(java.awt.event.ActionEvent evt) {
       BotonOp3ActionPerformed(evt);}});


	BotonAyuda.setText("Ayuda");
	if(i==4)BotonAyuda.setEnabled(false);
	PanelBotones.add(BotonAyuda);
	PanelTop.add(PanelBotones);
	PanelTop.add(jSeparator1);
	add(PanelTop, java.awt.BorderLayout.NORTH);
}//initComponents

private void BotonOp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
     op1.setVisible(true);
     ayuda.setVisible(false);
	ayuda.repaint();
      }//GEN-LAST:event_BotonOp2ActionPerformed

private void BotonOp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
       op2.setVisible(true);
     ayuda.setVisible(false);
    ayuda.repaint();
       }//GEN-LAST:event_BotonOp2ActionPerformed

private void BotonOp3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
       op3.setVisible(true);
       ayuda.setVisible(false);
       op3.repaint();
       }//GEN-LAST:event_BotonOp2ActionPerformed
}