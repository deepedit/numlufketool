package org.deepedit.addons.nomlufketool.influencefactors;

public class Ayuda extends javax.swing.JPanel {
	
    sinCarga SC;
    conCarga CC;

    public Ayuda() {
        initComponents();
    }

    public void Metodos(sinCarga sc, conCarga cc){
    	SC=sc;
    	CC=cc;
    }

    private void initComponents() {
    	setLayout(new java.awt.BorderLayout());
    	
        jPanel4 = new javax.swing.JPanel();
        jTextArea1 = new javax.swing.JTextArea();
        
        java.awt.GridBagConstraints gridBagConstraints;
        javax.swing.JPanel PanelTopTop = new javax.swing.JPanel();
        javax.swing.JPanel PanelTitulo = new javax.swing.JPanel();
        javax.swing.JLabel LabelTitulo = new javax.swing.JLabel();
        javax.swing.JPanel PanelBotones = new javax.swing.JPanel();
        javax.swing.JButton BotonsC = new javax.swing.JButton();
        javax.swing.JButton BotoncC = new javax.swing.JButton();
        javax.swing.JButton BotonAyuda = new javax.swing.JButton();
        javax.swing.JSeparator jSeparator2 = new javax.swing.JSeparator();
        javax.swing.JSeparator jSeparator1 = new javax.swing.JSeparator();
        
        setMinimumSize(new java.awt.Dimension(768, 590));
        setPreferredSize(new java.awt.Dimension(768, 590));
        PanelTopTop.setLayout(new javax.swing.BoxLayout(PanelTopTop, javax.swing.BoxLayout.Y_AXIS));
        
        PanelTopTop.setMinimumSize(new java.awt.Dimension(768, 70));
        PanelTopTop.setPreferredSize(new java.awt.Dimension(768, 70));
        PanelTitulo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));

        LabelTitulo.setFont(new java.awt.Font("Verdana", 1, 14));
        LabelTitulo.setText("Ayuda");
        PanelTitulo.setBackground(new java.awt.Color(204,204,204));
        PanelTitulo.add(LabelTitulo);

        PanelTopTop.add(PanelTitulo);

        PanelTopTop.add(jSeparator2);

        PanelBotones.setLayout(/*new java.awt.GridLayout(1,4,0,5));//*/new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));
        
        BotonsC.setText("Sin Carga Previa");
        BotonsC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonsCActionPerformed(evt);
            }
        });
        
        PanelBotones.add(BotonsC);
        
        BotoncC.setText("Con Carga");
        BotoncC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotoncCActionPerformed(evt);
            }
        });

        PanelBotones.add(BotoncC);

        BotonAyuda.setText("Ayuda");
        BotonAyuda.setEnabled(false);
        PanelBotones.add(BotonAyuda);
        
        PanelTopTop.add(PanelBotones);

        PanelTopTop.add(jSeparator1);

        add(PanelTopTop, java.awt.BorderLayout.NORTH);
        
        
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel4.setToolTipText("");
        jTextArea1.setFont(new java.awt.Font("Verdana",1,12));
        jTextArea1.setBackground(new java.awt.Color(240,240,240));
        jTextArea1.setText("\n\n\nSin carga\n\nLa idea es verificar si el motor es capaz de partir, esto se comprueba si la tensi\u00f3n de partida  \nes superior a 3.6 [kV], tensi\u00f3n espec\u00edfica del motor.\nComo el sistema estaba en circuito abierto el voltaje en los bornes a los cuales se conecta \nel motor (barra C) es 4.08 [kV], impuesto por el voltaje constante del sistema interconectado \n(67.32 [kV]) que luego es reducido por el transformador de raz\u00f3n 66/4 [kV], obteniendo 4.08 \n[kV]. Luego calculando los factores de influencia es posible calcular la variaci\u00f3n de voltaje \nproducido por el ingreso del motor al sistema.\nEsta es restada al voltaje que se ten\u00eda en vac\u00edo (en un principio) y se obtiene el voltaje en el \nmotor al momento de conectar. Si esta tensi\u00f3n es mayor a 3.6 [kV] entonces el motor es \ncapaz de partir y en el caso contrario el motor no parte.\n\n\n\nCon carga\n\nSi la conexi\u00f3n del motor al sistema fue exitosa el voltaje en la barra C se observ\u00f3 fuertemente \ndisminuido, por ende es necesaria una compensaci\u00f3n de voltaje. Esto se logra inyectando \nreactivos a la barra C mediante un condensador.\nLa capacidad exacta de \u00e9ste se calcula mediante los Factores de Influencia, a mayor voltaje \nque se necesite mayor debe ser la compensaci\u00f3n de reactivos.\n");
        jTextArea1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextArea1.setMinimumSize(new java.awt.Dimension(750, 500));
        jTextArea1.setPreferredSize(new java.awt.Dimension(750, 500));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.ipady = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        
        jPanel4.add(jTextArea1, gridBagConstraints);
        add(jPanel4);
	}
	
    private void BotoncCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp1ActionPerformed
		CC.setVisible(true);
		setVisible(false);
		CC.repaint();
    }//GEN-LAST:event_BotonOp1ActionPerformed


    private void BotonsCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp1ActionPerformed
		SC.setVisible(true);
		setVisible(false);
		SC.repaint();
    }//GEN-LAST:event_BotonOp1ActionPerformed
	
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextArea jTextArea1;
}