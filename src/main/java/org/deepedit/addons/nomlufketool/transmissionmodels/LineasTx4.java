package org.deepedit.addons.nomlufketool.transmissionmodels;

public class LineasTx4 extends javax.swing.JApplet {

    public LineasTx4() {
	Ayuda33 ayuda=new Ayuda33();
        Op1 op1=new Op1();
        Op2 op2=new Op2();
        Op3 op3=new Op3();
		op1.Metodos(op2,op3,ayuda);
		op2.Metodos(op1,op3,ayuda);
		op3.top.Metodos(op1,op2,op3,ayuda);
		ayuda.PanelTop.Metodos(op1,op2,op3,ayuda);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(op1);
        getContentPane().add(op2);
        getContentPane().add(op3);
	getContentPane().add(ayuda);
        op1.setVisible(false);
        op2.setVisible(false);
        op3.setVisible(false);
        ayuda.setVisible(true);
        repaint();
    }
}
