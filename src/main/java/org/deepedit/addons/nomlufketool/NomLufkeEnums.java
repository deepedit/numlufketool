/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         NomLufkeEnums.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Nom Lufke toolbox enums
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */
package org.deepedit.addons.nomlufketool;

/**
 * Nom Lufke toolbox enums class
 * @author Frank Leanez
 */
public class NomLufkeEnums {
    
    /**
     * Application
     */
    public enum App {
        /**
         * Phasor Calculator
         */
        PhasorCalculator {
            @Override
            public String toString() {
                return "Phasor Calculator";
            }
        },
        /**
         * Radial pu Conversor
         */
        RadialpuConversor {
            @Override
            public String toString() {
                return "Radial pu Conversor";
            }
        },
        /**
         * Quadripoles
         */
        Quadripoles {
            @Override
            public String toString() {
                return super.toString();
            }
        }, 
        /**
         * Sync Operation Circle
         */
        SyncOperationCircle {
            @Override
            public String toString() {
                return "Sync Operation Circle";
            }
        }, 
        /**
         * Transmission Parameters
         */
        TransmissionParameters {
            @Override
            public String toString() {
                return "Transmission Parameters";
            }
        }, 
        /**
         * Transformer Model
         */
        TransformerModel {
            @Override
            public String toString() {
                return "Transformer Model";
            }
            
        }, 
        /**
         * Transmission Line Models
         */
        TransmissionLineModels {
            @Override
            public String toString() {
                return  "Transmission Line Models";
            }
        }, 
        /**
         * Gradient
         */
        Gradient {
            @Override
            public String toString() {
                return super.toString();
            }
        }, 
        /**
         * Influence Factors
         */
        InfluenceFactors {
            @Override
            public String toString() {
                return  "Influence Factors";
            }
        }, 
        /**
         * Gauss-Seidel
         */
        Gauss_Seidel {
            @Override
            public String toString() {
                return  "Gauss-Seidel";
            }
        }, 
        /**
         * Frequency Control
         */
        FrequencyControl {
            @Override
            public String toString() {
                return  "Frequency Control";
            }
        }, 
        /**
         * Unbalanced Components
         */
        UnbalancedComponents {
            @Override
            public String toString() {
                return  "Unbalanced Components";
            }
        }, 
        /**
         * Relays (Example1)
         */
        Relays_Example1 {
            @Override
            public String toString() {
                return  "Relays (Example1)";
            }
        }, 
        /**
         * Relays (Example2)
         */
        Relays_Example2 {
            @Override
            public String toString() {
                return  "Relays (Example2)";
            }
        }, 
        /**
         * Transient Stability
         */
        TransientStability {
            @Override
            public String toString() {
                return  "Transient Stability";
            }
        }, 
        /**
         * Economic Dispatch
         */
        EconomicDispatch {
            @Override
            public String toString() {
                return  "Economic Dispatch";
            }
        }
        
    }
    
    /**
     * NumLufke toolbox supported display languages
     */
    public enum Language {
        /**
         * English language
         */
        English,
        /**
         * Spanish language
         */
        Spanish,
        /**
         * German language
         */
        German,
    }
    
}
