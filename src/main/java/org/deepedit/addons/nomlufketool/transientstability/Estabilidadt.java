package org.deepedit.addons.nomlufketool.transientstability;

/*
Applet para el c�lculo de la estabilidad transitoria 
para un generador conectado a barra infinita. 
Autores: Cristian Chong, Nicol�s Castro  
Fecha: 12/07/2005
Version: 2.0 (basado en el applet del semestre I del a�o 2002)

*/
import java.awt.event.*;
import java.awt.*;
import java.text.NumberFormat;
import org.deepedit.addons.nomlufketool.utils.Complex;


public class Estabilidadt extends javax.swing.JApplet 
					implements ActionListener{
	
	int failkind;
	double presicion = 0.00001;
	double decimal = Math.round(1/presicion);
	double presicion2 = 0.01;
	double dec = Math.round(1/presicion2);
	double Xt, Xd, Xl_1,Xl_2, Pm, Fp, V_b, Q, X, Pmax_aux;
	double Z, P2max_aux, X_post, e_abs1,P3max_aux;
	
	//Scrollbar dist;
    //Label km = new Label("128 [km] de la barra",Label.RIGHT);
    //Label org = new Label("La falla se encuentra a:");
	
	Complex I =  new Complex((double)0,(double)0);
    Complex S =  new Complex((double)0,(double)0);
    Complex V =  new Complex((double)0,(double)0);
	Complex Iconj =  new Complex((double)0,(double)0);
    Complex X12 = new Complex((double)0,(double)0);
    Complex E = new Complex((double)0,(double)0);
    Complex XI = new Complex((double)0,(double)0);
    
    
	/* Variables actuales */
	double Pmax        = 1.5;
	double Ppostfalla  = 1.2;
	double Pdurante    = 0.8;
	double angulo_despeje = 0.78;
	double Pmec        = 1.0;

	double Pnegro    = 0.73;
	double Pceleste1 = 0.985;
	double Pceleste2 = 2.156;
	double Projo     = 1.14;


	/* Variables undo */
	double Pmax2        = 1.5;
	double Ppostfalla2  = 1.2;
	double Pdurante2    = 0.8;
	double angulo_despeje2 = 0.78;
	double Pmec2        = 1.0;

	double Pnegro2    = 0.73;
	double Pceleste12 = 0.985;
	double Pceleste22 = 2.156;
	double Projo2     = 1.14;
	
		
	/* Elementos gr�ficos */
	Grafico graf = new Grafico();
	Panel panel  = new Panel();
	Panel panelsup  = new Panel();
	Grafico esq =new Grafico();
	
	javax.swing.JButton bUpdate = new javax.swing.JButton("Cargar los par�metros.");
	javax.swing.JButton bAng1   = new javax.swing.JButton("++");
	javax.swing.JButton bAng2   = new javax.swing.JButton("--");
	javax.swing.JButton bV1     = new javax.swing.JButton("++");
	javax.swing.JButton bV2     = new javax.swing.JButton("--");
	javax.swing.JButton bXd1    = new javax.swing.JButton("++");
	javax.swing.JButton bXd2    = new javax.swing.JButton("--");
	javax.swing.JButton bXt1    = new javax.swing.JButton("++");
	javax.swing.JButton bXt2    = new javax.swing.JButton("--");
	javax.swing.JButton bPmec1  = new javax.swing.JButton("++");
	javax.swing.JButton bPmec2  = new javax.swing.JButton("--");
	javax.swing.JButton bXl11   = new javax.swing.JButton("++");
	javax.swing.JButton bXl12   = new javax.swing.JButton("--");
	javax.swing.JButton bXl21   = new javax.swing.JButton("++");
	javax.swing.JButton bXl22   = new javax.swing.JButton("--");
	javax.swing.JButton bFp1    = new javax.swing.JButton("++");
	javax.swing.JButton bFp2    = new javax.swing.JButton("--");
	
	Label grafoptions      = new Label("Modo gr�fico:");		
	Label lblPprefalla     = new Label("Potencia prefalla:");
	Label lblPpostfalla    = new Label("Potencia postfalla:");
	Label lblPdurantefalla = new Label("Potencia durante falla:");
	Label lblAnguloDesp    = new Label("�ngulo de despeje:");
	Label lblPmec          = new Label("Potencia mec�nica:");
	Label lblXd			   = new Label("Xd' (Impedancia Generador Transiente):");
	Label lblXt			   = new Label("Xt (Impedancia Transformador):");
	Label lblXl_1		   = new Label("X1 (Impedancia L�nea 1):");
	Label lblXl_2		   = new Label("X2 (Impedancia L�nea 2):");
	Label lblFp			   = new Label("Factor de potencia:");
	Label lblV			   = new Label("Voltaje:");
	Label datos			   = new Label("DATOS",1);
	Label res			   = new Label("RESULTADOS",1);
	Label tipo			   = new Label("TIPO DE FALLA:");
	Label [] pu            = new Label[10];
	Label rad              = new Label("[rad]");
	Label lblE             = new Label("Voltaje E generado:");

	TextField txtPprefalla     = new TextField("", 4);
	TextField txtPpostfalla    = new TextField("", 4);
	TextField txtPdurantefalla = new TextField("", 4);
	TextField txtAnguloDesp    = new TextField("", 4);
	TextField txtPmec          = new TextField("", 4);
	TextField txtXd            = new TextField("0.4", 4);
	TextField txtXt            = new TextField("0.21", 4);
	TextField txtXl_1          = new TextField("0.4", 4);
	TextField txtXl_2          = new TextField("0.4", 4);
	TextField txtFp            = new TextField("0.996073", 4);
	TextField txtV             = new TextField("1", 4);
	TextField txtE             = new TextField("", 4);

	CheckboxGroup grupo 	= new CheckboxGroup();
	Checkbox margin			= new Checkbox("Marginal", grupo, true);
	Checkbox eac 			= new Checkbox("EAC", grupo, false); 
	
	CheckboxGroup falla 	= new CheckboxGroup();
	Checkbox falla1			= new Checkbox("Falla en la mitad de la l�nea", falla, true);
	Checkbox falla2 		= new Checkbox("Falla antes de la l�nea", falla, false); 
	
	
	
	
	GridBagLayout reticula,superior; 
	GridBagConstraints restricciones;
	
	public Estabilidadt(){
            init();
	}
	
	public void init() {
		
		setBackground ( new Color(240,240,240) );
		
		for(int i=0; i<10; ++i){pu[i] = new Label("[p.u.]");}
		
		/* Fijar los valores de la inicializaci�n */
		txtPprefalla.setText(String.valueOf("1.5"));
		txtPpostfalla.setText(String.valueOf("1.2"));
		txtPdurantefalla.setText(String.valueOf("0.8"));
		txtAnguloDesp.setText(String.valueOf("1.1"));
		txtPmec.setText(String.valueOf("0.9"));
		superior = new GridBagLayout(); 
		factible();
		bUpdate.setBounds(170,0,150,25);
		eac.setBounds(50,0,100,25);
		margin.setBounds(220,0,100,25);
		
		/**************************************/
		Panel Completo = new Panel();
		
		Completo.setLayout(new BorderLayout());
		Completo.add("North",panelsup);
		Completo.add("Center", graf);
		Completo.add("South",  panel);
		
		
		
		Panel PanelTop = new Panel();
		PanelTop.setLayout(new GridLayout(1,1,5,0));
		
		Label Titulo = new Label();
		Titulo.setBackground(new Color(204,204,204));
		Titulo.setFont(new Font("Verdana",1,14));
		Titulo.setText("Estabilidad Transitoria");
		PanelTop.add(Titulo);
		
		Panel PanelTopTop = new Panel();
		PanelTopTop.setLayout(new BorderLayout());
		PanelTopTop.add("North",PanelTop);
		
		javax.swing.JLabel Foto1 = new javax.swing.JLabel(new javax.swing.ImageIcon(getClass().getResource("/transientstability/images/estabilidad2.gif")));
		Foto1.setMaximumSize(new Dimension(500,200));
		Foto1.setMinimumSize(new Dimension(500,200));
		Foto1.setPreferredSize(new Dimension(500,200));
		
		PanelTopTop.add("Center",Foto1);
		
		
		/***************************************/
		
		getContentPane().setLayout(new BorderLayout());
		
		getContentPane().add("Center",Completo);
		getContentPane().add("North",PanelTopTop);

		
		/* Crear el panel de control de la aplicaci�n */
		reticula = new GridBagLayout(); 
		restricciones = new GridBagConstraints(); 
		panel.setLayout(reticula);
		
		restricciones.weightx = 1; 
		restricciones.weighty = 1; 
		restricciones.fill = GridBagConstraints.BOTH;
		
		 
	
		addComponent(datos, reticula, restricciones, 0, 0, 1, 1,1 );
				
		addComponent(lblV, reticula, restricciones, 1, 0, 1, 1,1 ); 
		addComponent(txtV, reticula, restricciones, 1, 1, 1, 1,1 );
		addComponent(pu[0], reticula, restricciones, 1, 2, 1, 1,1 );  
		addComponent(bV1, reticula, restricciones, 1, 3, 1, 1,1 ); 
		addComponent(bV2, reticula, restricciones, 1, 4, 1, 1,1 ); 
		
		addComponent(lblXd, reticula, restricciones, 2, 0, 1, 1,1 ); 
		addComponent(txtXd, reticula, restricciones, 2, 1, 1, 1,1 );
		addComponent(pu[1], reticula, restricciones, 2, 2, 1, 1,1 );  
		addComponent(bXd1, reticula, restricciones, 2, 3, 1, 1,1 ); 
		addComponent(bXd2, reticula, restricciones, 2, 4, 1, 1,1 ); 
		
		addComponent(lblXt, reticula, restricciones, 3, 0, 1, 1,1 ); 
		addComponent(txtXt, reticula, restricciones, 3, 1, 1, 1,1 );
		addComponent(pu[2], reticula, restricciones, 3, 2, 1, 1,1 );  
		addComponent(bXt1, reticula, restricciones, 3, 3, 1, 1,1 ); 
		addComponent(bXt2, reticula, restricciones, 3, 4, 1, 1,1 ); 
		
		addComponent(lblXl_1, reticula, restricciones, 4, 0, 1, 1,1 ); 
		addComponent(txtXl_1, reticula, restricciones, 4, 1, 1, 1,1 );
		addComponent(pu[3], reticula, restricciones, 4, 2, 1, 1,1 );  
		addComponent(bXl11, reticula, restricciones, 4, 3, 1, 1,1 ); 
		addComponent(bXl12, reticula, restricciones, 4, 4, 1, 1,1 ); 
		
		addComponent(lblXl_2, reticula, restricciones, 5, 0, 1, 1,1 ); 
		addComponent(txtXl_2, reticula, restricciones, 5, 1, 1, 1,1 );
		addComponent(pu[4], reticula, restricciones, 5, 2, 1, 1,1 );  
		addComponent(bXl21, reticula, restricciones, 5, 3, 1, 1,1 ); 
		addComponent(bXl22, reticula, restricciones, 5, 4, 1, 1,1 ); 
		
		addComponent(lblFp, reticula, restricciones, 6, 0, 1, 1,1 ); 
		addComponent(txtFp, reticula, restricciones, 6, 1, 1, 1,1 );
		 
		addComponent(bFp1, reticula, restricciones, 6, 3, 1, 1,1 ); 
		addComponent(bFp2, reticula, restricciones, 6, 4, 1, 1,1 ); 

		addComponent(lblAnguloDesp, reticula, restricciones, 7, 0, 1, 1,1 ); 
		addComponent(txtAnguloDesp, reticula, restricciones, 7, 1, 1, 1,1 );
		addComponent(rad, reticula, restricciones, 7, 2, 1, 1,1 );  
		addComponent(bAng1, reticula, restricciones, 7, 3, 1, 1,1 ); 
		addComponent(bAng2, reticula, restricciones, 7, 4, 1, 1,1 ); 

		addComponent(lblPmec, reticula, restricciones, 8, 0, 1, 1,1 ); 
		addComponent(txtPmec, reticula, restricciones, 8, 1, 1, 1,1 );
		addComponent(pu[5], reticula, restricciones, 8, 2, 1, 1,1 );  
		addComponent(bPmec1, reticula, restricciones, 8, 3, 1, 1,1 ); 
		addComponent(bPmec2, reticula, restricciones, 8, 4, 1, 1,1 );
		
		
		addComponent(res, reticula, restricciones, 9, 0, 1, 1,1 );
		
		addComponent(lblE, reticula, restricciones, 10, 0, 1, 1,1); 
		addComponent(txtE, reticula, restricciones, 10, 1, 1, 1,1 );
		addComponent(pu[6], reticula, restricciones, 10, 2, 1, 1,1 ); 
		
		addComponent(lblPprefalla, reticula, restricciones, 11, 0, 1, 1,1); 
		addComponent(txtPprefalla, reticula, restricciones, 11, 1, 1, 1,1 );
		addComponent(pu[7], reticula, restricciones, 11, 2, 1, 1,1 );  
	
		addComponent(lblPpostfalla, reticula, restricciones, 12, 0, 1, 1,1 ); 
		addComponent(txtPpostfalla, reticula, restricciones, 12, 1, 1, 1,1 );
		addComponent(pu[8], reticula, restricciones, 12, 2, 1, 1,1 );  
	
		addComponent(lblPdurantefalla, reticula, restricciones, 13, 0, 1, 1,1 ); 
		addComponent(txtPdurantefalla, reticula, restricciones, 13, 1, 1, 1,1);
		addComponent(pu[9], reticula, restricciones, 13, 2, 1, 1,1 );  
			
		panelsup.setLayout(reticula);
		addComponent(grafoptions, reticula, restricciones, 1, 0, 1, 1,2 );
		addComponent(eac, reticula, restricciones, 1, 1, 1, 1,2 ); 
		addComponent(margin, reticula, restricciones, 1, 2, 1, 1,2 ); 
		addComponent(bUpdate, reticula, restricciones, 2, 2, 1, 1,2 );
		addComponent(tipo, reticula, restricciones, 0, 0, 1, 1,2 );
		addComponent(falla1, reticula, restricciones, 0, 2, 1, 1,2 ); 
    	addComponent(falla2, reticula, restricciones, 0, 1, 1, 1,2 );


		bUpdate.addActionListener(this);
		bAng1.addActionListener(this);
		bAng2.addActionListener(this);
		bV1.addActionListener(this);
		bV2.addActionListener(this);
		bXd1.addActionListener(this);
		bXd2.addActionListener(this);
		bXt1.addActionListener(this);
		bXt2.addActionListener(this);
		bPmec1.addActionListener(this);
		bPmec2.addActionListener(this);
		bXl11.addActionListener(this);
		bXl12.addActionListener(this);
		bXl21.addActionListener(this);
		bXl22.addActionListener(this);
		bFp1.addActionListener(this);
		bFp2.addActionListener(this);
	}
	
	
	void addComponent( Component comp, GridBagLayout bag, GridBagConstraints bagcons, 
                                    int linea, int columna, int ancho, int alto, int p) {

		bagcons.gridx = columna; 
		bagcons.gridy = linea; 
		bagcons.gridwidth = ancho; 
		bagcons.gridheight = alto; 
		bag.setConstraints( comp, bagcons ); 
		if (p==1)
		panel.add( comp );
	 	else
		panelsup.add( comp ); 
		
	} 

	
//accion de los botones

	public void actionPerformed(ActionEvent e)
	{
		
		if (e.getSource() == bAng1)
			txtAnguloDesp.setText("" + (Math.round((Double.parseDouble(txtAnguloDesp.getText()) + 0.01)*decimal)/decimal));
		if (e.getSource() == bAng2)
			txtAnguloDesp.setText("" + (Math.round((Double.parseDouble(txtAnguloDesp.getText()) - 0.01)*decimal)/decimal));

		if (e.getSource() == bV1)
			txtV.setText("" + (Math.round((Double.parseDouble(txtV.getText())+ 0.01)*dec)/dec));
		if (e.getSource() == bV2)
			txtV.setText("" + (Math.round((Double.parseDouble(txtV.getText())- 0.01)*dec)/dec));

		if (e.getSource() == bXd1)
			txtXd.setText("" + (Math.round((Double.parseDouble(txtXd.getText())+ 0.01)*dec)/dec));
		if (e.getSource() == bXd2)
			txtXd.setText("" +(Math.round( (Double.parseDouble(txtXd.getText())- 0.01)*dec)/dec));

		if (e.getSource() == bXt1)
			txtXt.setText("" + (Math.round((Double.parseDouble(txtXt.getText())+ 0.01)*dec)/dec));
		if (e.getSource() == bXt2)
			txtXt.setText("" + (Math.round((Double.parseDouble(txtXt.getText())- 0.01)*dec)/dec));

		if (e.getSource() == bPmec1)
			txtPmec.setText("" + (Double.parseDouble(txtPmec.getText()) + 0.01));
		if (e.getSource() == bPmec2)
			txtPmec.setText("" + (Double.parseDouble(txtPmec.getText()) - 0.01));
			
		if (e.getSource() == bFp1){
			double aux = Double.parseDouble(txtFp.getText());
			if(aux!=1){
				txtFp.setText("" + (Math.round((Double.parseDouble(txtFp.getText())+ 0.01)*dec)/dec));}
			else{
				txtFp.setText("" + 1);}}
			
		if (e.getSource() == bFp2)
			txtFp.setText("" + (Math.round((Double.parseDouble(txtFp.getText())- 0.01)*dec)/dec));
			
		if (e.getSource() == bXl11)
			txtXl_1.setText("" + (Math.round((Double.parseDouble(txtXl_1.getText())+ 0.01)*dec)/dec));
		if (e.getSource() == bXl12)
			txtXl_1.setText("" + (Math.round((Double.parseDouble(txtXl_1.getText())- 0.01)*dec)/dec));
			
		if (e.getSource() == bXl21)
			txtXl_2.setText("" + (Math.round((Double.parseDouble(txtXl_2.getText())+ 0.01)*dec)/dec));
		if (e.getSource() == bXl22)
			txtXl_2.setText("" + (Math.round((Double.parseDouble(txtXl_2.getText())- 0.01)*dec)/dec));
		
		if (eac.getState())
			graf.eac=1;
		else
			graf.eac=0;
			
		if (falla2.getState())
			failkind=1;
		else
			failkind=0;
			
		factible();
		repaint();
		graf.repaint();
		
	}
	
	public double paralelo(double num1, double num2){
		double n ;
		n = num1*num2/(num1+num2);
		return n;	
	}
	
	public void factible()
	{
		
   	    Xt = Double.parseDouble(txtXt.getText());
	 	Xd = Double.parseDouble(txtXd.getText());
		Xl_1 = Double.parseDouble(txtXl_1.getText());
		Xl_2 = Double.parseDouble(txtXl_2.getText());
		Pm = Double.parseDouble(txtPmec.getText());
	 	Fp = Double.parseDouble(txtFp.getText());
	 	V_b = Double.parseDouble(txtV.getText());
	 	 	
	 	Q = ( Pm / Fp ) * Math.sqrt(1-Math.pow(Fp,2));  
		S = new Complex(Pm, Q);
		V = new Complex(V_b, 0);
		Iconj = Complex.divide(S,V);
		I = Complex.conjugate(Iconj);
		X = Xd + Xt + paralelo(Xl_1,Xl_2);
		X12 = new Complex(0, X);
		XI = Complex.multiply(I,X12);
		E = Complex.add(V, XI); 
		Pmax_aux = Complex.abs(E) * V_b / X ; 
	
		//Set text de E'
		e_abs1 = Complex.abs(E);
		txtE.setText(String.valueOf((Math.round(decimal*e_abs1))/decimal));
		
		
		if(failkind==1){
			angulo_despeje = Double.parseDouble(txtAnguloDesp.getText());
			Pmec           = Double.parseDouble(txtPmec.getText());	
			Pmax  		   = Math.round(decimal*Pmax_aux)/decimal;
			Pdurante 	   = Math.round(decimal*0.0)/decimal;
			Ppostfalla	   = Math.round(decimal*Pmax_aux)/decimal;	
		
		}
		
		else{
			//Impedancia entre barra infinita y Generador
	 		Z = ((Xd + Xt) * (Xl_2/2 + Xl_1) + Xl_1 * Xl_2 / 2 )/ (Xl_2 / 2);
	 		//P?otencia durante la falla
	 		P2max_aux = Complex.abs(E) * V_b / Z ;
	 		//Impedancia con una linea menos (la 2)
	 		X_post = Xt + Xd + Xl_1;
	 		//Potencia despu�s de la falla 
	 		P3max_aux = Complex.abs(E) * V_b / X_post;
	 		
	 		angulo_despeje = Double.parseDouble(txtAnguloDesp.getText());
			Pmec           = Double.parseDouble(txtPmec.getText());
		 	Pmax	       = Math.round(decimal*Pmax_aux)/decimal;
			Pdurante 	   = Math.round(decimal*P2max_aux)/decimal;
			Ppostfalla     = Math.round(decimal*P3max_aux)/decimal;
		}
	

		/* Calcular �ngulos */

		Pnegro = Math.asin(Pmec / Pmax);
		Pceleste1 = Math.asin(Pmec / Ppostfalla);
		Pceleste2 = Math.PI - Pceleste1;
		Projo = Math.acos((Pmec*(Pceleste2-Pnegro)+Ppostfalla*Math.cos(Pceleste2)-Pdurante*Math.cos(Pnegro))/
				(Ppostfalla-Pdurante));
		
		/* Verificar que el �ngulo de despeje caiga dentro la regi�n factible */
		/* Arreglar los par�metros */
			
		if (Pmax < Ppostfalla)
			Pmax = Ppostfalla;
		
		if (Pdurante > Ppostfalla)
			Pdurante = Ppostfalla;
		
		if (Pmec > Ppostfalla)
			Pmec = Ppostfalla;
					
		if (angulo_despeje < Pnegro)
			angulo_despeje = Pnegro;
		
		if (angulo_despeje > Projo)
			angulo_despeje = Projo;
			
		if (!(Projo < Math.PI / 2) && !(Projo > Pnegro))
		{
			Pmax           = Pmax2;
			Ppostfalla     = Ppostfalla2;
			Pdurante       = Pdurante2;
			angulo_despeje = angulo_despeje2;
			Pmec           = Pmec2;
			Pnegro 		   = Pnegro2;
			Pceleste1 	   = Pceleste12;
			Pceleste2	   = Pceleste22;
			Projo		   = Projo2;

		}
			
		/* Una vez calculada la factibilidad, entreg�rselas a los */
		/* botones y al gr�fico */
		
		/*Guardo el estado anterior */
		
		Pmax2           = Pmax;
		Ppostfalla2     = Ppostfalla;
		Pdurante2       = Pdurante;
		angulo_despeje2 = angulo_despeje;
		Pmec2           = Pmec;
		Pnegro2 		= Pnegro;
		Pceleste12 		= Pceleste1;
		Pceleste22		= Pceleste2;
		Projo2			= Projo;

		graf.Pmax      = Pmax;
		graf.Ppost     = Ppostfalla;
		graf.Pdura     = Pdurante;
		graf.Pmec      = Pmec;
		graf.Pnegro    = Pnegro;
		graf.Pceleste1 = Pceleste1;
		graf.Pceleste2 = Pceleste2;
		graf.Projo     = Projo;
		graf.Pverde    = angulo_despeje;
		
		/* Se actualizan los textfields con los valores  */
		txtPprefalla.setText(Pmax + "");
		txtPpostfalla.setText(Ppostfalla + "");
		txtPdurantefalla.setText(Pdurante + "");
		txtAnguloDesp.setText(angulo_despeje + "");
		txtPmec.setText(Pmec + "");
		

	}
	
	public void paint(Graphics g) {

 	} 
}


class Grafico extends Canvas
{
	/* Variables de instancia del canvas */
	public double Pmax;
	public double Ppost;
	public double Pdura;
	public double Pmec;
	public double Pnegro;
	public double Pverde;
	public double Pceleste1;
	public double Pceleste2;
	public double Projo;
	public double Pamarillo;
	public double eac;
	
	int aux2;
	int ancho;
	int alto;
	int gAlto;
	int gAncho;
	int gBordeX;
	int gBordeY;
	
	
	private int altura(double Pmax, double h)
	{
		double alturaPantallaMax = gAlto / 15.0 * 14.0;
		return gBordeY + gAlto - (int) (h * alturaPantallaMax / Pmax);
	}
	
	private int ancho(double w)
	{
		return (int)(w / Math.PI * gAncho);
	}

	public void paint(Graphics g)
	{
		
		NumberFormat nf = NumberFormat.getInstance();
	    nf.setMaximumFractionDigits(3); 
	    
		// Calcular los bordes
		ancho = (int)(this.getSize().getWidth());
		alto  = (int)(this.getSize().getHeight());
		gAlto = alto * 7 / 10;
		gAncho = ancho * 8 / 10;
		gBordeX = ancho / 10;
		gBordeY = alto / 10;
				
		//Ejes
		g.drawLine(gBordeX, gBordeY, gBordeX, gBordeY + gAlto);
		g.drawLine(gBordeX, gBordeY + gAlto, gBordeX + gAncho+10, gBordeY + gAlto);
		g.drawString("Pe[pu]", gBordeX-35, gBordeY + gAlto/8);
		g.drawString("Pi[rad]", gBordeX + gAncho, gBordeY + gAlto+12);
		g.drawString("Pi/2", (gBordeX + gAncho)/2, gBordeY + gAlto+12);
		g.drawString("0�", gBordeX, gBordeY + gAlto+12);
		
		//area azul y magenta
		double area1 = 0.0;
		double area2 = 0.0;
		int aux = 0;
		Pamarillo = 0.0;
					
		// graficar las 4 regiones
		for (int i=ancho(Pnegro); i<ancho(Pceleste2); ++i)
		{
			double y;
			y = Math.sin(Math.PI * i / gAncho);
			if (i < ancho(Pverde))
				y *= Pdura;
			else
				y *= Ppost;
				
			if (y > Pmec)
				{
					g.setColor(Color.magenta);
					area2 += (y - Pmec) / gAncho ;
				}
			else
				{
					g.setColor(Color.blue);
					area1 += (Pmec - y) / gAncho;
				}
				
			if (aux == 0 && area1 < area2)
			{
				Pamarillo = (double)i / gAncho * Math.PI;
				aux = i;
			}
				
			g.drawLine(gBordeX + i, altura(Pmax, Pmec), gBordeX + i, altura(Pmax, y));
			if(eac==1 && aux==i)
			break;
		}
		
		
		//Incluyo las areas en el grafico
		g.drawString("�rea Frenado:" + nf.format(area2), gBordeX+gAncho-100 , gBordeY + 12);
		g.setColor(Color.blue);
		g.drawString("�rea Aceleraci�n:"+ nf.format(area1),gBordeX+gAncho-100, gBordeY );
		g.setColor(Color.red);
		if (area2>area1)
		g.drawString("Sistema ESTABLE",gBordeX+gAncho-100, altura(Pmax, 0.0)+36);
		else
		g.drawString("Sistema INESTABLE!!",gBordeX+gAncho-100, altura(Pmax, 0.0)+36);
				
		// Potencia mec�nica
		g.setColor(Color.blue);
		g.drawLine(gBordeX, altura(Pmax, Pmec), gBordeX + gAncho, altura(Pmax, Pmec));
		g.drawString("Pm ="+ nf.format(Pmec), gBordeX+10, altura(Pmax, Pmec)-2);
		g.drawString("Pmax ="+ nf.format(Pmax), (gBordeX+ancho)/2-50, altura(Pmax, Pmax)-12);
		
		// graficar las l�neas de Potencia
		

		for (int i=0; i<gAncho; i++)
		{
			double y1, y2;
			y1 = Math.sin((Math.PI *   i   / gAncho));
			y2 = Math.sin((Math.PI * (i+1) / gAncho));
			
			g.setColor(Color.black);
			g.drawLine(gBordeX + i, altura(Pmax, Pmax*y1), gBordeX + i + 1, altura(Pmax, Pmax*y2));
			g.setColor(Color.cyan);
			g.drawLine(gBordeX + i, altura(Pmax, Ppost*y1), gBordeX + i + 1, altura(Pmax, Ppost*y2));
			g.setColor(Color.red);
			g.drawLine(gBordeX + i, altura(Pmax, Pdura*y1), gBordeX + i + 1, altura(Pmax, Pdura*y2));
		}
		
		// graficar las 4 l�neas y los �gulos importantes
		double y;
    	g.setColor(Color.black);
		g.drawLine(gBordeX + ancho(Pnegro), altura(Pmax, Pmec), gBordeX + ancho(Pnegro), altura(Pmax, 0.0));
		g.setColor(Color.cyan);
		y = Ppost * Math.sin(Pceleste1);
		g.drawLine(gBordeX + ancho(Pceleste1), altura(Pmax, y), gBordeX + ancho(Pceleste1), altura(Pmax, 0.0));
		g.drawLine(gBordeX + ancho(Pceleste2), altura(Pmax, y), gBordeX + ancho(Pceleste2), altura(Pmax, 0.0));
		g.setColor(Color.green);
		y = Ppost * Math.sin(Pverde);
		g.drawLine(gBordeX + ancho(Pverde), altura(Pmax, y), gBordeX + ancho(Pverde), altura(Pmax, 0.0));
		g.setColor(Color.red);
		y = Ppost * Math.sin(Projo);
		g.drawLine(gBordeX + ancho(Projo), altura(Pmax, y), gBordeX + ancho(Projo), altura(Pmax, 0.0));
		g.setColor(Color.yellow);
		y = Ppost * Math.sin(Pamarillo);
		g.drawLine(gBordeX + ancho(Pamarillo), altura(Pmax, y), gBordeX + ancho(Pamarillo), altura(Pmax, 0.0));
		
		g.setColor(Color.cyan);
		g.drawString("� ="+ nf.format(Pceleste2), ancho(Pceleste2), altura(Pmax, 0.0)+12);
		g.drawString("� ="+ nf.format(Pceleste1), ancho(Pceleste1), altura(Pmax, 0.0)+12);
		g.setColor(Color.green);
		g.drawString("� ="+ nf.format(Pverde), ancho(Pverde), altura(Pmax, 0.0)+24);
		g.setColor(Color.black);
		g.drawString("� ="+ nf.format(Pnegro), gBordeX +ancho(Pnegro), altura(Pmax, 0.0)-5);
		g.setColor(Color.red);
		y = Ppost * Math.sin(Projo);
		g.drawString("� ="+ nf.format(Projo), ancho(Projo), altura(Pmax, 0.0)-17);
	}
	
	public void repaint(Graphics g)
	{
		paint(g);
	}
}