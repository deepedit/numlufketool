package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import javax.swing.*;

public class Zoom extends JPanel{

		JPanel PanelTituloZoom;
		JPanel PanelOpcionZoom;
		JLabel TituloZoom;
		ButtonGroup ButtonGroupZoom;
		JRadioButton RadioButtonZoom1;
		JRadioButton RadioButtonZoom2;
		JButton ButtonZoom3;
		JLabel LFactor;
		JTextField TFactor;
		JPanel PFactor;
		String zoom="in";

public Zoom(int x,int y) {
        initComponents(x,y);
    }

private void initComponents(int x,int y) {

        java.awt.GridBagConstraints gridBagConstraints;

		PanelTituloZoom = new JPanel();
		PanelOpcionZoom = new JPanel();
		TituloZoom=new JLabel("Opciones de Zoom");
		ButtonGroupZoom = new ButtonGroup();
		RadioButtonZoom1 = new JRadioButton();
		RadioButtonZoom2 = new JRadioButton();
		ButtonZoom3 = new JButton();

		LFactor=new JLabel("ZoomFactor = ");
		TFactor=new JTextField("1.2");
		PFactor=new JPanel();
		PFactor.setLayout(new GridLayout(1,0));
		PFactor.add(LFactor);
		PFactor.add(TFactor);

		PanelOpcionZoom.setLayout(new java.awt.GridLayout(0,1));
		PanelOpcionZoom.setMaximumSize(new java.awt.Dimension(150, 65));
		PanelOpcionZoom.setMinimumSize(new java.awt.Dimension(150, 65));
		PanelOpcionZoom.setPreferredSize(new java.awt.Dimension(150, 65));

		RadioButtonZoom1.setText("Zoom In");
		RadioButtonZoom1.setSelected(true);
        	ButtonGroupZoom.add(RadioButtonZoom1);

		RadioButtonZoom1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioButtonZoom1ActionPerformed(evt);
            }});


		PanelOpcionZoom.add(RadioButtonZoom1);
        RadioButtonZoom2.setText("Zoom Out");
        ButtonGroupZoom.add(RadioButtonZoom2);

		RadioButtonZoom2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioButtonZoom2ActionPerformed(evt);
            }});


		PanelOpcionZoom.add(RadioButtonZoom2);
		PanelOpcionZoom.add(PFactor);
		//PanelOpcionZoom.setBorder(new EtchedBorder());

	setMinimumSize(new java.awt.Dimension(x, y));
	setPreferredSize(new java.awt.Dimension(x, y));
    setLayout(new java.awt.GridBagLayout());
	gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
    add(TituloZoom, gridBagConstraints);
    	gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 1;
    add(PanelOpcionZoom,gridBagConstraints);
//	setBorder(new javax.swing.border.EtchedBorder());

}//CONSTRUCTOR

private void RadioButtonZoom1ActionPerformed(java.awt.event.ActionEvent evt){}

private void RadioButtonZoom2ActionPerformed(java.awt.event.ActionEvent evt){}


}